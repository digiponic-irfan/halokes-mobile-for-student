import 'package:moor_flutter/moor_flutter.dart';

@DataClassName("Interlocutor")
class InterlocutorEntity extends Table{
  TextColumn get idUrl => text()();
  TextColumn get displayName => text()();
  TextColumn get displayFotoUrl => text()();
  TextColumn get lastChatId => text()();
  TextColumn get displayLastChat => text()();
  RealColumn get lastChatTime => real()();

  @override
  Set<Column> get primaryKey => {idUrl};

  @override
  String get tableName => "interlocutor";
}