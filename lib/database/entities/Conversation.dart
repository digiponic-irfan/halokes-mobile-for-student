import 'package:moor_flutter/moor_flutter.dart';

@DataClassName("Conversation")
class ConversationEntity extends Table{
  static const STATUS_SEND = 0;
  static const STATUS_RECEIVED = 1;
  static const STATUS_SENT = 2;
  static const STATUS_READ = 3;

  static const ME = -1;
  static const THEM = -2;

  TextColumn get interlocutorId => text()();
  TextColumn get conversationId => text()();
  TextColumn get conversation => text()();
  IntColumn get conversationStatus => integer()();
  IntColumn get sender => integer()();
  RealColumn get sendAt => real().nullable()();
  RealColumn get receivedByServerAt => real().nullable()();
  RealColumn get sentAt => real().nullable()();
  RealColumn get readAt => real().nullable()();

  @override
  Set<Column> get primaryKey => { conversationId };

  @override
  String get tableName => "conversation";
}