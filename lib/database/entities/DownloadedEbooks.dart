import 'package:moor_flutter/moor_flutter.dart';

class DownloadedEbooks extends Table{
  TextColumn get idEbookUrl => text()();
  TextColumn get path => text()();

  @override
  Set<Column> get primaryKey => {idEbookUrl};
}