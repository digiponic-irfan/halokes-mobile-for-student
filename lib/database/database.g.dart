// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'database.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps, unnecessary_this
class Interlocutor extends DataClass implements Insertable<Interlocutor> {
  final String idUrl;
  final String displayName;
  final String displayFotoUrl;
  final String lastChatId;
  final String displayLastChat;
  final double lastChatTime;
  Interlocutor(
      {@required this.idUrl,
      @required this.displayName,
      @required this.displayFotoUrl,
      @required this.lastChatId,
      @required this.displayLastChat,
      @required this.lastChatTime});
  factory Interlocutor.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final stringType = db.typeSystem.forDartType<String>();
    final doubleType = db.typeSystem.forDartType<double>();
    return Interlocutor(
      idUrl:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}id_url']),
      displayName: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}display_name']),
      displayFotoUrl: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}display_foto_url']),
      lastChatId: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}last_chat_id']),
      displayLastChat: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}display_last_chat']),
      lastChatTime: doubleType
          .mapFromDatabaseResponse(data['${effectivePrefix}last_chat_time']),
    );
  }
  factory Interlocutor.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return Interlocutor(
      idUrl: serializer.fromJson<String>(json['idUrl']),
      displayName: serializer.fromJson<String>(json['displayName']),
      displayFotoUrl: serializer.fromJson<String>(json['displayFotoUrl']),
      lastChatId: serializer.fromJson<String>(json['lastChatId']),
      displayLastChat: serializer.fromJson<String>(json['displayLastChat']),
      lastChatTime: serializer.fromJson<double>(json['lastChatTime']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'idUrl': serializer.toJson<String>(idUrl),
      'displayName': serializer.toJson<String>(displayName),
      'displayFotoUrl': serializer.toJson<String>(displayFotoUrl),
      'lastChatId': serializer.toJson<String>(lastChatId),
      'displayLastChat': serializer.toJson<String>(displayLastChat),
      'lastChatTime': serializer.toJson<double>(lastChatTime),
    };
  }

  @override
  InterlocutorEntityCompanion createCompanion(bool nullToAbsent) {
    return InterlocutorEntityCompanion(
      idUrl:
          idUrl == null && nullToAbsent ? const Value.absent() : Value(idUrl),
      displayName: displayName == null && nullToAbsent
          ? const Value.absent()
          : Value(displayName),
      displayFotoUrl: displayFotoUrl == null && nullToAbsent
          ? const Value.absent()
          : Value(displayFotoUrl),
      lastChatId: lastChatId == null && nullToAbsent
          ? const Value.absent()
          : Value(lastChatId),
      displayLastChat: displayLastChat == null && nullToAbsent
          ? const Value.absent()
          : Value(displayLastChat),
      lastChatTime: lastChatTime == null && nullToAbsent
          ? const Value.absent()
          : Value(lastChatTime),
    );
  }

  Interlocutor copyWith(
          {String idUrl,
          String displayName,
          String displayFotoUrl,
          String lastChatId,
          String displayLastChat,
          double lastChatTime}) =>
      Interlocutor(
        idUrl: idUrl ?? this.idUrl,
        displayName: displayName ?? this.displayName,
        displayFotoUrl: displayFotoUrl ?? this.displayFotoUrl,
        lastChatId: lastChatId ?? this.lastChatId,
        displayLastChat: displayLastChat ?? this.displayLastChat,
        lastChatTime: lastChatTime ?? this.lastChatTime,
      );
  @override
  String toString() {
    return (StringBuffer('Interlocutor(')
          ..write('idUrl: $idUrl, ')
          ..write('displayName: $displayName, ')
          ..write('displayFotoUrl: $displayFotoUrl, ')
          ..write('lastChatId: $lastChatId, ')
          ..write('displayLastChat: $displayLastChat, ')
          ..write('lastChatTime: $lastChatTime')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      idUrl.hashCode,
      $mrjc(
          displayName.hashCode,
          $mrjc(
              displayFotoUrl.hashCode,
              $mrjc(lastChatId.hashCode,
                  $mrjc(displayLastChat.hashCode, lastChatTime.hashCode))))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is Interlocutor &&
          other.idUrl == this.idUrl &&
          other.displayName == this.displayName &&
          other.displayFotoUrl == this.displayFotoUrl &&
          other.lastChatId == this.lastChatId &&
          other.displayLastChat == this.displayLastChat &&
          other.lastChatTime == this.lastChatTime);
}

class InterlocutorEntityCompanion extends UpdateCompanion<Interlocutor> {
  final Value<String> idUrl;
  final Value<String> displayName;
  final Value<String> displayFotoUrl;
  final Value<String> lastChatId;
  final Value<String> displayLastChat;
  final Value<double> lastChatTime;
  const InterlocutorEntityCompanion({
    this.idUrl = const Value.absent(),
    this.displayName = const Value.absent(),
    this.displayFotoUrl = const Value.absent(),
    this.lastChatId = const Value.absent(),
    this.displayLastChat = const Value.absent(),
    this.lastChatTime = const Value.absent(),
  });
  InterlocutorEntityCompanion.insert({
    @required String idUrl,
    @required String displayName,
    @required String displayFotoUrl,
    @required String lastChatId,
    @required String displayLastChat,
    @required double lastChatTime,
  })  : idUrl = Value(idUrl),
        displayName = Value(displayName),
        displayFotoUrl = Value(displayFotoUrl),
        lastChatId = Value(lastChatId),
        displayLastChat = Value(displayLastChat),
        lastChatTime = Value(lastChatTime);
  InterlocutorEntityCompanion copyWith(
      {Value<String> idUrl,
      Value<String> displayName,
      Value<String> displayFotoUrl,
      Value<String> lastChatId,
      Value<String> displayLastChat,
      Value<double> lastChatTime}) {
    return InterlocutorEntityCompanion(
      idUrl: idUrl ?? this.idUrl,
      displayName: displayName ?? this.displayName,
      displayFotoUrl: displayFotoUrl ?? this.displayFotoUrl,
      lastChatId: lastChatId ?? this.lastChatId,
      displayLastChat: displayLastChat ?? this.displayLastChat,
      lastChatTime: lastChatTime ?? this.lastChatTime,
    );
  }
}

class $InterlocutorEntityTable extends InterlocutorEntity
    with TableInfo<$InterlocutorEntityTable, Interlocutor> {
  final GeneratedDatabase _db;
  final String _alias;
  $InterlocutorEntityTable(this._db, [this._alias]);
  final VerificationMeta _idUrlMeta = const VerificationMeta('idUrl');
  GeneratedTextColumn _idUrl;
  @override
  GeneratedTextColumn get idUrl => _idUrl ??= _constructIdUrl();
  GeneratedTextColumn _constructIdUrl() {
    return GeneratedTextColumn(
      'id_url',
      $tableName,
      false,
    );
  }

  final VerificationMeta _displayNameMeta =
      const VerificationMeta('displayName');
  GeneratedTextColumn _displayName;
  @override
  GeneratedTextColumn get displayName =>
      _displayName ??= _constructDisplayName();
  GeneratedTextColumn _constructDisplayName() {
    return GeneratedTextColumn(
      'display_name',
      $tableName,
      false,
    );
  }

  final VerificationMeta _displayFotoUrlMeta =
      const VerificationMeta('displayFotoUrl');
  GeneratedTextColumn _displayFotoUrl;
  @override
  GeneratedTextColumn get displayFotoUrl =>
      _displayFotoUrl ??= _constructDisplayFotoUrl();
  GeneratedTextColumn _constructDisplayFotoUrl() {
    return GeneratedTextColumn(
      'display_foto_url',
      $tableName,
      false,
    );
  }

  final VerificationMeta _lastChatIdMeta = const VerificationMeta('lastChatId');
  GeneratedTextColumn _lastChatId;
  @override
  GeneratedTextColumn get lastChatId => _lastChatId ??= _constructLastChatId();
  GeneratedTextColumn _constructLastChatId() {
    return GeneratedTextColumn(
      'last_chat_id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _displayLastChatMeta =
      const VerificationMeta('displayLastChat');
  GeneratedTextColumn _displayLastChat;
  @override
  GeneratedTextColumn get displayLastChat =>
      _displayLastChat ??= _constructDisplayLastChat();
  GeneratedTextColumn _constructDisplayLastChat() {
    return GeneratedTextColumn(
      'display_last_chat',
      $tableName,
      false,
    );
  }

  final VerificationMeta _lastChatTimeMeta =
      const VerificationMeta('lastChatTime');
  GeneratedRealColumn _lastChatTime;
  @override
  GeneratedRealColumn get lastChatTime =>
      _lastChatTime ??= _constructLastChatTime();
  GeneratedRealColumn _constructLastChatTime() {
    return GeneratedRealColumn(
      'last_chat_time',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [
        idUrl,
        displayName,
        displayFotoUrl,
        lastChatId,
        displayLastChat,
        lastChatTime
      ];
  @override
  $InterlocutorEntityTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'interlocutor';
  @override
  final String actualTableName = 'interlocutor';
  @override
  VerificationContext validateIntegrity(InterlocutorEntityCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.idUrl.present) {
      context.handle(
          _idUrlMeta, idUrl.isAcceptableValue(d.idUrl.value, _idUrlMeta));
    } else if (isInserting) {
      context.missing(_idUrlMeta);
    }
    if (d.displayName.present) {
      context.handle(_displayNameMeta,
          displayName.isAcceptableValue(d.displayName.value, _displayNameMeta));
    } else if (isInserting) {
      context.missing(_displayNameMeta);
    }
    if (d.displayFotoUrl.present) {
      context.handle(
          _displayFotoUrlMeta,
          displayFotoUrl.isAcceptableValue(
              d.displayFotoUrl.value, _displayFotoUrlMeta));
    } else if (isInserting) {
      context.missing(_displayFotoUrlMeta);
    }
    if (d.lastChatId.present) {
      context.handle(_lastChatIdMeta,
          lastChatId.isAcceptableValue(d.lastChatId.value, _lastChatIdMeta));
    } else if (isInserting) {
      context.missing(_lastChatIdMeta);
    }
    if (d.displayLastChat.present) {
      context.handle(
          _displayLastChatMeta,
          displayLastChat.isAcceptableValue(
              d.displayLastChat.value, _displayLastChatMeta));
    } else if (isInserting) {
      context.missing(_displayLastChatMeta);
    }
    if (d.lastChatTime.present) {
      context.handle(
          _lastChatTimeMeta,
          lastChatTime.isAcceptableValue(
              d.lastChatTime.value, _lastChatTimeMeta));
    } else if (isInserting) {
      context.missing(_lastChatTimeMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {idUrl};
  @override
  Interlocutor map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Interlocutor.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(InterlocutorEntityCompanion d) {
    final map = <String, Variable>{};
    if (d.idUrl.present) {
      map['id_url'] = Variable<String, StringType>(d.idUrl.value);
    }
    if (d.displayName.present) {
      map['display_name'] = Variable<String, StringType>(d.displayName.value);
    }
    if (d.displayFotoUrl.present) {
      map['display_foto_url'] =
          Variable<String, StringType>(d.displayFotoUrl.value);
    }
    if (d.lastChatId.present) {
      map['last_chat_id'] = Variable<String, StringType>(d.lastChatId.value);
    }
    if (d.displayLastChat.present) {
      map['display_last_chat'] =
          Variable<String, StringType>(d.displayLastChat.value);
    }
    if (d.lastChatTime.present) {
      map['last_chat_time'] = Variable<double, RealType>(d.lastChatTime.value);
    }
    return map;
  }

  @override
  $InterlocutorEntityTable createAlias(String alias) {
    return $InterlocutorEntityTable(_db, alias);
  }
}

class Conversation extends DataClass implements Insertable<Conversation> {
  final String interlocutorId;
  final String conversationId;
  final String conversation;
  final int conversationStatus;
  final int sender;
  final double sendAt;
  final double receivedByServerAt;
  final double sentAt;
  final double readAt;
  Conversation(
      {@required this.interlocutorId,
      @required this.conversationId,
      @required this.conversation,
      @required this.conversationStatus,
      @required this.sender,
      this.sendAt,
      this.receivedByServerAt,
      this.sentAt,
      this.readAt});
  factory Conversation.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final stringType = db.typeSystem.forDartType<String>();
    final intType = db.typeSystem.forDartType<int>();
    final doubleType = db.typeSystem.forDartType<double>();
    return Conversation(
      interlocutorId: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}interlocutor_id']),
      conversationId: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}conversation_id']),
      conversation: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}conversation']),
      conversationStatus: intType.mapFromDatabaseResponse(
          data['${effectivePrefix}conversation_status']),
      sender: intType.mapFromDatabaseResponse(data['${effectivePrefix}sender']),
      sendAt:
          doubleType.mapFromDatabaseResponse(data['${effectivePrefix}send_at']),
      receivedByServerAt: doubleType.mapFromDatabaseResponse(
          data['${effectivePrefix}received_by_server_at']),
      sentAt:
          doubleType.mapFromDatabaseResponse(data['${effectivePrefix}sent_at']),
      readAt:
          doubleType.mapFromDatabaseResponse(data['${effectivePrefix}read_at']),
    );
  }
  factory Conversation.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return Conversation(
      interlocutorId: serializer.fromJson<String>(json['interlocutorId']),
      conversationId: serializer.fromJson<String>(json['conversationId']),
      conversation: serializer.fromJson<String>(json['conversation']),
      conversationStatus: serializer.fromJson<int>(json['conversationStatus']),
      sender: serializer.fromJson<int>(json['sender']),
      sendAt: serializer.fromJson<double>(json['sendAt']),
      receivedByServerAt:
          serializer.fromJson<double>(json['receivedByServerAt']),
      sentAt: serializer.fromJson<double>(json['sentAt']),
      readAt: serializer.fromJson<double>(json['readAt']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'interlocutorId': serializer.toJson<String>(interlocutorId),
      'conversationId': serializer.toJson<String>(conversationId),
      'conversation': serializer.toJson<String>(conversation),
      'conversationStatus': serializer.toJson<int>(conversationStatus),
      'sender': serializer.toJson<int>(sender),
      'sendAt': serializer.toJson<double>(sendAt),
      'receivedByServerAt': serializer.toJson<double>(receivedByServerAt),
      'sentAt': serializer.toJson<double>(sentAt),
      'readAt': serializer.toJson<double>(readAt),
    };
  }

  @override
  ConversationEntityCompanion createCompanion(bool nullToAbsent) {
    return ConversationEntityCompanion(
      interlocutorId: interlocutorId == null && nullToAbsent
          ? const Value.absent()
          : Value(interlocutorId),
      conversationId: conversationId == null && nullToAbsent
          ? const Value.absent()
          : Value(conversationId),
      conversation: conversation == null && nullToAbsent
          ? const Value.absent()
          : Value(conversation),
      conversationStatus: conversationStatus == null && nullToAbsent
          ? const Value.absent()
          : Value(conversationStatus),
      sender:
          sender == null && nullToAbsent ? const Value.absent() : Value(sender),
      sendAt:
          sendAt == null && nullToAbsent ? const Value.absent() : Value(sendAt),
      receivedByServerAt: receivedByServerAt == null && nullToAbsent
          ? const Value.absent()
          : Value(receivedByServerAt),
      sentAt:
          sentAt == null && nullToAbsent ? const Value.absent() : Value(sentAt),
      readAt:
          readAt == null && nullToAbsent ? const Value.absent() : Value(readAt),
    );
  }

  Conversation copyWith(
          {String interlocutorId,
          String conversationId,
          String conversation,
          int conversationStatus,
          int sender,
          double sendAt,
          double receivedByServerAt,
          double sentAt,
          double readAt}) =>
      Conversation(
        interlocutorId: interlocutorId ?? this.interlocutorId,
        conversationId: conversationId ?? this.conversationId,
        conversation: conversation ?? this.conversation,
        conversationStatus: conversationStatus ?? this.conversationStatus,
        sender: sender ?? this.sender,
        sendAt: sendAt ?? this.sendAt,
        receivedByServerAt: receivedByServerAt ?? this.receivedByServerAt,
        sentAt: sentAt ?? this.sentAt,
        readAt: readAt ?? this.readAt,
      );
  @override
  String toString() {
    return (StringBuffer('Conversation(')
          ..write('interlocutorId: $interlocutorId, ')
          ..write('conversationId: $conversationId, ')
          ..write('conversation: $conversation, ')
          ..write('conversationStatus: $conversationStatus, ')
          ..write('sender: $sender, ')
          ..write('sendAt: $sendAt, ')
          ..write('receivedByServerAt: $receivedByServerAt, ')
          ..write('sentAt: $sentAt, ')
          ..write('readAt: $readAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      interlocutorId.hashCode,
      $mrjc(
          conversationId.hashCode,
          $mrjc(
              conversation.hashCode,
              $mrjc(
                  conversationStatus.hashCode,
                  $mrjc(
                      sender.hashCode,
                      $mrjc(
                          sendAt.hashCode,
                          $mrjc(receivedByServerAt.hashCode,
                              $mrjc(sentAt.hashCode, readAt.hashCode)))))))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is Conversation &&
          other.interlocutorId == this.interlocutorId &&
          other.conversationId == this.conversationId &&
          other.conversation == this.conversation &&
          other.conversationStatus == this.conversationStatus &&
          other.sender == this.sender &&
          other.sendAt == this.sendAt &&
          other.receivedByServerAt == this.receivedByServerAt &&
          other.sentAt == this.sentAt &&
          other.readAt == this.readAt);
}

class ConversationEntityCompanion extends UpdateCompanion<Conversation> {
  final Value<String> interlocutorId;
  final Value<String> conversationId;
  final Value<String> conversation;
  final Value<int> conversationStatus;
  final Value<int> sender;
  final Value<double> sendAt;
  final Value<double> receivedByServerAt;
  final Value<double> sentAt;
  final Value<double> readAt;
  const ConversationEntityCompanion({
    this.interlocutorId = const Value.absent(),
    this.conversationId = const Value.absent(),
    this.conversation = const Value.absent(),
    this.conversationStatus = const Value.absent(),
    this.sender = const Value.absent(),
    this.sendAt = const Value.absent(),
    this.receivedByServerAt = const Value.absent(),
    this.sentAt = const Value.absent(),
    this.readAt = const Value.absent(),
  });
  ConversationEntityCompanion.insert({
    @required String interlocutorId,
    @required String conversationId,
    @required String conversation,
    @required int conversationStatus,
    @required int sender,
    this.sendAt = const Value.absent(),
    this.receivedByServerAt = const Value.absent(),
    this.sentAt = const Value.absent(),
    this.readAt = const Value.absent(),
  })  : interlocutorId = Value(interlocutorId),
        conversationId = Value(conversationId),
        conversation = Value(conversation),
        conversationStatus = Value(conversationStatus),
        sender = Value(sender);
  ConversationEntityCompanion copyWith(
      {Value<String> interlocutorId,
      Value<String> conversationId,
      Value<String> conversation,
      Value<int> conversationStatus,
      Value<int> sender,
      Value<double> sendAt,
      Value<double> receivedByServerAt,
      Value<double> sentAt,
      Value<double> readAt}) {
    return ConversationEntityCompanion(
      interlocutorId: interlocutorId ?? this.interlocutorId,
      conversationId: conversationId ?? this.conversationId,
      conversation: conversation ?? this.conversation,
      conversationStatus: conversationStatus ?? this.conversationStatus,
      sender: sender ?? this.sender,
      sendAt: sendAt ?? this.sendAt,
      receivedByServerAt: receivedByServerAt ?? this.receivedByServerAt,
      sentAt: sentAt ?? this.sentAt,
      readAt: readAt ?? this.readAt,
    );
  }
}

class $ConversationEntityTable extends ConversationEntity
    with TableInfo<$ConversationEntityTable, Conversation> {
  final GeneratedDatabase _db;
  final String _alias;
  $ConversationEntityTable(this._db, [this._alias]);
  final VerificationMeta _interlocutorIdMeta =
      const VerificationMeta('interlocutorId');
  GeneratedTextColumn _interlocutorId;
  @override
  GeneratedTextColumn get interlocutorId =>
      _interlocutorId ??= _constructInterlocutorId();
  GeneratedTextColumn _constructInterlocutorId() {
    return GeneratedTextColumn(
      'interlocutor_id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _conversationIdMeta =
      const VerificationMeta('conversationId');
  GeneratedTextColumn _conversationId;
  @override
  GeneratedTextColumn get conversationId =>
      _conversationId ??= _constructConversationId();
  GeneratedTextColumn _constructConversationId() {
    return GeneratedTextColumn(
      'conversation_id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _conversationMeta =
      const VerificationMeta('conversation');
  GeneratedTextColumn _conversation;
  @override
  GeneratedTextColumn get conversation =>
      _conversation ??= _constructConversation();
  GeneratedTextColumn _constructConversation() {
    return GeneratedTextColumn(
      'conversation',
      $tableName,
      false,
    );
  }

  final VerificationMeta _conversationStatusMeta =
      const VerificationMeta('conversationStatus');
  GeneratedIntColumn _conversationStatus;
  @override
  GeneratedIntColumn get conversationStatus =>
      _conversationStatus ??= _constructConversationStatus();
  GeneratedIntColumn _constructConversationStatus() {
    return GeneratedIntColumn(
      'conversation_status',
      $tableName,
      false,
    );
  }

  final VerificationMeta _senderMeta = const VerificationMeta('sender');
  GeneratedIntColumn _sender;
  @override
  GeneratedIntColumn get sender => _sender ??= _constructSender();
  GeneratedIntColumn _constructSender() {
    return GeneratedIntColumn(
      'sender',
      $tableName,
      false,
    );
  }

  final VerificationMeta _sendAtMeta = const VerificationMeta('sendAt');
  GeneratedRealColumn _sendAt;
  @override
  GeneratedRealColumn get sendAt => _sendAt ??= _constructSendAt();
  GeneratedRealColumn _constructSendAt() {
    return GeneratedRealColumn(
      'send_at',
      $tableName,
      true,
    );
  }

  final VerificationMeta _receivedByServerAtMeta =
      const VerificationMeta('receivedByServerAt');
  GeneratedRealColumn _receivedByServerAt;
  @override
  GeneratedRealColumn get receivedByServerAt =>
      _receivedByServerAt ??= _constructReceivedByServerAt();
  GeneratedRealColumn _constructReceivedByServerAt() {
    return GeneratedRealColumn(
      'received_by_server_at',
      $tableName,
      true,
    );
  }

  final VerificationMeta _sentAtMeta = const VerificationMeta('sentAt');
  GeneratedRealColumn _sentAt;
  @override
  GeneratedRealColumn get sentAt => _sentAt ??= _constructSentAt();
  GeneratedRealColumn _constructSentAt() {
    return GeneratedRealColumn(
      'sent_at',
      $tableName,
      true,
    );
  }

  final VerificationMeta _readAtMeta = const VerificationMeta('readAt');
  GeneratedRealColumn _readAt;
  @override
  GeneratedRealColumn get readAt => _readAt ??= _constructReadAt();
  GeneratedRealColumn _constructReadAt() {
    return GeneratedRealColumn(
      'read_at',
      $tableName,
      true,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [
        interlocutorId,
        conversationId,
        conversation,
        conversationStatus,
        sender,
        sendAt,
        receivedByServerAt,
        sentAt,
        readAt
      ];
  @override
  $ConversationEntityTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'conversation';
  @override
  final String actualTableName = 'conversation';
  @override
  VerificationContext validateIntegrity(ConversationEntityCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.interlocutorId.present) {
      context.handle(
          _interlocutorIdMeta,
          interlocutorId.isAcceptableValue(
              d.interlocutorId.value, _interlocutorIdMeta));
    } else if (isInserting) {
      context.missing(_interlocutorIdMeta);
    }
    if (d.conversationId.present) {
      context.handle(
          _conversationIdMeta,
          conversationId.isAcceptableValue(
              d.conversationId.value, _conversationIdMeta));
    } else if (isInserting) {
      context.missing(_conversationIdMeta);
    }
    if (d.conversation.present) {
      context.handle(
          _conversationMeta,
          conversation.isAcceptableValue(
              d.conversation.value, _conversationMeta));
    } else if (isInserting) {
      context.missing(_conversationMeta);
    }
    if (d.conversationStatus.present) {
      context.handle(
          _conversationStatusMeta,
          conversationStatus.isAcceptableValue(
              d.conversationStatus.value, _conversationStatusMeta));
    } else if (isInserting) {
      context.missing(_conversationStatusMeta);
    }
    if (d.sender.present) {
      context.handle(
          _senderMeta, sender.isAcceptableValue(d.sender.value, _senderMeta));
    } else if (isInserting) {
      context.missing(_senderMeta);
    }
    if (d.sendAt.present) {
      context.handle(
          _sendAtMeta, sendAt.isAcceptableValue(d.sendAt.value, _sendAtMeta));
    }
    if (d.receivedByServerAt.present) {
      context.handle(
          _receivedByServerAtMeta,
          receivedByServerAt.isAcceptableValue(
              d.receivedByServerAt.value, _receivedByServerAtMeta));
    }
    if (d.sentAt.present) {
      context.handle(
          _sentAtMeta, sentAt.isAcceptableValue(d.sentAt.value, _sentAtMeta));
    }
    if (d.readAt.present) {
      context.handle(
          _readAtMeta, readAt.isAcceptableValue(d.readAt.value, _readAtMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {conversationId};
  @override
  Conversation map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Conversation.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(ConversationEntityCompanion d) {
    final map = <String, Variable>{};
    if (d.interlocutorId.present) {
      map['interlocutor_id'] =
          Variable<String, StringType>(d.interlocutorId.value);
    }
    if (d.conversationId.present) {
      map['conversation_id'] =
          Variable<String, StringType>(d.conversationId.value);
    }
    if (d.conversation.present) {
      map['conversation'] = Variable<String, StringType>(d.conversation.value);
    }
    if (d.conversationStatus.present) {
      map['conversation_status'] =
          Variable<int, IntType>(d.conversationStatus.value);
    }
    if (d.sender.present) {
      map['sender'] = Variable<int, IntType>(d.sender.value);
    }
    if (d.sendAt.present) {
      map['send_at'] = Variable<double, RealType>(d.sendAt.value);
    }
    if (d.receivedByServerAt.present) {
      map['received_by_server_at'] =
          Variable<double, RealType>(d.receivedByServerAt.value);
    }
    if (d.sentAt.present) {
      map['sent_at'] = Variable<double, RealType>(d.sentAt.value);
    }
    if (d.readAt.present) {
      map['read_at'] = Variable<double, RealType>(d.readAt.value);
    }
    return map;
  }

  @override
  $ConversationEntityTable createAlias(String alias) {
    return $ConversationEntityTable(_db, alias);
  }
}

class DownloadedEbook extends DataClass implements Insertable<DownloadedEbook> {
  final String idEbookUrl;
  final String path;
  DownloadedEbook({@required this.idEbookUrl, @required this.path});
  factory DownloadedEbook.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final stringType = db.typeSystem.forDartType<String>();
    return DownloadedEbook(
      idEbookUrl: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}id_ebook_url']),
      path: stringType.mapFromDatabaseResponse(data['${effectivePrefix}path']),
    );
  }
  factory DownloadedEbook.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return DownloadedEbook(
      idEbookUrl: serializer.fromJson<String>(json['idEbookUrl']),
      path: serializer.fromJson<String>(json['path']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'idEbookUrl': serializer.toJson<String>(idEbookUrl),
      'path': serializer.toJson<String>(path),
    };
  }

  @override
  DownloadedEbooksCompanion createCompanion(bool nullToAbsent) {
    return DownloadedEbooksCompanion(
      idEbookUrl: idEbookUrl == null && nullToAbsent
          ? const Value.absent()
          : Value(idEbookUrl),
      path: path == null && nullToAbsent ? const Value.absent() : Value(path),
    );
  }

  DownloadedEbook copyWith({String idEbookUrl, String path}) => DownloadedEbook(
        idEbookUrl: idEbookUrl ?? this.idEbookUrl,
        path: path ?? this.path,
      );
  @override
  String toString() {
    return (StringBuffer('DownloadedEbook(')
          ..write('idEbookUrl: $idEbookUrl, ')
          ..write('path: $path')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(idEbookUrl.hashCode, path.hashCode));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is DownloadedEbook &&
          other.idEbookUrl == this.idEbookUrl &&
          other.path == this.path);
}

class DownloadedEbooksCompanion extends UpdateCompanion<DownloadedEbook> {
  final Value<String> idEbookUrl;
  final Value<String> path;
  const DownloadedEbooksCompanion({
    this.idEbookUrl = const Value.absent(),
    this.path = const Value.absent(),
  });
  DownloadedEbooksCompanion.insert({
    @required String idEbookUrl,
    @required String path,
  })  : idEbookUrl = Value(idEbookUrl),
        path = Value(path);
  DownloadedEbooksCompanion copyWith(
      {Value<String> idEbookUrl, Value<String> path}) {
    return DownloadedEbooksCompanion(
      idEbookUrl: idEbookUrl ?? this.idEbookUrl,
      path: path ?? this.path,
    );
  }
}

class $DownloadedEbooksTable extends DownloadedEbooks
    with TableInfo<$DownloadedEbooksTable, DownloadedEbook> {
  final GeneratedDatabase _db;
  final String _alias;
  $DownloadedEbooksTable(this._db, [this._alias]);
  final VerificationMeta _idEbookUrlMeta = const VerificationMeta('idEbookUrl');
  GeneratedTextColumn _idEbookUrl;
  @override
  GeneratedTextColumn get idEbookUrl => _idEbookUrl ??= _constructIdEbookUrl();
  GeneratedTextColumn _constructIdEbookUrl() {
    return GeneratedTextColumn(
      'id_ebook_url',
      $tableName,
      false,
    );
  }

  final VerificationMeta _pathMeta = const VerificationMeta('path');
  GeneratedTextColumn _path;
  @override
  GeneratedTextColumn get path => _path ??= _constructPath();
  GeneratedTextColumn _constructPath() {
    return GeneratedTextColumn(
      'path',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [idEbookUrl, path];
  @override
  $DownloadedEbooksTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'downloaded_ebooks';
  @override
  final String actualTableName = 'downloaded_ebooks';
  @override
  VerificationContext validateIntegrity(DownloadedEbooksCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.idEbookUrl.present) {
      context.handle(_idEbookUrlMeta,
          idEbookUrl.isAcceptableValue(d.idEbookUrl.value, _idEbookUrlMeta));
    } else if (isInserting) {
      context.missing(_idEbookUrlMeta);
    }
    if (d.path.present) {
      context.handle(
          _pathMeta, path.isAcceptableValue(d.path.value, _pathMeta));
    } else if (isInserting) {
      context.missing(_pathMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {idEbookUrl};
  @override
  DownloadedEbook map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return DownloadedEbook.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(DownloadedEbooksCompanion d) {
    final map = <String, Variable>{};
    if (d.idEbookUrl.present) {
      map['id_ebook_url'] = Variable<String, StringType>(d.idEbookUrl.value);
    }
    if (d.path.present) {
      map['path'] = Variable<String, StringType>(d.path.value);
    }
    return map;
  }

  @override
  $DownloadedEbooksTable createAlias(String alias) {
    return $DownloadedEbooksTable(_db, alias);
  }
}

abstract class _$MyDatabase extends GeneratedDatabase {
  _$MyDatabase(QueryExecutor e) : super(SqlTypeSystem.defaultInstance, e);
  $InterlocutorEntityTable _interlocutorEntity;
  $InterlocutorEntityTable get interlocutorEntity =>
      _interlocutorEntity ??= $InterlocutorEntityTable(this);
  $ConversationEntityTable _conversationEntity;
  $ConversationEntityTable get conversationEntity =>
      _conversationEntity ??= $ConversationEntityTable(this);
  $DownloadedEbooksTable _downloadedEbooks;
  $DownloadedEbooksTable get downloadedEbooks =>
      _downloadedEbooks ??= $DownloadedEbooksTable(this);
  ListContactResult _rowToListContactResult(QueryRow row) {
    return ListContactResult(
      idUrl: row.readString('id_url'),
      displayName: row.readString('display_name'),
      displayFotoUrl: row.readString('display_foto_url'),
      lastChatTime: row.readDouble('last_chat_time'),
      displayLastChat: row.readString('display_last_chat'),
      conversationStatus: row.readInt('conversation_status'),
      sender: row.readInt('sender'),
      unreadCount: row.readInt('unread_count'),
    );
  }

  Selectable<ListContactResult> listContactQuery() {
    return customSelectQuery(
        'SELECT i.id_url, i.display_name, i.display_foto_url, i.last_chat_time, i.display_last_chat, c.conversation_status, c.sender, (SELECT COUNT(*) FROM conversation cv WHERE cv.interlocutor_id = i.id_url AND cv.sender = -2 AND cv.conversation_status != 3) AS unread_count FROM interlocutor i LEFT JOIN conversation c ON i.last_chat_id = c.conversation_id ORDER BY i.last_chat_time DESC',
        variables: [],
        readsFrom: {
          interlocutorEntity,
          conversationEntity
        }).map(_rowToListContactResult);
  }

  Future<List<ListContactResult>> listContact() {
    return listContactQuery().get();
  }

  Stream<List<ListContactResult>> watchListContact() {
    return listContactQuery().watch();
  }

  @override
  Iterable<TableInfo> get allTables => allSchemaEntities.whereType<TableInfo>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities =>
      [interlocutorEntity, conversationEntity, downloadedEbooks];
}

class ListContactResult {
  final String idUrl;
  final String displayName;
  final String displayFotoUrl;
  final double lastChatTime;
  final String displayLastChat;
  final int conversationStatus;
  final int sender;
  final int unreadCount;
  ListContactResult({
    this.idUrl,
    this.displayName,
    this.displayFotoUrl,
    this.lastChatTime,
    this.displayLastChat,
    this.conversationStatus,
    this.sender,
    this.unreadCount,
  });
}
