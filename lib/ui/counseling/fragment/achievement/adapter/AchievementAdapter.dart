import 'package:flutter/material.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/extension/Size.dart';
import 'package:halokes_siswa/network/response/general/CounselingResponse.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';

class AchievementItem extends StatelessWidget {
  final AchievementDetail detail;

  AchievementItem({@required this.detail});

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2,
      child: Padding(
        padding: EdgeInsets.all(adaptiveWidth(context, 14)),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  StyledText(detail.namaLomba,
                    fontWeight: FontWeight.w600,
                    size: sp(context, 14),
                  ),
                  SizedBox(height: adaptiveWidth(context, 5)),
                  Row(
                    children: <Widget>[
                      Icon(Icons.date_range,
                        size: sp(context, 10),
                      ),
                      SizedBox(width: adaptiveWidth(context, 5)),
                      StyledText(detail.tanggal,
                        size: sp(context, 10),
                        fontWeight: FontWeight.w600,
                      ),
                    ],
                  )
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(
                vertical: adaptiveWidth(context, 5),
                horizontal: adaptiveWidth(context, 10),
              ),
              decoration: BoxDecoration(
                  border: Border.all(color: ColorUtils.primary),
                  borderRadius: BorderRadius.circular(5)
              ),
              child: Column(
                children: <Widget>[
                  StyledText("Point",
                    size: sp(context, 14),
                    color: ColorUtils.primary,
                    fontWeight: FontWeight.bold,
                  ),
                  StyledText("${detail.poin}",
                    size: sp(context, 24),
                    color: ColorUtils.primary,
                    fontWeight: FontWeight.bold,
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}


