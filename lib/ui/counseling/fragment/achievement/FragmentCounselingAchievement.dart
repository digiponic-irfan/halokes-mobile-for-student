import 'package:flutter/material.dart';
import 'package:halokes_siswa/network/response/general/CounselingResponse.dart';
import 'package:halokes_siswa/ui/counseling/fragment/achievement/adapter/AchievementAdapter.dart';

class FragmentCounselingAchievement extends StatefulWidget {
  final Achievement data;

  FragmentCounselingAchievement({@required this.data});

  @override
  _FragmentCounselingAchievementState createState() => _FragmentCounselingAchievementState();
}

class _FragmentCounselingAchievementState extends State<FragmentCounselingAchievement> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: widget.data.dataPrestasiDetail.length,
      itemBuilder: (_, index) => AchievementItem(detail: widget.data.dataPrestasiDetail[index]),
    );
  }
}
