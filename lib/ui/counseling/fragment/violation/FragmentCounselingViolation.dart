import 'package:flutter/material.dart';
import 'package:halokes_siswa/network/response/general/CounselingResponse.dart';
import 'package:halokes_siswa/ui/counseling/fragment/violation/adapter/ViolationAdapter.dart';

class FragmentCounselingViolation extends StatefulWidget {
  final Violation data;

  FragmentCounselingViolation({@required this.data});

  @override
  _FragmentCounselingViolationState createState() => _FragmentCounselingViolationState();
}

class _FragmentCounselingViolationState extends State<FragmentCounselingViolation> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: widget.data.dataPelanggaranDetail.length,
      itemBuilder: (_, index) => ViolationItem(detail: widget.data.dataPelanggaranDetail[index]),
    );
  }
}
