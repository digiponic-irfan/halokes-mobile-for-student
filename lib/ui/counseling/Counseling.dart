import 'package:flutter/material.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/extension/BaseStateExt.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/network/response/general/CounselingResponse.dart';
import 'package:halokes_siswa/network/response/general/UserResponse.dart';
import 'package:halokes_siswa/preference/AppPreference.dart';
import 'package:halokes_siswa/ui/counseling/fragment/achievement/FragmentCounselingAchievement.dart';
import 'package:halokes_siswa/ui/counseling/fragment/violation/FragmentCounselingViolation.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';

class Counseling extends StatefulWidget {
  final CounselingResponse argument;

  Counseling({@required this.argument});

  @override
  _CounselingState createState() => _CounselingState();
}

class _CounselingState extends BaseState<Counseling> {
  UserData _user;

  var _content = <Widget>[];

  @override
  void initState() {
    super.initState();
    _content.add(FragmentCounselingAchievement(data: widget.argument.data.dataPrestasi));
    _content.add(FragmentCounselingViolation(data: widget.argument.data.dataPelanggaran));
  }

  @override
  void afterWidgetBuilt() async {
    var user = await AppPreference.getUser();
    setState(() => _user = user);
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: _content.length,
      child: Scaffold(
        appBar: AppBar(
          title: StyledText("Konseling",
            color: Colors.white,
            fontWeight: FontWeight.w600,
          ),
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios, color: Colors.white),
            onPressed: () => finish(),
          ),
        ),
        body: Padding(
          padding: EdgeInsets.all(width(24)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: width(24),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    StyledText(_user == null ? "" : _user.namaSiswa,
                      size: sp(20),
                      fontWeight: FontWeight.bold,
                    ),
                    StyledText("Kelas ${_user == null ? "" : _user.kelas} / Aktif",
                      size: sp(15),
                    ),
                    SizedBox(height: width(10)),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            padding: EdgeInsets.all(width(10)),
                            decoration: BoxDecoration(
                              border: Border.all(color: ColorUtils.primary),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Row(
                              children: <Widget>[
                                StyledText("${widget.argument.data.dataPrestasi.poinPrestasi}",
                                  fontWeight: FontWeight.bold,
                                  size: sp(28),
                                  color: ColorUtils.primary,
                                ),
                                SizedBox(width: width(15)),
                                Expanded(
                                  child: StyledText("Poin Prestasi",
                                    size: sp(14),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(width: width(20)),
                        Expanded(
                          child: Container(
                            padding: EdgeInsets.all(width(10)),
                            decoration: BoxDecoration(
                              border: Border.all(color: ColorUtils.danger),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Row(
                              children: <Widget>[
                                StyledText("${widget.argument.data.dataPelanggaran.poinPelanggaran}",
                                  fontWeight: FontWeight.bold,
                                  size: sp(28),
                                  color: ColorUtils.danger,
                                ),
                                SizedBox(width: width(15)),
                                Expanded(
                                  child: StyledText("Poin Pelanggaran",
                                    size: sp(14),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: width(20)),
                  ],
                ),
              ),
              SizedBox(height: width(15)),
              Container(
                width: double.infinity,
                height: 1,
                color: Colors.black.withOpacity(0.1),
                margin: EdgeInsets.symmetric(
                  horizontal: width(10),
                ),
              ),
              SizedBox(height: width(5)),
              TabBar(
                labelColor: ColorUtils.primary,
                unselectedLabelColor: Colors.black,
                indicatorColor: ColorUtils.primary,
                tabs: [
                  Tab(text: "Prestasi"),
                  Tab(text: "Pelanggaran"),
                ],
              ),
              Expanded(
                child: TabBarView(
                  children: _content,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
