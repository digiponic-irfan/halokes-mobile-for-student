import 'package:halokes_siswa/ancestor/BasePresenter.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/ui/splash_screen/SplashScreenDelegate.dart';
import 'package:package_info/package_info.dart';

class SplashScreenPresenter extends BasePresenter {
  static const REQUEST_CHECK_VERSION = 0;

  SplashScreenDelegate _delegate;

  SplashScreenPresenter(BaseState state, this._delegate) : super(state);

  void executeCheckUpdateVersion() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();

    managementRepo.executeGetVersionInfo(REQUEST_CHECK_VERSION,
      packageInfo.buildNumber, "3", "1", (r){
        _delegate.onUpdateVersion(r.requiredUpdate);
      },
    );
  }
}