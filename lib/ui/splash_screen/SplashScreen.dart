import 'dart:io';

import 'package:flutter/material.dart';
import 'package:halokes_siswa/ancestor/BaseResponse.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/main.dart';
import 'package:halokes_siswa/preference/AppPreference.dart';
import 'package:halokes_siswa/ui/splash_screen/SplashScreenDelegate.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';
import 'package:halokes_siswa/utils/ImageUtils.dart';
import 'package:mcnmr_common_ext/FutureDelayed.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:halokes_siswa/extension/BaseStateExt.dart';

import 'SplashScreenPresenter.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends BaseState<SplashScreen> implements SplashScreenDelegate{
  static const DIALOG_UPDATE_VERSION = 0;
  static const DIALOG_NO_CONNECTION = 1;

  SplashScreenPresenter _presenter;

  @override
  void initState() {
    super.initState();
    _presenter = SplashScreenPresenter(this, this);
  }

  @override
  void afterWidgetBuilt() => _presenter.executeCheckUpdateVersion();

  @override
  void onDialogClosed(tag) {
    if(tag == DIALOG_NO_CONNECTION){
      delay(1000, () => _presenter.executeCheckUpdateVersion());
    }
  }

  @override
  void onDialogResult(tag, result) {
    if(tag == DIALOG_UPDATE_VERSION){
      if(result){
        if(Platform.isIOS){
          //_presenter.underDevelopment();
        }else {
          print("On Result = $result");
          launch("https://play.google.com/store/apps/details?id=com.digiponic.halokes.siswa");
        }
      }
    }
  }

  @override
  void onAlreadyLatestVersion() async {
    if (await AppPreference.getUser() == null) {
      navigateTo(MyApp.ROUTE_LOGIN, singleTop: true);
    } else {
      navigateTo(MyApp.ROUTE_HOME, singleTop: true);
    }
  }

  @override
  void onUpdateVersion(bool required) {
    if(required){
      alert(tag: DIALOG_UPDATE_VERSION,
        title: "Versi Baru Tersedia",
        message: "Aplikasi dengan versi baru telah tersedia di Play Store, update aplikasi Anda",
        positiveTitle: "Update",
        positiveColor: ColorUtils.primary,
        barrierDismissible: false,
        onPositive: () => finish(result: true),
      );
    }else {
      alert(tag: DIALOG_UPDATE_VERSION,
          title: "Versi Baru Tersedia",
          message: "Aplikasi dengan versi baru telah tersedia di Play Store, update aplikasi Anda",
          positiveTitle: "Update",
          positiveColor: ColorUtils.primary,
          onPositive: () => finish(result: true),
          negativeTitle: "Tidak",
          negativeColor: Colors.grey,
          barrierDismissible: false,
          onNegative: (){
            finish();
            onAlreadyLatestVersion();
          }
      );
    }
  }

  @override
  void shouldHideLoading(int typeRequest) {}

  @override
  void shouldShowLoading(int typeRequest) {}

  @override
  void onResponseError(int typeRequest, ResponseException exception) {
    if(typeRequest == SplashScreenPresenter.REQUEST_CHECK_VERSION){
      onAlreadyLatestVersion();
    }
  }

  @override
  void onRequestTimeOut(int typeRequest) => delay(1000, () => _presenter.executeCheckUpdateVersion());

  @override
  void onNoConnection(int typeRequest) {
    alert(tag: DIALOG_NO_CONNECTION,
      title: "Tidak terhubung internet",
      message: "Anda tidak terhubung ke internet, hubungkan lalu tekan tombol Ulangi",
      positiveTitle: "Ulangi",
      barrierDismissible: false,
      onPositive: (){
        finish();
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          stops: [0, 0.5, 1],
          colors: [
            ColorUtils.primary,
            ColorUtils.primaryLight,
            ColorUtils.primary,
          ],
        ),
      ),
      child: Center(
        child: Image.asset(ImageUtils.ic_logo,
          width: width(350),
          height: width(280),
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
