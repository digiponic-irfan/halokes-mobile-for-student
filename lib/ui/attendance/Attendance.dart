import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/custom/dialog/AndroidBottomSheetDialog.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/extension/BaseStateExt.dart';
import 'package:halokes_siswa/external_plugin/flutter_calendar/table_calendar.dart';
import 'package:halokes_siswa/network/response/general/AttendanceResponse.dart';
import 'package:halokes_siswa/provider/SemesterProvider.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';
import 'package:halokes_siswa/utils/ImageUtils.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class Attendance extends StatefulWidget {
  final AttendanceResponse argument;

  Attendance({@required this.argument});

  @override
  _AttendanceState createState() => _AttendanceState();
}

class _AttendanceState extends BaseState<Attendance> {
  CalendarController _calendarController = CalendarController();

  bool _noteVisible = false;
  String _note = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: StyledText("Absensi", color: Colors.white),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.white),
          onPressed: () => finish(),
        ),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(width(24)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: width(16),
              ),
              child: Row(
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      StyledText("Persentase \nKehadiran Siswa :",
                        size: sp(14),
                        fontWeight: FontWeight.bold,
                      ),
                      SizedBox(height: width(20)),
                      StyledText("${widget.argument.data.persenHadir}%",
                        size: sp(26),
                        color: ColorUtils.primary,
                        fontWeight: FontWeight.bold,
                      ),
                      StyledText("Baik",
                        size: sp(14),
                        color: ColorUtils.primary,
                      ),
                    ],
                  ),
                  Spacer(),
                  CircularPercentIndicator(
                    radius: width(100),
                    lineWidth: width(10),
                    percent: 0.8,
                    progressColor: ColorUtils.primary,
                    backgroundColor: ColorUtils.primary.withOpacity(0.25),
                  )
                ],
              ),
            ),
            SizedBox(height: width(30)),
            Container(
              width: double.infinity,
              height: 1,
              color: Colors.black.withOpacity(0.10),
            ),
            SizedBox(height: width(30)),
            Row(
              children: <Widget>[
                Expanded(
                  child: Card(
                    elevation: 2,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(width(15)),
                    ),
                    child: Padding(
                      padding: EdgeInsets.all(width(8)),
                      child: Row(
                        children: <Widget>[
                          Image.asset(ImageUtils.ic_attendance_h,
                            width: width(40),
                            height: width(40),
                          ),
                          SizedBox(width: width(15)),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              StyledText("Hadir",
                                size: sp(14),
                                fontWeight: FontWeight.w500,
                              ),
                              SizedBox(height: width(5)),
                              Row(
                                children: <Widget>[
                                  StyledText("${widget.argument.data.hadir}",
                                    size: sp(20),
                                    color: ColorUtils.primary,
                                  ),
                                  SizedBox(width: width(5)),
                                  StyledText("Hari",
                                    size: sp(10),
                                    color: ColorUtils.primary,
                                  ),
                                ],
                              ),
                              SizedBox(height: width(15)),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(width: width(10)),
                Expanded(
                  child: Card(
                    elevation: 2,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(width(15)),
                    ),
                    child: Padding(
                      padding: EdgeInsets.all(width(8)),
                      child: Row(
                        children: <Widget>[
                          Image.asset(ImageUtils.ic_attendance_i,
                            width: width(40),
                            height: width(40),
                          ),
                          SizedBox(width: width(15)),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              StyledText("Ijin",
                                size: sp(14),
                                fontWeight: FontWeight.w500,
                              ),
                              SizedBox(height: width(5)),
                              Row(
                                children: <Widget>[
                                  StyledText("${widget.argument.data.izin}",
                                    size: sp(20),
                                    color: ColorUtils.primary,
                                  ),
                                  SizedBox(width: width(5)),
                                  StyledText("Hari",
                                    size: sp(10),
                                    color: ColorUtils.primary,
                                  ),
                                ],
                              ),
                              SizedBox(height: width(15)),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: width(10)),
            Row(
              children: <Widget>[
                Expanded(
                  child: Card(
                    elevation: 2,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(width(15)),
                    ),
                    child: Padding(
                      padding: EdgeInsets.all(width(8)),
                      child: Row(
                        children: <Widget>[
                          Image.asset(ImageUtils.ic_attendance_s,
                            width: width(40),
                            height: width(40),
                          ),
                          SizedBox(width: width(15)),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              StyledText("Sakit",
                                size: sp(14),
                                fontWeight: FontWeight.w500,
                              ),
                              SizedBox(height: width(5)),
                              Row(
                                children: <Widget>[
                                  StyledText("${widget.argument.data.sakit}",
                                    size: sp(20),
                                    color: ColorUtils.primary,
                                  ),
                                  SizedBox(width: width(5)),
                                  StyledText("Hari",
                                    size: sp(10),
                                    color: ColorUtils.primary,
                                  ),
                                ],
                              ),
                              SizedBox(height: width(15)),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(width: width(10)),
                Expanded(
                  child: Card(
                    elevation: 2,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(width(15)),
                    ),
                    child: Padding(
                      padding: EdgeInsets.all(width(8)),
                      child: Row(
                        children: <Widget>[
                          Image.asset(ImageUtils.ic_attendance_a,
                            width: width(40),
                            height: width(40),
                          ),
                          SizedBox(width: width(15)),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              StyledText("Alpha",
                                size: sp(14),
                                fontWeight: FontWeight.w500,
                              ),
                              SizedBox(height: width(5)),
                              Row(
                                children: <Widget>[
                                  StyledText("${widget.argument.data.alpha}",
                                    size: sp(20),
                                    color: ColorUtils.primary,
                                  ),
                                  SizedBox(width: width(5)),
                                  StyledText("Hari",
                                    size: sp(10),
                                    color: ColorUtils.primary,
                                  ),
                                ],
                              ),
                              SizedBox(height: width(15)),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: width(30)),
            Consumer<SemesterProvider>(
              builder: (_, provider, __){
                var startDate = DateFormat("yyyy-MM-dd").parse(provider.selectedSemester.semesterMulai);
                var endDate = DateFormat("yyyy-MM-dd").parse(provider.selectedSemester.semesterAkhir);

                return TableCalendar(
                  calendarController: _calendarController,
                  alphaDate: widget.argument.data.tglAlpha,
                  permitDate: widget.argument.data.tglIzin,
                  sickDate: widget.argument.data.tglSakit,
                  availableGestures: AvailableGestures.horizontalSwipe,
                  startDay: startDate,
                  endDay: endDate,
                  initialSelectedDay: startDate,
                  availableCalendarFormats: {
                    CalendarFormat.month : "Month"
                  },
                  onDaySelected: (date, _){
                    if(widget.argument.data.tglAlpha.contains(AttendanceDate.fromDateTime(date))){
                      _showBottomSheetDialog(DateFormat("dd MMMM yyyy").format(date), "Tidak Hadir");
                    }else if(widget.argument.data.tglSakit.contains(AttendanceDate.fromDateTime(date))){
                      _showBottomSheetDialog(DateFormat("dd MMMM yyyy").format(date), "Sakit");
                    }else if(widget.argument.data.tglIzin.contains(AttendanceDate.fromDateTime(date))){
                      _showBottomSheetDialog(DateFormat("dd MMMM yyyy").format(date), "Izin");
                    }
                  },
                  headerStyle: HeaderStyle(
                      centerHeaderTitle: true,
                      formatButtonVisible: false
                  ),
                );
              },
            ),
            SizedBox(height: width(10)),
            Visibility(
              visible: _noteVisible,
              child: StyledText("Keterangan : $_note",
                size: sp(14),
                fontWeight: FontWeight.w600,
              ),
            )
          ],
        ),
      ),
    );
  }

  void _showBottomSheetDialog(String date, String note){
    if(Platform.isIOS){
      showCupertinoModalPopup(context: context,
        builder: (_) => CupertinoActionSheet(
          title: Text(date),
          message: Text(note),
        ),
      );
    }else {
      showModalBottomSheet(
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(width(40)),
            topRight: Radius.circular(width(40)),
          )
        ),
        builder: (_) => AndroidBottomSheetDialog(
          title: date,
          message: note,
        ),
      );
    }
  }
}
