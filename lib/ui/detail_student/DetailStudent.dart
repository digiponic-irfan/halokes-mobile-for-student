import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/extension/BaseStateExt.dart';
import 'package:halokes_siswa/external_plugin/cached_network_image/cached_network_image.dart';
import 'package:halokes_siswa/network/response/general/DetailStudentResponse.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';
import 'package:halokes_siswa/utils/ImageUtils.dart';
import 'package:shimmer/shimmer.dart';

class DetailStudent extends StatefulWidget {
  final DetailStudentResponse argument;

  DetailStudent({@required this.argument});

  @override
  _DetailStudentState createState() => _DetailStudentState();
}

class _DetailStudentState extends BaseState<DetailStudent> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorUtils.primary,
      body: Column(
        children: <Widget>[
          AppBar(
            title: StyledText("Detail Siswa", color: Colors.white),
            leading: IconButton(
              icon: Icon(Icons.arrow_back_ios, color: Colors.white),
              onPressed: () => finish(),
            ),
          ),
          SizedBox(height: width(130)),
          Expanded(
            child: Stack(
              alignment: Alignment.topCenter,
              overflow: Overflow.visible,
              children: <Widget>[
                Container(
                  width: double.infinity,
                  height: double.infinity,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(width(20)),
                      topRight: Radius.circular(width(20)),
                    )
                  ),
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: width(50)),
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            StyledText(widget.argument.siswaNama,
                              fontWeight: FontWeight.bold,
                              size: sp(16),
                            ),
                            SizedBox(height: width(10)),
                            StyledText("NIS : ${widget.argument.siswaNis}}",
                              size: sp(14),
                            ),
                            StyledText("Wali : ${widget.argument.namaAyah != "" ?
                            widget.argument.namaAyah : widget.argument.namaIbu != "" ?
                            widget.argument.namaIbu : widget.argument.namaWali != "" ?
                            widget.argument.namaWali : "-"}",
                              size: sp(14),
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  SvgPicture.asset(ImageUtils.ic_student_location,
                                    width: width(40),
                                    height: width(40),
                                  ),
                                  SizedBox(height: width(20)),
                                  StyledText(widget.argument.siswaAlamat != ""
                                      ? widget.argument.siswaAlamat : "-",
                                    size: sp(14),
                                  ),
                                ],
                              ),
                            ),
                            Expanded(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  SvgPicture.asset(ImageUtils.ic_student_phone,
                                    width: width(40),
                                    height: width(40),
                                  ),
                                  SizedBox(height: width(20)),
                                  StyledText("${widget.argument.noHpAyah != "" ?
                                  widget.argument.noHpAyah : widget.argument.noHpIbu != "" ?
                                  widget.argument.noHpIbu : widget.argument.noHpWali != "" ?
                                  widget.argument.noHpWali : "-"}",
                                    size: sp(14),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  top: width(-75),
                  child: CachedNetworkImage(
                    imageUrl: widget.argument.siswaFoto,
                    imageBuilder: (ctx, provider) => Container(
                      width: width(150),
                      height: width(150),
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                            image: provider,
                          )
                      ),
                    ),
                    placeholder: (_, __) => Shimmer.fromColors(
                      child: Container(
                        width: width(150),
                        height: width(150),
                        decoration: BoxDecoration(
                          color: Colors.grey[200],
                          shape: BoxShape.circle,
                        ),
                      ),
                      baseColor: Colors.grey[200],
                      highlightColor: Colors.white,
                    ),
                    errorWidget: (_, __, ___) => Container(
                      width: width(150),
                      height: width(150),
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                          image: AssetImage(ImageUtils.image_not_found),
                          fit: BoxFit.cover
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
