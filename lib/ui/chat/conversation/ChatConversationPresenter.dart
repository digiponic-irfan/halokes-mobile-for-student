import 'package:halokes_siswa/ancestor/BasePresenter.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/database/database.dart';
import 'package:halokes_siswa/network/request/SocketChatRepository.dart';
import 'package:halokes_siswa/network/response/general/ChatUserResponse.dart';
import 'package:halokes_siswa/network/response/socket_chat/ServerReceivedYourMessageResponse.dart';
import 'package:intl/intl.dart';
import 'package:uuid/uuid.dart';

class ChatConversationPresenter extends BasePresenter {
  static const REQUEST_GET_LAST_SEEN = 0;

  Uuid _uuid;
  ChatDisplayUser _interlocutor;
  Stream<List<Conversation>> conversationStream;

  ChatConversationPresenter(BaseState state, ChatDisplayUser interlocutor) : super(state){
    this._uuid = Uuid();
    this._interlocutor = interlocutor;
    conversationStream = database.watchConversation(_interlocutor.idUrl);
  }

  void watchUser(){
    chatSocketRepo.watchUser(_interlocutor.idUrl);
  }

  void unwatch(){
    chatSocketRepo.unwatch();
  }

  void executeGetLastSeen(String userId, Function(String) completion){
    generalRepo.executeGetLastSeen(REQUEST_GET_LAST_SEEN, userId, (r){
      String _lastSeen = r.lastSeen;

      if(r.lastSeen != "Online"){
        var today = DateTime.now();
        var lastSeen = DateFormat("yyyy-MM-dd HH:mm:ss").parse(r.lastSeen);
        var differDays = today.weekday - lastSeen.weekday;

        if(today.year == lastSeen.year && today.month == lastSeen.month){
          if(differDays == 1){
            _lastSeen = "Terakhir dilihat kemarin ini pukul ${DateFormat("HH:mm").format(lastSeen)}";
          }else if(differDays == 0){
            _lastSeen = "Terakhir dilihat hari ini pukul ${DateFormat("HH:mm").format(lastSeen)}";
          }else {
            _lastSeen = "Terakhir dilihat ${DateFormat("dd MMM").format(lastSeen)} ini pukul ${DateFormat("HH:mm").format(lastSeen)}";
          }
        }else {
          _lastSeen = "Terakhir dilihat ${DateFormat("dd MMM").format(lastSeen)} ini pukul ${DateFormat("HH:mm").format(lastSeen)}";
        }
      }

      completion(_lastSeen);
    });
  }
  
  void typing(){
    var data = { "target_url_id" : _interlocutor.idUrl };
    chatSocketRepo.emit(SocketChatRepository.ACTION_TYPING, data);
  }

  Future<void> sendMessage(String message) async {
    if(message == "") return;

    var generatedId = _uuid.v5(_uuid.v1(), _uuid.v4());
    var data = {
      "conversation_id" : generatedId,
      "conversation" : message,
      "send_at" : DateTime.now().millisecondsSinceEpoch,
      "target_url_id" : _interlocutor.idUrl,
    };
    await database.sendMessage(_interlocutor, data);

    chatSocketRepo.emitWithCallback(SocketChatRepository.ACTION_SEND_MESSAGE, data, (data) async {
      await database.yourMessageReceivedByServer(ServerReceivedYourMessageResponse.fromJson(data));
    });
  }

  Future<void> readMessage() async {
    chatSocketRepo.emit(SocketChatRepository.ACTION_READ_MESSAGE, {"sender_id" : _interlocutor.idUrl});
    await database.readMessage(_interlocutor.idUrl);
  }
}