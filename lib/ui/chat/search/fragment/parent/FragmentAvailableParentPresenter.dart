import 'package:halokes_siswa/ancestor/BasePresenter.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/network/response/general/ChatUserResponse.dart';
import 'package:mcnmr_request_wrapper/RequestWrapper.dart';

class FragmentAvailableParentPresenter extends BasePresenter {
  static const REQUEST_GET_AVAILABLE_PARENT = 0;

  FragmentAvailableParentPresenter(BaseState state) : super(state);

  void executeGetAvailableParent(RequestWrapper<AvailableParentResponse> wrapper){
    wrapper.doRequest();
    generalRepo.executeGetAvailableParent(REQUEST_GET_AVAILABLE_PARENT, wrapper.finishRequest);
  }
}