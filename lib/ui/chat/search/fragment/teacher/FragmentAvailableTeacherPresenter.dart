import 'package:halokes_siswa/ancestor/BasePresenter.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/network/response/general/ChatUserResponse.dart';
import 'package:mcnmr_request_wrapper/RequestWrapper.dart';

class FragmentAvailableTeacherPresenter extends BasePresenter {
  static const REQUEST_GET_AVAILABLE_TEACHER = 0;

  FragmentAvailableTeacherPresenter(BaseState state) : super(state);

  void executeGetAvailableTeacher(RequestWrapper<AvailableTeacherResponse> wrapper){
    wrapper.doRequest();
    generalRepo.executeGetAvailableTeacher(REQUEST_GET_AVAILABLE_TEACHER, wrapper.finishRequest);
  }
}