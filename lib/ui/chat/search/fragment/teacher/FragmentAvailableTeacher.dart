import 'package:flutter/material.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/main.dart';
import 'package:halokes_siswa/network/response/general/ChatUserResponse.dart';
import 'package:halokes_siswa/ui/chat/search/adapter/ChatSearchAdapter.dart';
import 'package:halokes_siswa/ui/chat/search/fragment/teacher/FragmentAvailableTeacherPresenter.dart';
import 'package:mcnmr_request_wrapper/RequestWrapper.dart';
import 'package:mcnmr_request_wrapper/RequestWrapperWidget.dart';
import 'package:halokes_siswa/extension/BaseStateExt.dart';

class FragmentAvailableTeacher extends StatefulWidget {
  @override
  _FragmentAvailableTeacherState createState() => _FragmentAvailableTeacherState();
}

class _FragmentAvailableTeacherState extends BaseState<FragmentAvailableTeacher>
    with AutomaticKeepAliveClientMixin {
  FragmentAvailableTeacherPresenter _presenter;
  RequestWrapper<AvailableTeacherResponse> _teacherWrapper = RequestWrapper();

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    _presenter = FragmentAvailableTeacherPresenter(this);
  }

  @override
  void afterWidgetBuilt() => _presenter.executeGetAvailableTeacher(_teacherWrapper);

  @override
  void shouldHideLoading(int typeRequest) {}

  @override
  void shouldShowLoading(int typeRequest) {}

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return RequestWrapperWidget<AvailableTeacherResponse>(
      requestWrapper: _teacherWrapper,
      placeholder: (_) => ListView.builder(
        itemCount: 5,
        itemBuilder: (_, index) => ShimmerChatSearch(),
      ),
      builder: (_, data){
        if(data.data.length > 0){
          return ListView.builder(
            itemCount: data.data.length,
            itemBuilder: (_, index) => ChatSearchItem(data: data.data[index],
              onSelected: _onUserSelected,
            ),
          );
        }

        return Center(
          child: StyledText(data.message,
            fontWeight: FontWeight.w600,
            size: sp(16),
          ),
        );
      },
    );
  }

  void _onUserSelected(ChatDisplayUser user) => navigateTo(MyApp.ROUTE_CHAT_CONVERSATION, arguments: user);
}
