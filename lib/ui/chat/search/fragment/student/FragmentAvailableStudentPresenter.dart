import 'package:halokes_siswa/ancestor/BasePresenter.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/network/response/general/ChatUserResponse.dart';
import 'package:mcnmr_request_wrapper/RequestWrapper.dart';

class FragmentAvailableStudentPresenter extends BasePresenter {
  static const REQUEST_GET_AVAILABLE_STUDENT = 0;

  FragmentAvailableStudentPresenter(BaseState state) : super(state);

  void executeGetAvailableStudent(RequestWrapper<AvailableStudentResponse> wrapper){
    wrapper.doRequest();
    generalRepo.executeGetAvailableStudent(REQUEST_GET_AVAILABLE_STUDENT, wrapper.finishRequest);
  }
}