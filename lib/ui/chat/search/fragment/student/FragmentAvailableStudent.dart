import 'package:flutter/material.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/main.dart';
import 'package:halokes_siswa/network/response/general/ChatUserResponse.dart';
import 'package:halokes_siswa/ui/chat/search/adapter/ChatSearchAdapter.dart';
import 'package:halokes_siswa/ui/chat/search/fragment/student/FragmentAvailableStudentPresenter.dart';
import 'package:mcnmr_request_wrapper/RequestWrapper.dart';
import 'package:mcnmr_request_wrapper/RequestWrapperWidget.dart';
import 'package:halokes_siswa/extension/BaseStateExt.dart';

class FragmentAvailableStudent extends StatefulWidget {
  @override
  _FragmentAvailableStudentState createState() => _FragmentAvailableStudentState();
}

class _FragmentAvailableStudentState extends BaseState<FragmentAvailableStudent>
    with AutomaticKeepAliveClientMixin {
  FragmentAvailableStudentPresenter _presenter;
  RequestWrapper<AvailableStudentResponse> _studentWrapper = RequestWrapper();

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    _presenter = FragmentAvailableStudentPresenter(this);
  }

  @override
  void afterWidgetBuilt() => _presenter.executeGetAvailableStudent(_studentWrapper);

  @override
  void shouldHideLoading(int typeRequest) {}

  @override
  void shouldShowLoading(int typeRequest) {}

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return RequestWrapperWidget<AvailableStudentResponse>(
      requestWrapper: _studentWrapper,
      placeholder: (_) => ListView.builder(
        itemCount: 5,
        itemBuilder: (_, index) => ShimmerChatSearch(),
      ),
      builder: (_, data){
        if(data.data.length > 0){
          return ListView.builder(
            itemCount: data.data.length,
            itemBuilder: (_, index) => ChatSearchItem(data: data.data[index],
              onSelected: _onUserSelected,
            ),
          );
        }

        return Center(
          child: StyledText(data.message,
            fontWeight: FontWeight.w600,
            size: sp(16),
          ),
        );
      },
    );
  }

  void _onUserSelected(ChatDisplayUser user) => navigateTo(MyApp.ROUTE_CHAT_CONVERSATION, arguments: user);
}
