import 'package:flutter/material.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/ui/chat/search/fragment/parent/FragmentAvailableParent.dart';
import 'package:halokes_siswa/ui/chat/search/fragment/student/FragmentAvailableStudent.dart';
import 'package:halokes_siswa/ui/chat/search/fragment/teacher/FragmentAvailableTeacher.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';

class ChatSearch extends StatefulWidget {
  @override
  _ChatSearchState createState() => _ChatSearchState();
}

class _ChatSearchState extends BaseState<ChatSearch> {
  var _content = [
    FragmentAvailableTeacher(),
    FragmentAvailableParent(),
    FragmentAvailableStudent(),
  ];
  
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: ColorUtils.appbar,
          elevation: 0,
          leading: IconButton(icon: Icon(Icons.arrow_back, color: ColorUtils.primary),
            onPressed: () => finish(),
          ),
          title: StyledText("Cari Kontak", color: ColorUtils.primary),
          bottom: TabBar(
            tabs: [
              Tab(text: "Guru"),
              Tab(text: "Orang Tua"),
              Tab(text: "Siswa"),
            ],
            labelColor: ColorUtils.primary,
            unselectedLabelColor: ColorUtils.primary.withOpacity(0.5),
            indicatorColor: ColorUtils.primary,
          ),
        ),
        body: TabBarView(children: _content),
      ),
    );
  }
}
