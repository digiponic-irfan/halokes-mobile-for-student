import 'package:flutter/material.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/extension/Size.dart';
import 'package:halokes_siswa/network/response/general/ChatUserResponse.dart';
import 'package:halokes_siswa/provider/ChatLastSeenProvider.dart';
import 'package:halokes_siswa/provider/TypingProvider.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';
import 'package:halokes_siswa/utils/ImageUtils.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

class ChatSearchItem extends StatelessWidget {
  final ChatDisplayUser data;
  final Function(ChatDisplayUser) onSelected;

  ChatSearchItem({@required this.data, @required this.onSelected})
      : assert(data != null), assert(onSelected != null);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => onSelected(data),
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(adaptiveWidth(context, 15)),
            child: Row(
              children: <Widget>[
                Container(
                  width: adaptiveWidth(context, 48),
                  height: adaptiveWidth(context, 48),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                      image: AssetImage(ImageUtils.image_not_found),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                SizedBox(width: adaptiveWidth(context, 15)),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      StyledText(data.displayName,
                        size: sp(context, 14),
                        fontWeight: FontWeight.w600,
                      ),
                      SizedBox(height: adaptiveWidth(context, 3)),
                      Consumer2<ChatLastSeenProvider, TypingProvider>(
                        builder: (_, lastSeen, typing, __) =>
                            StyledText(_getUserStatus(lastSeen, typing, data.displayOnline),
                              size: sp(context, 12),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              color: ColorUtils.grey7e7e,
                              fontStyle: FontStyle.italic,
                            ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          Container(
            width: double.infinity,
            height: 1,
            color: ColorUtils.greyf1f1,
          ),
        ],
      ),
    );
  }

  String _getUserStatus(ChatLastSeenProvider lastSeen, TypingProvider typing, String def){
    if(typing.isTyping(data.idUrl)){
      return "Sedang mengetik...";
    }

    if(lastSeen.isLastSeenExists(data.idUrl)){
      return lastSeen.getOnlineStatus(data.idUrl);
    }

    return def;
  }
}

class ShimmerChatSearch extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          color: Colors.white,
          padding: EdgeInsets.all(adaptiveWidth(context, 15)),
          child: Row(
            children: <Widget>[
              Shimmer.fromColors(
                child: Container(
                  width: adaptiveWidth(context, 48),
                  height: adaptiveWidth(context, 48),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.grey[200],
                  ),
                ),
                baseColor: Colors.grey[200],
                highlightColor: Colors.white,
              ),
              SizedBox(width: adaptiveWidth(context, 15)),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Shimmer.fromColors(
                    child: Container(
                      width: adaptiveWidth(context, 200),
                      height: sp(context, 14),
                      color: Colors.grey[200],
                    ),
                    baseColor: Colors.grey[200],
                    highlightColor: Colors.white,
                  ),
                  SizedBox(height: adaptiveWidth(context, 3)),
                  Shimmer.fromColors(
                    child: Container(
                      width: adaptiveWidth(context, 80),
                      height: sp(context, 12),
                      color: Colors.grey[200],
                    ),
                    baseColor: Colors.grey[200],
                    highlightColor: Colors.white,
                  ),
                ],
              ),
            ],
          ),
        ),
        Container(
          width: double.infinity,
          height: 1,
          color: ColorUtils.greyf1f1,
        ),
      ],
    );
  }
}

