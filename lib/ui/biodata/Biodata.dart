import 'package:flutter/material.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/extension/BaseStateExt.dart';
import 'package:halokes_siswa/network/response/general/StudentResponse.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';

class Biodata extends StatefulWidget {
  final StudentData argument;

  Biodata({@required this.argument});

  @override
  _BiodataState createState() => _BiodataState();
}

class _BiodataState extends BaseState<Biodata> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorUtils.appbar,
        elevation: 0,
        centerTitle: false,
        title: StyledText("Biodata",
          color: ColorUtils.primary,
          fontWeight: FontWeight.w600,
        ),
        leading: IconButton(icon: Icon(Icons.arrow_back_ios, color: ColorUtils.primary),
          onPressed: () => finish(),
        ),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(width(20)),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                SizedBox(width: width(10)),
                Expanded(
                  child: StyledText("Nama",
                    color: ColorUtils.grey8585,
                    size: sp(12),
                    fontWeight: FontWeight.w600,
                  ),
                  flex: 35,
                ),
                Expanded(
                  child: StyledText(widget.argument.nama,
                    color: Colors.black,
                    size: sp(12),
                    fontWeight: FontWeight.w600,
                  ),
                  flex: 65,
                )
              ],
            ),
            SizedBox(height: width(5)),
            Container(
              height: 1,
              color: Colors.black.withOpacity(0.1),
            ),
            SizedBox(height: width(5)),

            Row(
              children: <Widget>[
                SizedBox(width: width(10)),
                Expanded(
                  child: StyledText("NISN",
                    color: ColorUtils.grey8585,
                    size: sp(12),
                    fontWeight: FontWeight.w600,
                  ),
                  flex: 35,
                ),
                Expanded(
                  child: StyledText(widget.argument.nis,
                    color: Colors.black,
                    size: sp(12),
                    fontWeight: FontWeight.w600,
                  ),
                  flex: 65,
                )
              ],
            ),
            SizedBox(height: width(5)),
            Container(
              height: 1,
              color: Colors.black.withOpacity(0.1),
            ),
            SizedBox(height: width(5)),

            Row(
              children: <Widget>[
                SizedBox(width: width(10)),
                Expanded(
                  child: StyledText("Tempat Lahir",
                    color: ColorUtils.grey8585,
                    size: sp(12),
                    fontWeight: FontWeight.w600,
                  ),
                  flex: 35,
                ),
                Expanded(
                  child: StyledText(widget.argument.dataDiri.tempatLahir,
                    color: Colors.black,
                    fontWeight: FontWeight.w600,
                    size: sp(12),
                  ),
                  flex: 65,
                )
              ],
            ),
            SizedBox(height: width(5)),
            Container(
              height: 1,
              color: Colors.black.withOpacity(0.1),
            ),
            SizedBox(height: width(5)),

            Row(
              children: <Widget>[
                SizedBox(width: width(10)),
                Expanded(
                  child: StyledText("Tanggal Lahir",
                    color: ColorUtils.grey8585,
                    size: sp(12),
                    fontWeight: FontWeight.w600,
                  ),
                  flex: 35,
                ),
                Expanded(
                  child: StyledText(widget.argument.dataDiri.tglLahir,
                    color: Colors.black,
                    size: sp(12),
                    fontWeight: FontWeight.w600,
                  ),
                  flex: 65,
                )
              ],
            ),
            SizedBox(height: width(5)),
            Container(
              height: 1,
              color: Colors.black.withOpacity(0.1),
            ),
            SizedBox(height: width(5)),

            Row(
              children: <Widget>[
                SizedBox(width: width(10)),
                Expanded(
                  child: StyledText("Jenis Kelamin",
                    color: ColorUtils.grey8585,
                    size: sp(12),
                    fontWeight: FontWeight.w600,
                  ),
                  flex: 35,
                ),
                Expanded(
                  child: StyledText(widget.argument.dataDiri.jkel,
                    color: Colors.black,
                    size: sp(12),
                    fontWeight: FontWeight.w600,
                  ),
                  flex: 65,
                )
              ],
            ),
            SizedBox(height: width(5)),
            Container(
              height: 1,
              color: Colors.black.withOpacity(0.1),
            ),
            SizedBox(height: width(5)),

            Row(
              children: <Widget>[
                SizedBox(width: width(10)),
                Expanded(
                  child: StyledText("Alamat",
                    color: ColorUtils.grey8585,
                    size: sp(12),
                    fontWeight: FontWeight.w600,
                  ),
                  flex: 35,
                ),
                Expanded(
                  child: StyledText(widget.argument.dataDiri.alamat,
                    color: Colors.black,
                    size: sp(12),
                    fontWeight: FontWeight.w600,
                  ),
                  flex: 65,
                )
              ],
            ),
            SizedBox(height: width(5)),
            Container(
              height: 1,
              color: Colors.black.withOpacity(0.1),
            ),
            SizedBox(height: width(5)),

            Row(
              children: <Widget>[
                SizedBox(width: width(10)),
                Expanded(
                  child: StyledText("Nomor HP",
                    color: ColorUtils.grey8585,
                    size: sp(12),
                    fontWeight: FontWeight.w600,
                  ),
                  flex: 35,
                ),
                Expanded(
                  child: StyledText(widget.argument.dataDiri.noHp,
                    color: Colors.black,
                    size: sp(12),
                    fontWeight: FontWeight.w600,
                  ),
                  flex: 65,
                )
              ],
            ),
            SizedBox(height: width(5)),
            Container(
              height: 1,
              color: Colors.black.withOpacity(0.1),
            ),
            SizedBox(height: width(5)),

            Row(
              children: <Widget>[
                SizedBox(width: width(10)),
                Expanded(
                  child: StyledText("Email",
                    color: ColorUtils.grey8585,
                    size: sp(12),
                    fontWeight: FontWeight.w600,
                  ),
                  flex: 35,
                ),
                Expanded(
                  child: StyledText(widget.argument.dataDiri.email,
                    color: Colors.black,
                    size: sp(12),
                    fontWeight: FontWeight.w600,
                  ),
                  flex: 65,
                )
              ],
            ),
            SizedBox(height: width(5)),
            Container(
              height: 1,
              color: Colors.black.withOpacity(0.1),
            ),
            SizedBox(height: width(5)),

            Row(
              children: <Widget>[
                SizedBox(width: width(10)),
                Expanded(
                  child: StyledText("Agama",
                    color: ColorUtils.grey8585,
                    size: sp(12),
                    fontWeight: FontWeight.w600,
                  ),
                  flex: 35,
                ),
                Expanded(
                  child: StyledText(widget.argument.dataDiri.agama,
                    color: Colors.black,
                    fontWeight: FontWeight.w600,
                    size: sp(12),
                  ),
                  flex: 65,
                )
              ],
            ),
            SizedBox(height: width(5)),
            Container(
              height: 1,
              color: Colors.black.withOpacity(0.1),
            ),
            SizedBox(height: width(5)),

            Row(
              children: <Widget>[
                SizedBox(width: width(10)),
                Expanded(
                  child: StyledText("Asal Sekolah",
                    color: ColorUtils.grey8585,
                    size: sp(12),
                    fontWeight: FontWeight.w600,
                  ),
                  flex: 35,
                ),
                Expanded(
                  child: StyledText(widget.argument.dataDiri.asalSekolah,
                    color: Colors.black,
                    fontWeight: FontWeight.w600,
                    size: sp(12),
                  ),
                  flex: 65,
                )
              ],
            ),
            SizedBox(height: width(5)),
            Container(
              height: 1,
              color: Colors.black.withOpacity(0.1),
            ),

            SizedBox(height: width(40)),

            Row(
              children: <Widget>[
                SizedBox(width: width(10)),
                Expanded(
                  child: StyledText("Nama Ayah",
                    color: ColorUtils.grey8585,
                    size: sp(12),
                    fontWeight: FontWeight.w600,
                  ),
                  flex: 35,
                ),
                Expanded(
                  child: StyledText(widget.argument.dataOrtu.namaAyah,
                    color: Colors.black,
                    size: sp(12),
                    fontWeight: FontWeight.w600,
                  ),
                  flex: 65,
                )
              ],
            ),
            SizedBox(height: width(5)),
            Container(
              height: 1,
              color: Colors.black.withOpacity(0.1),
            ),
            SizedBox(height: width(5)),

            Row(
              children: <Widget>[
                SizedBox(width: width(10)),
                Expanded(
                  child: StyledText("Nomor HP Ayah",
                    color: ColorUtils.grey8585,
                    size: sp(12),
                    fontWeight: FontWeight.w600,
                  ),
                  flex: 35,
                ),
                Expanded(
                  child: StyledText(widget.argument.dataOrtu.nohpAyah,
                    color: Colors.black,
                    size: sp(12),
                    fontWeight: FontWeight.w600,
                  ),
                  flex: 65,
                )
              ],
            ),
            SizedBox(height: width(5)),
            Container(
              height: 1,
              color: Colors.black.withOpacity(0.1),
            ),
            SizedBox(height: width(5)),

            Row(
              children: <Widget>[
                SizedBox(width: width(10)),
                Expanded(
                  child: StyledText("Nama Ibu",
                    color: ColorUtils.grey8585,
                    size: sp(12),
                    fontWeight: FontWeight.w600,
                  ),
                  flex: 35,
                ),
                Expanded(
                  child: StyledText(widget.argument.dataOrtu.namaIbu,
                    color: Colors.black,
                    size: sp(12),
                    fontWeight: FontWeight.w600,
                  ),
                  flex: 65,
                )
              ],
            ),
            SizedBox(height: width(5)),
            Container(
              height: 1,
              color: Colors.black.withOpacity(0.1),
            ),
            SizedBox(height: width(5)),

            Row(
              children: <Widget>[
                SizedBox(width: width(10)),
                Expanded(
                  child: StyledText("Nomor HP Ibu",
                    color: ColorUtils.grey8585,
                    size: sp(12),
                    fontWeight: FontWeight.w600,
                  ),
                  flex: 35,
                ),
                Expanded(
                  child: StyledText(widget.argument.dataOrtu.nohpIbu,
                    color: Colors.black,
                    size: sp(12),
                    fontWeight: FontWeight.w600,
                  ),
                  flex: 65,
                )
              ],
            ),
            SizedBox(height: width(5)),
            Container(
              height: 1,
              color: Colors.black.withOpacity(0.1),
            ),
            SizedBox(height: width(5)),

            Row(
              children: <Widget>[
                SizedBox(width: width(10)),
                Expanded(
                  child: StyledText("Nama Wali",
                    color: ColorUtils.grey8585,
                    size: sp(12),
                    fontWeight: FontWeight.w600,
                  ),
                  flex: 35,
                ),
                Expanded(
                  child: StyledText(widget.argument.dataOrtu.namaWali,
                    color: Colors.black,
                    fontWeight: FontWeight.w600,
                    size: sp(12),
                  ),
                  flex: 65,
                )
              ],
            ),
            SizedBox(height: width(5)),
            Container(
              height: 1,
              color: Colors.black.withOpacity(0.1),
            ),
            SizedBox(height: width(5)),

            Row(
              children: <Widget>[
                SizedBox(width: width(10)),
                Expanded(
                  child: StyledText("Nomor HP Wali",
                    color: ColorUtils.grey8585,
                    size: sp(12),
                    fontWeight: FontWeight.w600,
                  ),
                  flex: 35,
                ),
                Expanded(
                  child: StyledText(widget.argument.dataOrtu.nohpWali,
                    color: Colors.black,
                    size: sp(12),
                    fontWeight: FontWeight.w600,
                  ),
                  flex: 65,
                )
              ],
            ),
            SizedBox(height: width(5)),
            Container(
              height: 1,
              color: Colors.black.withOpacity(0.1),
            ),
            SizedBox(height: width(5)),
          ],
        ),
      ),
    );
  }
}
