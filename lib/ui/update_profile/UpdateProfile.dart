import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:halokes_siswa/ancestor/BaseResponse.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/custom/dialog/AndroidMultipleChoiceDialog.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/external_plugin/cached_network_image/cached_network_image.dart';
import 'package:halokes_siswa/network/response/general/UserResponse.dart';
import 'package:halokes_siswa/ui/update_profile/UpdateProfileDelegate.dart';
import 'package:halokes_siswa/ui/update_profile/UpdateProfilePresenter.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';
import 'package:halokes_siswa/extension/BaseStateExt.dart';
import 'package:halokes_siswa/utils/FontUtils.dart';
import 'package:halokes_siswa/utils/ImageUtils.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mcnmr_request_wrapper/RequestWrapper.dart';
import 'package:mcnmr_request_wrapper/RequestWrapperWidget.dart';
import 'package:mcnmr_result_object_observer/Holder.dart';
import 'package:mcnmr_result_object_observer/ObjectObserverBuilder.dart';
import 'package:shimmer/shimmer.dart';

class UpdateProfile extends StatefulWidget {
  @override
  _UpdateProfileState createState() => _UpdateProfileState();
}

class _UpdateProfileState extends BaseState<UpdateProfile> implements UpdateProfileDelegate{
  static const TAG_DIALOG_UPDATE_IMAGE = 0x122;
  static const OPEN_GALLERY = 0x123;
  static const OPEN_CAMERA = 0x124;
  static const CANCEL = 0x125;

  UpdateProfilePresenter _presenter;
  RequestWrapper<UserData> _userWrapper = RequestWrapper();
  Holder<File, bool> _imageHolder = Holder((it) => it != null, initialResult: false);

  FocusNode _emailFocus = FocusNode();
  FocusNode _passwordFocus = FocusNode();
  FocusNode _confirmPasswordFocus = FocusNode();

  String _email = "";
  String _password = "";
  String _confirmPassword = "";

  @override
  void afterWidgetBuilt() => _presenter.getUser(_userWrapper);

  @override
  void initState() {
    super.initState();
    _presenter = UpdateProfilePresenter(this, this);
  }

  @override
  void onDialogResult(tag, result) async {
    if(tag == TAG_DIALOG_UPDATE_IMAGE){
      if(result == OPEN_GALLERY){
        _imageHolder.object = await ImagePicker.pickImage(source: ImageSource.gallery);
      }else if(result == OPEN_CAMERA){
        _imageHolder.object = await ImagePicker.pickImage(source: ImageSource.camera);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: StyledText("Ubah Profile",
          color: Colors.white,
          fontWeight: FontWeight.w600,
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.white),
          onPressed: () => finish(),
        ),
      ),
      body: Container(
        width: double.infinity,
        child: SingleChildScrollView(
          padding: EdgeInsets.all(width(20)),
          child: Column(
            children: <Widget>[
              Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  RequestWrapperWidget<UserData>(
                    requestWrapper: _userWrapper,
                    placeholder: (_) => Shimmer.fromColors(
                      child: Container(
                        width: width(130),
                        height: width(130),
                        decoration: BoxDecoration(
                          color: Colors.grey[100],
                          shape: BoxShape.circle,
                        ),
                      ),
                      baseColor: Colors.grey[100],
                      highlightColor: Colors.white,
                    ),
                    builder: (_, data) => CachedNetworkImage(
                      imageUrl: data.foto,
                      imageBuilder: (ctx, provider) => ObjectObserverBuilder<File>(
                        observer: _imageHolder,
                        builder: (_, file) => Container(
                          width: width(130),
                          height: width(130),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                              image: file != null ? FileImage(file) : provider,
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                      ),
                      placeholder: (_, __) => Shimmer.fromColors(
                        child: Container(
                          width: width(130),
                          height: width(130),
                          decoration: BoxDecoration(
                            color: Colors.grey[200],
                            shape: BoxShape.circle,
                          ),
                        ),
                        baseColor: Colors.grey[200],
                        highlightColor: Colors.white,
                      ),
                      errorWidget: (_, __, ___) => ObjectObserverBuilder<File>(
                        observer: _imageHolder,
                        builder: (_, file) => Container(
                          width: width(130),
                          height: width(130),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                              image: file != null ? FileImage(file) : AssetImage(ImageUtils.missing_avatar),
                              fit: BoxFit.fill
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    child: Container(
                      width: width(130),
                      height: width(130),
                      decoration: BoxDecoration(
                        color: Colors.black.withOpacity(0.6),
                        shape: BoxShape.circle,
                      ),
                      child: Center(
                        child: StyledText("Pilih Foto Profl",
                          size: sp(14),
                          color: Colors.white,
                          textAlign: TextAlign.center,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    onTap: () async {
                      if(Platform.isIOS){
                        var result = await showCupertinoModalPopup(
                          context: context,
                          builder: (_) => CupertinoActionSheet(
                            title: Text("Ambil gambar dengan"),
                            actions: <Widget>[
                              CupertinoActionSheetAction(
                                onPressed: () => finish(result: OPEN_GALLERY),
                                child: Text("Gallery"),
                              ),
                              CupertinoActionSheetAction(
                                onPressed: () => finish(result: OPEN_CAMERA),
                                child: Text("Camera"),
                              ),
                            ],
                            cancelButton: CupertinoActionSheetAction(
                              onPressed: () => finish(result: CANCEL),
                              child: Text("Batal"),
                            ),
                          ),
                        );

                        onDialogResult(TAG_DIALOG_UPDATE_IMAGE, result);
                      }else {
                        openDialog(tag: TAG_DIALOG_UPDATE_IMAGE,
                          context: context,
                          builder: (_) => AndroidMultipleChoiceDialog(title: "Ambil gambar dengan",
                            actions: [
                              MultipleChoiceAction(text: "Gallery", action: () => finish(result: OPEN_GALLERY)),
                              MultipleChoiceAction(text: "Camera", action: () => finish(result: OPEN_CAMERA)),
                            ],
                            cancel: MultipleChoiceAction(text: "Batal", action: () => finish(result: CANCEL)),
                          ),
                        );
                      }
                    },
                  ),
                ],
              ),
              SizedBox(height: width(20)),
              Container(
                width: double.infinity,
                height: 1,
                color: Colors.black.withOpacity(0.1),
              ),
              SizedBox(height: width(20)),
              Container(
                padding: EdgeInsets.symmetric(
                  horizontal: width(10),
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(width(20)),
                  border: Border.all(color: Colors.black.withOpacity(0.5))
                ),
                child: Row(
                  children: <Widget>[
                    Icon(Icons.email, color: ColorUtils.primary),
                    SizedBox(width: width(15)),
                    Expanded(
                      child: TextFormField(
                        onChanged: (it) => _email = it,
                        focusNode: _emailFocus,
                        textInputAction: TextInputAction.next,
                        onFieldSubmitted: (it){
                          _emailFocus.unfocus();
                          _passwordFocus.requestFocus();
                        },
                        keyboardType: TextInputType.emailAddress,
                        style: TextStyle(
                          fontSize: width(14),
                          fontFamily: FontUtils.segoeui
                        ),
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          contentPadding: EdgeInsets.zero,
                          hintText: "Email"
                        ),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(height: width(10)),
              Container(
                padding: EdgeInsets.symmetric(
                  horizontal: width(10),
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(width(20)),
                  border: Border.all(color: Colors.black.withOpacity(0.5))
                ),
                child: Row(
                  children: <Widget>[
                    Icon(Icons.lock, color: ColorUtils.primary),
                    SizedBox(width: width(15)),
                    Expanded(
                      child: TextFormField(
                        onChanged: (it) => _password = it,
                        focusNode: _passwordFocus,
                        textInputAction: TextInputAction.next,
                        onFieldSubmitted: (it){
                          _passwordFocus.unfocus();
                          _confirmPasswordFocus.requestFocus();
                        },
                        keyboardType: TextInputType.visiblePassword,
                        obscureText: true,
                        style: TextStyle(
                          fontSize: width(14),
                          fontFamily: FontUtils.segoeui
                        ),
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          contentPadding: EdgeInsets.zero,
                          hintText: "Password"
                        ),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(height: width(10)),
              Container(
                padding: EdgeInsets.symmetric(
                  horizontal: width(10),
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(width(20)),
                  border: Border.all(color: Colors.black.withOpacity(0.5))
                ),
                child: Row(
                  children: <Widget>[
                    Icon(Icons.lock, color: Colors.transparent),
                    SizedBox(width: width(15)),
                    Expanded(
                      child: TextFormField(
                        focusNode: _confirmPasswordFocus,
                        onChanged: (it) => _confirmPassword = it,
                        textInputAction: TextInputAction.done,
                        onFieldSubmitted: (it) => _confirmPasswordFocus.unfocus(),
                        keyboardType: TextInputType.visiblePassword,
                        obscureText: true,
                        style: TextStyle(
                          fontSize: width(14),
                          fontFamily: FontUtils.segoeui
                        ),
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          contentPadding: EdgeInsets.zero,
                          hintText: "Konfirmasi Password"
                        ),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(height: width(40)),
              MaterialButton(
                onPressed: () => _presenter.executeUpdateProfile(_email, _password,
                    _confirmPassword, _imageHolder.object),
                minWidth: width(140),
                height: width(40),
                color: ColorUtils.primary,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(width(30)),
                ),
                child: StyledText("KONFIRMASI",
                  color: Colors.white,
                  size: sp(12),
                  fontWeight: FontWeight.w600,
                ),
              ),
              SizedBox(height: width(40)),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void onSuccessUpdateProfile(BaseResponse response) {
    if(Platform.isIOS){
      iOSAlert(title: "Berhasil", message: "Berhasil ubah profil", positiveTitle: "Kembali", onPositive: (){
        finish();
        finish(result: true);
      });
    }else {
      Fluttertoast.showToast(msg: "Berhasil ubah profil");
      finish(result: true);
    }
  }

}
