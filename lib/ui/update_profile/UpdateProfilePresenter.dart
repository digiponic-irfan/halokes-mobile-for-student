import 'dart:io';

import 'package:dio/dio.dart';
import 'package:halokes_siswa/ancestor/BasePresenter.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/network/response/general/UserResponse.dart';
import 'package:halokes_siswa/preference/AppPreference.dart';
import 'package:halokes_siswa/ui/update_profile/UpdateProfileDelegate.dart';
import 'package:mcnmr_request_wrapper/RequestWrapper.dart';

class UpdateProfilePresenter extends BasePresenter {
  static const REQUEST_UPDATE_PROFILE = 0;

  final UpdateProfileDelegate _delegate;

  UpdateProfilePresenter(BaseState state, this._delegate) : super(state);

  void getUser(RequestWrapper<UserData> wrapper) async {
    wrapper.doRequest();
    wrapper.finishRequest(await AppPreference.getUser());
  }

  void executeUpdateProfile(String email, String password, String confirmPassword, File image) async {
    if(email == ""){
      state.alert(title: "Error", message: "Email wajib diisi", positiveTitle: "Tutup");
      return;
    }

    if(password == ""){
      state.alert(title: "Error", message: "Password wajib diisi", positiveTitle: "Tutup");
      return;
    }

    if(confirmPassword == ""){
      state.alert(title: "Error", message: "Konfirmasi Password wajib diisi", positiveTitle: "Tutup");
      return;
    }

    if(password != confirmPassword){
      state.alert(title: "Error", message: "Password dan Konfirmasi Password tidak sama", positiveTitle: "Tutup");
      return;
    }

    var params = <String, dynamic>{
      "email" : email,
      "password" : password,
    };

    if(image != null){
      params["image"] = MultipartFile.fromFileSync(image.path, filename: "myfile.jpg");
    }

    generalRepo.executeUpdateProfile(REQUEST_UPDATE_PROFILE, params, _delegate.onSuccessUpdateProfile);
  }
}