import 'package:flutter/material.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/extension/BaseStateExt.dart';
import 'package:halokes_siswa/network/response/general/ScoreResponse.dart';
import 'package:halokes_siswa/ui/score/fragment/FragmentScore.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';

class Score extends StatefulWidget {
  final ScoreResponse argument;

  Score({@required this.argument});

  @override
  _ScoreState createState() => _ScoreState();
}

class _ScoreState extends BaseState<Score> {

  var _content = <FragmentScore>[];

  @override
  void initState() {
    super.initState();
    widget.argument.data.forEach((it) => _content.add(FragmentScore(data: it)));
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: widget.argument.data.length,
      child: Scaffold(
        appBar: AppBar(
          title: StyledText("Nilai",
            color: Colors.white,
            fontWeight: FontWeight.w600,
          ),
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios, color: Colors.white),
            onPressed: () => finish(),
          ),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Center(
              child: TabBar(
                isScrollable: true,
                labelColor: ColorUtils.primary,
                unselectedLabelColor: Colors.black,
                tabs: widget.argument.data.map((it) => Tab(text: it.mapelNama)).toList(),
              ),
            ),
            SizedBox(height: width(10)),
            Expanded(child: TabBarView(
              children: _content,
            ))
          ],
        ),
      ),
    );
  }
}
