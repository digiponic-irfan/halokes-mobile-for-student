import 'package:flutter/material.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/custom/view/chart/VerticalBarLabelChart.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/network/response/general/ScoreResponse.dart';
import 'package:halokes_siswa/extension/BaseStateExt.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';

class FragmentScore extends StatefulWidget {
  final ScoreData data;

  FragmentScore({@required this.data});

  @override
  _FragmentScoreState createState() => _FragmentScoreState();
}

class _FragmentScoreState extends BaseState<FragmentScore>
    with AutomaticKeepAliveClientMixin{

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Card(
            elevation: 2,
            margin: EdgeInsets.all(width(12)),
            child: Padding(
              padding: EdgeInsets.all(width(16)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  StyledText("Tugas",
                    size: sp(16),
                    fontWeight: FontWeight.bold,
                  ),
                  SizedBox(height: width(10)),
                  Row(
                    children: <Widget>[
                      StyledText("Nilai rata-rata",
                        size: sp(12),
                        color: ColorUtils.primary,
                      ),
                      SizedBox(width: width(5)),
                      StyledText("${_getAverageAssignment().toStringAsFixed(2)}",
                        size: sp(16),
                        color: ColorUtils.primary,
                        fontWeight: FontWeight.w500,
                      ),
                    ],
                  ),
                  Container(
                    width: double.infinity,
                    height: height(250),
                    child: VerticalBarLabelChart.withActualStringData(widget.data.tugas),
                  )
                ],
              ),
            ),
          ),
          Card(
            elevation: 2,
            margin: EdgeInsets.all(width(12)),
            child: Padding(
              padding: EdgeInsets.all(width(16)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  StyledText("Ulangan Harian",
                    size: sp(16),
                    fontWeight: FontWeight.bold,
                  ),
                  SizedBox(height: width(10)),
                  Row(
                    children: <Widget>[
                      StyledText("Nilai rata-rata",
                        size: sp(12),
                        color: ColorUtils.primary,
                      ),
                      SizedBox(width: width(5)),
                      StyledText("${_getAverageExam().toStringAsFixed(2)}",
                        size: sp(16),
                        color: ColorUtils.primary,
                        fontWeight: FontWeight.w500,
                      ),
                    ],
                  ),
                  Container(
                    width: double.infinity,
                    height: height(250),
                    child: VerticalBarLabelChart.withActualStringData(widget.data.ulanganHarian),
                  )
                ],
              ),
            ),
          ),
          Card(
            elevation: 2,
            margin: EdgeInsets.all(width(12)),
            child: Padding(
              padding: EdgeInsets.all(width(16)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  StyledText("UTS",
                    size: sp(16),
                    fontWeight: FontWeight.bold,
                  ),
                  SizedBox(height: width(10)),
                  Row(
                    children: <Widget>[
                      StyledText("Nilai",
                        size: sp(12),
                        color: ColorUtils.primary,
                      ),
                      SizedBox(width: width(5)),
                      StyledText("${widget.data.uts}",
                        size: sp(16),
                        color: ColorUtils.primary,
                        fontWeight: FontWeight.w500,
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Card(
            elevation: 2,
            margin: EdgeInsets.all(width(12)),
            child: Padding(
              padding: EdgeInsets.all(width(16)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  StyledText("UAS",
                    size: sp(16),
                    fontWeight: FontWeight.bold,
                  ),
                  SizedBox(height: width(10)),
                  Row(
                    children: <Widget>[
                      StyledText("Nilai",
                        size: sp(12),
                        color: ColorUtils.primary,
                      ),
                      SizedBox(width: width(5)),
                      StyledText("${widget.data.uas}",
                        size: sp(16),
                        color: ColorUtils.primary,
                        fontWeight: FontWeight.w500,
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  double _getAverageAssignment(){
    var totalScore = 0.0;

    if(widget.data.tugas.length > 0){
      widget.data.tugas.forEach((it) => totalScore += double.parse(it));
      return totalScore / widget.data.tugas.length;
    }

    return 0.0;
  }

  double _getAverageExam(){
    var totalScore = 0.0;

    if(widget.data.ulanganHarian.length > 0){
      widget.data.ulanganHarian.forEach((it) => totalScore += double.parse(it));
      return totalScore / widget.data.ulanganHarian.length;
    }

    return 0.0;
  }
}

