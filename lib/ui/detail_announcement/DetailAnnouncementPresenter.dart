import 'package:flutter_tts/flutter_tts.dart';
import 'package:halokes_siswa/ancestor/BasePresenter.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:html/parser.dart';

class DetailAnnouncementPresenter extends BasePresenter {

  final _tts = FlutterTts();

  DetailAnnouncementPresenter(BaseState state) : super(state);

  void speak(String data) async {
    await _tts.setLanguage("id-ID");
    await _tts.setSpeechRate(0.5);
    await _tts.setVolume(1.0);
    await _tts.setPitch(1.0);
    await _tts.stop();

    var content = parseHtmlString(data);

    print(content);

    await _tts.speak(parseHtmlString(data));
  }

  void stopSpeak() async {
    await _tts.stop();
  }

  String parseHtmlString(String htmlString) {

    var document = parse(htmlString);

    String parsedString = parse(document.body.text).documentElement.text;

    return parsedString;
  }
}
