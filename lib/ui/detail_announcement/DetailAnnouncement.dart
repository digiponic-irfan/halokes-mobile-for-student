import 'package:flutter/material.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/external_plugin/cached_network_image/cached_network_image.dart';
import 'package:halokes_siswa/network/response/general/AnnouncementResponse.dart';
import 'package:halokes_siswa/extension/BaseStateExt.dart';
import 'package:halokes_siswa/ui/detail_announcement/DetailAnnouncementPresenter.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';
import 'package:halokes_siswa/utils/ImageUtils.dart';
import 'package:shimmer/shimmer.dart';

class DetailAnnouncement extends StatefulWidget {
  final AnnouncementData argument;

  DetailAnnouncement({@required this.argument});

  @override
  _DetailAnnouncementState createState() => _DetailAnnouncementState();
}

class _DetailAnnouncementState extends BaseState<DetailAnnouncement> {
  DetailAnnouncementPresenter _presenter;

  @override
  void initState() {
    super.initState();
    _presenter = DetailAnnouncementPresenter(this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: StyledText("Pengumuman",
          color: Colors.white,
          fontWeight: FontWeight.w600,
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.white),
          onPressed: () => finish(),
        ),
        actions: <Widget>[
          IconButton(
            onPressed: () => _presenter.speak(widget.argument.isiBerita),
            icon: Icon(Icons.record_voice_over, color: ColorUtils.primary),
          )
        ],
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(width(20)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            StyledText(widget.argument.judulBerita,
              size: sp(14),
              fontWeight: FontWeight.bold,
            ),
            SizedBox(height: width(10)),
            CachedNetworkImage(
              imageUrl: widget.argument.gambarBerita,
              imageBuilder: (ctx, provider) => Container(
                height: width(200),
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: provider,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              placeholder: (_, __) => Shimmer.fromColors(
                child: Container(
                  color: Colors.grey[200],
                  height: width(200),
                ),
                baseColor: Colors.grey[200],
                highlightColor: Colors.white,
              ),
              errorWidget: (_, __, ___) => Container(
                height: width(200),
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(ImageUtils.image_not_found),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            SizedBox(height: width(20)),
            Container(
              child: StyledText("Ditulis Oleh : ${widget.argument.penulis}, pada : ${widget.argument.tanggal}",
                size: sp(10),
                fontStyle: FontStyle.italic,
                decoration: TextDecoration.underline,
              ),
              alignment: Alignment.centerRight,
            ),
            SizedBox(height: width(10)),
            StyledText(_presenter.parseHtmlString(widget.argument.isiBerita),
              size: sp(12),
            ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    _presenter.stopSpeak();
    super.dispose();
  }
}
