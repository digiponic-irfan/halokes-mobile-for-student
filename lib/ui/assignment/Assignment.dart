import 'package:flutter/material.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/network/response/general/AssignmentResponse.dart';
import 'package:halokes_siswa/ui/assignment/fragment/FragmentAssignment.dart';
import 'package:halokes_siswa/extension/BaseStateExt.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';

class Assignment extends StatefulWidget {
  final AssignmentResponse argument;

  Assignment({@required this.argument});

  @override
  _AssignmentState createState() => _AssignmentState();
}

class _AssignmentState extends BaseState<Assignment>{
  List<FragmentAssignment> _content = List();

  @override
  void initState() {
    super.initState();
    widget.argument.data.forEach((it) => _content.add(FragmentAssignment(data: it)));
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: widget.argument.data.length,
      child: Scaffold(
        appBar: AppBar(
          centerTitle: false,
          title: StyledText("Tugas",
            fontWeight: FontWeight.w600,
            color: Colors.white,
          ),
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios, color: Colors.white),
            onPressed: () => finish(),
          ),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            TabBar(
              labelColor: ColorUtils.primary,
              unselectedLabelColor: ColorUtils.primary.withOpacity(0.5),
              isScrollable: true,
              tabs: widget.argument.data.map((it) =>
                  Tab(text: it.namaMapel)
              ).toList(),
            ),
            SizedBox(height: width(10)),
            Expanded(
              child: TabBarView(
                children: _content,
              ),
            )
          ],
        ),
      ),
    );
  }


}
