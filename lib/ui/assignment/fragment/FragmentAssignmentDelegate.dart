import 'package:halokes_siswa/ancestor/BaseResponse.dart';
import 'package:halokes_siswa/network/response/general/AssignmentResponse.dart';

class FragmentAssignmentDelegate {
  void onSelectedAssignment(AssignmentDetail detail){}

  void onSuccessSubmitAssignment(BaseResponse response){}
}