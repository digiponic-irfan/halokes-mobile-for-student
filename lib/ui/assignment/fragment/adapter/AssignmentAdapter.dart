import 'package:flutter/material.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/extension/Size.dart';
import 'package:halokes_siswa/network/response/general/AssignmentResponse.dart';
import 'package:halokes_siswa/provider/AssignmentProvider.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

class AssignmentItem extends StatelessWidget {
  final AssignmentDetail detail;
  final Function(AssignmentDetail) onSelected;

  AssignmentItem({@required this.detail, @required this.onSelected});

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.symmetric(
        horizontal: adaptiveWidth(context, 12),
        vertical: adaptiveWidth(context, 8)
      ),
      elevation: 2,
      child: InkWell(
        onTap: () => onSelected(detail),
        highlightColor: Colors.transparent,
        splashColor: ColorUtils.primary.withOpacity(0.5),
        child: Consumer<AssignmentProvider>(
          builder: (_, provider, __) => Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(adaptiveWidth(context, 12)),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      width: adaptiveWidth(context, 12),
                      height: adaptiveWidth(context, 12),
                      decoration: BoxDecoration(
                        color: _getColorIndicator(provider.getStatus(detail)),
                        shape: BoxShape.circle,
                      ),
                    ),
                    SizedBox(width: adaptiveWidth(context, 15)),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          StyledText(detail.judulTugas,
                            size: sp(context, 14),
                            fontWeight: FontWeight.bold,
                          ),
                          SizedBox(height: adaptiveWidth(context, 3)),
                          StyledText(detail.deskripsi,
                            size: sp(context, 12),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(width: adaptiveWidth(context, 15)),
                    Container(
                      width: adaptiveWidth(context, 120),
                      alignment: Alignment.center,
                      padding: EdgeInsets.symmetric(
                        vertical: adaptiveWidth(context, 5),
                      ),
                      child: StyledText(_getSisaWaktu(provider.getStatus(detail)),
                        color: _getColorIndicator(provider.getStatus(detail)),
                        size: sp(context, 10),
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(adaptiveWidth(context, 20)),
                          border: Border.all(color: _getColorIndicator(provider.getStatus(detail)))
                      ),
                    )
                  ],
                ),
              ),
              Container(
                width: double.infinity,
                height: 1,
                color: Colors.grey,
              ),
              Padding(
                padding: EdgeInsets.all(adaptiveWidth(context, 12)),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: StyledText("Batas pengumpulan : ${detail.deadline}",
                        size: sp(context, 10),
                      ),
                      flex: 6,
                    ),
                    Expanded(child:
                      StyledText(_getStatusIndicator(provider.getStatus(detail)),
                        size: sp(context, 12),
                        textAlign: TextAlign.end,
                        color: _getColorIndicator(provider.getStatus(detail)),
                      ),
                      flex: 4,
                    ),
                    SizedBox(width: adaptiveWidth(context, 5)),
                    _getIconIndicator(context, provider.getStatus(detail))
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Color _getColorIndicator(int status){
    switch(status){
      case 0 :
        return ColorUtils.warning;
      case 1 :
        return ColorUtils.primary;
      case 2 :
        return ColorUtils.success;
      default :
        return ColorUtils.danger;
    }
  }

  Widget _getIconIndicator(BuildContext context, int status){
    switch(status){
      case 1 :
        return Icon(Icons.more_horiz,
          size: sp(context, 12),
          color: _getColorIndicator(status),
        );
      case 2 :
        return Icon(Icons.check,
          size: sp(context, 12),
          color: _getColorIndicator(status),
        );
      default :
        return SizedBox();
    }
  }

  String _getStatusIndicator(int status){
    switch(status){
      case 0 :
        return "Belum dikerjakan";
      case 1 :
        return "Sedang Dinilai";
      case 2 :
        return "Sudah Dikerjakan";
      default :
        return "";
    }
  }

  String _getSisaWaktu(int status){
    if(status == 1){
      return "Proses...";
    }else if(status == 2){
      return "Selesai";
    }else if(status == 0){
      int hoursLeft = detail.sisaWaktu;
      int daysLeft = (detail.sisaWaktu / 24).round();

      if (hoursLeft < 0) {
        if (hoursLeft < -24) {
          return "Lewat ${daysLeft * (-1)} hari ";
        } else {
          return "Lewat ${hoursLeft * (-1)} jam";
        }
      } else if (hoursLeft < 24) {
        if (hoursLeft < 1) {
          return ">1 jam lagi";
        } else {
          return "$hoursLeft jam lagi";
        }
      } else {
        return "$daysLeft hari lagi";
      }
    }else {
      return "";
    }
  }
}

class ShimmerAssignmentItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.symmetric(
          horizontal: adaptiveWidth(context, 12),
          vertical: adaptiveWidth(context, 6)
      ),
      elevation: 2,
      child: Shimmer.fromColors(
        child: Container(
          width: double.infinity,
          height: adaptiveWidth(context, 100),
          color: Colors.grey[200],
        ),
        baseColor: Colors.grey[200],
        highlightColor: Colors.white,
      ),
    );
  }
}

