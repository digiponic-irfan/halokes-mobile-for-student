import 'package:halokes_siswa/ancestor/BasePresenter.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/network/response/general/AssignmentResponse.dart';
import 'package:halokes_siswa/preference/AppPreference.dart';
import 'package:halokes_siswa/ui/assignment/fragment/FragmentAssignmentDelegate.dart';

class FragmentAssignmentPresenter extends BasePresenter {
  static const REQUEST_SUBMIT_ASSIGNMENT = 1;

  final FragmentAssignmentDelegate _delegate;

  FragmentAssignmentPresenter(BaseState state, this._delegate) : super(state);

  void executeSubmitAssignment(AssignmentDetail detail) async {
    var user = await AppPreference.getUser();

    var params = {
      "siswa" : user.idUrl,
      "id_tugas" : detail.idTugas,
      "status" : 1
    };

    generalRepo.executeSubmitAssignment(REQUEST_SUBMIT_ASSIGNMENT, params, _delegate.onSuccessSubmitAssignment);
  }
}