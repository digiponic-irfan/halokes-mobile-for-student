import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:halokes_siswa/ancestor/BaseResponse.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/custom/dialog/AndroidOptionDialog.dart';
import 'package:halokes_siswa/network/response/general/AssignmentResponse.dart';
import 'package:halokes_siswa/provider/AssignmentProvider.dart';
import 'package:halokes_siswa/ui/assignment/fragment/FragmentAssignmentDelegate.dart';
import 'package:halokes_siswa/ui/assignment/fragment/FragmentAssignmentPresenter.dart';
import 'package:halokes_siswa/ui/assignment/fragment/adapter/AssignmentAdapter.dart';
import 'package:provider/provider.dart';

class FragmentAssignment extends StatefulWidget {
  final AssignmentData data;

  FragmentAssignment({@required this.data});

  final _state = _FragmentAssignmentState();

  @override
  _FragmentAssignmentState createState() => _state;
}

class _FragmentAssignmentState extends BaseState<FragmentAssignment>
    with AutomaticKeepAliveClientMixin
    implements FragmentAssignmentDelegate{
  static const TAG_SUBMIT_ASSIGNMENT = 0;
  static const SUBMIT_ASSIGNMENT = 1;

  FragmentAssignmentPresenter _presenter;

  AssignmentDetail _selectedAssignment;

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    _presenter = FragmentAssignmentPresenter(this, this);
  }

  @override
  void shouldHideLoading(int typeRequest) {
    if(typeRequest == FragmentAssignmentPresenter.REQUEST_SUBMIT_ASSIGNMENT){
      super.shouldHideLoading(typeRequest);
    }
  }

  @override
  void shouldShowLoading(int typeRequest) {
    if(typeRequest == FragmentAssignmentPresenter.REQUEST_SUBMIT_ASSIGNMENT){
      super.shouldShowLoading(typeRequest);
    }
  }

  @override
  void onDialogResult(tag, result) {
    if(tag == TAG_SUBMIT_ASSIGNMENT && result == SUBMIT_ASSIGNMENT){
      _presenter.executeSubmitAssignment(_selectedAssignment);
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return ListView.builder(
      itemCount: widget.data.dataTugas.length,
      itemBuilder: (_, position) => AssignmentItem(detail: widget.data.dataTugas[position],
        onSelected: onSelectedAssignment,
      ),
    );
  }

  @override
  void onSelectedAssignment(AssignmentDetail detail) {
    _selectedAssignment = detail;

    if(detail.status == 0){
      if(Platform.isIOS){
        openDialog(tag: TAG_SUBMIT_ASSIGNMENT,
          context: context,
          builder: (_) => CupertinoAlertDialog(
            content: Text("Apakah anda telah menyelesaikan tugas ini ?"),
            actions: <Widget>[
              CupertinoDialogAction(child: Text("BELUM"), onPressed: () => finish()),
              CupertinoDialogAction(child: Text("SUDAH"), onPressed: () => finish(result: SUBMIT_ASSIGNMENT)),
            ],
          ),
        );
      }else {
        openDialog(tag: TAG_SUBMIT_ASSIGNMENT,
          context: context,
          builder: (_) =>
              AndroidOptionDialog(
                message: "Apakah anda telah menyelesaikan tugas ini ?",
                negativeTitle: "BELUM",
                positiveTitle: "SUDAH",
                onPositive: () => finish(result: SUBMIT_ASSIGNMENT),
              ),
        );
      }
    }
  }

  @override
  void onSuccessSubmitAssignment(BaseResponse response) {
    Provider.of<AssignmentProvider>(context).submitAssignment(_selectedAssignment);
  }
}
