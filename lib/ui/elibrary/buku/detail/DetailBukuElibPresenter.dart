import 'dart:io';

import 'package:dio/dio.dart';
import 'package:halokes_siswa/ancestor/BasePresenter.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/network/response/management/EbookResponse.dart';
import 'package:halokes_siswa/provider/DownloadEbookProvider.dart';
import 'package:halokes_siswa/ui/elibrary/buku/detail/DetailBukuElibArgument.dart';
import 'package:halokes_siswa/ui/elibrary/buku/list/ElibraryBukuArgument.dart';
import 'package:mcnmr_request_wrapper/RequestWrapper.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';

class DetailBukuElibPresenter extends BasePresenter {
  static const REQUEST_GET_OTHER_EBOOK = 0;
  static const REQUEST_DOWNLOAD_EBOOK = 1;
  static const REQUEST_EBOOK_VIEWS_COUNTER = 2;
  static const REQUEST_EBOOK_DOWNLOAD_COUNTER = 3;

  DetailBukuElibPresenter(BaseState state) : super(state);

  void executeGetOtherEbook(DetailBukuElibArgument argument, RequestWrapper<EbookResponse> wrapper) {
    wrapper.doRequest();

    var kategori = "";

    if(argument.kategori.type == ElibraryBukuType.PELAJARAN){
      kategori = "pelajaran";
    }else if(argument.kategori.type == ElibraryBukuType.NONPELAJARAN){
      kategori = "non-pelajaran";
    }else if(argument.kategori.type == ElibraryBukuType.LATIHAN){
      kategori = "latihan";
    }

    managementRepo.executeGetEbook(REQUEST_GET_OTHER_EBOOK, kategori, (r) => wrapper.finishRequest(r));
  }

  void executeAddEbookViewsCounter(EbookData data){
    managementRepo.addEbookViewCounter(REQUEST_EBOOK_VIEWS_COUNTER, data.idEbookUrl, (_){});
  }

  void executeAddEbookDownloadCounter(EbookData data){
    managementRepo.addEbookDownloadCounter(REQUEST_EBOOK_DOWNLOAD_COUNTER, {"src" : data.file}, (_){});
  }

  void checkDownload(EbookData data, DownloadEbookProvider provider) async {
    var ebook = await database.getEbookByIdUrl(data.idEbookUrl);
    if (ebook != null) {
      provider.alreadyDownloaded(data.idEbookUrl, DownloadEbookTask.fromDownloadedEbooksTable(ebook));
    }
  }

  void downloadEbook(EbookData data, DownloadEbookProvider provider) async {
    var isPermissionGranted = true;
    if (Platform.isAndroid) {
      isPermissionGranted = await _checkStoragePermission();
    }

    if (isPermissionGranted) {
      var fileName = data.file.split("/").last;
      var pathDir = "";

      if (Platform.isIOS) {
        pathDir = (await getApplicationDocumentsDirectory()).path + "/" + fileName;
      } else {
        pathDir = (await getExternalStorageDirectory()).path + "/" + fileName;
      }

      var cancelToken = CancelToken();

      provider.startDownload(data.idEbookUrl, pathDir, cancelToken);
      executeAddEbookDownloadCounter(data);

      managementRepo.download(REQUEST_DOWNLOAD_EBOOK, data.file, pathDir,
          (count, total) {
        provider.updateProgress(data.idEbookUrl, count, total);

        if (count >= total) {
          database.insertToDownloadedEbooks(data.idEbookUrl, pathDir);
        }
      }, cancelToken: cancelToken);
    }
  }

  void open(DownloadEbookTask task) async {
    await OpenFile.open(task.path);
  }

  Future<bool> _checkStoragePermission() async {
    var handler = PermissionHandler();

    var status = await handler.checkPermissionStatus(PermissionGroup.storage);
    if (status == PermissionStatus.granted) {
      return true;
    } else if (status == PermissionStatus.denied) {
      var request = await handler.requestPermissions([PermissionGroup.storage]);
      if (request[PermissionGroup.storage] == PermissionStatus.granted) {
        return true;
      } else {
        if (!await handler
            .shouldShowRequestPermissionRationale(PermissionGroup.storage)) {
          state.alert(
              title: "Perizinan",
              message:
                  "Izin mengakses storage dibutuhkan untuk mendownload file",
              positiveTitle: "Pergi Ke Settings",
              onPositive: () async => handler.openAppSettings());
        } else {
          state.alert(
              title: "Perizinan",
              message:
                  "Izin mengakses storage dibutuhkan untuk mendownload file",
              positiveTitle: "Tutup");
        }
        return false;
      }
    }

    return false;
  }
}
