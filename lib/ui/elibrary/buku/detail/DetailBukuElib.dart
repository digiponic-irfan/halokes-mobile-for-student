import 'package:flutter/material.dart';
import 'package:halokes_siswa/ancestor/BaseResponse.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/extension/BaseStateExt.dart';
import 'package:halokes_siswa/external_plugin/cached_network_image/cached_network_image.dart';
import 'package:halokes_siswa/main.dart';
import 'package:halokes_siswa/network/response/management/EbookResponse.dart';
import 'package:halokes_siswa/provider/DownloadEbookProvider.dart';
import 'package:halokes_siswa/ui/elibrary/buku/detail/DetailBukuElibArgument.dart';
import 'package:halokes_siswa/ui/elibrary/buku/detail/DetailBukuElibDelegate.dart';
import 'package:halokes_siswa/ui/elibrary/buku/detail/DetailBukuElibPresenter.dart';
import 'package:halokes_siswa/ui/elibrary/buku/list/adapter/ElibraryBukuAdapter.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';
import 'package:halokes_siswa/utils/ImageUtils.dart';
import 'package:mcnmr_request_wrapper/RequestWrapper.dart';
import 'package:mcnmr_request_wrapper/RequestWrapperWidget.dart';
import 'package:mcnmr_wrap_horizontal_listview/common.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

class DetailBukuElib extends StatefulWidget {
  final DetailBukuElibArgument argument;

  DetailBukuElib({@required this.argument});

  @override
  _DetailBukuElibState createState() => _DetailBukuElibState();
}

class _DetailBukuElibState extends BaseState<DetailBukuElib> implements DetailBukuElibDelegate{
  DetailBukuElibPresenter _presenter;
  DownloadEbookProvider _downloadEbookProvider;

  RequestWrapper<EbookResponse> _ebookWrapper = RequestWrapper();

  @override
  void initState() {
    super.initState();
    _presenter = DetailBukuElibPresenter(this);
  }

  @override
  void afterWidgetBuilt(){
    _downloadEbookProvider = Provider.of<DownloadEbookProvider>(context);

    _presenter.checkDownload(widget.argument.ebook, _downloadEbookProvider);
    _presenter.executeGetOtherEbook(widget.argument, _ebookWrapper);
    _presenter.executeAddEbookViewsCounter(widget.argument.ebook);
  }

  @override
  void shouldHideLoading(int typeRequest) {}

  @override
  void shouldShowLoading(int typeRequest) {}

  @override
  void onRequestTimeOut(int typeRequest) {}

  @override
  void onNoConnection(int typeRequest) {}

  @override
  void onResponseError(int typeRequest, ResponseException exception) {}

  @override
  void onUnknownError(int typeRequest, String msg) {
    super.onUnknownError(typeRequest, msg);
    if(typeRequest == DetailBukuElibPresenter.REQUEST_DOWNLOAD_EBOOK){
      _downloadEbookProvider.cancel(widget.argument.ebook.idEbookUrl);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: StyledText("Detail Buku", color: Colors.white),
        leading: IconButton(
          onPressed: () => finish(),
          icon: Icon(Icons.arrow_back_ios, color: Colors.white),
        ),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(
          vertical: width(15)
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(horizontal: width(15)),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  CachedNetworkImage(
                    imageUrl: widget.argument.ebook.thumbnail,
                    imageBuilder: (_, image) => Container(
                      margin: EdgeInsets.only(top: width(5)),
                      width: width(180),
                      height: width(240),
                      decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey,
                            spreadRadius: 2.0,
                            blurRadius: 4.0,
                            offset: Offset(0, 1.5),
                          )
                        ],
                        borderRadius: BorderRadius.circular(width(10)),
                        image: DecorationImage(
                          image: image,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    placeholder: (_, __) => Shimmer.fromColors(
                      child: Container(
                        margin: EdgeInsets.only(top: width(5)),
                        width: width(180),
                        height: width(240),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(width(10)),
                          color: Colors.grey[200]
                        ),
                      ),
                      baseColor: Colors.grey[200],
                      highlightColor: Colors.white,
                    ),
                    errorWidget: (_, __, ___) => Container(
                      margin: EdgeInsets.only(top: width(5)),
                      width: width(180),
                      height: width(240),
                      decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey,
                            spreadRadius: 2.0,
                            blurRadius: 4.0,
                            offset: Offset(0, 1.5),
                          )
                        ],
                        borderRadius: BorderRadius.circular(width(10)),
                        image: DecorationImage(
                          image: AssetImage(ImageUtils.image_not_found),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(width: width(20)),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(height: width(20)),
                        StyledText(widget.argument.ebook.judulEbook,
                          fontWeight: FontWeight.w600,
                          size: sp(14),
                        ),
                        SizedBox(height: width(15)),
                        StyledText("${widget.argument.ebook.pengarang}\n"
                            "${widget.argument.ebook.penerbit}",
                          color: ColorUtils.greyaaaa,
                          size: sp(12),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: width(30)),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: width(15)),
              child: StyledText("(${widget.argument.ebook.views}) Dilihat",
                size: sp(12),
              ),
            ),
            SizedBox(height: width(30)),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: width(15)),
              child: StyledText("Deskripsi",
                size: sp(14),
                fontWeight: FontWeight.w600,
              ),
            ),
            SizedBox(height: width(15)),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: width(15)),
              child: StyledText(widget.argument.ebook.deskripsiEbook,
                size: sp(12),
              ),
            ),
            SizedBox(height: width(50)),
            Row(
              children: <Widget>[
                SizedBox(width: width(15)),
                Expanded(
                  child: Consumer<DownloadEbookProvider>(
                    builder: (_, provider, __){
                      if(provider.contains(widget.argument.ebook.idEbookUrl)){
                        var status = provider.getStatus(widget.argument.ebook.idEbookUrl);

                        if(status == DownloadEbookStatus.PREPARING){
                          return MaterialButton(
                            onPressed: (){},
                            height: width(40),
                            color: ColorUtils.primary,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(width(10)),
                            ),
                            child: StyledText("Mendownload",
                              maxLines: 1,
                              size: width(14),
                              color: Colors.white,
                            ),
                          );
                        }else if(status == DownloadEbookStatus.DOWNLOADING){
                          var task = provider.getTask(widget.argument.ebook.idEbookUrl);
                          var current = task.current / 1000000;
                          var total = task.total / 1000000;

                          return MaterialButton(
                            onPressed: (){},
                            height: width(40),
                            color: ColorUtils.primary,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(width(10)),
                            ),
                            child: StyledText("${current.toStringAsFixed(2)}MB / ${total.toStringAsFixed(2)}MB",
                              maxLines: 1,
                              size: width(14),
                              color: Colors.white,
                            ),
                          );
                        }else if(status == DownloadEbookStatus.DOWNLOADED){
                          return MaterialButton(
                            onPressed: () => _presenter.open(provider.getTask(widget.argument.ebook.idEbookUrl)),
                            height: width(40),
                            color: ColorUtils.primary,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(width(10)),
                            ),
                            child: StyledText("Baca",
                              maxLines: 1,
                              size: width(14),
                              color: Colors.white,
                            ),
                          );
                        }
                      }

                      return MaterialButton(
                        onPressed: () => _presenter.downloadEbook(widget.argument.ebook, _downloadEbookProvider),
                        height: width(40),
                        color: ColorUtils.primary,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(width(10)),
                        ),
                        child: StyledText("Download",
                          maxLines: 1,
                          size: width(14),
                          color: Colors.white,
                        ),
                      );
                    },
                  ),
                ),
                Consumer<DownloadEbookProvider>(
                  builder: (_, provider, __){
                    var spacerWidth = 0.0;
                    if(provider.contains(widget.argument.ebook.idEbookUrl)){
                      var status = provider.getStatus(widget.argument.ebook.idEbookUrl);
                      if(status == DownloadEbookStatus.DOWNLOADING){
                        spacerWidth = width(20);
                      }
                    }

                    return AnimatedContainer(
                      duration: Duration(milliseconds: 300),
                      width: spacerWidth,
                      height: width(40),
                    );
                  },
                ),
                Consumer<DownloadEbookProvider>(
                  builder: (_, provider, __){
                    var buttonWidth = 0.0;
                    if(provider.contains(widget.argument.ebook.idEbookUrl)){
                      var status = provider.getStatus(widget.argument.ebook.idEbookUrl);
                      if(status == DownloadEbookStatus.DOWNLOADING){
                        buttonWidth = width(100);
                      }
                    }

                    return AnimatedContainer(
                      duration: Duration(milliseconds: 300),
                      width: buttonWidth,
                      height: width(40),
                      child: MaterialButton(
                        onPressed: () => provider.cancel(widget.argument.ebook.idEbookUrl),
                        height: width(40),
                        color: ColorUtils.danger,
                        minWidth: double.infinity,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(width(10)),
                        ),
                        child: StyledText("Batalkan",
                          maxLines: 1,
                          size: sp(14),
                          color: Colors.white,
                        ),
                      ),
                    );
                  },
                ),
                SizedBox(width: width(20)),
                MaterialButton(
                  onPressed: () => _presenter.underDevelopment(),
                  height: width(40),
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                    side: BorderSide(color: ColorUtils.primary),
                    borderRadius: BorderRadius.circular(width(10)),
                  ),
                  child: Icon(Icons.favorite_border, color: ColorUtils.primary),
                ),
                SizedBox(width: width(15)),
              ],
            ),
            SizedBox(height: width(60)),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: width(10)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  StyledText("Buku Terkait",
                    size: sp(18),
                    fontWeight: FontWeight.bold,
                  ),
                  StyledText("Lihat Semua",
                    size: sp(14),
                    color: ColorUtils.primary,
                  ),
                ],
              ),
            ),
            SizedBox(height: width(15)),
            RequestWrapperWidget<EbookResponse>(
              requestWrapper: _ebookWrapper,
              placeholder: (_) => WrapHorizontalListView(
                itemCount: 5,
                builder: (_, __) => ShimmerElibraryBukuItem(),
              ),
              builder: (_, data) => WrapHorizontalListView(
                itemCount: data.data.length,
                builder: (_, index) => ElibraryBukuItem(ebook: data.data[index],
                  onSelected: onBookSelected,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  void onBookSelected(EbookData ebook) => navigateTo(MyApp.ROUTE_DETAIL_ELIB_BOOK, arguments: ebook);

}
