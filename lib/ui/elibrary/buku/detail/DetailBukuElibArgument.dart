import 'package:flutter/cupertino.dart';
import 'package:halokes_siswa/network/response/management/EbookResponse.dart';
import 'package:halokes_siswa/ui/elibrary/buku/list/ElibraryBukuArgument.dart';

class DetailBukuElibArgument {
  final EbookData ebook;
  final ElibraryBukuArgument kategori;

  DetailBukuElibArgument({@required this.ebook, @required this.kategori}) :
        assert(ebook != null), assert(kategori != null);
}