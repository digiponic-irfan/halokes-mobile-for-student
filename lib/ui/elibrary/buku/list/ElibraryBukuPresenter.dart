import 'package:halokes_siswa/ancestor/BasePresenter.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/network/response/management/EbookResponse.dart';
import 'package:halokes_siswa/ui/elibrary/buku/list/ElibraryBukuArgument.dart';
import 'package:halokes_siswa/ui/elibrary/filter/ElibraryFilterResult.dart';
import 'package:mcnmr_request_wrapper/RequestWrapper.dart';

class ElibraryBukuPresenter extends BasePresenter {
  static const REQUEST_GET_EBOOK = 0;

  ElibraryBukuPresenter(BaseState state) : super(state);

  void executeGetAllEbook(ElibraryBukuArgument argument, RequestWrapper<EbookResponse> wrapper){
    wrapper.doRequest();

    var kategori = "";

    if(argument.type == ElibraryBukuType.PELAJARAN){
      kategori = "pelajaran";
    }else if(argument.type == ElibraryBukuType.NONPELAJARAN){
      kategori = "non-pelajaran";
    }else if(argument.type == ElibraryBukuType.LATIHAN){
      kategori = "latihan";
    }

    managementRepo.executeGetEbook(REQUEST_GET_EBOOK, kategori, wrapper.finishRequest);
  }

  void executeFilterEbook(ElibraryBukuArgument argument,
      ElibraryFilterResult result, RequestWrapper<EbookResponse> wrapper){
    wrapper.doRequest();

    var kategori = "";
    var grade = result.grade == 0 ? "all" : result.grade.toString();
    var subject = result.subject != null ? result.subject.idMapelUrl : result.department.idJurusanUrl;

    if(argument.type == ElibraryBukuType.PELAJARAN){
      kategori = "pelajaran";
    }else if(argument.type == ElibraryBukuType.NONPELAJARAN){
      kategori = "non-pelajaran";
    }else if(argument.type == ElibraryBukuType.LATIHAN){
      kategori = "latihan";
    }

    managementRepo.executeFilterEbook(REQUEST_GET_EBOOK, kategori, grade, subject, wrapper.finishRequest);
  }
}