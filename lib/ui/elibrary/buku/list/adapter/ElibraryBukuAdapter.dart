import 'package:flutter/material.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/extension/Size.dart';
import 'package:halokes_siswa/external_plugin/cached_network_image/cached_network_image.dart';
import 'package:halokes_siswa/network/response/management/EbookResponse.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';
import 'package:halokes_siswa/utils/ImageUtils.dart';
import 'package:shimmer/shimmer.dart';

class ElibraryBukuItem extends StatelessWidget {
  final EbookData ebook;
  final Function(EbookData ebook) onSelected;

  ElibraryBukuItem({@required this.ebook, @required this.onSelected});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => onSelected(ebook),
      child: Container(
        margin: EdgeInsets.symmetric(
            horizontal: adaptiveWidth(context, 10)
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            CachedNetworkImage(
              imageUrl: ebook.thumbnail,
              imageBuilder: (ctx, provider) => Container(
                margin: EdgeInsets.only(top: adaptiveWidth(context, 5)),
                width: adaptiveWidth(context, 115),
                height: adaptiveWidth(context, 150),
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey,
                      spreadRadius: 2.0,
                      blurRadius: 4.0,
                      offset: Offset(0, 1.5),
                    )
                  ],
                  borderRadius: BorderRadius.circular(adaptiveWidth(context, 10)),
                  image: DecorationImage(
                    image: provider,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              placeholder: (_, __) => Shimmer.fromColors(
                child: Container(
                  margin: EdgeInsets.only(top: adaptiveWidth(context, 5)),
                  width: adaptiveWidth(context, 115),
                  height: adaptiveWidth(context, 150),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(adaptiveWidth(context, 10)),
                      color: Colors.grey[200]
                  ),
                ),
                baseColor: Colors.grey[200],
                highlightColor: Colors.white,
              ),
              errorWidget: (_, __, ___) => Container(
                margin: EdgeInsets.only(top: adaptiveWidth(context, 5)),
                width: adaptiveWidth(context, 115),
                height: adaptiveWidth(context, 150),
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey,
                      spreadRadius: 2.0,
                      blurRadius: 4.0,
                      offset: Offset(0, 1.5),
                    )
                  ],
                  borderRadius: BorderRadius.circular(adaptiveWidth(context, 10)),
                  image: DecorationImage(
                    image: AssetImage(ImageUtils.image_not_found),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            SizedBox(height: adaptiveWidth(context, 10)),
            Container(
              width: adaptiveWidth(context, 115),
              child: StyledText(ebook.judulEbook,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                fontWeight: FontWeight.w600,
                size: sp(context, 14),
              ),
            ),
            SizedBox(height: adaptiveWidth(context, 3)),
            Container(
              width: adaptiveWidth(context, 115),
              child: StyledText(ebook.pengarang,
                maxLines: 4,
                overflow: TextOverflow.ellipsis,
                size: sp(context, 12),
                color: ColorUtils.greyaaaa,
              ),
            )
          ],
        ),
      ),
    );
  }
}

class ShimmerElibraryBukuItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: adaptiveWidth(context, 10),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Shimmer.fromColors(
            child: Container(
              margin: EdgeInsets.only(top: adaptiveWidth(context, 5)),
              width: adaptiveWidth(context, 115),
              height: adaptiveWidth(context, 150),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(adaptiveWidth(context, 10)),
                color: Colors.grey[200]
              ),
            ),
            baseColor: Colors.grey[200],
            highlightColor: Colors.white,
          ),
          SizedBox(height: adaptiveWidth(context, 10)),
          Shimmer.fromColors(
            child: Container(
              width: adaptiveWidth(context, 115),
              height: adaptiveWidth(context, 14),
              color: Colors.grey[200],
            ),
            baseColor: Colors.grey[200],
            highlightColor: Colors.white,
          ),
          SizedBox(height: adaptiveWidth(context, 3)),
          Shimmer.fromColors(
            child: Container(
              width: adaptiveWidth(context, 115),
              height: adaptiveWidth(context, 12),
              color: Colors.grey[200],
            ),
            baseColor: Colors.grey[200],
            highlightColor: Colors.white,
          )
        ],
      ),
    );
  }
}

