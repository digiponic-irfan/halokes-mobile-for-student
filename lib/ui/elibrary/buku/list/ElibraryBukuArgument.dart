class ElibraryBukuArgument {
  final ElibraryBukuType type;

  ElibraryBukuArgument({this.type = ElibraryBukuType.NONPELAJARAN});
}

enum ElibraryBukuType {
  PELAJARAN, LATIHAN, NONPELAJARAN
}