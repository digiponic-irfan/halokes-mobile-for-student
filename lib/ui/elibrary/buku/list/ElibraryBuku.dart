import 'package:flutter/material.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/main.dart';
import 'package:halokes_siswa/network/response/management/EbookResponse.dart';
import 'package:halokes_siswa/ui/elibrary/buku/detail/DetailBukuElibArgument.dart';
import 'package:halokes_siswa/ui/elibrary/buku/list/ElibraryBukuArgument.dart';
import 'package:halokes_siswa/ui/elibrary/buku/list/ElibraryBukuDelegate.dart';
import 'package:halokes_siswa/ui/elibrary/buku/list/ElibraryBukuPresenter.dart';
import 'package:halokes_siswa/ui/elibrary/buku/list/adapter/ElibraryBukuAdapter.dart';
import 'package:halokes_siswa/ui/elibrary/filter/ElibraryFilterResult.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';
import 'package:halokes_siswa/extension/BaseStateExt.dart';
import 'package:halokes_siswa/utils/FontUtils.dart';
import 'package:mcnmr_request_wrapper/RequestWrapper.dart';
import 'package:mcnmr_request_wrapper/RequestWrapperWidget.dart';

class ElibraryBuku extends StatefulWidget {
  final ElibraryBukuArgument argument;

  ElibraryBuku({@required this.argument}) : assert(argument != null);

  @override
  _ElibraryBukuState createState() => _ElibraryBukuState();
}

class _ElibraryBukuState extends BaseState<ElibraryBuku> implements ElibraryBukuDelegate{
  ElibraryBukuPresenter _presenter;
  RequestWrapper<EbookResponse> _ebookWrapper = RequestWrapper();
  
  bool _isFiltering = false;
  String _queryFilter = "";

  @override
  void initState() {
    super.initState();
    _presenter = ElibraryBukuPresenter(this);
  }

  @override
  void afterWidgetBuilt() => _presenter.executeGetAllEbook(widget.argument, _ebookWrapper);

  @override
  void shouldHideLoading(int typeRequest) {}

  @override
  void shouldShowLoading(int typeRequest) {}

  @override
  void onNavigationResult(String from, resultData) {
    if(from == MyApp.ROUTE_ELIB_FILTER){
      var result = resultData as ElibraryFilterResult;
      _presenter.executeFilterEbook(widget.argument, result, _ebookWrapper);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: _isFiltering ? TextFormField(
          autofocus: true,
          style: TextStyle(
            fontFamily: FontUtils.segoeui,
            color: Colors.white,
          ),
          decoration: InputDecoration(
            border: InputBorder.none,
            hintText: "Cari disini...",
          ),
          onChanged: (it) => setState(() => _queryFilter = it),
        ) : StyledText("Buku Pelajaran", color: Colors.white),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.white),
          onPressed: () => finish(),
        ),
        actions: _actions(),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(
          vertical: width(20),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            RequestWrapperWidget<EbookResponse>(
              requestWrapper: _ebookWrapper,
              placeholder: (_) => Center(
                child: Wrap(
                  runSpacing: width(15),
                  children: <Widget>[
                    ShimmerElibraryBukuItem(),
                    ShimmerElibraryBukuItem(),
                    ShimmerElibraryBukuItem(),
                    ShimmerElibraryBukuItem(),
                    ShimmerElibraryBukuItem(),
                    ShimmerElibraryBukuItem(),
                    ShimmerElibraryBukuItem(),
                    ShimmerElibraryBukuItem(),
                    ShimmerElibraryBukuItem(),
                  ]
                ),
              ),
              builder: (_, data){
                var list = <EbookData>[];

                if(_queryFilter != ""){
                  data.data.forEach((it){
                    if(it.toString().toLowerCase().contains(_queryFilter.toLowerCase())){
                      list.add(it);
                    }
                  });
                }else {
                  list.addAll(data.data);
                }

                if(list.length == 0){
                  return Center(
                    child: StyledText("Buku tidak ditemukan",
                      size: sp(16),
                      fontWeight: FontWeight.bold,
                    ),
                  );
                }

                if(list.length < 3){
                  return Wrap(
                    runSpacing: width(15),
                    children: list.map((it) =>
                        ElibraryBukuItem(ebook: it, onSelected: onBookSelected)
                    ).toList(),
                  );
                }

                return Center(
                  child: Wrap(
                    runSpacing: width(15),
                    children: list.map((it) =>
                        ElibraryBukuItem(ebook: it, onSelected: onBookSelected)
                    ).toList(),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  @override
  void onBookSelected(EbookData ebook) => navigateTo(MyApp.ROUTE_DETAIL_ELIB_BOOK,
    arguments: DetailBukuElibArgument(
      ebook: ebook,
      kategori: widget.argument,
    ),
  );

  List<Widget> _actions(){
    var list = <Widget>[];
    
    if(_isFiltering){
      list.add(IconButton(
        icon: Icon(Icons.close, color: ColorUtils.primary),
        onPressed: () => setState((){
          _isFiltering = false;
          _queryFilter = "";
        }),
      ));
    }else {
      list.add(IconButton(
        icon: Icon(Icons.filter_list, color: ColorUtils.primary),
        onPressed: () => navigateTo(MyApp.ROUTE_ELIB_FILTER),
      ));
      list.add(IconButton(
        icon: Icon(Icons.search, color: ColorUtils.primary),
        onPressed: () => setState(() => _isFiltering = true),
      ));
    }

    return list;
  }
}
