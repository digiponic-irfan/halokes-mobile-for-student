import 'package:flutter/material.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/main.dart';
import 'package:halokes_siswa/ui/elibrary/buku/list/ElibraryBukuArgument.dart';
import 'package:halokes_siswa/extension/BaseStateExt.dart';
import 'package:halokes_siswa/utils/ImageUtils.dart';

class Elibrary extends StatefulWidget {
  @override
  _ElibraryState createState() => _ElibraryState();
}

class _ElibraryState extends BaseState<Elibrary> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: StyledText("E-Library", color: Colors.white),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.white),
          onPressed: () => finish(),
        ),
      ),
      body: Container(
        width: double.infinity,
        padding: EdgeInsets.only(top: width(40)),
        child: Wrap(
          alignment: WrapAlignment.center,
          runSpacing: width(25),
          spacing: width(25),
          children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.circular(width(10)),
              child: Stack(
                children: <Widget>[
                  Container(
                    width: width(165),
                    height: width(165),
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage(ImageUtils.elib_mata_pelajaran),
                        fit: BoxFit.cover,
                      ),
                    ),
                    child: Container(
                      width: double.infinity,
                      height: double.infinity,
                      color: Colors.black.withOpacity(0.2),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          StyledText("Buku",
                            size: sp(14),
                            color: Colors.white,
                          ),
                          StyledText("Mata Pelajaran",
                            size: sp(16),
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Material(
                    type: MaterialType.transparency,
                    child: InkWell(
                      onTap: () => navigateTo(MyApp.ROUTE_ELIB_BOOK,
                        arguments: ElibraryBukuArgument(type: ElibraryBukuType.PELAJARAN),
                      ),
                      child: Container(
                        width: width(165),
                        height: width(165),
                      ),
                    ),
                  )
                ],
              ),
            ),
            ClipRRect(
              borderRadius: BorderRadius.circular(width(10)),
              child: Stack(
                children: <Widget>[
                  Container(
                    width: width(165),
                    height: width(165),
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage(ImageUtils.elib_video_online),
                        fit: BoxFit.cover,
                      ),
                    ),
                    child: Container(
                      width: double.infinity,
                      height: double.infinity,
                      color: Colors.black.withOpacity(0.2),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          StyledText("Video",
                            size: sp(14),
                            color: Colors.white,
                          ),
                          StyledText("Pembelajaran",
                            size: sp(16),
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Material(
                    type: MaterialType.transparency,
                    child: InkWell(
                      onTap: () => navigateTo(MyApp.ROUTE_ELIB_VIDEO),
                      child: Container(
                        width: width(165),
                        height: width(165),
                      ),
                    ),
                  )
                ],
              ),
            ),
            ClipRRect(
              borderRadius: BorderRadius.circular(width(10)),
              child: Stack(
                children: <Widget>[
                  Container(
                    width: width(165),
                    height: width(165),
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage(ImageUtils.elib_latihan_soal),
                        fit: BoxFit.cover,
                      ),
                    ),
                    child: Container(
                      width: double.infinity,
                      height: double.infinity,
                      color: Colors.black.withOpacity(0.2),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          StyledText("Buku",
                            size: sp(14),
                            color: Colors.white,
                          ),
                          StyledText("Latihan Soal",
                            size: sp(16),
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Material(
                    type: MaterialType.transparency,
                    child: InkWell(
                      onTap: () => navigateTo(MyApp.ROUTE_ELIB_BOOK,
                        arguments: ElibraryBukuArgument(type: ElibraryBukuType.LATIHAN),
                      ),
                      child: Container(
                        width: width(165),
                        height: width(165),
                      ),
                    ),
                  )
                ],
              ),
            ),
            ClipRRect(
              borderRadius: BorderRadius.circular(width(10)),
              child: Stack(
                children: <Widget>[
                  Container(
                    width: width(165),
                    height: width(165),
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage(ImageUtils.elib_other_book),
                        fit: BoxFit.cover,
                      ),
                    ),
                    child: Container(
                      width: double.infinity,
                      height: double.infinity,
                      color: Colors.black.withOpacity(0.2),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          StyledText("Buku",
                            size: sp(14),
                            color: Colors.white,
                          ),
                          StyledText("Lainnya",
                            size: sp(16),
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Material(
                    type: MaterialType.transparency,
                    child: InkWell(
                      onTap: () => navigateTo(MyApp.ROUTE_ELIB_BOOK,
                        arguments: ElibraryBukuArgument(type: ElibraryBukuType.NONPELAJARAN),
                      ),
                      child: Container(
                        width: width(165),
                        height: width(165),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
