import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/extension/Size.dart';
import 'package:halokes_siswa/external_plugin/cached_network_image/cached_network_image.dart';
import 'package:halokes_siswa/network/response/management/VideoResponse.dart';
import 'package:shimmer/shimmer.dart';

class ElibraryVideoItem extends StatelessWidget {
  final VideoData data;
  final Function(VideoData) onSelected;

  ElibraryVideoItem({@required this.data, @required this.onSelected}) :
        assert(data != null), assert(onSelected != null);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: adaptiveWidth(context, 10),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          CachedNetworkImage(
            imageUrl: data.thumbnailMq,
            imageBuilder: (ctx, provider) => Container(
              width: adaptiveWidth(context, 200),
              height: adaptiveWidth(context, 125),
              child: Stack(
                children: <Widget>[
                  Container(
                    width: double.infinity,
                    height: double.infinity,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: provider,
                        fit: BoxFit.cover,
                      ),
                    ),
                    child: Icon(Icons.play_circle_outline,
                      color: Colors.white,
                      size: adaptiveWidth(context, 75),
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    height: double.infinity,
                    color: Colors.black.withOpacity(0.1),
                  ),
                  Material(
                    type: MaterialType.transparency,
                    child: InkWell(
                      onTap: () => onSelected(data),
                      child: SizedBox(
                        width: double.infinity,
                        height: double.infinity,
                      ),
                    ),
                  )
                ],
              ),
            ),
            placeholder: (_, __) => Shimmer.fromColors(
              child: Container(
                width: adaptiveWidth(context, 200),
                height: adaptiveWidth(context, 125),
                color: Colors.grey[200],
              ),
              baseColor: Colors.grey[200],
              highlightColor: Colors.white,
            ),
          ),
          SizedBox(height: adaptiveWidth(context, 10)),
          Container(
            width: adaptiveWidth(context, 200),
            child: StyledText(data.judulVideo,
              size: sp(context, 14),
              fontWeight: FontWeight.w600,
            ),
          ),
          SizedBox(height: adaptiveWidth(context, 5)),
        ],
      ),
    );
  }
}

class ElibraryVerticalVideoItem extends StatelessWidget {
  final VideoData data;
  final Function(VideoData) onSelected;

  ElibraryVerticalVideoItem({@required this.data, @required this.onSelected}) :
        assert(data != null), assert(onSelected != null);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(adaptiveWidth(context, 10)),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          CachedNetworkImage(
            imageUrl: data.thumbnailMq,
            imageBuilder: (ctx, provider) => Container(
              width: adaptiveWidth(context, 200),
              height: adaptiveWidth(context, 125),
              child: Stack(
                children: <Widget>[
                  Container(
                    width: double.infinity,
                    height: double.infinity,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: provider,
                        fit: BoxFit.cover,
                      ),
                    ),
                    child: Icon(Icons.play_circle_outline,
                      color: Colors.white,
                      size: adaptiveWidth(context, 75),
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    height: double.infinity,
                    color: Colors.black.withOpacity(0.1),
                  ),
                  Material(
                    type: MaterialType.transparency,
                    child: InkWell(
                      onTap: () => onSelected(data),
                      child: SizedBox(
                        width: double.infinity,
                        height: double.infinity,
                      ),
                    ),
                  )
                ],
              ),
            ),
            placeholder: (_, __) => Shimmer.fromColors(
              child: Container(
                width: adaptiveWidth(context, 200),
                height: adaptiveWidth(context, 125),
                color: Colors.grey[200],
              ),
              baseColor: Colors.grey[200],
              highlightColor: Colors.white,
            ),
          ),
          SizedBox(width: adaptiveWidth(context, 10)),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  width: adaptiveWidth(context, 200),
                  child: StyledText(data.judulVideo,
                    size: sp(context, 14),
                    fontWeight: FontWeight.w600,
                  ),
                ),
                SizedBox(height: adaptiveWidth(context, 5)),
              ],
            ),
          )
        ],
      ),
    );
  }
}

class ShimmerElibraryVideoItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: adaptiveWidth(context, 10),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Shimmer.fromColors(
            child: Container(
              width: adaptiveWidth(context, 200),
              height: adaptiveWidth(context, 125),
              color: Colors.grey[200],
            ),
            baseColor: Colors.grey[200],
            highlightColor: Colors.white,
          ),
          SizedBox(height: adaptiveWidth(context, 10)),
          Shimmer.fromColors(
            child: Container(
              width: adaptiveWidth(context, 200),
              height: sp(context, 14),
              color: Colors.grey[200],
            ),
            baseColor: Colors.grey[200],
            highlightColor: Colors.white,
          ),
          SizedBox(height: adaptiveWidth(context, 5)),
        ],
      ),
    );
  }
}

class ShimmerElibraryVerticalVideoItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(adaptiveWidth(context, 10)),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Shimmer.fromColors(
            child: Container(
              width: adaptiveWidth(context, 200),
              height: adaptiveWidth(context, 125),
              color: Colors.grey[200],
            ),
            baseColor: Colors.grey[200],
            highlightColor: Colors.white,
          ),
          SizedBox(width: adaptiveWidth(context, 10)),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Shimmer.fromColors(
                  child: Container(
                    width: adaptiveWidth(context, 200),
                    height: sp(context, 14),
                    color: Colors.grey[200],
                  ),
                  baseColor: Colors.grey[200],
                  highlightColor: Colors.white,
                ),
                SizedBox(height: adaptiveWidth(context, 5)),
              ],
            ),
          )
        ],
      ),
    );
  }

}


