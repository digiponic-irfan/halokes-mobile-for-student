import 'dart:io';

import 'package:flutter/material.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/external_plugin/cached_network_image/cached_network_image.dart';
import 'package:halokes_siswa/network/response/management/VideoResponse.dart';
import 'package:halokes_siswa/ui/elibrary/video/ElibraryVideoDelegate.dart';
import 'package:halokes_siswa/ui/elibrary/video/ElibraryVideoPresenter.dart';
import 'package:halokes_siswa/ui/elibrary/video/adapter/ElibraryVideoAdapter.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';
import 'package:halokes_siswa/extension/BaseStateExt.dart';
import 'package:mcnmr_request_wrapper/RequestWrapper.dart';
import 'package:mcnmr_request_wrapper/RequestWrapperWidget.dart';
import 'package:mcnmr_wrap_horizontal_listview/common.dart';
import 'package:shimmer/shimmer.dart';
import 'package:url_launcher/url_launcher.dart';

class ElibraryVideo extends StatefulWidget {
  @override
  _ElibraryVideoState createState() => _ElibraryVideoState();
}

class _ElibraryVideoState extends BaseState<ElibraryVideo> implements ElibraryVideoDelegate{
  static const HEADER_VIDEO_COUNT = 1;
  static const POPULAR_VIDEO_COUNT = 5;
  static const VERTICAL_VIDEO_COUNT = 10;

  ElibraryVideoPresenter _presenter;
  RequestWrapper<VideoResponse> _videoWrapper = RequestWrapper();

  @override
  void initState() {
    super.initState();
    _presenter = ElibraryVideoPresenter(this);
  }

  @override
  void afterWidgetBuilt() => _presenter.executeGetVideo(_videoWrapper);

  @override
  void shouldHideLoading(int typeRequest) {}

  @override
  void shouldShowLoading(int typeRequest) {}

  @override
  void onVideoSelected(VideoData data) async {
    if(Platform.isIOS){
      if(await canLaunch(data.linkVideo)){
        await launch(data.linkVideo, forceSafariVC: false);
      }else {
        alert(title: "Error",
          message: "Anda tidak memiliki aplikasi youtube",
          positiveTitle: "Tutup"
        );
      }
    }else {
      if(await canLaunch(data.linkVideo)){
        await launch(data.linkVideo);
      }else {
        alert(title: "Error",
          message: "Anda tidak memiliki aplikasi youtube",
          positiveTitle: "Tutup",
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: StyledText("Video Pembelajaran", color: Colors.white),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.white),
          onPressed: () => finish(),
        ),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(
          vertical: width(20)
        ),
        child: Column(
          children: <Widget>[
            RequestWrapperWidget<VideoResponse>(
              requestWrapper: _videoWrapper,
              placeholder: (_) => Shimmer.fromColors(
                child: Container(
                  margin: EdgeInsets.symmetric(
                    horizontal: width(10),
                  ),
                  width: double.infinity,
                  height: width(230),
                  color: Colors.grey[200],
                ),
                baseColor: Colors.grey[200],
                highlightColor: Colors.white,
              ),
              builder: (_, data){
                if(data.data.length > 0){
                  return CachedNetworkImage(
                    imageUrl: data.data[0].thumbnailMq,
                    imageBuilder: (ctx, provider) => Container(
                      margin: EdgeInsets.symmetric(
                        horizontal: width(10),
                      ),
                      width: double.infinity,
                      height: width(230),
                      child: Stack(
                        children: <Widget>[
                          Container(
                            width: double.infinity,
                            height: double.infinity,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: provider,
                                fit: BoxFit.cover,
                              ),
                            ),
                            child: Icon(Icons.play_circle_outline,
                              color: Colors.white,
                              size: width(75),
                            ),
                          ),
                          Container(
                            width: double.infinity,
                            height: double.infinity,
                            color: Colors.black.withOpacity(0.1),
                          ),
                          Material(
                            type: MaterialType.transparency,
                            child: InkWell(
                              onTap: () => onVideoSelected(data.data[0]),
                              child: SizedBox(
                                width: double.infinity,
                                height: double.infinity,
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    placeholder: (_, __) => Shimmer.fromColors(
                      child: Container(
                        margin: EdgeInsets.symmetric(
                          horizontal: width(10),
                        ),
                        width: double.infinity,
                        height: width(230),
                        color: Colors.grey[200],
                      ),
                      baseColor: Colors.grey[200],
                      highlightColor: Colors.white,
                    ),
                  );
                }

                return Container();
              },
            ),
            SizedBox(height: width(5)),
            RequestWrapperWidget<VideoResponse>(
              requestWrapper: _videoWrapper,
              placeholder: (_) => Container(
                width: double.infinity,
                margin: EdgeInsets.symmetric(
                  horizontal: width(10),
                ),
                child: Shimmer.fromColors(
                  child: Container(
                    width: width(180),
                    height: sp(16),
                    color: Colors.grey[200],
                  ),
                  baseColor: Colors.grey[200],
                  highlightColor: Colors.white,
                ),
              ),
              builder: (_, data){
                if(data.data.length > 0){
                  return Container(
                    width: double.infinity,
                    margin: EdgeInsets.symmetric(
                      horizontal: width(10),
                    ),
                    child: StyledText(data.data[0].judulVideo,
                      fontWeight: FontWeight.w600,
                      size: sp(16),
                    ),
                  );
                }

                return Container();
              },
            ),

            SizedBox(height: width(30)),

            RequestWrapperWidget<VideoResponse>(
              requestWrapper: _videoWrapper,
              placeholder: (_) => Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: width(10)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Shimmer.fromColors(
                          child: Container(
                            width: width(100),
                            height: sp(18),
                            color: Colors.grey[200],
                          ),
                          baseColor: Colors.grey[200],
                          highlightColor: Colors.white,
                        ),
                        Shimmer.fromColors(
                          child: Container(
                            width: width(80),
                            height: sp(14),
                            color: Colors.grey[200],
                          ),
                          baseColor: Colors.grey[200],
                          highlightColor: Colors.white,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: width(15)),
                  WrapHorizontalListView(
                    itemCount: 5,
                    builder: (_, __) => ShimmerElibraryVideoItem(),
                  ),
                ],
              ),
              builder: (_, data){
                if(data.data.length > HEADER_VIDEO_COUNT){
                  if(data.data.length > HEADER_VIDEO_COUNT + POPULAR_VIDEO_COUNT){
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: width(10)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              StyledText("Populer",
                                size: sp(18),
                                fontWeight: FontWeight.bold,
                              ),
                              StyledText("Lihat Semua",
                                size: sp(14),
                                color: ColorUtils.primary,
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: width(15)),
                        WrapHorizontalListView(
                          itemCount: POPULAR_VIDEO_COUNT,
                          builder: (_, index) => ElibraryVideoItem(
                            data: data.data[index+HEADER_VIDEO_COUNT],
                            onSelected: onVideoSelected,
                          ),
                        ),
                      ],
                    );
                  }else {
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: width(10)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              StyledText("Populer",
                                size: sp(18),
                                fontWeight: FontWeight.bold,
                              ),
                              StyledText("Lihat Semua",
                                size: sp(14),
                                color: ColorUtils.primary,
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: width(15)),
                        WrapHorizontalListView(
                          itemCount: data.data.length - HEADER_VIDEO_COUNT,
                          builder: (_, index) => ElibraryVideoItem(
                            data: data.data[index+HEADER_VIDEO_COUNT],
                            onSelected: onVideoSelected,
                          ),
                        ),
                      ],
                    );
                  }
                }

                return Container();
              },
            ),

            SizedBox(height: width(30)),

            RequestWrapperWidget<VideoResponse>(
              requestWrapper: _videoWrapper,
              placeholder: (_) => Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: width(10)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Shimmer.fromColors(
                          child: Container(
                            width: width(100),
                            height: sp(18),
                            color: Colors.grey[200],
                          ),
                          baseColor: Colors.grey[200],
                          highlightColor: Colors.white,
                        ),
                        Shimmer.fromColors(
                          child: Container(
                            width: width(80),
                            height: sp(14),
                            color: Colors.grey[200],
                          ),
                          baseColor: Colors.grey[200],
                          highlightColor: Colors.white,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: width(5)),
                  ListView.builder(
                    shrinkWrap: true,
                    itemCount: 5,
                    physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (_, __) => ShimmerElibraryVerticalVideoItem(),
                  )
                ],
              ),
              builder: (_, data){
                if(data.data.length > HEADER_VIDEO_COUNT + POPULAR_VIDEO_COUNT){
                  if(data.data.length > HEADER_VIDEO_COUNT + POPULAR_VIDEO_COUNT + VERTICAL_VIDEO_COUNT){
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: width(10)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              StyledText("Lainnya",
                                size: sp(18),
                                fontWeight: FontWeight.bold,
                              ),
                              StyledText("Lihat Semua",
                                size: sp(14),
                                color: ColorUtils.primary,
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: width(5)),
                        ListView.builder(
                          shrinkWrap: true,
                          itemCount: VERTICAL_VIDEO_COUNT,
                          physics: NeverScrollableScrollPhysics(),
                          itemBuilder: (_, index) => ElibraryVerticalVideoItem(
                            data: data.data[index + HEADER_VIDEO_COUNT + POPULAR_VIDEO_COUNT],
                            onSelected: onVideoSelected,
                          ),
                        )
                      ],
                    );
                  }else {
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: width(10)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              StyledText("Lainnya",
                                size: sp(18),
                                fontWeight: FontWeight.bold,
                              ),
                              StyledText("Lihat Semua",
                                size: sp(14),
                                color: ColorUtils.primary,
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: width(5)),
                        ListView.builder(
                          shrinkWrap: true,
                          itemCount: data.data.length - (HEADER_VIDEO_COUNT + POPULAR_VIDEO_COUNT),
                          physics: NeverScrollableScrollPhysics(),
                          itemBuilder: (_, index) => ElibraryVerticalVideoItem(
                            data: data.data[index + HEADER_VIDEO_COUNT + POPULAR_VIDEO_COUNT],
                            onSelected: onVideoSelected,
                          ),
                        )
                      ],
                    );
                  }
                }

                return Container();
              },
            )
          ],
        ),
      ),
    );
  }
}
