import 'package:halokes_siswa/ancestor/BasePresenter.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/network/response/management/VideoResponse.dart';
import 'package:mcnmr_request_wrapper/RequestWrapper.dart';

class ElibraryVideoPresenter extends BasePresenter{
  static const REQUEST_GET_EBOOK = 0;

  ElibraryVideoPresenter(BaseState state) : super(state);

  void executeGetVideo(RequestWrapper<VideoResponse> wrapper){
    wrapper.doRequest();
    managementRepo.executeGetVideo(REQUEST_GET_EBOOK, wrapper.finishRequest);
  }
}