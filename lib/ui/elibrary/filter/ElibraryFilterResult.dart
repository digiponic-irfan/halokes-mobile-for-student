import 'package:flutter/cupertino.dart';
import 'package:halokes_siswa/network/response/management/DepartmentResponse.dart';
import 'package:halokes_siswa/network/response/management/SubjectResponse.dart';

class ElibraryFilterResult {
  final int grade;
  final DepartmentData department;
  final SubjectData subject;

  ElibraryFilterResult({@required this.grade, @required this.department, @required this.subject});
}