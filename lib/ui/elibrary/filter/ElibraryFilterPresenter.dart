import 'package:halokes_siswa/ancestor/BasePresenter.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/network/response/management/DepartmentResponse.dart';
import 'package:halokes_siswa/network/response/management/SubjectResponse.dart';
import 'package:mcnmr_request_wrapper/RequestWrapper.dart';

class ElibraryFilterPresenter extends BasePresenter{
  static const REQUEST_GET_SUBJECT = 0;
  static const REQUEST_GET_DEPARTMENT = 1;

  ElibraryFilterPresenter(BaseState state) : super(state);

  void executeGetSubject(RequestWrapper<List<SubjectData>> wrapper){
    wrapper.doRequest();
    managementRepo.executeGetSubject(REQUEST_GET_SUBJECT, (r){
      var list = <SubjectData>[];
      list.add(SubjectData.all());
      list.addAll(r.data);

      wrapper.finishRequest(list);
    });
  }

  void executeGetDepartment(RequestWrapper<List<DepartmentData>> wrapper){
    wrapper.doRequest();
    managementRepo.executeGetDepartment(REQUEST_GET_DEPARTMENT, (r){
      var list = <DepartmentData>[];
      list.add(DepartmentData.all());
      list.addAll(r.data);

      wrapper.finishRequest(list);
    });
  }
}