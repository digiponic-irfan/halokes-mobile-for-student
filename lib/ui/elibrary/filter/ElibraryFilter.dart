import 'package:flutter/material.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/custom/view/chip/Chip.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/extension/BaseStateExt.dart';
import 'package:halokes_siswa/network/response/management/DepartmentResponse.dart';
import 'package:halokes_siswa/network/response/management/SubjectResponse.dart';
import 'package:halokes_siswa/ui/elibrary/filter/ElibraryFilterPresenter.dart';
import 'package:halokes_siswa/ui/elibrary/filter/ElibraryFilterResult.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';
import 'package:mcnmr_request_wrapper/RequestWrapper.dart';
import 'package:mcnmr_request_wrapper/RequestWrapperWidget.dart';

class ElibraryFilter extends StatefulWidget {
  @override
  _ElibraryFilterState createState() => _ElibraryFilterState();
}

class _ElibraryFilterState extends BaseState<ElibraryFilter>{
  ElibraryFilterPresenter _presenter;
  RequestWrapper<List<SubjectData>> _subjectWrapper = RequestWrapper();
  RequestWrapper<List<DepartmentData>> _departmentWrapper = RequestWrapper();

  var _grades = {
    "Semua" : 0,
    "I" : 1,
    "II" : 2,
    "III" : 3,
    "IV" : 4,
    "V" : 5,
    "VI" : 6,
    "VII" : 7,
    "VIII" : 8,
    "IX" : 9,
    "X" : 10,
    "XI" : 11,
    "XII" : 12
  };

  String _selectedGrades = "Semua";
  DepartmentData _selectedDepartment;
  SubjectData _selectedSubject = SubjectData.all();

  bool _showDepartment = false;

  @override
  void initState() {
    super.initState();
    _presenter = ElibraryFilterPresenter(this);
  }

  @override
  void afterWidgetBuilt() {
    _presenter.executeGetSubject(_subjectWrapper);
    _presenter.executeGetDepartment(_departmentWrapper);
  }

  @override
  void shouldHideLoading(int typeRequest) {}

  @override
  void shouldShowLoading(int typeRequest) {}

  @override
  void onGradeSelected(String data){
    if(_grades[data] > 9){
      setState(() {
       _selectedGrades = data;

       _showDepartment = true;
      });
    }else {
      if(_selectedDepartment != null){
        setState((){
          _selectedGrades = data;
          _selectedDepartment = null;
          _selectedSubject = SubjectData.all();

          _showDepartment = false;
        });
      }else {
        setState((){
          _selectedGrades = data;

          _showDepartment = false;
        });
      }
    }
  }

  @override
  void onDepartmentSelected(DepartmentData data) {
    setState(() {
      _selectedSubject = null;
      _selectedDepartment = data;
    });
  }

  @override
  void onSubjectSelected(SubjectData data) {
    setState(() {
      _selectedDepartment = null;
      _selectedSubject = data;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black.withOpacity(0.5),
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(width(10)),
            bottomRight: Radius.circular(width(10)),
          )
        ),
        child: SingleChildScrollView(
          padding: EdgeInsets.symmetric(
            vertical: width(40),
            horizontal: width(20),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              StyledText("Kelas",
                size: sp(14),
                fontWeight: FontWeight.w600,
              ),
              SizedBox(height: width(20)),
              Wrap(
                runSpacing: 10,
                spacing: 10,
                children: _grades.map((key, value) =>
                    MapEntry(key, ChipItem<String>(item: key,
                      onSelected: onGradeSelected,
                      isSelected: (it) => _selectedGrades == it,
                    )))
                    .values.toList()
              ),
              SizedBox(height: width(20)),
              StyledText("Mata Pelajaran",
                size: sp(14),
                fontWeight: FontWeight.w600,
              ),
              SizedBox(height: width(20)),
              RequestWrapperWidget<List<SubjectData>>(
                requestWrapper: _subjectWrapper,
                placeholder: (_) => Wrap(
                  runSpacing: 10,
                  spacing: 10,
                  children: <Widget>[
                    ShimmerChip(),
                    ShimmerChip(),
                    ShimmerChip(),
                    ShimmerChip(),
                    ShimmerChip(),
                    ShimmerChip(),
                  ],
                ),
                builder: (_, data) => Wrap(
                  runSpacing: 10,
                  spacing: 10,
                  children: data.map((it) =>
                      ChipItem<SubjectData>(item: it,
                        onSelected: onSubjectSelected,
                        isSelected: (it) => _selectedSubject == it,
                      )
                  ).toList()
                ),
              ),

              Visibility(
                visible: _showDepartment,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    SizedBox(height: width(20)),
                    StyledText("Jurusan (SMK)",
                      size: sp(14),
                      fontWeight: FontWeight.w600,
                    ),
                    SizedBox(height: width(20)),
                    RequestWrapperWidget<List<DepartmentData>>(
                      requestWrapper: _departmentWrapper,
                      placeholder: (_) => Wrap(
                        runSpacing: 10,
                        spacing: 10,
                        children: <Widget>[
                          ShimmerChip(),
                          ShimmerChip(),
                          ShimmerChip(),
                          ShimmerChip(),
                          ShimmerChip(),
                          ShimmerChip(),
                        ],
                      ),
                      builder: (_, data) => Wrap(
                          runSpacing: 10,
                          spacing: 10,
                          children: data.map((it) =>
                              ChipItem<DepartmentData>(item: it,
                                onSelected: onDepartmentSelected,
                                isSelected: (it) => _selectedDepartment == it,
                              )
                          ).toList()
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(height: width(50)),

              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  MaterialButton(
                    onPressed: () => finish(),
                    color: Colors.white,
                    minWidth: width(150),
                    height: width(40),
                    shape: RoundedRectangleBorder(
                      side: BorderSide(color: ColorUtils.primary),
                      borderRadius: BorderRadius.circular(width(40)),
                    ),
                    child: StyledText("Batal",
                      size: sp(16),
                      color: ColorUtils.primary,
                    ),
                  ),
                  SizedBox(width: width(30)),
                  MaterialButton(
                    onPressed: () => finish(result: ElibraryFilterResult(
                      grade: _grades[_selectedGrades],
                      department: _selectedDepartment,
                      subject: _selectedSubject,
                    )),
                    color: ColorUtils.primary,
                    minWidth: width(150),
                    height: width(40),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(width(40)),
                    ),
                    child: StyledText("Terapkan",
                      size: sp(16),
                      color: Colors.white,
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      )
    );
  }
}
