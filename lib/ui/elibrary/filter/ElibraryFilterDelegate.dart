import 'package:halokes_siswa/network/response/management/DepartmentResponse.dart';
import 'package:halokes_siswa/network/response/management/SubjectResponse.dart';

class ElibraryFilterDelegate {
  void onGradeSelected(String data){}
  void onSubjectSelected(SubjectData data){}
  void onDepartmentSelected(DepartmentData data){}
}