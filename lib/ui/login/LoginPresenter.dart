import 'dart:convert';

import 'package:fluttertoast/fluttertoast.dart';
import 'package:halokes_siswa/ancestor/BasePresenter.dart';
import 'package:halokes_siswa/ancestor/BaseResponse.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/network/response/management/SchoolResponse.dart';
import 'package:halokes_siswa/ui/login/LoginDelegate.dart';
import 'package:mcnmr_request_wrapper/RequestWrapper.dart';

class LoginPresenter extends BasePresenter {
  static const REQUEST_GET_SCHOOL = 0;
  static const REQUEST_LOGIN = 1;

  final LoginDelegate _delegate;

  LoginPresenter(BaseState state, this._delegate) : super(state);

  void executeGetSchool(RequestWrapper<SchoolResponse> wrapper){
    wrapper.doRequest();
    managementRepo.executeGetSchool(REQUEST_GET_SCHOOL, (r) => wrapper.finishRequest(r));
  }

  void executeLogin(SchoolData school, String username, String password){
    if(school == null){
      Fluttertoast.showToast(msg: "Sekolah belum dipilih");
      return;
    }

    if(username == ""){
      Fluttertoast.showToast(msg: "Username / Email masih kosong");
      return;
    }

    if(password == ""){
      Fluttertoast.showToast(msg: "Password masih kosong");
    }

    var params = {
      "username" : username,
      "password" : password,
    };

    generalRepo.executeLogin(REQUEST_LOGIN, school, params, _delegate.onSuccessLogin, (r){
      var json = jsonDecode(r.data);
      var role = json["data"]["main_role"] ?? "";

      if(role != "siswa"){
        return new ResponseException("Hanya akun siswa yang dapat mengakses Halokes Siswa");
      }

      return null;
    });
  }
}