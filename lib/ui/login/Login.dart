import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/extension/BaseStateExt.dart';
import 'package:halokes_siswa/external_plugin/autocomplete_textfield/autocomplete_textfield.dart';
import 'package:halokes_siswa/main.dart';
import 'package:halokes_siswa/network/response/management/SchoolResponse.dart';
import 'package:halokes_siswa/network/response/general/UserResponse.dart';
import 'package:halokes_siswa/preference/AppPreference.dart';
import 'package:halokes_siswa/ui/login/LoginDelegate.dart';
import 'package:halokes_siswa/ui/login/LoginPresenter.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';
import 'package:halokes_siswa/utils/ImageUtils.dart';
import 'package:mcnmr_common_ext/FutureDelayed.dart';
import 'package:mcnmr_request_wrapper/RequestWrapper.dart';
import 'package:mcnmr_request_wrapper/RequestWrapperWidget.dart';
import 'package:mcnmr_result_object_observer/Holder.dart';
import 'package:mcnmr_result_object_observer/ResultObserverBuilder.dart';
import 'package:shimmer/shimmer.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends BaseState<Login> implements LoginDelegate{
  LoginPresenter _presenter;
  RequestWrapper<SchoolResponse> _schoolWrapper = RequestWrapper();

  TextEditingController _schoolController = TextEditingController();
  FocusNode _schoolFocus = FocusNode();
  FocusNode _usernameFocus = FocusNode();
  FocusNode _passwordFocus = FocusNode();

  GlobalKey<AutoCompleteTextFieldState<SchoolData>> _autoCompleteKey = GlobalKey();

  Holder<SchoolData, bool> _schoolHolder = Holder((it) => it != null, initialResult: false);
  String _username = "";
  String _password = "";

  @override
  void initState() {
    super.initState();
    _presenter = LoginPresenter(this, this);
  }

  @override
  void afterWidgetBuilt() => _presenter.executeGetSchool(_schoolWrapper);

  @override
  void shouldShowLoading(int typeRequest) {
    if(typeRequest != LoginPresenter.REQUEST_GET_SCHOOL){
      super.shouldShowLoading(typeRequest);
    }
  }

  @override
  void shouldHideLoading(int typeRequest) {
    if(typeRequest != LoginPresenter.REQUEST_GET_SCHOOL){
      super.shouldHideLoading(typeRequest);
    }
  }

  @override
  void onRequestTimeOut(int typeRequest){
    if(typeRequest == LoginPresenter.REQUEST_GET_SCHOOL){
      delay(5000, () => _presenter.executeGetSchool(_schoolWrapper));
    }
  }

  @override
  void onNoConnection(int typeRequest){
    if(typeRequest == LoginPresenter.REQUEST_GET_SCHOOL){
      delay(5000, () => _presenter.executeGetSchool(_schoolWrapper));
    }
  }

  @override
  void onSuccessLogin(UserResponse response) async {
    await AppPreference.saveSchool(_schoolHolder.object);
    await AppPreference.saveUser(response.data);
    navigateTo(MyApp.ROUTE_HOME, singleTop: true);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              width: double.infinity,
              height: fullHeight(),
              child: Stack(
                children: <Widget>[
                  Container(
                    width: double.infinity,
                    height: width(230),
                    color: ColorUtils.primary,
                    child: Image.asset(ImageUtils.ic_logo),
                  ),
                  Container(
                    width: double.infinity,
                    margin: EdgeInsets.only(
                      top: width(210),
                    ),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(width(20)),
                        topLeft: Radius.circular(width(20)),
                      ),
                    ),
                    padding: EdgeInsets.all(width(20)),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        StyledText("MASUK",
                          size: sp(16),
                          fontWeight: FontWeight.w600,
                        ),
                        StyledText("Selamat datang, silahkan masuk dengan akun anda",
                          size: sp(14),
                          fontWeight: FontWeight.w600,
                          color: ColorUtils.grey4e4e,
                        ),
                        SizedBox(height: width(50)),
                        Container(
                          width: double.infinity,
                          height: width(50),
                          decoration: BoxDecoration(
                            color: ColorUtils.greyf1f1,
                            borderRadius: BorderRadius.circular(width(5)),
                            border: Border.all(color: ColorUtils.greydede),
                          ),
                          padding: EdgeInsets.symmetric(
                            horizontal: width(10),
                          ),
                          child: Row(
                            children: <Widget>[
                              Icon(Icons.school, color: ColorUtils.grey7070),
                              SizedBox(width: width(10)),
                              Expanded(
                                child: RequestWrapperWidget<SchoolResponse>(
                                  requestWrapper: _schoolWrapper,
                                  placeholder: (_) => Shimmer.fromColors(
                                    child: Container(
                                      width: double.infinity,
                                      height: double.infinity,
                                      color: Colors.grey[200],
                                    ),
                                    baseColor: Colors.grey[200],
                                    highlightColor: Colors.white,
                                  ),
                                  builder: (_, data) => ResultObserverBuilder(
                                    observer: _schoolHolder,
                                    builder: (_, isSelected) => Row(
                                      children: <Widget>[
                                        Expanded(
                                          child: AutoCompleteTextField<SchoolData>(
                                            itemSubmitted: (it){
                                              _schoolHolder.object = it;
                                              _schoolController.text = it.sekolahNama;
                                            },
                                            clearOnSubmit: false,
                                            controller: _schoolController,
                                            focusNode: _schoolFocus,
                                            itemFilter: (item, query) => item.sekolahNama.toLowerCase()
                                                .contains(query.toLowerCase()),
                                            suggestions: data.data,
                                            itemSorter: (a, b) => a.sekolahNama.compareTo(b.sekolahNama),
                                            key: _autoCompleteKey,
                                            enabled: !isSelected,
                                            itemBuilder: (_, suggestion) => Container(
                                              padding: EdgeInsets.all(width(8)),
                                              child: StyledText(suggestion.sekolahNama,
                                                size: sp(14),
                                              ),
                                            ),
                                            decoration: InputDecoration(
                                              contentPadding: EdgeInsets.all(0),
                                              border: InputBorder.none,
                                              hintText: "Cari Sekolah Anda...",
                                            ),
                                          ),
                                        ),
                                        isSelected ? IconButton(
                                          onPressed: (){
                                            _schoolHolder.object = null;
                                            _schoolController.text = "";
                                          },
                                          icon: Icon(Icons.close),
                                        ) : SizedBox(width: width(12)),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: width(20)),
                        Container(
                          width: double.infinity,
                          height: width(50),
                          decoration: BoxDecoration(
                            color: ColorUtils.greyf1f1,
                            borderRadius: BorderRadius.circular(width(5)),
                            border: Border.all(color: ColorUtils.greydede),
                          ),
                          padding: EdgeInsets.symmetric(
                            horizontal: width(10),
                          ),
                          child: Row(
                            children: <Widget>[
                              Icon(Icons.person_outline, color: ColorUtils.grey7070),
                              SizedBox(width: width(10)),
                              Expanded(
                                child: TextFormField(
                                  onChanged: (it) => _username = it,
                                  focusNode: _usernameFocus,
                                  keyboardType: TextInputType.emailAddress,
                                  textInputAction: TextInputAction.next,
                                  onFieldSubmitted: (_){
                                    _usernameFocus.unfocus();
                                    _passwordFocus.requestFocus();
                                  },
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: "Username / Email"
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: width(20)),
                        Container(
                          width: double.infinity,
                          height: width(50),
                          decoration: BoxDecoration(
                            color: ColorUtils.greyf1f1,
                            borderRadius: BorderRadius.circular(width(5)),
                            border: Border.all(color: ColorUtils.greydede),
                          ),
                          padding: EdgeInsets.symmetric(
                            horizontal: width(10),
                          ),
                          child: Row(
                            children: <Widget>[
                              Icon(Icons.lock_outline, color: ColorUtils.grey7070),
                              SizedBox(width: width(10)),
                              Expanded(
                                child: TextFormField(
                                  onChanged: (it) => _password = it,
                                  obscureText: true,
                                  keyboardType: TextInputType.visiblePassword,
                                  textInputAction: TextInputAction.done,
                                  onFieldSubmitted: (_) => _passwordFocus.unfocus(),
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: "Password",
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        Spacer(),
                        MaterialButton(
                          onPressed: () => _presenter.executeLogin(_schoolHolder.object, _username, _password),
                          color: ColorUtils.primary,
                          minWidth: double.infinity,
                          height: width(40),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(width(5)),
                          ),
                          child: StyledText("Masuk",
                            color: Colors.white,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

}
