import 'package:flutter/material.dart';
import 'package:halokes_siswa/ancestor/BaseLifecycleState.dart';
import 'package:halokes_siswa/main.dart';
import 'package:halokes_siswa/provider/ChatLastSeenProvider.dart';
import 'package:halokes_siswa/provider/TypingProvider.dart';
import 'package:halokes_siswa/ui/home/HomePresenter.dart';
import 'package:halokes_siswa/ui/home/fragment/akun/FragmentAkun.dart';
import 'package:halokes_siswa/ui/home/fragment/chat/FragmentChatList.dart';
import 'package:halokes_siswa/ui/home/fragment/explore/FragmentExplore.dart';
import 'package:halokes_siswa/ui/home/fragment/inbox/FragmentInbox.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';
import 'package:halokes_siswa/extension/BaseStateExt.dart';
import 'package:provider/provider.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends BaseLifecycleState<Home> {
  HomePresenter _presenter;

  int _currentIndex = 0;

  var _content = <Widget>[
    FragmentExplore(),
    FragmentInbox(),
    FragmentChatList(),
    FragmentAkun(),
  ];

  @override
  void initState() {
    super.initState();
    _presenter = HomePresenter(this);
  }

  @override
  void afterWidgetBuilt() => _presenter.connectToSocketServer(
    Provider.of<ChatLastSeenProvider>(context),
    Provider.of<TypingProvider>(context),
  );

  @override
  void onResume() => _presenter.goToForeground();

  @override
  void onPause() => _presenter.goToBackground();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () => navigateTo(MyApp.ROUTE_FORUM_LIST),
        tooltip: "Forum Tanya Jawab",
        backgroundColor: ColorUtils.primary,
        child: Icon(Icons.forum, color: Colors.white),
        heroTag: "Forum",
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        clipBehavior: Clip.antiAlias,
        child: BottomNavigationBar(
          selectedItemColor: ColorUtils.primary,
          unselectedItemColor: Colors.grey,
          selectedFontSize: width(10),
          unselectedFontSize: width(10),
          showUnselectedLabels: true,
          currentIndex: _currentIndex == 3 ? 4 : _currentIndex == 2 ? 3 : _currentIndex,
          onTap: (it){
            switch(it){
              case 0:
                setState(() => _currentIndex = it);
                break;
              case 1:
                setState(() => _currentIndex = it);
                break;
              case 2 :
                navigateTo(MyApp.ROUTE_FORUM_LIST);
                break;
              case 3:
                setState(() => _currentIndex = 2);
                break;
              case 4:
                setState(() => _currentIndex = 3);
                break;
            }
          },
          type: BottomNavigationBarType.fixed,
          items: [
            BottomNavigationBarItem(icon: Icon(Icons.explore), title: Text("Explore")),
            BottomNavigationBarItem(icon: Icon(Icons.mail_outline), title: Text("Inbox")),
            BottomNavigationBarItem(icon: Icon(Icons.forum), title: Text("Tanya Jawab")),
            BottomNavigationBarItem(icon: Icon(Icons.chat), title: Text("Chat")),
            BottomNavigationBarItem(icon: Icon(Icons.person_outline), title: Text("Akun")),
          ],
        ),
      ),
      body: SafeArea(
        top: false,
        child: IndexedStack(
          children: _content,
          index: _currentIndex,
        ),
      ),
    );
  }
}
