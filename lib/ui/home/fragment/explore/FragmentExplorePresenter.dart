import 'package:halokes_siswa/ancestor/BasePresenter.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/network/response/general/AnnouncementResponse.dart';
import 'package:halokes_siswa/network/response/management/SchoolResponse.dart';
import 'package:halokes_siswa/network/response/general/SemesterResponse.dart';
import 'package:halokes_siswa/network/response/general/UserResponse.dart';
import 'package:halokes_siswa/preference/AppPreference.dart';
import 'package:halokes_siswa/provider/SemesterProvider.dart';
import 'package:halokes_siswa/ui/home/fragment/explore/FragmentExploreDelegate.dart';
import 'package:mcnmr_request_wrapper/RequestWrapper.dart';

class FragmentExplorePresenter extends BasePresenter{
  static const REQUEST_GET_ANNOUNCEMENT = 0;
  static const REQUEST_GET_SEMESTER = 8;

  static const REQUEST_GET_ASSIGNMENT = 1;
  static const REQUEST_GET_EXTRACURRICULAR = 2;
  static const REQUEST_GET_ATTENDANCE = 3;
  static const REQUEST_GET_SCORE = 4;
  static const REQUEST_GET_CLASS = 5;
  static const REQUEST_GET_SCHEDULE = 6;
  static const REQUEST_GET_COUNSELING = 7;

  static const KATEGORI_BERITA_ARTICLE = "berita";
  static const KATEGORI_PENGUMUMAN = "pengumuman";

  final FragmentExploreDelegate _delegate;

  FragmentExplorePresenter(BaseState state, this._delegate) : super(state);

  void getSchool(RequestWrapper<SchoolData> wrapper) async {
    wrapper.doRequest();
    wrapper.finishRequest(await AppPreference.getSchool());
  }

  void getUser(RequestWrapper<UserData> wrapper) async {
    wrapper.doRequest();
    wrapper.finishRequest(await AppPreference.getUser());
  }

  void executeGetSemester(RequestWrapper<SemesterResponse> wrapper){
    wrapper.doRequest();
    generalRepo.executeGetSemester(REQUEST_GET_SEMESTER, (r) => wrapper.finishRequest(r));
  }

  void executeGetAnnouncementArticle(RequestWrapper<List<AnnouncementData>> announcementWrapper,
      RequestWrapper<List<AnnouncementData>> articleWrapper){
    announcementWrapper.doRequest();
    articleWrapper.doRequest();
    generalRepo.executeGetAnnouncement(REQUEST_GET_ANNOUNCEMENT, (r){

      var berita = List<AnnouncementData>();
      var pengumuman = List<AnnouncementData>();

      r.data.forEach((it) {
        if(it.kategori.toLowerCase() == KATEGORI_BERITA_ARTICLE){
          berita.add(it);
        }else{
          pengumuman.add(it);
        }
      });

      announcementWrapper.finishRequest(pengumuman);
      articleWrapper.finishRequest(berita);
    });
  }

  void executeGetAssignment(SemesterProvider provider) {
    generalRepo.executeGetAssignment(REQUEST_GET_ASSIGNMENT, provider.selectedSemester.id,
        _delegate.onSuccessGetAssignment);
  }

  void executeGetExtra(SemesterProvider provider) {
    generalRepo.executeGetExtra(REQUEST_GET_EXTRACURRICULAR, provider.selectedSemester.id,
        _delegate.onSuccessGetExtracurricular);
  }

  void executeGetAttendance(SemesterProvider provider) {
    generalRepo.executeGetAttendance(REQUEST_GET_ATTENDANCE, provider.selectedSemester.id,
        _delegate.onSuccessGetAttendance);
  }

  void executeGetScore(SemesterProvider provider) {
    generalRepo.executeGetScore(REQUEST_GET_SCORE, provider.selectedSemester.id,
        _delegate.onSuccessGetScore);
  }

  void executeGetCounseling() {
    generalRepo.executeGetCounseling(REQUEST_GET_COUNSELING, _delegate.onSuccessGetCounseling);
  }

  void executeGetClass() {
    generalRepo.executeGetClass(REQUEST_GET_CLASS, _delegate.onSuccessGetClass);
  }

  void executeGetSchedule() async {
    generalRepo.executeGetSchedule(REQUEST_GET_SCHEDULE, _delegate.onSuccessGetSchedule);
  }
}