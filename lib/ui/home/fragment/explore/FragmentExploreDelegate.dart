import 'package:halokes_siswa/network/response/general/AssignmentResponse.dart';
import 'package:halokes_siswa/network/response/general/AttendanceResponse.dart';
import 'package:halokes_siswa/network/response/general/ClassResponse.dart';
import 'package:halokes_siswa/network/response/general/CounselingResponse.dart';
import 'package:halokes_siswa/network/response/general/ExtraResponse.dart';
import 'package:halokes_siswa/network/response/general/ScheduleResponse.dart';
import 'package:halokes_siswa/network/response/general/ScoreResponse.dart';

class FragmentExploreDelegate {
  void onSuccessGetAttendance(AttendanceResponse data){}
  void onSuccessGetAssignment(AssignmentResponse data){}
  void onSuccessGetExtracurricular(ExtraResponse data){}
  void onSuccessGetScore(ScoreResponse data){}
  void onSuccessGetClass(ClassResponse data){}
  void onSuccessGetSchedule(ScheduleResponse data){}
  void onSuccessGetCounseling(CounselingResponse data){}
}