import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/extension/BaseStateExt.dart';
import 'package:halokes_siswa/main.dart';
import 'package:halokes_siswa/network/response/general/AnnouncementResponse.dart';
import 'package:halokes_siswa/network/response/general/AssignmentResponse.dart';
import 'package:halokes_siswa/network/response/general/AttendanceResponse.dart';
import 'package:halokes_siswa/network/response/general/ClassResponse.dart';
import 'package:halokes_siswa/network/response/general/CounselingResponse.dart';
import 'package:halokes_siswa/network/response/general/ExtraResponse.dart';
import 'package:halokes_siswa/network/response/general/ScheduleResponse.dart';
import 'package:halokes_siswa/network/response/management/SchoolResponse.dart';
import 'package:halokes_siswa/network/response/general/ScoreResponse.dart';
import 'package:halokes_siswa/network/response/general/SemesterResponse.dart';
import 'package:halokes_siswa/network/response/general/UserResponse.dart';
import 'package:halokes_siswa/provider/SemesterProvider.dart';
import 'package:halokes_siswa/ui/home/fragment/explore/FragmentExploreDelegate.dart';
import 'package:halokes_siswa/ui/home/fragment/explore/FragmentExplorePresenter.dart';
import 'package:halokes_siswa/ui/home/fragment/explore/adapter/AnnouncementArticleAdapter.dart';
import 'package:halokes_siswa/ui/home/fragment/explore/adapter/MenuAdapter.dart';
import 'package:halokes_siswa/utils/ImageUtils.dart';
import 'package:mcnmr_request_wrapper/RequestWrapper.dart';
import 'package:mcnmr_request_wrapper/RequestWrapperWidget.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

class FragmentExplore extends StatefulWidget {
  @override
  _FragmentExploreState createState() => _FragmentExploreState();
}

class MenuItems {
  final String text;
  final String image;
  final Function action;

  MenuItems({@required this.text, @required  this.image, @required  this.action});
}

class _FragmentExploreState extends BaseState<FragmentExplore> implements FragmentExploreDelegate{
  FragmentExplorePresenter _presenter;
  SemesterProvider _semesterProvider;

  RequestWrapper<List<AnnouncementData>> _announcementWrapper = RequestWrapper();
  RequestWrapper<List<AnnouncementData>> _articleWrapper = RequestWrapper();
  RequestWrapper<SemesterResponse> _semesterWrapper = RequestWrapper();
  RequestWrapper<UserData> _userWrapper = RequestWrapper();
  RequestWrapper<SchoolData> _schoolWrapper = RequestWrapper();

  List<MenuItems> _items;

  @override
  void initState() {
    super.initState();
    _presenter = FragmentExplorePresenter(this, this);

    _semesterWrapper.subscribeOnFinishedAndNonNull((it){
      it.semester.forEach((its){
        if(its.smtStatus == "1"){
          _semesterProvider.currentlyActiveSemester(its);
        }
      });
    });

    _items = [
      MenuItems(text: "E-Library",
        image: ImageUtils.ic_menu_elibrary,
        action: () => navigateTo(MyApp.ROUTE_ELIB),
      ),
      MenuItems(text: "Tugas",
        image: ImageUtils.ic_menu_tugas,
        action: () => _presenter.executeGetAssignment(_semesterProvider),
      ),
      MenuItems(text: "Ekskul",
        image: ImageUtils.ic_menu_ekskul,
        action: () => _presenter.executeGetExtra(_semesterProvider),
      ),
      MenuItems(text: "Absensi",
        image: ImageUtils.ic_menu_absensi,
        action: () => _presenter.executeGetAttendance(_semesterProvider),
      ),
      MenuItems(text: "Nilai",
        image: ImageUtils.ic_menu_nilai,
        action: () => _presenter.executeGetScore(_semesterProvider),
      ),
      MenuItems(text: "Konseling",
        image: ImageUtils.ic_menu_konseling,
        action: _presenter.executeGetCounseling,
      ),
      MenuItems(text: "Kelas",
        image: ImageUtils.ic_menu_kelas,
        action: _presenter.executeGetClass,
      ),
      MenuItems(text: "Jadwal",
        image: ImageUtils.ic_menu_jadwal,
        action: _presenter.executeGetSchedule,
      ),
    ];
  }

  @override
  void afterWidgetBuilt() {
    _semesterProvider = Provider.of<SemesterProvider>(context);

    _presenter.getUser(_userWrapper);
    _presenter.getSchool(_schoolWrapper);
    _presenter.executeGetAnnouncementArticle(_announcementWrapper, _articleWrapper);
    _presenter.executeGetSemester(_semesterWrapper);
  }

  @override
  void shouldShowLoading(int typeRequest) {
    if(typeRequest == FragmentExplorePresenter.REQUEST_GET_ANNOUNCEMENT){

    }else if(typeRequest == FragmentExplorePresenter.REQUEST_GET_SEMESTER){

    }else {
      super.shouldShowLoading(typeRequest);
    }
  }

  @override
  void shouldHideLoading(int typeRequest) {
    if(typeRequest == FragmentExplorePresenter.REQUEST_GET_ANNOUNCEMENT){

    }else if(typeRequest == FragmentExplorePresenter.REQUEST_GET_SEMESTER){

    }else {
      super.shouldHideLoading(typeRequest);
    }
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Stack(
            overflow: Overflow.visible,
            children: <Widget>[
              Container(
                width: double.infinity,
                height: width(280),
                child: SvgPicture.asset(ImageUtils.bg_home_curve,
                  width: double.infinity,
                  height: double.infinity,
                  fit: BoxFit.fill,
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: width(50)),
                padding: EdgeInsets.symmetric(
                  horizontal: width(20),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Container(
                          width: width(50),
                          height: width(50),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            shape: BoxShape.circle,
                          ),
                        ),
                        SizedBox(width: width(15)),
                        Stack(
                          alignment: Alignment.centerLeft,
                          children: <Widget>[
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                RequestWrapperWidget<SchoolData>(
                                  requestWrapper: _schoolWrapper,
                                  placeholder: (_) => StyledText("",
                                    color: Colors.white,
                                    size: sp(14),
                                    fontWeight: FontWeight.w600,
                                  ),
                                  builder: (_, school) => StyledText(school.sekolahNama,
                                    color: Colors.white,
                                    size: sp(14),
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                                Consumer<SemesterProvider>(
                                  builder: (_, provider, __) => RequestWrapperWidget<SemesterResponse>(
                                    requestWrapper: _semesterWrapper,
                                    placeholder: (_) => Column(
                                      children: <Widget>[
                                        SizedBox(height: width(8)),
                                        Shimmer.fromColors(
                                          child: Container(
                                            width: width(75),
                                            height: sp(12),
                                            color: Colors.grey[200],
                                          ),
                                          baseColor: Colors.grey[200],
                                          highlightColor: Colors.white,
                                        ),
                                      ],
                                    ),
                                    builder: (_, __) => Row(
                                      children: <Widget>[
                                        StyledText(provider.selectedSemester.toString(),
                                          color: Colors.white,
                                          size: sp(12),
                                        ),
                                        SizedBox(width: width(5)),
                                        Icon(Icons.arrow_drop_down, color: Colors.white)
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Container(
                              width: width(150),
                              height: width(50),
                              child: RequestWrapperWidget<SemesterResponse>(
                                requestWrapper: _semesterWrapper,
                                placeholder: (_) => SizedBox(),
                                builder: (_, data) => DropdownButton<SemesterData>(
                                  items: data.semester.map((it) =>
                                      DropdownMenuItem<SemesterData>(
                                        child: StyledText(it.toString(), size: sp(12)),
                                        value: it,
                                      )).toList(),
                                  iconEnabledColor: Colors.transparent,
                                  iconDisabledColor: Colors.transparent,
                                  underline: SizedBox(),
                                  onChanged: _semesterProvider.changeSemester,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    SizedBox(height: width(20)),
                    StyledText("Hallo",
                      color: Colors.white,
                      size: sp(22),
                      fontWeight: FontWeight.bold,
                    ),
                    RequestWrapperWidget<UserData>(
                      requestWrapper: _userWrapper,
                      placeholder: (_) => StyledText("",
                        color: Colors.white,
                        size: sp(14),
                      ),
                      builder: (_, user) => StyledText(user.namaSiswa,
                        color: Colors.white,
                        size: sp(14),
                      ),
                    ),
                    SizedBox(height: width(20)),
                    Card(
                      elevation: 6,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(width(10)),
                      ),
                      child: Container(
                        width: double.infinity,
                        padding: EdgeInsets.all(width(20)),
                        child: Center(
                          child: Wrap(
                            spacing: width(15),
                            runSpacing: width(20),
                            children: _items.map((it) => MenuItem(item: it)).toList(),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: width(30)),
                    StyledText("Pengumuman",
                      size: sp(16),
                      fontWeight: FontWeight.w600,
                    ),
                    RequestWrapperWidget<List<AnnouncementData>>(
                      requestWrapper: _announcementWrapper,
                      placeholder: (_) => ListView.builder(
                        itemCount: 1,
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemBuilder: (_, index) => ShimmerAnnouncementArticle(),
                      ),
                      builder: (_, data) => ListView.builder(
                        itemCount: data.length > 3 ? 3 : data.length,
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemBuilder: (_, index) => AnnouncementArticleItem(data: data[index]),
                      ),
                    ),
                    SizedBox(height: width(20)),
                    StyledText("Berita",
                      size: sp(16),
                      fontWeight: FontWeight.w600,
                    ),
                    RequestWrapperWidget<List<AnnouncementData>>(
                      requestWrapper: _articleWrapper,
                      placeholder: (_) => ListView.builder(
                        itemCount: 1,
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemBuilder: (_, index) => ShimmerAnnouncementArticle(),
                      ),
                      builder: (_, data) => ListView.builder(
                        itemCount: data.length,
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemBuilder: (_, index) => AnnouncementArticleItem(data: data[index]),
                      ),
                    ),
                    SizedBox(height: width(20)),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  void onSuccessGetAssignment(AssignmentResponse data) =>
      navigateTo(MyApp.ROUTE_ASSIGNMENT, arguments: data);

  @override
  void onSuccessGetExtracurricular(ExtraResponse data) =>
      navigateTo(MyApp.ROUTE_EXTRACURRICULAR, arguments: data);

  @override
  void onSuccessGetAttendance(AttendanceResponse data) =>
      navigateTo(MyApp.ROUTE_ATTENDANCE, arguments: data);

  @override
  void onSuccessGetScore(ScoreResponse data) =>
      navigateTo(MyApp.ROUTE_SCORE, arguments: data);

  @override
  void onSuccessGetClass(ClassResponse data) =>
      navigateTo(MyApp.ROUTE_CLASS, arguments: data);

  @override
  void onSuccessGetSchedule(ScheduleResponse data) =>
      navigateTo(MyApp.ROUTE_SCHEDULE, arguments: data);

  @override
  void onSuccessGetCounseling(CounselingResponse data) =>
      navigateTo(MyApp.ROUTE_COUNSELING, arguments: data);

}
