import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/extension/Size.dart';
import 'package:halokes_siswa/ui/home/fragment/explore/FragmentExplore.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';

class MenuItem extends StatelessWidget {
  final MenuItems item;

  MenuItem({@required this.item});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: BorderRadius.circular(adaptiveWidth(context, 20)),
      onTap: item.action,
      child: Padding(
        padding: EdgeInsets.symmetric(
          vertical: adaptiveWidth(context, 10),
          horizontal: adaptiveWidth(context, 20),
        ),
        child: Column(
          children: <Widget>[
            SvgPicture.asset(item.image,
              width: adaptiveWidth(context, 50),
              height: adaptiveWidth(context, 50),
            ),
            SizedBox(height: adaptiveWidth(context, 10)),
            StyledText(item.text,
              color: ColorUtils.primary,
              size: sp(context, 14),
            ),
          ],
        ),
      ),
    );
  }
}
