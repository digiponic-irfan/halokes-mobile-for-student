import 'package:flutter/material.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/extension/Size.dart';
import 'package:halokes_siswa/external_plugin/cached_network_image/cached_network_image.dart';
import 'package:halokes_siswa/network/response/general/AnnouncementResponse.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';
import 'package:halokes_siswa/utils/ImageUtils.dart';
import 'package:shimmer/shimmer.dart';

class AnnouncementArticleItem extends StatelessWidget {
  final AnnouncementData data;

  AnnouncementArticleItem({@required this.data}) : assert(data != null);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Card(
          elevation: 2,
          color: Colors.white,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          clipBehavior: Clip.antiAlias,
          child: Stack(
            children: <Widget>[
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    CachedNetworkImage(
                      imageUrl: data.gambarBerita,
                      placeholder: (_, __) => Shimmer.fromColors(
                        child: Container(
                          width: double.infinity,
                          height: adaptiveWidth(context, 150),
                          color: Colors.grey[200],
                        ),
                        baseColor: Colors.grey[200],
                        highlightColor: Colors.white,
                      ),
                      errorWidget: (_, __, ___) => Image.asset(ImageUtils.image_not_found,
                        width: double.infinity,
                        height: adaptiveWidth(context, 150),
                        fit: BoxFit.cover,
                      ),
                      imageBuilder: (_, provider) => Container(
                        width: double.infinity,
                        height: adaptiveWidth(context, 150),
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: provider,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(adaptiveWidth(context, 15)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          StyledText(data.judulBerita,
                            size: sp(context, 14),
                            fontWeight: FontWeight.w600,
                          ),
                          StyledText(data.tanggal,
                            size: sp(context, 12),
                            color: ColorUtils.grey9999,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Positioned.fill(
                child: Material(
                  color: Colors.transparent,
                  child: InkWell(
                    onTap: (){},
                  ),
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: adaptiveWidth(context, 10)),
      ],
    );
  }
}

class ShimmerAnnouncementArticle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Card(
          elevation: 2,
          color: Colors.white,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          clipBehavior: Clip.antiAlias,
          child: Shimmer.fromColors(
            child: Container(
              width: double.infinity,
              height: adaptiveWidth(context, 180),
              color: Colors.grey[200],
            ),
            baseColor: Colors.grey[200],
            highlightColor: Colors.white,
          ),
        ),
        SizedBox(height: adaptiveWidth(context, 10)),
      ],
    );
  }
}

