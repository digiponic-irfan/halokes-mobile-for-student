import 'package:dio/dio.dart';
import 'package:halokes_siswa/ancestor/BasePresenter.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/network/response/general/SemesterResponse.dart';
import 'package:halokes_siswa/network/response/general/StudentResponse.dart';
import 'package:halokes_siswa/network/response/general/UserResponse.dart';
import 'package:halokes_siswa/preference/AppPreference.dart';
import 'package:halokes_siswa/ui/home/fragment/akun/FragmentAkunDelegate.dart';
import 'package:mcnmr_request_wrapper/RequestWrapper.dart';

class FragmentAkunPresenter extends BasePresenter {
  static const REQUEST_GET_BIO = 0;
  static const REQUEST_GET_PAYMENT = 1;

  CancelToken _bioCancelToken;

  FragmentAkunDelegate _delegate;

  FragmentAkunPresenter(BaseState state, this._delegate) : super(state);

  void getAccount(RequestWrapper<UserData> wrapper) async {
    wrapper.doRequest();
    wrapper.finishRequest(await AppPreference.getUser());
  }

  void executeGetBio(RequestWrapper<StudentResponse> wrapper, SemesterData semester) {
    if(_bioCancelToken != null){
      _bioCancelToken.cancel();
    }
    _bioCancelToken = CancelToken();

    wrapper.doRequest();

    generalRepo.executeGetStudentBio(REQUEST_GET_BIO, semester.id,
            (r) => wrapper.finishRequest(r), cancelToken: _bioCancelToken);
  }

  void executeGeyPayment() {
    generalRepo.executeGetTanggunganKeuangan(REQUEST_GET_PAYMENT, _delegate.onSuccessGetTanggungan);
  }

  Future<void> clearChatHistory() async {
    await database.clearChat();
  }
}