import 'dart:io';

import 'package:halokes_siswa/external_plugin/cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/custom/dialog/AndroidOptionDialog.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/extension/BaseStateExt.dart';
import 'package:halokes_siswa/main.dart';
import 'package:halokes_siswa/network/response/general/FinanceResponse.dart';
import 'package:halokes_siswa/network/response/general/StudentResponse.dart';
import 'package:halokes_siswa/network/response/general/UserResponse.dart';
import 'package:halokes_siswa/preference/AppPreference.dart';
import 'package:halokes_siswa/provider/SemesterProvider.dart';
import 'package:halokes_siswa/ui/home/fragment/akun/FragmentAkunDelegate.dart';
import 'package:halokes_siswa/ui/home/fragment/akun/FragmentAkunPresenter.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';
import 'package:halokes_siswa/utils/ImageUtils.dart';
import 'package:mcnmr_common_ext/FutureDelayed.dart';
import 'package:mcnmr_request_wrapper/RequestWrapper.dart';
import 'package:mcnmr_request_wrapper/RequestWrapperWidget.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

class FragmentAkun extends StatefulWidget {
  @override
  _FragmentAkunState createState() => _FragmentAkunState();
}

class _FragmentAkunState extends BaseState<FragmentAkun> implements FragmentAkunDelegate{
  static const TAG_LOGOUT_DIALOG = 0x220;

  FragmentAkunPresenter _presenter;
  RequestWrapper<UserData> _userWrapper = RequestWrapper();
  RequestWrapper<StudentResponse> _bioWrapper = RequestWrapper();

  @override
  void initState() {
    super.initState();
    _presenter = FragmentAkunPresenter(this, this);
  }

  @override
  void afterWidgetBuilt() {
    Provider.of<SemesterProvider>(context).subscribe((it) => _presenter.executeGetBio(_bioWrapper, it));
    _presenter.getAccount(_userWrapper);
  }

  @override
  void shouldHideLoading(int typeRequest) {
    if(typeRequest != FragmentAkunPresenter.REQUEST_GET_BIO){
      super.shouldHideLoading(typeRequest);
    }
  }

  @override
  void shouldShowLoading(int typeRequest) {
    if(typeRequest != FragmentAkunPresenter.REQUEST_GET_BIO){
      super.shouldShowLoading(typeRequest);
    }
  }

  @override
  void onRequestTimeOut(int typeRequest) {
    if(typeRequest == FragmentAkunPresenter.REQUEST_GET_BIO){
      delay(5000, () => _presenter.executeGetBio(_bioWrapper,
          Provider.of<SemesterProvider>(context).selectedSemester));
    }else {
      super.onRequestTimeOut(typeRequest);
    }
  }

  @override
  void onNoConnection(int typeRequest) {
    if(typeRequest == FragmentAkunPresenter.REQUEST_GET_BIO){
      delay(5000, () => _presenter.executeGetBio(_bioWrapper,
          Provider.of<SemesterProvider>(context).selectedSemester));
    }else {
      super.onNoConnection(typeRequest);
    }
  }

  @override
  void onDialogResult(tag, result) async {
    if(tag == TAG_LOGOUT_DIALOG){
      if(result){
        await _presenter.clearChatHistory();
        await AppPreference.removeUser();
        await AppPreference.removeSchool();
        navigateTo(MyApp.ROUTE_LOGIN, singleTop: true);
      }
    }
  }

  @override
  void onSuccessGetTanggungan(FinanceResponse response) =>
      navigateTo(MyApp.ROUTE_PAYMENT, arguments: response);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorUtils.appbar,
      appBar: AppBar(
        backgroundColor: ColorUtils.appbar,
        title: StyledText("Akun",
          fontWeight: FontWeight.w600,
          color: ColorUtils.primary,
        ),
        elevation: 1,
        centerTitle: false,
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: SingleChildScrollView(
              padding: EdgeInsets.all(width(12)),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      RequestWrapperWidget<UserData>(
                        requestWrapper: _userWrapper,
                        placeholder: (_) => Shimmer.fromColors(
                          child: Container(
                            width: width(100),
                            height: width(100),
                            decoration: BoxDecoration(
                              color: Colors.grey[100],
                              shape: BoxShape.circle,
                            ),
                          ),
                          baseColor: Colors.grey[100],
                          highlightColor: Colors.white,
                        ),
                        builder: (_, data) => CachedNetworkImage(
                          imageUrl: data.foto,
                          imageBuilder: (ctx, provider) => Container(
                            width: width(100),
                            height: width(100),
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                image: provider,
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                          placeholder: (_, __) => Shimmer.fromColors(
                            child: Container(
                              width: width(100),
                              height: width(100),
                              decoration: BoxDecoration(
                                color: Colors.grey[100],
                                shape: BoxShape.circle,
                              ),
                            ),
                            baseColor: Colors.grey[200],
                            highlightColor: Colors.white,
                          ),
                          errorWidget: (_, __, ___) => Container(
                            width: width(100),
                            height: width(100),
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                image: AssetImage(ImageUtils.missing_avatar),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(width: width(30)),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          RequestWrapperWidget<UserData>(
                            requestWrapper: _userWrapper,
                            placeholder: (_) => StyledText("",
                              size: sp(14),
                            ),
                            builder: (_, user) => StyledText(user.namaSiswa,
                              size: sp(14),
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          RequestWrapperWidget<UserData>(
                            requestWrapper: _userWrapper,
                            placeholder: (_) => StyledText("",
                              size: sp(10),
                            ),
                            builder: (_, user) => StyledText("NISN : ${user.nis}",
                              size: sp(10),
                            ),
                          ),
                          SizedBox(height: width(5)),
                          MaterialButton(
                            onPressed: (){
                              if(_bioWrapper.status != RequestWrapper.STATUS_FINISHED){
                                alert(title: "Error",
                                    message: "Masih dalam proses pengambilan data, coba beberapa saat lagi",
                                    positiveTitle: "Tutup"
                                );
                                return;
                              }

                              navigateTo(MyApp.ROUTE_BIODATA, arguments: _bioWrapper.result.data);
                            },
                            padding: EdgeInsets.all(0),
                            color: ColorUtils.primary,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(width(20))
                            ),
                            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                            height: width(25),
                            minWidth: width(140),
                            child: StyledText("Biodata Siswa", size: sp(10), color: Colors.white),
                          )
                        ],
                      )
                    ],
                  ),
                  SizedBox(height: width(25)),
                  Card(
                    elevation: 2,
                    child: Container(
                      width: double.infinity,
                      padding: EdgeInsets.symmetric(vertical: width(16), horizontal: width(24)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          StyledText("Kegiatan hari ini", size: sp(12)),
                          SizedBox(height: width(5)),
                          StyledText("- Ulangan Matematika", size: sp(12), fontWeight: FontWeight.bold),
                          StyledText("- Ulangan Bahasa Indonesia", size: sp(12), fontWeight: FontWeight.bold),
                          StyledText("- Rapat OSIS", size: sp(12), fontWeight: FontWeight.bold),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: width(5)),
                  Card(
                    elevation: 2,
                    child: Container(
                      width: double.infinity,
                      padding: EdgeInsets.symmetric(vertical: width(20), horizontal: width(24)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Column(
                            children: <Widget>[
                              StyledText("Kehadiran", size: sp(12)),
                              Consumer<SemesterProvider>(
                                builder: (_, provider, __) => StyledText(provider.selectedSemester.toString(),
                                  size: sp(16),
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                          RequestWrapperWidget<StudentResponse>(
                            requestWrapper: _bioWrapper,
                            placeholder: (_) => Shimmer.fromColors(
                              child: Container(
                                width: width(50),
                                height: width(40),
                                color: Colors.grey[200],
                              ),
                              baseColor: Colors.grey[200],
                              highlightColor: Colors.white,
                            ),
                            builder: (_, data) => Column(
                              children: <Widget>[
                                StyledText("${data.data.totalKehadiran} %", size: sp(20), color: ColorUtils.primary, fontWeight: FontWeight.bold),
                                StyledText("Kehadiran", size: sp(12), color: ColorUtils.primary, fontWeight: FontWeight.bold),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: width(5)),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Card(
                          elevation: 2,
                          child: RequestWrapperWidget<StudentResponse>(
                            requestWrapper: _bioWrapper,
                            placeholder: (_) => Shimmer.fromColors(
                              child: Container(
                                width: double.infinity,
                                height: width(106),
                                color: Colors.grey[200],
                              ),
                              baseColor: Colors.grey[200],
                              highlightColor: Colors.white,
                            ),
                            builder: (_, data) => Container(
                              width: double.infinity,
                              padding: EdgeInsets.symmetric(vertical: width(20)),
                              child: Column(
                                children: <Widget>[
                                  StyledText(data.data.totalPrestasi.toString(), fontWeight: FontWeight.bold, size: sp(32), color: ColorUtils.primary),
                                  StyledText("POIN", size: sp(12), color: ColorUtils.primary),
                                  StyledText("Prestasi", size: sp(12)),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Card(
                          elevation: 2,
                          child: RequestWrapperWidget<StudentResponse>(
                            requestWrapper: _bioWrapper,
                            placeholder: (_) => Shimmer.fromColors(
                              child: Container(
                                width: double.infinity,
                                height: width(106),
                                color: Colors.grey[200],
                              ),
                              baseColor: Colors.grey[200],
                              highlightColor: Colors.white,
                            ),
                            builder: (_, data) => Container(
                              width: double.infinity,
                              padding: EdgeInsets.symmetric(vertical: width(20)),
                              child: Column(
                                children: <Widget>[
                                  StyledText(data.data.totalPelanggaran.toString(), fontWeight: FontWeight.bold, size: sp(32), color: ColorUtils.danger),
                                  StyledText("POIN", size: sp(12), color: ColorUtils.danger),
                                  StyledText("Pelanggaran", size: sp(12)),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Card(
                          elevation: 2,
                          child: RequestWrapperWidget<StudentResponse>(
                            requestWrapper: _bioWrapper,
                            placeholder: (_) => Shimmer.fromColors(
                              child: Container(
                                width: double.infinity,
                                height: width(106),
                                color: Colors.grey[200],
                              ),
                              baseColor: Colors.grey[200],
                              highlightColor: Colors.white,
                            ),
                            builder: (_, data) => Container(
                              width: double.infinity,
                              padding: EdgeInsets.symmetric(vertical: width(20)),
                              child: Column(
                                children: <Widget>[
                                  StyledText(data.data.totalTugas.toString(), fontWeight: FontWeight.bold, size: sp(32), color: ColorUtils.primary),
                                  StyledText("Tugas belum dikerjakan",
                                    size: sp(12),
                                    textAlign: TextAlign.center,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: width(15)),
                  Container(
                      width: double.infinity,
                      height: 1,
                      color: Colors.grey[300]
                  ),
                  SizedBox(height: width(15)),
                  Card(
                    elevation: 2,
                    child: Column(
                      children: <Widget>[
                        InkWell(
                          onTap: () => navigateTo(MyApp.ROUTE_UPDATE_PROFILE),
                          child: Padding(
                            padding: EdgeInsets.all(width(15)),
                            child: Row(
                              children: <Widget>[
                                Icon(Icons.person),
                                SizedBox(width: width(20)),
                                Expanded(
                                  child: StyledText("Ubah Profile",
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                                SizedBox(width: width(20)),
                                Icon(Icons.arrow_forward_ios),
                              ],
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: width(40)),
                          child: Container(
                            width: double.infinity,
                            height: 1,
                            color: Colors.grey[300],
                          ),
                        ),
                        InkWell(
                          onTap: () => _presenter.executeGeyPayment(),
                          child: Padding(
                            padding: EdgeInsets.all(width(15)),
                            child: Row(
                              children: <Widget>[
                                Icon(Icons.credit_card),
                                SizedBox(width: width(20)),
                                Expanded(
                                  child: StyledText("Info Pembayaran",
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                                SizedBox(width: width(20)),
                                Icon(Icons.arrow_forward_ios),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: width(15)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      StyledText("Versi pengembangan 0.1.0",
                        size: sp(12),
                        color: ColorUtils.grey9f9f,
                      ),
                      StyledText("#HalokesConnectingUs",
                        size: sp(12),
                        color: ColorUtils.grey9f9f,
                      )
                    ],
                  ),
                  SizedBox(height: width(15)),
                  MaterialButton(
                    onPressed: (){
                      if(Platform.isIOS){
                        openDialog(tag: TAG_LOGOUT_DIALOG,
                            context: context,
                            builder: (_) => CupertinoAlertDialog(
                              title: null,
                              content: Text("Apakah kamu yakin ingin keluar akun? Semua riwayat percakapan akan terhapus"),
                              actions: <Widget>[
                                CupertinoDialogAction(child: Text("Batal"), onPressed: () => finish()),
                                CupertinoDialogAction(child: Text("Keluar"), onPressed: () => finish(result: true)),
                              ],
                            )
                        );
                      }else {
                        openDialog(tag: TAG_LOGOUT_DIALOG,
                          context: context,
                          builder: (_) => AndroidOptionDialog(
                            message: "Apakah kamu yakin ingin keluar akun? Semua riwayat percakapan akan terhapus",
                            negativeTitle: "Keluar",
                            positiveTitle: "Batal",
                            onNegative: () => finish(result: true),
                          ),
                        );
                      }
                    },
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(width(10)),
                      side: BorderSide(color: ColorUtils.primary, width: 1),
                    ),
                    minWidth: double.infinity,
                    height: sp(40),
                    child: StyledText("Logout",
                      size: sp(14),
                      color: ColorUtils.primary,
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
