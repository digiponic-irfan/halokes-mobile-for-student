import 'package:flutter/material.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/ui/home/fragment/inbox/adapter/InboxAdapter.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';
import 'package:halokes_siswa/extension/BaseStateExt.dart';

class FragmentInbox extends StatefulWidget {
  @override
  _FragmentInboxState createState() => _FragmentInboxState();
}

class InboxItems{
  final String title;
  final String date;
  final String content;
  final String category;

  InboxItems({@required this.title, @required this.date, @required this.content, @required this.category});
}

class _FragmentInboxState extends BaseState<FragmentInbox> {

  var _items = <InboxItems>[
    InboxItems(title: "Tugas Bahasa Inggris: Tugas Membuat Clipping telah dinilai",
        date: "19 Des 2019",
        content: "Tugas Bahasa Inggris yang anda kumpulan tgl 17 Des 2019 telah diverifikasi "
            "oleh Rahmat Muliyadi ,S.Pd "
            "semoga semakin semangat mengerjakan tugas yang lainnya ya!",
        category: "Tugas - Konfirmasi"),
    InboxItems(title: "Penambahan Poin pelanggaran, [Membolos] : 12",
        date: "17 Des 2019",
        content: "Pada hari Senin, 16 Des 2019, Siswa Afni Hanifah Putri masuk tanpa alasan,"
            " sebab itu Siswa akan dikenai tambahan Poin Pelanggaran sebanyak 12",
        category: "Pelanggaran - Poin - Kehadiran"),
    InboxItems(title: "Penambahan Poin prestasi, [peserta olimpiade sains] : 24",
        date: "11 Des 2019",
        content: "Pada hari Selasa, 10 Des 2019, Siswa Afni Hanifah Putri mengikuti Olimpiade Sains Jawa Timur tahun 2019, "
            "sebab itu Siswa akan diberi tambahan Poin Prestasi sebanyak 24",
        category: "Prestasi - Poin - Kompetisi")
  ];

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        AppBar(
          title: StyledText("Inbox",
            fontWeight: FontWeight.w600,
            color: Colors.white,
          ),
          elevation: 1,
          centerTitle: false,
          actions: [
            PopupMenuButton<int>(
              icon: Icon(Icons.more_vert, color: ColorUtils.primary),
              itemBuilder: (_) => [
                PopupMenuItem<int>(
                  child: StyledText("Read All", size: sp(12)),
                  value: 0,
                ),
                PopupMenuItem<int>(
                  child: StyledText("Delete All", size: sp(12)),
                  value: 1,
                ),
              ],
            )
          ],
        ),
        Expanded(
          child: ListView.builder(
            itemCount: _items.length,
            itemBuilder: (_, position) => InboxItem(item: _items[position]),
          ),
        )
      ],
    );
  }
}
