import 'package:flutter/material.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/extension/Size.dart';
import 'package:halokes_siswa/ui/home/fragment/inbox/FragmentInbox.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';

class InboxItem extends StatelessWidget {

  final InboxItems item;

  InboxItem({@required this.item});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){},
      highlightColor: Colors.transparent,
      splashColor: ColorUtils.primary.withOpacity(0.5),
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(12),
            child: Row(
              children: <Widget>[
                /**SvgPicture.asset(ImageUtils.ic_mail,
                  width: adaptiveWidth(context, 28),
                  height: adaptiveWidth(context, 28),
                ),*/
                SizedBox(width: adaptiveWidth(context, 15)),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Expanded(
                            child: StyledText(item.title,
                              size: sp(context, 14),
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Container(
                            width: adaptiveWidth(context, 90),
                            margin: EdgeInsets.only(top: adaptiveWidth(context, 2)),
                            child: StyledText(item.date,
                              color: ColorUtils.primary,
                              textAlign: TextAlign.center,
                              size: sp(context, 12),
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: adaptiveWidth(context, 5)),
                      StyledText(item.content,
                        size: sp(context, 12),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                      SizedBox(height: adaptiveWidth(context, 5)),
                      Container(
                        padding: EdgeInsets.symmetric(
                          horizontal: adaptiveWidth(context, 12),
                          vertical: adaptiveWidth(context, 3),
                        ),
                        child: StyledText(item.category,
                          color: Colors.white,
                          size: sp(context, 10),
                        ),
                        decoration: BoxDecoration(
                          color: ColorUtils.primary,
                          borderRadius: BorderRadius.circular(10),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
          Container(
            height: 1,
            width: double.infinity,
            color: Colors.grey,
          )
        ],
      ),
    );
  }
}
