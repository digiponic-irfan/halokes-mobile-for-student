import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/database/database.dart';
import 'package:halokes_siswa/database/entities/Conversation.dart';
import 'package:halokes_siswa/extension/Size.dart';
import 'package:halokes_siswa/extension/Date.dart';
import 'package:halokes_siswa/provider/TypingProvider.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';
import 'package:halokes_siswa/utils/ImageUtils.dart';
import 'package:provider/provider.dart';

class ChatListItem extends StatelessWidget {
  final ListContactResult contact;
  final Function(ListContactResult) onSelected;

  ChatListItem({@required this.contact, @required this.onSelected})
      : assert(contact != null), assert(onSelected != null);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => onSelected(contact),
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(adaptiveWidth(context, 15)),
            child: Row(
              children: <Widget>[
                Container(
                  width: adaptiveWidth(context, 48),
                  height: adaptiveWidth(context, 48),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                      image: AssetImage(ImageUtils.image_not_found),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                SizedBox(width: adaptiveWidth(context, 15)),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      StyledText(contact.displayName,
                        size: sp(context, 14),
                        fontWeight: FontWeight.w600,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                      SizedBox(height: adaptiveWidth(context, 3),),
                      Consumer<TypingProvider>(
                        builder: (_, provider, __){
                          if(provider.isTyping(contact.idUrl)){
                            return StyledText("Sedang mengetik...",
                              size: sp(context, 12),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              color: ColorUtils.primary,
                            );
                          }

                          if(contact.sender == ConversationEntity.ME){
                            return Row(
                              children: <Widget>[
                                _getStatusIcon(context),
                                SizedBox(width: adaptiveWidth(context, 5)),
                                StyledText(contact.displayLastChat,
                                  size: sp(context, 12),
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                  color: ColorUtils.grey7e7e,
                                ),
                              ],
                            );
                          }else {
                            return Row(
                              children: <Widget>[
                                Expanded(
                                  child: StyledText(contact.displayLastChat,
                                    size: sp(context, 12),
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    color: ColorUtils.grey7e7e,
                                  ),
                                ),
                              ],
                            );
                          }
                        },
                      ),
                    ],
                  ),
                ),
                SizedBox(width: adaptiveWidth(context, 15)),
                Column(
                  children: <Widget>[
                    StyledText(_getLastChatTime(),
                      size: sp(context, 12),
                      color: contact.unreadCount > 0 ? ColorUtils.primary : ColorUtils.grey7e7e,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                    Container(
                      decoration: BoxDecoration(
                        color: contact.unreadCount > 0 ? ColorUtils.primary : Colors.transparent,
                        shape: BoxShape.circle,
                      ),
                      padding: EdgeInsets.all(adaptiveWidth(context, 5)),
                      alignment: Alignment.center,
                      child: StyledText(contact.unreadCount > 0 ? contact.unreadCount.toString() : "",
                        size: sp(context, 10),
                        color: contact.unreadCount > 0 ? Colors.white : Colors.transparent,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Container(
            width: double.infinity,
            height: 1,
            color: ColorUtils.greyf1f1,
          ),
        ],
      ),
    );
  }

  String _getLastChatTime(){
    DateTime lastChat = DateTime.fromMillisecondsSinceEpoch(contact.lastChatTime.toInt());
    DateTime today = DateTime.now();
    int differDays = today.weekday - lastChat.weekday;

    if(today.year == lastChat.year && today.month == lastChat.month){
      if(differDays == 1){
        return "Kemarin";
      }else if(differDays == 0){
        return fromMillisecond("HH:mm", contact.lastChatTime.toInt());
      }else {
        return fromMillisecond("dd/MM HH:mm", contact.lastChatTime.toInt());
      }
    }else {
      return fromMillisecond("dd/MM/yy", contact.lastChatTime.toInt());
    }
  }

  SvgPicture _getStatusIcon(BuildContext context){
    if(contact.conversationStatus == ConversationEntity.STATUS_SEND){
      return SvgPicture.asset(ImageUtils.ic_pending,
        width: adaptiveWidth(context, 10),
        height: adaptiveWidth(context, 10),
        fit: BoxFit.cover,
      );
    }else if(contact.conversationStatus == ConversationEntity.STATUS_RECEIVED){
      return SvgPicture.asset(ImageUtils.ic_received_by_server,
        width: adaptiveWidth(context, 10),
        height: adaptiveWidth(context, 10),
        fit: BoxFit.cover,
      );
    }else if(contact.conversationStatus == ConversationEntity.STATUS_SENT){
      return SvgPicture.asset(ImageUtils.ic_sent,
        width: adaptiveWidth(context, 10),
        height: adaptiveWidth(context, 10),
        fit: BoxFit.cover,
      );
    }else {
      return SvgPicture.asset(ImageUtils.ic_read,
        width: adaptiveWidth(context, 10),
        height: adaptiveWidth(context, 10),
        fit: BoxFit.cover,
      );
    }
  }
}
