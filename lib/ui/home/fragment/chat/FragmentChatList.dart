import 'package:flutter/material.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/database/database.dart';
import 'package:halokes_siswa/main.dart';
import 'package:halokes_siswa/network/response/general/ChatUserResponse.dart';
import 'package:halokes_siswa/ui/home/fragment/chat/FragmentChatPresenter.dart';
import 'package:halokes_siswa/ui/home/fragment/chat/adapter/ChatListAdapter.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';
import 'package:halokes_siswa/extension/BaseStateExt.dart';

class FragmentChatList extends StatefulWidget {
  @override
  _FragmentChatListState createState() => _FragmentChatListState();
}

class _FragmentChatListState extends BaseState<FragmentChatList> {
  FragmentChatPresenter _presenter;

  @override
  void initState() {
    super.initState();
    _presenter = FragmentChatPresenter(this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: FloatingActionButton(
        onPressed: () => navigateTo(MyApp.ROUTE_CHAT_SEARCH),
        heroTag: "Chat",
        tooltip: "Mulai Percakapan",
        child: Icon(Icons.chat, color: Colors.white),
      ),
      appBar: AppBar(
        elevation: 1,
        centerTitle: false,
        title: StyledText("Kontak", color: Colors.white),
      ),
      body: StreamBuilder<List<ListContactResult>>(
        stream: _presenter.contactStream,
        builder: (_, snapshot){
          if(snapshot.hasData){
            if(snapshot.data.length > 0){
              return ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (_, index) => ChatListItem(
                  contact: snapshot.data[index],
                  onSelected: _onContactSelected,
                ),
              );
            }
          }

          return Center(
            child: Center(
              child: StyledText("Tidak ada riwayat percakapan",
                fontWeight: FontWeight.w600,
                size: sp(16),
              ),
            ),
          );
        },
      ),
    );
  }

  void _onContactSelected(ListContactResult contact) =>
      navigateTo(MyApp.ROUTE_CHAT_CONVERSATION,
        arguments: ChatDisplayUser.fromListContact(contact),
      );
}
