import 'package:halokes_siswa/ancestor/BasePresenter.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/database/database.dart';

class FragmentChatPresenter extends BasePresenter {

  Stream<List<ListContactResult>> contactStream;

  FragmentChatPresenter(BaseState state) : super(state){
    contactStream = database.watchListContact();
  }
}