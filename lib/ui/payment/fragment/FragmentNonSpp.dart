import 'package:flutter/material.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/extension/BaseStateExt.dart';
import 'package:halokes_siswa/network/response/general/FinanceResponse.dart';
import 'package:halokes_siswa/ui/payment/adapter/PaymentAdapter.dart';

class FragmentNonSpp extends StatefulWidget {
  final List<FinanceData> data;

  FragmentNonSpp({@required this.data}) : assert(data != null);

  @override
  _FragmentNonSppState createState() => _FragmentNonSppState();
}

class _FragmentNonSppState extends BaseState<FragmentNonSpp> with AutomaticKeepAliveClientMixin{

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return SingleChildScrollView(
      padding: EdgeInsets.all(width(15)),
      child: ListView.builder(
        itemCount: widget.data.length,
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: (_, index) => PaymentItem(data: widget.data[index]),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
