import 'package:flutter/material.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/extension/BaseStateExt.dart';
import 'package:halokes_siswa/network/response/general/FinanceResponse.dart';
import 'package:halokes_siswa/ui/payment/adapter/PaymentAdapter.dart';

class FragmentSpp extends StatefulWidget {
  final List<FinanceData> data;

  FragmentSpp({@required this.data}) : assert(data != null);

  @override
  _FragmentSppState createState() => _FragmentSppState();
}

class _FragmentSppState extends BaseState<FragmentSpp> with AutomaticKeepAliveClientMixin{
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return SingleChildScrollView(
      padding: EdgeInsets.all(width(15)),
      child: ListView.builder(
        itemCount: widget.data.length,
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: (_, index) => PaymentItem(data: widget.data[index]),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;

}
