import 'package:flutter/material.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/extension/Size.dart';
import 'package:halokes_siswa/network/response/general/FinanceResponse.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';
import 'package:mcnmr_common_ext/NumberFormat.dart';

class PaymentItem extends StatelessWidget {
  final FinanceData data;

  PaymentItem({@required this.data}) : assert(data != null);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: EdgeInsets.all(adaptiveWidth(context, 20)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            StyledText(data.deskripsi,
              size: sp(context, 16),
              fontWeight: FontWeight.w600,
            ),
            SizedBox(height: adaptiveWidth(context, 30)),
            Row(
              children: <Widget>[
                Expanded(
                  child: StyledText("Status",
                    size: sp(context, 14),
                    color: ColorUtils.grey7070,
                  ),
                  flex: 40,
                ),
                Expanded(
                  child: StyledText(data.statusPembayaran == "1" ? "SUDAH DIBAYAR" : "BELUM DIBAYAR",
                    size: sp(context, 14),
                    fontWeight: FontWeight.bold,
                  ),
                  flex: 60,
                ),
              ],
            ),
            SizedBox(height: adaptiveWidth(context, 10)),
            Visibility(
              visible: data.statusPembayaran == "1",
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: StyledText("Tgl. Bayar",
                      size: sp(context, 14),
                      color: ColorUtils.grey7070,
                    ),
                    flex: 40,
                  ),
                  Expanded(
                    child: StyledText(data.tanggalPembayaran,
                      size: sp(context, 14),
                      fontWeight: FontWeight.w600,
                    ),
                    flex: 60,
                  ),
                ],
              ),
            ),
            Visibility(
              visible: data.statusPembayaran == "1",
              child: SizedBox(height: adaptiveWidth(context, 10)),
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: StyledText("Jatuh Tempo",
                    size: sp(context, 14),
                    color: ColorUtils.grey7070,
                  ),
                  flex: 40,
                ),
                Expanded(
                  child: StyledText(data.jatuhTempo,
                    size: sp(context, 14),
                    fontWeight: FontWeight.w600,
                  ),
                  flex: 60,
                ),
              ],
            ),
            SizedBox(height: adaptiveWidth(context, 15)),

            SizedBox(height: adaptiveWidth(context, 10)),
            Row(
              children: <Widget>[
                Expanded(
                  child: StyledText("Jumlah",
                    size: sp(context, 14),
                    fontWeight: FontWeight.bold,
                  ),
                  flex: 30,
                ),
                Expanded(
                  child: StyledText("Rp ${currencyFormatString(data.nominal)}",
                    size: sp(context, 20),
                    color: ColorUtils.danger,
                    fontWeight: FontWeight.bold,
                    textAlign: TextAlign.end,
                  ),
                  flex: 70,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
