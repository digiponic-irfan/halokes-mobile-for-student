import 'package:flutter/material.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/extension/BaseStateExt.dart';
import 'package:halokes_siswa/network/response/general/FinanceResponse.dart';
import 'package:halokes_siswa/ui/payment/fragment/FragmentNonSpp.dart';
import 'package:halokes_siswa/ui/payment/fragment/FragmentSpp.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';

class Payment extends StatefulWidget {
  final FinanceResponse argument;

  Payment({@required this.argument}) : assert(argument != null);

  @override
  _PaymentState createState() => _PaymentState();
}

class _PaymentState extends BaseState<Payment> {
  static const SPP = "spp";

  var _content = <Widget>[];

  @override
  void initState() {
    super.initState();
    var listSpp = <FinanceData>[];
    var listNonSpp = <FinanceData>[];

    widget.argument.data.forEach((it){
      if(it.keterangan.toLowerCase() == SPP){
        listSpp.add(it);
      }else {
        listNonSpp.add(it);
      }
    });

    _content.add(FragmentSpp(data: listSpp));
    _content.add(FragmentNonSpp(data: listNonSpp));
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        backgroundColor: ColorUtils.appbar,
        appBar: AppBar(
          title: StyledText("Pembayaran",
            color: Colors.white,
            fontWeight: FontWeight.w600,
          ),
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            onPressed: () => finish(),
            color: Colors.white,
          ),
        ),
        body: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(horizontal: width(10)),
              child: TabBar(
                tabs: <Tab>[
                  Tab(text: "SPP"),
                  Tab(text: "Non SPP")
                ],
                indicatorColor: ColorUtils.primary,
                labelColor: ColorUtils.primary,
                unselectedLabelColor: ColorUtils.primary.withOpacity(0.5),
              ),
            ),
            Expanded(
              child: TabBarView(
                children: _content,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
