import 'package:flutter/material.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/network/response/general/ScheduleResponse.dart';
import 'package:halokes_siswa/ui/schedule/fragment/FragmentSchedule.dart';
import 'package:halokes_siswa/extension/BaseStateExt.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';

class Schedule extends StatefulWidget {
  final ScheduleResponse argument;

  Schedule({@required this.argument});

  @override
  _ScheduleState createState() => _ScheduleState();
}

class _ScheduleState extends BaseState<Schedule> {

  var _content = <FragmentSchedule>[];

  @override
  void initState() {
    super.initState();
    widget.argument.data.forEach((it) => _content.add(FragmentSchedule(data: it)));
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: widget.argument.data.length,
      child: Scaffold(
        appBar: AppBar(
          title: StyledText("Jadwal",
            color: Colors.white,
            fontWeight: FontWeight.w600,
          ),
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios, color: Colors.white),
            onPressed: () => finish(),
          ),
        ),
        body: Column(
          children: <Widget>[
            Center(
              child: TabBar(
                isScrollable: true,
                tabs: widget.argument.data.map((it) => Tab(text: it.hari)).toList(),
                unselectedLabelColor: ColorUtils.primary.withOpacity(0.5),
                labelColor: ColorUtils.primary,
              ),
            ),
            SizedBox(height: width(10)),
            Expanded(
              child: TabBarView(
                children: _content,
              ),
            )
          ],
        ),
      ),
    );
  }
}
