import 'package:flutter/material.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/extension/Size.dart';
import 'package:halokes_siswa/network/response/general/ScheduleResponse.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';
import 'package:shimmer/shimmer.dart';

class ScheduleItem extends StatelessWidget {
  final ScheduleDetail detail;

  ScheduleItem({@required this.detail});

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2,
      margin: EdgeInsets.symmetric(
        vertical: adaptiveWidth(context, 6),
        horizontal: adaptiveWidth(context, 18)
      ),
      child: Padding(
        padding: EdgeInsets.all(adaptiveWidth(context, 12)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            IntrinsicWidth(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  StyledText(detail.mapel,
                    size: sp(context, 14),
                    fontWeight: FontWeight.w500,
                  ),
                  SizedBox(height: adaptiveWidth(context, 15)),
                  Container(
                    height: 1,
                    color: Colors.black,
                  )
                ],
              ),
            ),
            SizedBox(height: adaptiveWidth(context, 10)),
            Row(
              children: <Widget>[
                Expanded(
                  flex: 60,
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.person,
                        size: sp(context, 12),
                        color: ColorUtils.primary,
                      ),
                      SizedBox(width: adaptiveWidth(context, 5)),
                      StyledText(detail.guru,
                        size: sp(context, 12),
                      )
                    ],
                  ),
                ),
                Expanded(
                  flex: 40,
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.alarm,
                        size: sp(context, 12),
                      ),
                      SizedBox(width: adaptiveWidth(context, 5)),
                      StyledText(detail.jam,
                        size: sp(context, 12),
                      ),

                      SizedBox(width: adaptiveWidth(context, 25)),
                    ],
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}


class ShimmerScheduleItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2,
      margin: EdgeInsets.symmetric(
        vertical: adaptiveWidth(context, 6),
        horizontal: adaptiveWidth(context, 18),
      ),
      child: Shimmer.fromColors(
        child: Container(
          width: double.infinity,
          height: adaptiveWidth(context, 80),
          color: Colors.grey[100],
        ),
        baseColor: Colors.grey[100],
        highlightColor: Colors.white,
      ),
    );
  }
}
