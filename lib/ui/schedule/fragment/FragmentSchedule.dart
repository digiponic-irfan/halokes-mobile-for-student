import 'package:flutter/material.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/extension/BaseStateExt.dart';
import 'package:halokes_siswa/network/response/general/ScheduleResponse.dart';
import 'package:halokes_siswa/ui/schedule/fragment/adapter/ScheduleAdapter.dart';

class FragmentSchedule extends StatefulWidget {
  final ScheduleData data;

  FragmentSchedule({@required this.data});

  @override
  _FragmentScheduleState createState() => _FragmentScheduleState();
}

class _FragmentScheduleState extends BaseState<FragmentSchedule>
    with AutomaticKeepAliveClientMixin{

  @override
  bool get wantKeepAlive => true;

  @override
  void shouldHideLoading(int typeRequest) {}

  @override
  void shouldShowLoading(int typeRequest) {}

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          ListView.builder(
            itemCount: widget.data.dataJadwal.length,
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemBuilder: (_, index) => ScheduleItem(detail: widget.data.dataJadwal[index]),
          ),
          SizedBox(height: width(20))
        ],
      ),
    );
  }
}
