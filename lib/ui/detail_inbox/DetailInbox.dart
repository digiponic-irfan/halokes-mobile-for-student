import 'package:flutter/material.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/extension/BaseStateExt.dart';

class DetailInbox extends StatefulWidget {
  @override
  _DetailInboxState createState() => _DetailInboxState();
}

class _DetailInboxState extends BaseState<DetailInbox> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        title: Text("Notifikasi"),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () => finish(),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.record_voice_over),
            onPressed: (){},
          )
        ],
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(width(16)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            StyledText("Tugas Bahasa Indonesia: Tugas Membuat Puisi telah dinilai",
              size: sp(16),
              fontWeight: FontWeight.bold,
            ),
          ],
        ),
      ),
    );
  }
}
