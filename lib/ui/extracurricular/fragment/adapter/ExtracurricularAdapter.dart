import 'package:flutter/material.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/extension/Size.dart';
import 'package:halokes_siswa/network/response/general/ExtraResponse.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';
import 'package:shimmer/shimmer.dart';

class ExtracurricularItem extends StatelessWidget {
  final ExtraActivity kegiatan;

  ExtracurricularItem({@required this.kegiatan});

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2,
      margin: EdgeInsets.only(
        bottom: adaptiveWidth(context, 10),
      ),
      child: Padding(
        padding: EdgeInsets.all(adaptiveWidth(context, 12)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            IntrinsicWidth(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  StyledText(kegiatan.judul,
                    size: sp(context, 14),
                    fontWeight: FontWeight.w500,
                  ),
                  SizedBox(height: adaptiveWidth(context, 15)),
                  Container(
                    height: 1,
                    color: Colors.black,
                  )
                ],
              ),
            ),
            SizedBox(height: adaptiveWidth(context, 10)),
            Row(
              children: <Widget>[
                Expanded(
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.date_range,
                        size: sp(context, 12),
                        color: ColorUtils.primary,
                      ),
                      SizedBox(width: adaptiveWidth(context, 5)),
                      StyledText(kegiatan.tanggal,
                        size: sp(context, 12),
                        color: ColorUtils.primary,
                      )
                    ],
                  ),
                ),
                Expanded(
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.place,
                        size: sp(context, 12),
                        color: ColorUtils.primary,
                      ),
                      SizedBox(width: adaptiveWidth(context, 5)),
                      Expanded(
                        child: StyledText(kegiatan.tempat,
                          size: sp(context, 12),
                          color: ColorUtils.primary,
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class ShimmerExtracurricularItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2,
      margin: EdgeInsets.only(
        bottom: adaptiveWidth(context, 10),
      ),
      child: Shimmer.fromColors(
        child: Container(
          width: double.infinity,
          height: adaptiveWidth(context, 80),
          color: Colors.grey[200],
        ),
        baseColor: Colors.grey[200],
        highlightColor: Colors.white,
      ),
    );
  }
}

