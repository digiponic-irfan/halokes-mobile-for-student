import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:flutter/material.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/extension/BaseStateExt.dart';
import 'package:halokes_siswa/main.dart';
import 'package:halokes_siswa/network/response/general/ExtraResponse.dart';
import 'package:halokes_siswa/ui/extracurricular/fragment/adapter/ExtracurricularAdapter.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';

class FragmentExtracurricular extends StatefulWidget {
  final ExtraData data;

  FragmentExtracurricular({@required this.data});

  @override
  _FragmentExtracurricularState createState() => _FragmentExtracurricularState();
}

class _FragmentExtracurricularState extends BaseState<FragmentExtracurricular> with AutomaticKeepAliveClientMixin {

  @override
  bool get wantKeepAlive => true;

  @override
  void shouldHideLoading(int typeRequest) {}

  @override
  void shouldShowLoading(int typeRequest) {}

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return SingleChildScrollView(
      padding: EdgeInsets.symmetric(
        vertical: width(12),
        horizontal: width(20)
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: width(10)),
          Row(
            children: <Widget>[
              Icon(Icons.date_range,
                size: sp(12),
              ),
              SizedBox(width: width(5)),
              StyledText(getJadwal(),
                size: sp(12),
              )
            ],
          ),
          SizedBox(height: width(10)),
          Row(
            children: <Widget>[
              Expanded(
                child: Row(
                  children: <Widget>[
                    Icon(Icons.place,
                      size: sp(12),
                    ),
                    SizedBox(width: width(5)),
                    StyledText(widget.data.tempat,
                      size: sp(12),
                    )
                  ],
                ),
              ),
              Expanded(
                child: Row(
                  children: <Widget>[
                    Icon(Icons.person,
                      size: sp(12),
                    ),
                    SizedBox(width: width(5)),
                    StyledText(widget.data.detail.pembina,
                      size: sp(12),
                    )
                  ],
                ),
              )
            ],
          ),
          SizedBox(height: width(15)),
          MaterialButton(
            onPressed: () => navigateTo(MyApp.ROUTE_EXTRACURRICULAR_MEMBER, arguments: widget.data.detail),
            padding: EdgeInsets.all(0),
            color: ColorUtils.primary,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(width(20)),
            ),
            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
            height: width(25),
            minWidth: width(140),
            child: StyledText("Lihat Anggota", size: sp(10), color: Colors.white),
          ),
          SizedBox(height: width(30)),
          ListView.builder(
            itemCount: widget.data.detail.kegiatan.length,
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemBuilder: (_, position) => ExtracurricularItem(
              kegiatan: widget.data.detail.kegiatan[position],
            ),
          )
        ],
      ),
    );
  }

  String getJadwal(){
    var jadwal = "";

    for(int i=0;i<widget.data.jadwal.length;i++){
      jadwal += widget.data.jadwal[i].toString();
      if(i != widget.data.jadwal.length){
        jadwal += "\n";
      }
    }

    return jadwal;
  }
}
