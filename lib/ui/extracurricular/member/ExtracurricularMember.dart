import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/extension/BaseStateExt.dart';
import 'package:flutter/material.dart';
import 'package:halokes_siswa/network/response/general/ExtraResponse.dart';
import 'package:halokes_siswa/ui/extracurricular/member/adapter/ExtracurricularMemberAdapter.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';

class ExtracurricularMember extends StatefulWidget {
  final ExtraDetail argument;

  ExtracurricularMember({@required this.argument});

  @override
  _ExtracurricularMemberState createState() => _ExtracurricularMemberState();
}

class _ExtracurricularMemberState extends BaseState<ExtracurricularMember> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: StyledText("Anggota Ekskul",
          color: Colors.white,
          fontWeight: FontWeight.w600,
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.white),
          onPressed: () => finish(),
        ),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(width(26)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              width: double.infinity,
              height: 1,
              color: Colors.black.withOpacity(0.1),
            ),
            SizedBox(height: width(20)),
            Row(
              children: <Widget>[
                SizedBox(width: width(10)),
                Expanded(
                  child: StyledText("#",
                    size: sp(12),
                    color: ColorUtils.primary,
                    fontWeight: FontWeight.bold,
                  ),
                  flex: 15,
                ),
                Expanded(
                  child: StyledText("NIS",
                    size: sp(12),
                    color: ColorUtils.primary,
                    fontWeight: FontWeight.bold,
                  ),
                  flex: 35,
                ),
                Expanded(
                  child: StyledText("Nama",
                    size: sp(12),
                    color: ColorUtils.primary,
                    fontWeight: FontWeight.bold,
                  ),
                  flex: 50,
                ),
              ],
            ),
            ListView.builder(
              itemCount: widget.argument.anggota.length,
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemBuilder: (_, index) => ExtracurricularMemberItem(isEven: index % 2 == 1,
                data: widget.argument.anggota[index],
                index: index+1,
              ),
            )
          ],
        ),
      ),
    );
  }
}
