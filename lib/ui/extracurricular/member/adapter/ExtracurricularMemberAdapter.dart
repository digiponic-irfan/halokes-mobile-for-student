import 'package:flutter/material.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/extension/Size.dart';
import 'package:halokes_siswa/network/response/general/ExtraResponse.dart';

class ExtracurricularMemberItem extends StatelessWidget {
  final bool isEven;
  final ExtraMember data;
  final int index;

  ExtracurricularMemberItem({@required this.isEven, @required this.data, @required this.index});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
          vertical: adaptiveWidth(context, 5)
      ),
      color: isEven ? Colors.grey[300] : Colors.white,
      child: Row(
        children: <Widget>[
          SizedBox(width: adaptiveWidth(context, 10)),
          Expanded(
            child: StyledText("$index",
              size: sp(context, 12),
            ),
            flex: 15,
          ),
          Expanded(
            child: StyledText(data.nis,
              size: sp(context, 12),
            ),
            flex: 35,
          ),
          Expanded(
            child: StyledText(data.namaAnggota,
              size: sp(context, 12),
            ),
            flex: 50,
          ),
        ],
      ),
    );
  }
}