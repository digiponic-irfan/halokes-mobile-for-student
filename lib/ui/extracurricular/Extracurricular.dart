import 'package:flutter/material.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/extension/BaseStateExt.dart';
import 'package:halokes_siswa/network/response/general/ExtraResponse.dart';
import 'package:halokes_siswa/network/response/general/UserResponse.dart';
import 'package:halokes_siswa/ui/extracurricular/ExtracurricularPresenter.dart';
import 'package:halokes_siswa/ui/extracurricular/fragment/FragmentExtracurricular.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';
import 'package:mcnmr_request_wrapper/RequestWrapper.dart';
import 'package:mcnmr_request_wrapper/RequestWrapperWidget.dart';

class Extracurricular extends StatefulWidget {
  final ExtraResponse argument;

  Extracurricular({@required this.argument});

  @override
  _ExtracurricularState createState() => _ExtracurricularState();
}

class _ExtracurricularState extends BaseState<Extracurricular> {
  ExtracurricularPresenter _presenter;
  RequestWrapper<UserData> _userWrapper = RequestWrapper();

  List<FragmentExtracurricular> _content = List();

  @override
  void initState() {
    super.initState();
    _presenter = ExtracurricularPresenter(this);
    widget.argument.data.forEach((it) => _content.add(FragmentExtracurricular(data: it)));
  }

  @override
  void afterWidgetBuilt() => _presenter.getUser(_userWrapper);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: widget.argument.data.length,
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios, color: Colors.white),
            onPressed: () => finish(),
          ),
          title: StyledText("Ekstrakulikuler",
            color: Colors.white,
            fontWeight: FontWeight.w600,
          ),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(width(20)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(padding: EdgeInsets.symmetric(horizontal: width(16)),
                    child: RequestWrapperWidget<UserData>(
                      requestWrapper: _userWrapper,
                      placeholder: (_) => StyledText("",
                        size: sp(28),
                        fontWeight: FontWeight.w500,
                      ),
                      builder: (_, data) => StyledText(data.namaSiswa,
                        size: sp(28),
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                  Padding(padding: EdgeInsets.symmetric(horizontal: width(16)),
                    child: RequestWrapperWidget<UserData>(
                        requestWrapper: _userWrapper,
                        placeholder: (_) => StyledText("",
                          size: sp(14),
                        ),
                        builder: (_, data) => StyledText("${data.nis} / ${data.kelas}",
                          size: sp(14),
                        ),
                      ),
                  ),
                  SizedBox(height: width(20)),
                  TabBar(
                    isScrollable: true,
                    labelColor: ColorUtils.primary,
                    unselectedLabelColor: Colors.black,
                    tabs: widget.argument.data.map((it) => Tab(text: it.nama)).toList(),
                  ),
                ],
              ),
            ),
            Expanded(
              child: TabBarView(
                children: _content,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
