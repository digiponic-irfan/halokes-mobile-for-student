import 'package:halokes_siswa/ancestor/BasePresenter.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/network/response/general/UserResponse.dart';
import 'package:halokes_siswa/preference/AppPreference.dart';
import 'package:mcnmr_request_wrapper/RequestWrapper.dart';

class ExtracurricularPresenter extends BasePresenter{
  ExtracurricularPresenter(BaseState state) : super(state);

  void getUser(RequestWrapper<UserData> wrapper) async {
    wrapper.doRequest();
    wrapper.finishRequest(await AppPreference.getUser());
  }
}