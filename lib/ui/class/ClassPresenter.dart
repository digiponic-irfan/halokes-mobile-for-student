import 'package:halokes_siswa/ancestor/BasePresenter.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/network/response/general/ClassResponse.dart';
import 'package:halokes_siswa/ui/class/ClassDelegate.dart';

class ClassPresenter extends BasePresenter{
  static const REQUEST_GET_STUDENT_DETAIL = 0;

  final ClassDelegate _delegate;

  ClassPresenter(BaseState state, this._delegate) : super(state);
 
  void executeGetStudentDetail(ClassStudentData data){
    generalRepo.executeGetOtherStudentBio(REQUEST_GET_STUDENT_DETAIL, data.uid, _delegate.onSuccessGetStudentDetail);
  }
}