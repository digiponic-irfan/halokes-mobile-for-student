import 'package:flutter/material.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/extension/Size.dart';
import 'package:halokes_siswa/network/response/general/ClassResponse.dart';

class ClassItem extends StatelessWidget {
  final bool isEven;
  final ClassStudentData data;
  final int index;
  final Function(ClassStudentData) onSelected;

  ClassItem({@required this.isEven, @required this.data, @required this.index, @required this.onSelected});

  @override
  Widget build(BuildContext context) {
    return Material(
      color: isEven ? Colors.grey[300] : Colors.white,
      child: InkWell(
        onTap: () => onSelected(data),
        child: Container(
          padding: EdgeInsets.symmetric(
            vertical: adaptiveWidth(context, 5),
          ),
          child: Row(
            children: <Widget>[
              SizedBox(width: adaptiveWidth(context, 10)),
              Expanded(
                child: StyledText("$index",
                  size: sp(context, 12),
                ),
                flex: 15,
              ),
              Expanded(
                child: StyledText(data.nis,
                  size: sp(context, 12),
                ),
                flex: 35,
              ),
              Expanded(
                child: StyledText(data.nama,
                  size: sp(context, 12),
                ),
                flex: 50,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
