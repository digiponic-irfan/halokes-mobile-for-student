import 'package:flutter/material.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/extension/BaseStateExt.dart';
import 'package:halokes_siswa/main.dart';
import 'package:halokes_siswa/network/response/general/ClassResponse.dart';
import 'package:halokes_siswa/network/response/general/DetailStudentResponse.dart';
import 'package:halokes_siswa/network/response/general/UserResponse.dart';
import 'package:halokes_siswa/preference/AppPreference.dart';
import 'package:halokes_siswa/ui/class/ClassDelegate.dart';
import 'package:halokes_siswa/ui/class/ClassPresenter.dart';
import 'package:halokes_siswa/ui/class/adapter/ClassAdapter.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';

class Class extends StatefulWidget {
  final ClassResponse argument;

  Class({@required this.argument});

  @override
  _ClassState createState() => _ClassState();
}

class _ClassState extends BaseState<Class> implements ClassDelegate{
  ClassPresenter _presenter;
  UserData _user;

  @override
  void initState() {
    super.initState();
    _presenter = ClassPresenter(this, this);
  }

  @override
  void afterWidgetBuilt() async {
    var user = await AppPreference.getUser();
    setState(() => _user = user);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: StyledText("Kelas",
          color: Colors.white,
          fontWeight: FontWeight.w600,
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.white),
          onPressed: () => finish(),
        ),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(width(26)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: width(20),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  StyledText(_user == null ? "" : _user.namaSiswa,
                    size: sp(20),
                    fontWeight: FontWeight.w700,
                  ),
                  StyledText(_user == null ? "" : "NIS : ${_user.nis}",
                    size: sp(12),
                  ),
                  StyledText(_user == null ? "" : "Kelas ${_user.kelas}",
                    size: sp(12),
                  ),
                  StyledText(widget.argument.data.waliKelas,
                    size: sp(12),
                  ),
                ],
              ),
            ),
            SizedBox(height: width(20)),
            Container(
              width: double.infinity,
              height: 1,
              color: Colors.black.withOpacity(0.1),
            ),
            SizedBox(height: width(20)),
            Row(
              children: <Widget>[
                SizedBox(width: width(10)),
                Expanded(
                  child: StyledText("#",
                    size: sp(12),
                    color: ColorUtils.primary,
                    fontWeight: FontWeight.bold,
                  ),
                  flex: 15,
                ),
                Expanded(
                  child: StyledText("NIS",
                    size: sp(12),
                    color: ColorUtils.primary,
                    fontWeight: FontWeight.bold,
                  ),
                  flex: 35,
                ),
                Expanded(
                  child: StyledText("Nama",
                    size: sp(12),
                    color: ColorUtils.primary,
                    fontWeight: FontWeight.bold,
                  ),
                  flex: 50,
                ),
              ],
            ),
            ListView.builder(
              itemCount: widget.argument.data.dataSiswa.length,
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemBuilder: (_, index) => ClassItem(isEven: index % 2 == 1,
                data: widget.argument.data.dataSiswa[index],
                index: index+1,
                onSelected: onStudentSelected,
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  void onStudentSelected(ClassStudentData data) => _presenter.executeGetStudentDetail(data);

  @override
  void onSuccessGetStudentDetail(DetailStudentResponse data) => navigateTo(MyApp.ROUTE_DETAIL_STUDENT, arguments: data);

}
