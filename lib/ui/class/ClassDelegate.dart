import 'package:halokes_siswa/network/response/general/ClassResponse.dart';
import 'package:halokes_siswa/network/response/general/DetailStudentResponse.dart';

class ClassDelegate {
  void onStudentSelected(ClassStudentData data){}
  void onSuccessGetStudentDetail(DetailStudentResponse data){}
}