import 'package:flutter/material.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/main.dart';
import 'package:halokes_siswa/ui/forum/filter/FilterForumArgument.dart';
import 'package:halokes_siswa/ui/forum/list/fragment/all_forum/FragmentAllForum.dart';
import 'package:halokes_siswa/ui/forum/list/fragment/my_forum/FragmentMyForum.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';

class ForumList extends StatefulWidget {
  @override
  _ForumListState createState() => _ForumListState();
}

class _ForumListState extends BaseState<ForumList> with SingleTickerProviderStateMixin{
  static const TAG_FILTER_FORUM = 0;

  var _content = [
    FragmentAllForum(),
    FragmentMyForum(),
  ];

  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, vsync: this);
  }

  @override
  void onNavigationResult(String from, result) {
    if(from == MyApp.ROUTE_FORUM_FILTER){
      if(_tabController.index == 0){
        var filter = result as FilterForumArgument;
        (_content[_tabController.index] as FragmentAllForum).refilter(filter.grade, filter.mapel);
      }else {
        var filter = result as FilterForumArgument;
        (_content[_tabController.index] as FragmentMyForum).refilter(filter.grade, filter.mapel);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: Stack(
        children: <Widget>[
          Container(
            height: 60,
            width: 60,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: ColorUtils.primary,
              boxShadow: [
                BoxShadow(
                  color: ColorUtils.primary.withOpacity(0.5),
                  spreadRadius: 3.0,
                  blurRadius: 3.0,
                ),
              ],
            ),
            alignment: Alignment.center,
            child: Icon(Icons.edit, color: Colors.white),
          ),
          ClipRRect(
            borderRadius: BorderRadius.circular(60),
            child: Material(
              type: MaterialType.transparency,
              child: InkWell(
                onTap: () => navigateTo(MyApp.ROUTE_FORUM_CREATE),
                child: Container(
                  height: 60,
                  width: 60,
                ),
              ),
            ),
          )
        ],
      ),
      appBar: AppBar(
        title: StyledText("Forum Tanya Jawab", color: Colors.white),
        automaticallyImplyLeading: true,
        actions: <Widget>[
          IconButton(
            color: Colors.white,
            icon: Icon(Icons.filter_list),
            onPressed: (){
              if(_tabController.index == 0){
                navigateTo(MyApp.ROUTE_FORUM_FILTER, arguments: FilterForumArgument(
                  (_content[_tabController.index] as FragmentAllForum).selectedGrade,
                  (_content[_tabController.index] as FragmentAllForum).selectedMapel,
                ),
                );
              }else {
                navigateTo(MyApp.ROUTE_FORUM_FILTER, arguments: FilterForumArgument(
                  (_content[_tabController.index] as FragmentMyForum).selectedGrade,
                  (_content[_tabController.index] as FragmentMyForum).selectedMapel,
                ),
                );
              }
            },
          )
        ],
        bottom: TabBar(
          controller: _tabController,
          indicatorColor: Colors.white,
          tabs: [
            Tab(text: "Semua Pertanyaan"),
            Tab(text: "Pertanyaanku"),
          ],
        ),
      ),
      body: TabBarView(
        controller: _tabController,
        children: _content,
      ),
    );
  }
}
