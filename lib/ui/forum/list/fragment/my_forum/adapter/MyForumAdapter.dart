import 'package:flutter/material.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/extension/Size.dart';
import 'package:halokes_siswa/external_plugin/cached_network_image/src/cached_image_widget.dart';
import 'package:halokes_siswa/network/response/management/MyForumResponse.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';
import 'package:halokes_siswa/utils/ImageUtils.dart';
import 'package:shimmer/shimmer.dart';

class MyForumItem extends StatelessWidget {
  final MyForumData data;
  final Function(MyForumData) onSelected;

  MyForumItem({@required this.data, @required this.onSelected}) :
        assert(data != null), assert(onSelected != null);

  @override
  Widget build(BuildContext context) {
    var differentTime = DateTime.now().difference(data.time);
    var displayDifferentTime = "";

    if(differentTime.inDays > 0){
      if(differentTime.inDays < 30){
        displayDifferentTime = "${differentTime.inDays} hari lalu";
      }else {
        displayDifferentTime = data.displayTime;
      }
    }else if(differentTime.inHours > 0){
      displayDifferentTime = "${differentTime.inHours} jam lalu";
    }else if(differentTime.inMinutes > 0){
      displayDifferentTime = "${differentTime.inMinutes} menit lalu";
    }else {
      displayDifferentTime = "Beberapa detik lalu";
    }

    return Column(
      children: <Widget>[
        Material(
          color: Colors.white,
          child: InkWell(
            onTap: () => onSelected(data),
            child: Container(
              width: double.infinity,
              padding: EdgeInsets.symmetric(
                vertical: adaptiveWidth(context, 10),
                horizontal: adaptiveWidth(context, 25),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  StyledText("${data.grade} - ${data.mapel}",
                    size: sp(context, 14),
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                    fontWeight: FontWeight.w600,
                  ),
                  SizedBox(height: adaptiveWidth(context, 6)),
                  StyledText(data.question,
                    size: sp(context, 12),
                  ),
                  SizedBox(height: adaptiveWidth(context, 12)),
                  data.image != "" ? CachedNetworkImage(
                    imageUrl: data.image,
                    imageBuilder: (_, provider) => Container(
                      width: double.infinity,
                      height: adaptiveWidth(context, 150),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(adaptiveWidth(context, 10)),
                        image: DecorationImage(
                          image: provider,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    placeholder: (_, __) => Shimmer.fromColors(
                      child: Container(
                        width: double.infinity,
                        height: adaptiveWidth(context, 150),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(adaptiveWidth(context, 10)),
                          color: Colors.grey[200],
                        ),
                      ),
                      baseColor: Colors.grey[200],
                      highlightColor: Colors.white,
                    ),
                    errorWidget: (_, __, ___) => Container(
                      width: double.infinity,
                      height: adaptiveWidth(context, 150),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(adaptiveWidth(context, 10)),
                        image: DecorationImage(
                          image: AssetImage(ImageUtils.image_not_found),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ) : SizedBox(),
                  SizedBox(height: adaptiveWidth(context, 15)),
                  StyledText(displayDifferentTime,
                    size: sp(context, 14),
                    fontWeight: FontWeight.w600,
                    color: ColorUtils.grey7070,
                  ),
                ],
              ),
            ),
          ),
        ),
        SizedBox(height: adaptiveWidth(context, 5)),
      ],
    );
  }
}

class ShimmerMyForum extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          color: Colors.white,
          padding: EdgeInsets.symmetric(
            vertical: adaptiveWidth(context, 10),
            horizontal: adaptiveWidth(context, 25),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Shimmer.fromColors(
                child: Container(
                  width: adaptiveWidth(context, 250),
                  height: sp(context, 14),
                  color: Colors.grey[200],
                ),
                baseColor: Colors.grey[200],
                highlightColor: Colors.white,
              ),
              SizedBox(height: adaptiveWidth(context, 12)),
              Shimmer.fromColors(
                child: Container(
                  width: double.infinity,
                  height: adaptiveWidth(context, 150),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(adaptiveWidth(context, 10)),
                    color: Colors.grey[200],
                  ),
                ),
                baseColor: Colors.grey[200],
                highlightColor: Colors.white,
              ),
              SizedBox(height: adaptiveWidth(context, 15)),
              Shimmer.fromColors(
                child: Container(
                  width: adaptiveWidth(context, 100),
                  height: sp(context, 14),
                  color: Colors.grey[200],
                ),
                baseColor: Colors.grey[200],
                highlightColor: Colors.white,
              )
            ],
          ),
        ),
        SizedBox(height: adaptiveWidth(context, 5)),
      ],
    );
  }
}
