import 'package:dio/dio.dart';
import 'package:halokes_siswa/ancestor/BasePresenter.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/network/response/management/ForumGradeResponse.dart';
import 'package:halokes_siswa/network/response/management/ForumMapelResponse.dart';
import 'package:halokes_siswa/network/response/management/MyForumResponse.dart';
import 'package:halokes_siswa/ui/forum/list/fragment/my_forum/FragmentMyForumDelegate.dart';
import 'package:mcnmr_request_wrapper/RequestWrapper.dart';

class FragmentMyForumPresenter extends BasePresenter {
  static const REQUEST_GET_MY_FORUM = 0;
  static const REQUEST_LOAD_MORE_MY_FORUM = 1;

  bool _isLoadMore = false;
  CancelToken _forumCancelToken;

  final FragmentMyForumDelegate _delegate;

  FragmentMyForumPresenter(BaseState state, this._delegate) : super(state);

  void executeGetMyForum(RequestWrapper<MyForumResponse> wrapper,
      ForumGradeData selectedGrade, ForumMapelData selectedMapel){

    wrapper.doRequest();

    _forumCancelToken?.cancel();
    _forumCancelToken = CancelToken();
    _isLoadMore = true;

    managementRepo.executeGetMyForum(REQUEST_GET_MY_FORUM, "0",
      selectedGrade.idGrade, selectedMapel.idMapel, (response){
        if(response.data.length == 0){
          _delegate.alreadyLastItem();
        }else {
          if(response.data.length % 10 > 0){
            _delegate.alreadyLastItem();
          }

          wrapper.finishRequest(response);
        }
      }, cancelToken: _forumCancelToken,
    );
  }

  void executeLoadMoreMyForum(RequestWrapper<MyForumResponse> wrapper,
      ForumGradeData selectedGrade, ForumMapelData selectedMapel, String lastForumId){
    if(_isLoadMore) return;

    wrapper.doRequestKeepState();

    _forumCancelToken?.cancel();
    _forumCancelToken = CancelToken();
    _isLoadMore = true;

    managementRepo.executeGetMyForum(REQUEST_GET_MY_FORUM,
      selectedGrade.idGrade, selectedMapel.idMapel, lastForumId, (response) {
        if(response.data.length == 0){
          _delegate.alreadyLastItem();
        }else {
          if(response.data.length % 10 > 0){
            _delegate.alreadyLastItem();
          }

          wrapper.finishRequest(wrapper.result..merge(response));
        }

        _isLoadMore = false;
      }, cancelToken: _forumCancelToken,
    );
  }
}