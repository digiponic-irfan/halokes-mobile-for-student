import 'package:flutter/material.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/main.dart';
import 'package:halokes_siswa/network/response/management/ForumGradeResponse.dart';
import 'package:halokes_siswa/network/response/management/ForumMapelResponse.dart';
import 'package:halokes_siswa/network/response/management/MyForumResponse.dart';
import 'package:halokes_siswa/ui/forum/list/fragment/my_forum/FragmentMyForumDelegate.dart';
import 'package:halokes_siswa/ui/forum/list/fragment/my_forum/FragmentMyForumPresenter.dart';
import 'package:halokes_siswa/ui/forum/list/fragment/my_forum/adapter/MyForumAdapter.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';
import 'package:mcnmr_common_ext/FutureDelayed.dart';
import 'package:mcnmr_request_wrapper/RequestWrapper.dart';
import 'package:mcnmr_request_wrapper/RequestWrapperWidget.dart';
import 'package:halokes_siswa/extension/BaseStateExt.dart';

class FragmentMyForum extends StatefulWidget {
  final state = _FragmentMyForumState();

  ForumMapelData get selectedMapel => state.selectedForumMapel;
  ForumGradeData get selectedGrade => state.selectedForumGrade;

  @override
  _FragmentMyForumState createState() => state;

  void refilter(ForumGradeData grade, ForumMapelData mapel){
    state.refilter(grade, mapel);
  }
}

class _FragmentMyForumState extends BaseState<FragmentMyForum>
    with AutomaticKeepAliveClientMixin, FragmentMyForumDelegate{

  FragmentMyForumPresenter _presenter;
  RequestWrapper<MyForumResponse> _myForumWrapper = RequestWrapper();

  bool _isAlreadyLastItem = false;
  ForumGradeData selectedForumGrade = ForumGradeData.all();
  ForumMapelData selectedForumMapel = ForumMapelData.all();

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    _presenter = FragmentMyForumPresenter(this, this);
  }

  @override
  void afterWidgetBuilt() => _presenter.executeGetMyForum(_myForumWrapper, selectedForumGrade, selectedForumMapel);

  @override
  void shouldShowLoading(int typeRequest) {}

  @override
  void shouldHideLoading(int typeRequest) {}

  @override
  void onRequestTimeOut(int typeRequest) {
    if(typeRequest == FragmentMyForumPresenter.REQUEST_GET_MY_FORUM){
      delay(5000, () => _presenter.executeGetMyForum(_myForumWrapper, selectedForumGrade, selectedForumMapel));
    }else if(typeRequest == FragmentMyForumPresenter.REQUEST_LOAD_MORE_MY_FORUM){
      delay(5000, () => _presenter.executeLoadMoreMyForum(_myForumWrapper, selectedForumGrade,
          selectedForumMapel, _myForumWrapper.result.data.last.idForum));
    }
  }

  @override
  void onNoConnection(int typeRequest) {
    if(typeRequest == FragmentMyForumPresenter.REQUEST_GET_MY_FORUM){
      delay(5000, () => _presenter.executeGetMyForum(_myForumWrapper, selectedForumGrade, selectedForumMapel));
    }else if(typeRequest == FragmentMyForumPresenter.REQUEST_LOAD_MORE_MY_FORUM){
      delay(5000, () => _presenter.executeLoadMoreMyForum(_myForumWrapper, selectedForumGrade,
          selectedForumMapel, _myForumWrapper.result.data.last.idForum));
    }
  }

  @override
  void alreadyLastItem() => setState(() => _isAlreadyLastItem = true);

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      backgroundColor: ColorUtils.greyfafa,
      body: RequestWrapperWidget<MyForumResponse>(
        requestWrapper: _myForumWrapper,
        placeholder: (_) => ListView.builder(
          itemCount: 2,
          itemBuilder: (_, index) => ShimmerMyForum(),
        ),
        builder: (_, data) => RefreshIndicator(
          color: Colors.white,
          backgroundColor: ColorUtils.primary,
          onRefresh: () async {
            _presenter.executeGetMyForum(_myForumWrapper, selectedForumGrade, selectedForumMapel);
            setState(() => _isAlreadyLastItem = false);
          },
          child: NotificationListener<ScrollUpdateNotification>(
            onNotification: (it){
              if(!_isAlreadyLastItem){
                if(it.metrics.pixels == it.metrics.maxScrollExtent){
                  _presenter.executeLoadMoreMyForum(_myForumWrapper, selectedForumGrade,
                      selectedForumMapel, data.data.last.idForum);
                }
              }

              return true;
            },
            child: ListView.builder(
              itemCount: _isAlreadyLastItem ? data.data.length : data.data.length + 1,
              itemBuilder: (_, index){
                if(!_isAlreadyLastItem){
                  if(index == data.data.length){
                    return Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.all(width(10)),
                          child: StyledText("Sedang memuat lebih banyak...",
                            size: sp(12),
                          ),
                        ),
                        SizedBox(height: 40),
                      ],
                    );
                  }
                }

                return MyForumItem(data: data.data[index],
                  onSelected: _onForumSelected,
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  void _onForumSelected(MyForumData data){
    navigateTo(MyApp.ROUTE_FORUM_DETAIL_MINE, arguments: data);
  }

  void refilter(ForumGradeData grade, ForumMapelData mapel){
    _isAlreadyLastItem = false;
    selectedForumGrade = grade;
    selectedForumMapel = mapel;
    _presenter.executeGetMyForum(_myForumWrapper, selectedForumGrade, selectedForumMapel);
  }
}
