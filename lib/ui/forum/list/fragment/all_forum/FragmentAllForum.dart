import 'package:flutter/material.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/main.dart';
import 'package:halokes_siswa/network/response/management/AllForumResponse.dart';
import 'package:halokes_siswa/network/response/management/ForumGradeResponse.dart';
import 'package:halokes_siswa/network/response/management/ForumMapelResponse.dart';
import 'package:halokes_siswa/ui/forum/list/fragment/all_forum/FragmentAllForumDelegate.dart';
import 'package:halokes_siswa/ui/forum/list/fragment/all_forum/FragmentAllForumPresenter.dart';
import 'package:halokes_siswa/ui/forum/list/fragment/all_forum/adapter/AllForumAdapter.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';
import 'package:mcnmr_common_ext/FutureDelayed.dart';
import 'package:mcnmr_request_wrapper/RequestWrapper.dart';
import 'package:mcnmr_request_wrapper/RequestWrapperWidget.dart';
import 'package:halokes_siswa/extension/BaseStateExt.dart';

class FragmentAllForum extends StatefulWidget {
  final state = _FragmentAllForumState();

  ForumMapelData get selectedMapel => state.selectedForumMapel;
  ForumGradeData get selectedGrade => state.selectedForumGrade;

  @override
  _FragmentAllForumState createState() => state;

  void refilter(ForumGradeData grade, ForumMapelData mapel){
    state.refilter(grade, mapel);
  }
}

class _FragmentAllForumState extends BaseState<FragmentAllForum>
    with AutomaticKeepAliveClientMixin, FragmentAllForumDelegate{

  FragmentAllForumPresenter _presenter;
  RequestWrapper<AllForumResponse> _allForumWrapper = RequestWrapper();

  bool _isAlreadyLastItem = false;
  ForumGradeData selectedForumGrade = ForumGradeData.all();
  ForumMapelData selectedForumMapel = ForumMapelData.all();

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    _presenter = FragmentAllForumPresenter(this, this);
  }

  @override
  void afterWidgetBuilt() => _presenter.executeGetAllForum(_allForumWrapper, selectedForumGrade, selectedForumMapel);

  @override
  void shouldShowLoading(int typeRequest) {}

  @override
  void shouldHideLoading(int typeRequest) {}

  @override
  void onRequestTimeOut(int typeRequest) {
    if(typeRequest == FragmentAllForumPresenter.REQUEST_GET_ALL_FORUM){
      delay(5000, () => _presenter.executeGetAllForum(_allForumWrapper, selectedForumGrade, selectedForumMapel));
    }else if(typeRequest == FragmentAllForumPresenter.REQUEST_LOAD_MORE_FORUM){
      delay(5000, () => _presenter.executeLoadMoreForum(_allForumWrapper, selectedForumGrade,
          selectedForumMapel, _allForumWrapper.result.data.last.idForum));
    }
  }

  @override
  void onNoConnection(int typeRequest) {
    if(typeRequest == FragmentAllForumPresenter.REQUEST_GET_ALL_FORUM){
      delay(5000, () => _presenter.executeGetAllForum(_allForumWrapper, selectedForumGrade, selectedForumMapel));
    }else if(typeRequest == FragmentAllForumPresenter.REQUEST_LOAD_MORE_FORUM){
      delay(5000, () => _presenter.executeLoadMoreForum(_allForumWrapper, selectedForumGrade,
          selectedForumMapel, _allForumWrapper.result.data.last.idForum));
    }
  }

  @override
  void alreadyLastItem() => setState(() => _isAlreadyLastItem = true);

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      backgroundColor: ColorUtils.greyfafa,
      body: RequestWrapperWidget<AllForumResponse>(
        requestWrapper: _allForumWrapper,
        placeholder: (_) => ListView.builder(
          itemCount: 2,
          itemBuilder: (_, index) => ShimmerAllForum(),
        ),
        builder: (_, data) => RefreshIndicator(
          color: Colors.white,
          backgroundColor: ColorUtils.primary,
          onRefresh: () async {
            _presenter.executeGetAllForum(_allForumWrapper, selectedForumGrade, selectedForumMapel);
            setState(() => _isAlreadyLastItem = false);
          },
          child: NotificationListener<ScrollUpdateNotification>(
            onNotification: (it){
              if(!_isAlreadyLastItem){
                if(it.metrics.pixels == it.metrics.maxScrollExtent){
                  _presenter.executeLoadMoreForum(_allForumWrapper, selectedForumGrade,
                      selectedForumMapel, data.data.last.idForum);
                }
              }

              return true;
            },
            child: ListView.builder(
              itemCount: _isAlreadyLastItem ? data.data.length : data.data.length + 1,
              itemBuilder: (_, index){
                if(!_isAlreadyLastItem){
                  if(index == data.data.length){
                    return Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.all(width(10)),
                          child: StyledText("Sedang memuat lebih banyak...",
                            size: sp(12),
                          ),
                        ),
                        SizedBox(height: 40),
                      ],
                    );
                  }
                }

                return AllForumItem(data: data.data[index],
                  onSelected: _onForumSelected,
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  void _onForumSelected(AllForumData data){
    navigateTo(MyApp.ROUTE_FORUM_DETAIL_OTHER, arguments: data);
  }

  void refilter(ForumGradeData grade, ForumMapelData mapel){
    _isAlreadyLastItem = false;
    selectedForumGrade = grade;
    selectedForumMapel = mapel;
    _presenter.executeGetAllForum(_allForumWrapper, selectedForumGrade, selectedForumMapel);
  }
}
