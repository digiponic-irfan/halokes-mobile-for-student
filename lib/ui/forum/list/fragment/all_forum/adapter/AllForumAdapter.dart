import 'package:flutter/material.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/extension/Size.dart';
import 'package:halokes_siswa/external_plugin/cached_network_image/src/cached_image_widget.dart';
import 'package:halokes_siswa/network/response/management/AllForumResponse.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';
import 'package:halokes_siswa/utils/ImageUtils.dart';
import 'package:shimmer/shimmer.dart';

class AllForumItem extends StatelessWidget {
  final AllForumData data;
  final Function(AllForumData) onSelected;

  AllForumItem({@required this.data, @required this.onSelected}) :
        assert(data != null), assert(onSelected != null);

  @override
  Widget build(BuildContext context) {
    var differentTime = DateTime.now().difference(data.time);
    var displayDifferentTime = "";

    if(differentTime.inDays > 0){
      if(differentTime.inDays < 30){
        displayDifferentTime = "${differentTime.inDays} hari lalu";
      }else {
        displayDifferentTime = data.displayTime;
      }
    }else if(differentTime.inHours > 0){
      displayDifferentTime = "${differentTime.inHours} jam lalu";
    }else if(differentTime.inMinutes > 0){
      displayDifferentTime = "${differentTime.inMinutes} menit lalu";
    }else {
      displayDifferentTime = "Beberapa detik lalu";
    }

    return Column(
      children: <Widget>[
        Material(
          color: Colors.white,
          child: InkWell(
            onTap: () => onSelected(data),
            child: Container(
              padding: EdgeInsets.symmetric(
                vertical: adaptiveWidth(context, 10),
                horizontal: adaptiveWidth(context, 25),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      CachedNetworkImage(
                        imageUrl: data.questionerFoto,
                        imageBuilder: (_, provider) => Container(
                          width: adaptiveWidth(context, 50),
                          height: adaptiveWidth(context, 50),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                              image: provider,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        placeholder: (_, __) => Shimmer.fromColors(
                          child: Container(
                            width: adaptiveWidth(context, 50),
                            height: adaptiveWidth(context, 50),
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.grey[200],
                            ),
                          ),
                          baseColor: Colors.grey[200],
                          highlightColor: Colors.white,
                        ),
                        errorWidget: (_, __, ___) => Container(
                          width: adaptiveWidth(context, 50),
                          height: adaptiveWidth(context, 50),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                              image: AssetImage(ImageUtils.missing_avatar),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(width: adaptiveWidth(context, 12)),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          StyledText(data.questionerName,
                            size: sp(context, 13),
                            fontWeight: FontWeight.w600,
                          ),
                          SizedBox(height: adaptiveWidth(context, 3)),
                          StyledText("${data.questionerRole} di ${data.sekolahNama}",
                            size: sp(context, 11),
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(height: adaptiveWidth(context, 12)),
                  StyledText("${data.grade} - ${data.mapel}",
                    size: sp(context, 14),
                    fontWeight: FontWeight.w600,
                  ),
                  SizedBox(height: adaptiveWidth(context, 6)),
                  StyledText(data.question,
                    size: sp(context, 12),
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                  ),
                  SizedBox(height: adaptiveWidth(context, 12)),
                  data.image != "" ? CachedNetworkImage(
                    imageUrl: data.image,
                    imageBuilder: (_, provider) => Container(
                      width: double.infinity,
                      height: adaptiveWidth(context, 150),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(adaptiveWidth(context, 10)),
                        image: DecorationImage(
                          image: provider,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    placeholder: (_, __) => Shimmer.fromColors(
                      child: Container(
                        width: double.infinity,
                        height: adaptiveWidth(context, 150),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(adaptiveWidth(context, 10)),
                          color: Colors.grey[200],
                        ),
                      ),
                      baseColor: Colors.grey[200],
                      highlightColor: Colors.white,
                    ),
                    errorWidget: (_, __, ___) => Container(
                      width: double.infinity,
                      height: adaptiveWidth(context, 150),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(adaptiveWidth(context, 10)),
                        image: DecorationImage(
                          image: AssetImage(ImageUtils.image_not_found),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ) : SizedBox(),
                  SizedBox(height: adaptiveWidth(context, 15)),
                  StyledText(displayDifferentTime,
                    size: sp(context, 14),
                    fontWeight: FontWeight.w600,
                    color: ColorUtils.grey7070,
                  ),
                ],
              ),
            ),
          ),
        ),
        SizedBox(height: adaptiveWidth(context, 5)),
      ],
    );
  }
}

class ShimmerAllForum extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          color: Colors.white,
          padding: EdgeInsets.symmetric(
            vertical: adaptiveWidth(context, 10),
            horizontal: adaptiveWidth(context, 25),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Shimmer.fromColors(
                    child: Container(
                      width: adaptiveWidth(context, 50),
                      height: adaptiveWidth(context, 50),
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.grey[200],
                      ),
                    ),
                    baseColor: Colors.grey[200],
                    highlightColor: Colors.white,
                  ),
                  SizedBox(width: adaptiveWidth(context, 12)),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Shimmer.fromColors(
                        child: Container(
                          width: adaptiveWidth(context, 150),
                          height: sp(context, 12),
                          color: Colors.grey[200],
                        ),
                        baseColor: Colors.grey[200],
                        highlightColor: Colors.white,
                      ),
                      SizedBox(height: adaptiveWidth(context, 3)),
                      Shimmer.fromColors(
                        child: Container(
                          width: adaptiveWidth(context, 75),
                          height: sp(context, 11),
                          color: Colors.grey[200],
                        ),
                        baseColor: Colors.grey[200],
                        highlightColor: Colors.white,
                      ),
                    ],
                  ),
                ],
              ),
              SizedBox(height: adaptiveWidth(context, 12)),
              Shimmer.fromColors(
                child: Container(
                  width: adaptiveWidth(context, 250),
                  height: sp(context, 14),
                  color: Colors.grey[200],
                ),
                baseColor: Colors.grey[200],
                highlightColor: Colors.white,
              ),
              SizedBox(height: adaptiveWidth(context, 12)),
              Shimmer.fromColors(
                child: Container(
                  width: double.infinity,
                  height: adaptiveWidth(context, 150),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(adaptiveWidth(context, 10)),
                    color: Colors.grey[200],
                  ),
                ),
                baseColor: Colors.grey[200],
                highlightColor: Colors.white,
              ),
              SizedBox(height: adaptiveWidth(context, 15)),
              Shimmer.fromColors(
                child: Container(
                  width: adaptiveWidth(context, 100),
                  height: sp(context, 14),
                  color: Colors.grey[200],
                ),
                baseColor: Colors.grey[200],
                highlightColor: Colors.white,
              )
            ],
          ),
        ),
        SizedBox(height: adaptiveWidth(context, 5)),
      ],
    );
  }
}
