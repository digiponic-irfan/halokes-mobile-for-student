import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/custom/view/chip/Chip.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/extension/BaseStateExt.dart';
import 'package:flutter/material.dart';
import 'package:halokes_siswa/network/response/management/ForumGradeResponse.dart';
import 'package:halokes_siswa/network/response/management/ForumMapelResponse.dart';
import 'package:halokes_siswa/ui/forum/filter/FilterForumArgument.dart';
import 'package:halokes_siswa/ui/forum/filter/FilterForumPresenter.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';
import 'package:mcnmr_common_ext/FutureDelayed.dart';
import 'package:mcnmr_request_wrapper/RequestWrapper.dart';
import 'package:mcnmr_request_wrapper/RequestWrapperWidget.dart';

class FilterForum extends StatefulWidget {
  final FilterForumArgument argument;

  FilterForum({@required this.argument});

  @override
  _FilterForumState createState() => _FilterForumState();
}

class _FilterForumState extends BaseState<FilterForum> {
  FilterForumPresenter _presenter;
  RequestWrapper<ForumGradeResponse> _gradeWrapper = RequestWrapper();
  RequestWrapper<ForumMapelResponse> _mapelWrapper = RequestWrapper();

  @override
  void initState() {
    super.initState();
    _presenter = FilterForumPresenter(this);
  }

  @override
  void afterWidgetBuilt() {
    _presenter.executeGetForumGrade(_gradeWrapper);
    _presenter.executeGetForumMapel(_mapelWrapper);
  }

  @override
  void shouldShowLoading(int typeRequest) {}

  @override
  void shouldHideLoading(int typeRequest) {}

  @override
  void onRequestTimeOut(int typeRequest) {
    if(typeRequest == FilterForumPresenter.REQUEST_GET_FORUM_GRADE){
      delay(5000, () => _presenter.executeGetForumGrade(_gradeWrapper));
    }else if(typeRequest == FilterForumPresenter.REQUEST_GET_FORUM_MAPEL){
      delay(5000, () => _presenter.executeGetForumMapel(_mapelWrapper));
    }
  }

  @override
  void onNoConnection(int typeRequest) {
    if(typeRequest == FilterForumPresenter.REQUEST_GET_FORUM_GRADE){
      delay(5000, () => _presenter.executeGetForumGrade(_gradeWrapper));
    }else if(typeRequest == FilterForumPresenter.REQUEST_GET_FORUM_MAPEL){
      delay(5000, () => _presenter.executeGetForumMapel(_mapelWrapper));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black.withOpacity(0.5),
      appBar: AppBar(
        backgroundColor: ColorUtils.greyfafa,
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: finish,
        ),
      ),
      body: Container(
        color: ColorUtils.greyfafa,
        width: double.infinity,
        child: SingleChildScrollView(
          padding: EdgeInsets.all(width(10)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              StyledText("Jenjang Pendidikan",
                size: sp(14),
                fontWeight: FontWeight.w600,
              ),
              SizedBox(height: width(10)),
              RequestWrapperWidget<ForumGradeResponse>(
                requestWrapper: _gradeWrapper,
                placeholder: (_) => Wrap(
                  spacing: 10,
                  runSpacing: 10,
                  children: <Widget>[
                    ShimmerChip(),
                    ShimmerChip(),
                    ShimmerChip(),
                  ],
                ),
                builder: (_, data) => Wrap(
                  spacing: 10,
                  runSpacing: 10,
                  children: data.data.map((it) => ChipItem<ForumGradeData>(item: it,
                    onSelected: (it) => setState(() => widget.argument.grade = it),
                    isSelected: (it) => it == widget.argument.grade,
                  )).toList(),
                ),
              ),
              SizedBox(height: width(20)),
              StyledText("Mata Pelajaran",
                size: sp(14),
                fontWeight: FontWeight.w600,
              ),
              SizedBox(height: width(10)),
              RequestWrapperWidget<ForumMapelResponse>(
                requestWrapper: _mapelWrapper,
                placeholder: (_) => Wrap(
                  spacing: 10,
                  runSpacing: 10,
                  children: <Widget>[
                    ShimmerChip(),
                    ShimmerChip(),
                    ShimmerChip(),
                  ],
                ),
                builder: (_, data) => Wrap(
                  spacing: 10,
                  runSpacing: 10,
                  children: data.data.map((it) => ChipItem<ForumMapelData>(item: it,
                    onSelected: (it) => setState(() => widget.argument.mapel = it),
                    isSelected: (it) => it == widget.argument.mapel,
                  )).toList(),
                ),
              ),
              SizedBox(height: width(20)),
              Row(
                children: <Widget>[
                  Spacer(),
                  MaterialButton(
                    onPressed: () => finish(result: widget.argument),
                    color: ColorUtils.primary,
                    height: 40,
                    minWidth: 100,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: StyledText("Filter",
                      color: Colors.white,
                      size: sp(14),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
