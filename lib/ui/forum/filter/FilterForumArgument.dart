import 'package:halokes_siswa/network/response/management/ForumGradeResponse.dart';
import 'package:halokes_siswa/network/response/management/ForumMapelResponse.dart';

class FilterForumArgument {
  ForumGradeData grade;
  ForumMapelData mapel;

  FilterForumArgument(this.grade, this.mapel);
}