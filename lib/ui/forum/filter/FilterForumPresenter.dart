import 'package:halokes_siswa/ancestor/BasePresenter.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/network/response/management/ForumGradeResponse.dart';
import 'package:halokes_siswa/network/response/management/ForumMapelResponse.dart';
import 'package:mcnmr_request_wrapper/RequestWrapper.dart';

class FilterForumPresenter extends BasePresenter {
  static const REQUEST_GET_FORUM_MAPEL = 0;
  static const REQUEST_GET_FORUM_GRADE = 1;

  FilterForumPresenter(BaseState state) : super(state);

  void executeGetForumMapel(RequestWrapper<ForumMapelResponse> wrapper){
    wrapper.doRequest();
    managementRepo.executeGetForumMapel(REQUEST_GET_FORUM_MAPEL, (response){
      response.data.insert(0, ForumMapelData.all());
      wrapper.finishRequest(response);
    });
  }

  void executeGetForumGrade(RequestWrapper<ForumGradeResponse> wrapper){
    wrapper.doRequest();
    managementRepo.executeGetForumGrade(REQUEST_GET_FORUM_GRADE, (response){
      response.data.insert(0, ForumGradeData.all());
      wrapper.finishRequest(response);
    });
  }
}