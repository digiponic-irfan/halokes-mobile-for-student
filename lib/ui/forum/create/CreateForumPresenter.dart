import 'dart:io';

import 'package:dio/dio.dart';
import 'package:halokes_siswa/ancestor/BasePresenter.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/network/response/management/ForumGradeResponse.dart';
import 'package:halokes_siswa/network/response/management/ForumMapelResponse.dart';
import 'package:halokes_siswa/ui/forum/create/CreateForumDelegate.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mcnmr_request_wrapper/RequestWrapper.dart';

class CreateForumPresenter extends BasePresenter {
  static const REQUEST_GET_GRADE = 0;
  static const REQUEST_GET_MAPEL = 1;
  static const REQUEST_CREATE_FORUM = 2;

  final CreateForumDelegate _delegate;
  CreateForumPresenter(BaseState state, this._delegate) : super(state);

  Future<File> captureImage() async {
    var file = await ImagePicker.pickImage(
      source: ImageSource.camera,
      imageQuality: 40,
    );

    return file;
  }

  Future<File> getFromGallery() async {
    var file = await ImagePicker.pickImage(
      source: ImageSource.gallery,
      imageQuality: 80,
    );

    return file;
  }

  void executeGetGrade(RequestWrapper<ForumGradeResponse> wrapper){
    wrapper.doRequest();
    managementRepo.executeGetForumGrade(REQUEST_GET_GRADE, wrapper.finishRequest);
  }

  void executeGetMapel(RequestWrapper<ForumMapelResponse> wrapper){
    wrapper.doRequest();
    managementRepo.executeGetForumMapel(REQUEST_GET_MAPEL, wrapper.finishRequest);
  }

  void executeCreateForum(ForumGradeData selectedGrade,
      ForumMapelData selectedMapel, File image, String question){
    if(selectedGrade == null){
      state.alert(title: "Error", message: "Tingkat pendidikan wajib dipilih!");
      return;
    }

    if(selectedMapel == null){
      state.alert(title: "Error", message: "Mata pelajaran wajib dipilih!");
      return;
    }

    if(question == ""){
      state.alert(title: "Error", message: "Pertanyaan wajib diisi!");
      return;
    }

    var params = <String, dynamic>{};
    params["grade"] = selectedGrade.idGrade;
    params["mapel"] = selectedMapel.idMapel;
    params["question"] = question;

    if(image != null){
      params["image"] = MultipartFile.fromFileSync(image.path, filename: "my_question_file.jpg");
    }

    managementRepo.executeCreateForum(REQUEST_CREATE_FORUM, params, _delegate.onSuccessCreateForum);
  }
}