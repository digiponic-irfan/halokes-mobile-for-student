import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:halokes_siswa/ancestor/BaseLifecycleState.dart';
import 'package:halokes_siswa/ancestor/BaseResponse.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/main.dart';
import 'package:halokes_siswa/network/response/management/ForumGradeResponse.dart';
import 'package:halokes_siswa/network/response/management/ForumMapelResponse.dart';
import 'package:halokes_siswa/ui/forum/create/CreateForumDelegate.dart';
import 'package:halokes_siswa/ui/forum/create/CreateForumPresenter.dart';
import 'package:halokes_siswa/ui/image_previewer/ImagePreviewerArgument.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';
import 'package:halokes_siswa/extension/BaseStateExt.dart';
import 'package:halokes_siswa/utils/FontUtils.dart';
import 'package:mcnmr_common_ext/FutureDelayed.dart';
import 'package:mcnmr_request_wrapper/RequestWrapper.dart';
import 'package:mcnmr_request_wrapper/RequestWrapperWidget.dart';
import 'package:mcnmr_result_object_observer/BothObserverBuilder.dart';
import 'package:mcnmr_result_object_observer/Holder.dart';
import 'package:shimmer/shimmer.dart';

class CreateForum extends StatefulWidget {
  @override
  _CreateForumState createState() => _CreateForumState();
}

class _CreateForumState extends BaseLifecycleState<CreateForum> implements CreateForumDelegate{
  CreateForumPresenter _presenter;
  RequestWrapper<ForumGradeResponse> _gradeWrapper = RequestWrapper();
  RequestWrapper<ForumMapelResponse> _mapelWrapper = RequestWrapper();

  TextEditingController _questionController = TextEditingController();

  ForumGradeData _selectedGrade;
  ForumMapelData _selectedMapel;
  String _question = "";
  Holder<File, bool> _imageHolder = Holder((it) => it != null, initialObject: null, initialResult: false);

  @override
  void initState() {
    super.initState();
    _presenter = CreateForumPresenter(this, this);
  }

  @override
  void afterWidgetBuilt() {
    _presenter.executeGetGrade(_gradeWrapper);
    _presenter.executeGetMapel(_mapelWrapper);
  }

  @override
  void shouldShowLoading(int typeRequest) {
    if(typeRequest == CreateForumPresenter.REQUEST_GET_GRADE ||
      typeRequest == CreateForumPresenter.REQUEST_GET_MAPEL) {

    }else {
      super.shouldShowLoading(typeRequest);
    }
  }

  @override
  void shouldHideLoading(int typeRequest) {
    if(typeRequest == CreateForumPresenter.REQUEST_GET_GRADE ||
        typeRequest == CreateForumPresenter.REQUEST_GET_MAPEL) {

    }else {
      super.shouldHideLoading(typeRequest);
    }
  }

  @override
  void onNoConnection(int typeRequest) {
    if(typeRequest == CreateForumPresenter.REQUEST_GET_GRADE){
      delay(5000, () => _presenter.executeGetGrade(_gradeWrapper));
    }else if(typeRequest == CreateForumPresenter.REQUEST_GET_MAPEL){
      delay(5000, () => _presenter.executeGetMapel(_mapelWrapper));
    }else {
      super.onNoConnection(typeRequest);
    }
  }

  @override
  void onRequestTimeOut(int typeRequest) {
    if(typeRequest == CreateForumPresenter.REQUEST_GET_GRADE){
      delay(5000, () => _presenter.executeGetGrade(_gradeWrapper));
    }else if(typeRequest == CreateForumPresenter.REQUEST_GET_MAPEL){
      delay(5000, () => _presenter.executeGetMapel(_mapelWrapper));
    }else {
      super.onRequestTimeOut(typeRequest);
    }
  }

  @override
  void onResume() {
    _questionController.text = _question;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorUtils.greyfafa,
      appBar: AppBar(
        title: StyledText("Forum Tanya Jawab", color: Colors.white),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.white),
          onPressed: finish,
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          height: fullHeight(withStatusbar: false, withToolbar: false),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                width: double.infinity,
                color: Colors.white,
                padding: EdgeInsets.all(width(15)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    StyledText("Buat forum tanya jawab",
                      fontWeight: FontWeight.w600,
                      size: sp(14),
                    ),
                    StyledText("Ajukan soal / pertanyaan yang Anda tidak ketahui jawabannya untuk dijawab oleh orang lain",
                      size: sp(12),
                    ),
                  ],
                ),
              ),
              SizedBox(height: width(10)),
              RequestWrapperWidget<ForumGradeResponse>(
                requestWrapper: _gradeWrapper,
                placeholder: (_) => Shimmer.fromColors(
                  child: Container(
                    height: width(50),
                    width: double.infinity,
                    color: Colors.grey[200],
                  ),
                  baseColor: Colors.grey[200],
                  highlightColor: Colors.white,
                ),
                builder: (_, data) => Container(
                  height: width(50),
                  width: double.infinity,
                  color: Colors.white,
                  padding: EdgeInsets.symmetric(
                    horizontal: width(15),
                  ),
                  child: DropdownButton<ForumGradeData>(
                    style: StyledText.style(
                      size: sp(12),
                    ),
                    items: data.data.map((it) => DropdownMenuItem<ForumGradeData>(
                      value: it,
                      child: StyledText(it.grade, size: sp(12)),
                    )).toList(),
                    hint: StyledText("Tingkat pendidikan",
                      size: sp(12),
                      color: ColorUtils.grey8585,
                    ),
                    value: _selectedGrade,
                    onChanged: (it) => setState(() => _selectedGrade = it),
                    isExpanded: true,
                    underline: SizedBox(),
                    iconDisabledColor: Colors.black,
                    iconEnabledColor: Colors.black,
                  ),
                ),
              ),
              SizedBox(height: width(10)),
              RequestWrapperWidget<ForumMapelResponse>(
                requestWrapper: _mapelWrapper,
                placeholder: (_) => Shimmer.fromColors(
                  child: Container(
                    height: width(50),
                    width: double.infinity,
                    color: Colors.grey[200],
                  ),
                  baseColor: Colors.grey[200],
                  highlightColor: Colors.white,
                ),
                builder: (_, data) => Container(
                  height: width(50),
                  width: double.infinity,
                  color: Colors.white,
                  padding: EdgeInsets.symmetric(
                    horizontal: width(15),
                  ),
                  child: DropdownButton<ForumMapelData>(
                    style: StyledText.style(
                      size: sp(12),
                    ),
                    items: data.data.map((it) => DropdownMenuItem<ForumMapelData>(
                      value: it,
                      child: StyledText(it.mapel, size: sp(12)),
                    )).toList(),
                    hint: StyledText("Mata pelajaran",
                      size: sp(12),
                      color: ColorUtils.grey8585,
                    ),
                    value: _selectedMapel,
                    onChanged: (it) => setState(() => _selectedMapel = it),
                    isExpanded: true,
                    underline: SizedBox(),
                    iconDisabledColor: Colors.black,
                    iconEnabledColor: Colors.black,
                  ),
                ),
              ),
              SizedBox(height: width(10)),
              Expanded(
                child: Container(
                  width: double.infinity,
                  height: double.infinity,
                  color: Colors.white,
                  child: TextFormField(
                    controller: _questionController,
                    onChanged: (it) => _question = it,
                    maxLines: null,
                    textInputAction: TextInputAction.newline,
                    keyboardType: TextInputType.multiline,
                    textCapitalization: TextCapitalization.sentences,
                    style: TextStyle(
                      fontSize: sp(12),
                      fontFamily: FontUtils.segoeui,
                    ),
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.all(width(15)),
                      border: InputBorder.none,
                      hintText: "Tulis pertanyaan Anda",
                      hintStyle: TextStyle(
                        fontSize: sp(12),
                        color: ColorUtils.grey8585,
                        fontFamily: FontUtils.segoeui,
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: width(10)),
              Container(
                padding: EdgeInsets.all(width(15)),
                child: BothObserverBuilder(
                  observer: _imageHolder,
                  builder: (_, file, status) => Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Visibility(
                        visible: status,
                        child: Container(
                          width: width(90),
                          height: width(90),
                          child: Stack(
                            overflow: Overflow.visible,
                            children: <Widget>[
                              Positioned(
                                bottom: 0,
                                left: 0,
                                child: Material(
                                  type: MaterialType.transparency,
                                  child: InkWell(
                                    onTap: status ? () => navigateTo(MyApp.ROUTE_IMAGE_PREVIEWER,
                                      arguments: ImagePreviewerArgument.fromFile(file),
                                    ) : null,
                                    child: status ? Image.file(file,
                                      width: width(80),
                                      height: width(80),
                                      fit: BoxFit.cover,
                                    ) : SizedBox(),
                                  ),
                                ),
                              ),
                              Positioned(
                                top: 0,
                                right: 0,
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(width(15)),
                                  child: Material(
                                    color: Colors.red,
                                    child: InkWell(
                                      onTap: () => _imageHolder.object = null,
                                      child: Container(
                                        width: width(30),
                                        height: width(30),
                                        child: Icon(Icons.clear, color: Colors.white, size: width(15)),
                                      ),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      Visibility(
                        visible: !status,
                        child: Row(
                          children: <Widget>[
                            IconButton(
                              icon: Icon(Icons.camera_alt),
                              onPressed: () async => _imageHolder.object = await _presenter.captureImage(),
                            ),
                            SizedBox(width: width(10)),
                            IconButton(
                              icon: Icon(Icons.image),
                              onPressed: () async => _imageHolder.object = await _presenter.getFromGallery(),
                            ),
                          ],
                        ),
                      ),
                      Spacer(),
                      MaterialButton(
                        onPressed: () => _presenter.executeCreateForum(_selectedGrade, _selectedMapel,
                            _imageHolder.object, _question),
                        color: ColorUtils.primary,
                        height: 40,
                        minWidth: 140,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(50),
                        ),
                        child: StyledText("AJUKAN",
                          color: Colors.white,
                          size: sp(14),
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  void onSuccessCreateForum(BaseResponse response) {
    Fluttertoast.showToast(msg: "Forum berhasil dibuat, bisa dilihat di menu \"Pertanyaanku\"");
    finish(result: true);
  }
}
