import 'package:flutter/cupertino.dart';
import 'package:halokes_siswa/ancestor/BasePresenter.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/network/request/SocketForumRepository.dart';
import 'package:halokes_siswa/network/response/forum_chat/MarkAnswerResponse.dart';
import 'package:halokes_siswa/network/response/forum_chat/VoteAnswerResponse.dart';
import 'package:halokes_siswa/network/response/management/ForumAnswerResponse.dart';
import 'package:halokes_siswa/preference/AppPreference.dart';
import 'package:halokes_siswa/ui/forum/detail/DetailForumDelegate.dart';
import 'package:mcnmr_request_wrapper/RequestWrapper.dart';

class DetailForumPresenter extends BasePresenter {
  static const REQUEST_GET_FORUM_ANSWER = 0;
  static const REQUEST_WRITE_ANSWER = 1;
  static const REQUEST_UPVOTE = 2;
  static const REQUEST_DOWNVOTE = 3;
  static const REQUEST_MARK_ANSWER = 4;

  DetailForumDelegate _delegate;

  RequestWrapper<ForumAnswerResponse> answerWrapper = RequestWrapper();

  DetailForumPresenter(BaseState state, this._delegate) : super(state);

  void observeForum(String idUrlForum, RequestWrapper<ForumAnswerResponse> answerWrapper) async {
    this.answerWrapper = answerWrapper;

    forumSocketRepo.addEventListener(SocketForumRepository.EVENT_START_OBSERVING, _onConnect);
    forumSocketRepo.addEventListener(SocketForumRepository.EVENT_SERVER_SENDS_NEW_ANSWER, _onReceiveNewAnswer);
    forumSocketRepo.addEventListener(SocketForumRepository.EVENT_SOMEONE_UPVOTE_ANSWER, _onUpvote);
    forumSocketRepo.addEventListener(SocketForumRepository.EVENT_SOMEONE_DOWNVOTE_ANSWER, _onDownvote);
    forumSocketRepo.addEventListener(SocketForumRepository.EVENT_SOMEONE_MARK_ANSWER, _onMarkAnswer);
    forumSocketRepo.addEventListener(SocketForumRepository.EVENT_STOP_OBSERVING, _onDisconnect);

    forumSocketRepo.connect(
      header: {
        "x-halokes-user-url": (await AppPreference.getUser()).idUrl,
        "x-halokes-school-uid": (await AppPreference.getSchool()).sekolahUid,
        "x-halokes-user-role": "student",
      },
      query: {
        "x-halokes-forum-url": idUrlForum,
      },
    );
  }

  void stopObserveForum(){
    forumSocketRepo.disconnect();
  }

  void _onConnect(dynamic data){
    debugPrint("Socket forum event: Connected");
  }

  void _onReceiveNewAnswer(dynamic data){
    debugPrint("Socket forum event: Receive New Answer");
    debugPrint(data.toString());
  }

  void _onUpvote(dynamic data){
    VoteAnswerResponse response = VoteAnswerResponse.fromJson(data);

    var lastAnswer = answerWrapper.result;
    var replacedIndex = lastAnswer.data.indexOf(ForumAnswerData.justId(response.data.idAnswer));
    if(replacedIndex > -1){
      var replacedData = lastAnswer.data[replacedIndex];
      replacedData.upvoteCount = response.data.upvoteCount;
      replacedData.downvoteCount = response.data.downvoteCount;
      replacedData.yourVote = 1;
    }

    answerWrapper.finishRequest(lastAnswer);
  }

  void _onDownvote(dynamic data){
    VoteAnswerResponse response = VoteAnswerResponse.fromJson(data);

    var lastAnswer = answerWrapper.result;
    var replacedIndex = lastAnswer.data.indexOf(ForumAnswerData.justId(response.data.idAnswer));
    if(replacedIndex > -1){
      var replacedData = lastAnswer.data[replacedIndex];
      replacedData.upvoteCount = response.data.upvoteCount;
      replacedData.downvoteCount = response.data.downvoteCount;
      replacedData.yourVote = -1;
    }

    answerWrapper.finishRequest(lastAnswer);
  }

  void _onMarkAnswer(dynamic data){
    _delegate.onMarkedAsAnswer(MarkAnswerResponse.fromJson(data));
  }

  void _onDisconnect(dynamic data){
    debugPrint("Socket forum event: Disconnected");
  }

  void executeGetForumAnswer(RequestWrapper<ForumAnswerResponse> wrapper, String idUrlForum){
    wrapper.doRequest();
    managementRepo.executeGetForumAnswer(REQUEST_GET_FORUM_ANSWER, idUrlForum, wrapper.finishRequest);
  }

  void executeWriteAnswer(String answer, String detailAnswer, String idUrlForum){
    if(answer == ""){
      state.alert(title: "Error", message: "Jawaban wajib diisi!");
      return;
    }

    if(!forumSocketRepo.isConnected){
      state.onNoConnection(REQUEST_WRITE_ANSWER);
      return;
    }

    state.shouldShowLoading(REQUEST_WRITE_ANSWER);
    answerWrapper.doRequestKeepState();

    forumSocketRepo.emitWithCallback(SocketForumRepository.ACTION_WRITE_ANSWER,
        {"answer" : answer, "answer_desc" : detailAnswer}, (data){
      state.shouldHideLoading(REQUEST_WRITE_ANSWER);
      _delegate.onSuccessWriteAnswer();

      var lastAnswer = answerWrapper.result;
      lastAnswer.data.add(ForumAnswerData.fromJson(data["data"]));
      answerWrapper.finishRequest(lastAnswer);
    });
  }

  void executeUpvote(String idUrlAnswer){
    if(!forumSocketRepo.isConnected){
      state.onNoConnection(REQUEST_UPVOTE);
      return;
    }

    state.shouldShowLoading(REQUEST_UPVOTE);
    answerWrapper.doRequestKeepState();

    forumSocketRepo.emitWithCallback(SocketForumRepository.ACTION_UPVOTE_ANSWER,
        {"id_url_answer" : idUrlAnswer}, (data){
          state.shouldHideLoading(REQUEST_UPVOTE);
          _onUpvote(data);
    });
  }

  void executeDownvote(String idUrlAnswer){
    if(!forumSocketRepo.isConnected){
      state.onNoConnection(REQUEST_DOWNVOTE);
      return;
    }

    state.shouldShowLoading(REQUEST_DOWNVOTE);
    answerWrapper.doRequestKeepState();

    forumSocketRepo.emitWithCallback(SocketForumRepository.ACTION_DOWNVOTE_ANSWER,
        {"id_url_answer" : idUrlAnswer}, (data){
          state.shouldHideLoading(REQUEST_DOWNVOTE);
          _onDownvote(data);
    });
  }

  void executeMarkAnswer(String idUrlAnswer){
    if(!forumSocketRepo.isConnected){
      state.onNoConnection(REQUEST_MARK_ANSWER);
      return;
    }

    state.shouldShowLoading(REQUEST_MARK_ANSWER);

    forumSocketRepo.emitWithCallback(SocketForumRepository.ACTION_MARK_ANSWER,
        {"id_url_answer" : idUrlAnswer}, (data){
          state.shouldHideLoading(REQUEST_MARK_ANSWER);
          _onMarkAnswer(data);
    });
  }

}