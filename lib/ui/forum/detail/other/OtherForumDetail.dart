import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/extension/BaseStateExt.dart';
import 'package:flutter/material.dart';
import 'package:halokes_siswa/external_plugin/cached_network_image/cached_network_image.dart';
import 'package:halokes_siswa/network/response/management/AllForumResponse.dart';
import 'package:halokes_siswa/network/response/management/ForumAnswerResponse.dart';
import 'package:halokes_siswa/ui/forum/detail/DetailForumDelegate.dart';
import 'package:halokes_siswa/ui/forum/detail/DetailForumPresenter.dart';
import 'package:halokes_siswa/ui/forum/detail/adapter/AnswerAdapter.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';
import 'package:halokes_siswa/utils/FontUtils.dart';
import 'package:halokes_siswa/utils/ImageUtils.dart';
import 'package:mcnmr_common_ext/FutureDelayed.dart';
import 'package:mcnmr_request_wrapper/RequestWrapper.dart';
import 'package:mcnmr_request_wrapper/RequestWrapperWidget.dart';
import 'package:shimmer/shimmer.dart';

class OtherForumDetail extends StatefulWidget {
  final AllForumData argument;

  OtherForumDetail({@required this.argument});
  @override
  _OtherForumDetailState createState() => _OtherForumDetailState();
}

class _OtherForumDetailState extends BaseState<OtherForumDetail> with DetailForumDelegate{
  DetailForumPresenter _presenter;
  RequestWrapper<ForumAnswerResponse> _answerWrapper = RequestWrapper();

  String _answer = "";
  String _detailAnswer = "";

  TextEditingController _answerController = TextEditingController();
  TextEditingController _detailAnswerController = TextEditingController();
  
  @override
  void initState() {
    super.initState();
    _presenter = DetailForumPresenter(this, this);
  }

  @override
  void afterWidgetBuilt() {
    _presenter.observeForum(widget.argument.idUrlForum, _answerWrapper);
    _presenter.executeGetForumAnswer(_answerWrapper, widget.argument.idUrlForum);
  }

  @override
  void shouldShowLoading(int typeRequest) {
    if(typeRequest != DetailForumPresenter.REQUEST_GET_FORUM_ANSWER){
      super.shouldShowLoading(typeRequest);
    }
  }
  
  @override
  void shouldHideLoading(int typeRequest) {
    if(typeRequest != DetailForumPresenter.REQUEST_GET_FORUM_ANSWER){
      super.shouldHideLoading(typeRequest);
    }
  }
  
  @override
  void onNoConnection(int typeRequest) {
    if(typeRequest == DetailForumPresenter.REQUEST_GET_FORUM_ANSWER){
      delay(5000, () => _presenter.executeGetForumAnswer(_answerWrapper, widget.argument.idUrlForum));
    }else {
      super.onNoConnection(typeRequest);
    }
  }
  
  @override
  void onRequestTimeOut(int typeRequest) {
    if(typeRequest == DetailForumPresenter.REQUEST_GET_FORUM_ANSWER){
      delay(5000, () => _presenter.executeGetForumAnswer(_answerWrapper, widget.argument.idUrlForum));
    }else {
      super.onRequestTimeOut(typeRequest);
    }
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorUtils.greyfafa,
      appBar: AppBar(
        title: StyledText("Forum Tanya Jawab", color: Colors.white),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.white),
          onPressed: finish,
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              color: Colors.white,
              padding: EdgeInsets.all(width(10)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      CachedNetworkImage(
                        imageUrl: widget.argument.questionerFoto,
                        imageBuilder: (_, provider) => Container(
                          width: width(50),
                          height: width(50),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                              image: provider,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        placeholder: (_, __) => Shimmer.fromColors(
                          child: Container(
                            width: width(50),
                            height: width(50),
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.grey[200],
                            ),
                          ),
                          baseColor: Colors.grey[200],
                          highlightColor: Colors.white,
                        ),
                        errorWidget: (_, __, ___) => Container(
                          width: width(50),
                          height: width(50),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                              image: AssetImage(ImageUtils.missing_avatar),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(width: width(12)),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          StyledText(widget.argument.questionerName,
                            size: sp(13),
                            fontWeight: FontWeight.w600,
                          ),
                          SizedBox(height: width(3)),
                          StyledText(widget.argument.sekolahNama,
                            size: sp(11),
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(height: width(12)),
                  StyledText("${widget.argument.grade} - ${widget.argument.mapel}",
                    size: sp(14),
                    fontWeight: FontWeight.w600,
                  ),
                  SizedBox(height: width(6)),
                  StyledText(widget.argument.question,
                    size: sp(12),
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                  ),
                  SizedBox(height: width(12)),
                  widget.argument.image != "" ? Stack(
                    children: <Widget>[
                      CachedNetworkImage(
                        imageUrl: widget.argument.image,
                        imageBuilder: (_, provider) => Container(
                          width: double.infinity,
                          height: width(150),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(width(10)),
                            image: DecorationImage(
                              image: provider,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        placeholder: (_, __) => Shimmer.fromColors(
                          child: Container(
                            width: double.infinity,
                            height: width(150),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(width(10)),
                              color: Colors.grey[200],
                            ),
                          ),
                          baseColor: Colors.grey[200],
                          highlightColor: Colors.white,
                        ),
                        errorWidget: (_, __, ___) => Container(
                          width: double.infinity,
                          height: width(150),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(width(10)),
                            image: DecorationImage(
                              image: AssetImage(ImageUtils.image_not_found),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      ),
                      Positioned.fill(
                        child: InkWell(
                          onTap: (){},
                        ),
                      )
                    ],
                  ) : SizedBox(),
                  SizedBox(height: width(15)),
                  StyledText("7 hari lalu",
                    size: sp(14),
                    fontWeight: FontWeight.w600,
                    color: ColorUtils.grey7070,
                  ),
                ],
              ),
            ),
            SizedBox(height: width(15)),
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: width(10),
              ),
              child: RequestWrapperWidget<ForumAnswerResponse>(
                requestWrapper: _answerWrapper,
                placeholder: (_) => Shimmer.fromColors(
                  child: Container(
                    width: width(150),
                    height: sp(14),
                    color: Colors.grey[200],
                  ),
                  baseColor: Colors.grey[200],
                  highlightColor: Colors.white,
                ),
                builder: (_, data) => StyledText(data.data.length > 0 ? "${data.data.length} Jawaban" : "Belum ada jawaban",
                  size: sp(14),
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
            SizedBox(height: width(5)),
            RequestWrapperWidget<ForumAnswerResponse>(
              requestWrapper: _answerWrapper,
              placeholder: (_) => ListView.builder(
                itemCount: 2,
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemBuilder: (_, index) => ShimmerAnswerItem(),
              ),
              builder: (_, data) => ListView.builder(
                itemCount: data.data.length,
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemBuilder: (_, index) => AnswerItem(data: data.data[index],
                  isMarkedAnswer: widget.argument.idUrlMarkedAnswer == data.data[index].idUrlAnswer,
                  onUpvote: _onUpvote,
                  onDownvote: _onDownvote,
                ),
              ),
            ),
            SizedBox(height: width(25)),
            Padding(
              padding: EdgeInsets.all(width(10)),
              child: StyledText("Punya jawaban? Jawab disini",
                size: sp(14),
                fontWeight: FontWeight.w600,
              ),
            ),
            Container(
              width: double.infinity,
              height: width(50),
              color: Colors.white,
              child: TextFormField(
                controller: _answerController,
                onChanged: (it) => _answer = it,
                maxLines: null,
                textInputAction: TextInputAction.newline,
                keyboardType: TextInputType.multiline,
                textCapitalization: TextCapitalization.sentences,
                style: TextStyle(
                  fontSize: sp(12),
                  fontFamily: FontUtils.segoeui,
                ),
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(width(15)),
                  border: InputBorder.none,
                  hintText: "Tulis jawaban Anda",
                  hintStyle: TextStyle(
                    fontSize: sp(12),
                    color: ColorUtils.grey8585,
                    fontFamily: FontUtils.segoeui,
                  ),
                ),
              ),
            ),
            SizedBox(height: width(10)),
            Container(
              constraints: BoxConstraints(
                minWidth: double.infinity,
                minHeight: width(150),
              ),
              color: Colors.white,
              child: TextFormField(
                controller: _detailAnswerController,
                onChanged: (it) => _detailAnswer = it,
                maxLines: null,
                textInputAction: TextInputAction.newline,
                keyboardType: TextInputType.multiline,
                textCapitalization: TextCapitalization.sentences,
                style: TextStyle(
                  fontSize: sp(12),
                  fontFamily: FontUtils.segoeui,
                ),
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(width(15)),
                  border: InputBorder.none,
                  hintText: "Tulis detail jawaban Anda (Opsional)",
                  hintStyle: TextStyle(
                    fontSize: sp(12),
                    color: ColorUtils.grey8585,
                    fontFamily: FontUtils.segoeui,
                  ),
                ),
              ),
            ),
            SizedBox(height: width(10)),
            Row(
              children: <Widget>[
                Spacer(),
                MaterialButton(
                  onPressed: () => _presenter.executeWriteAnswer(_answer, _detailAnswer, widget.argument.idUrlForum),
                  color: ColorUtils.primary,
                  minWidth: width(100),
                  height: width(40),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(width(5)),
                  ),
                  child: StyledText("Jawab",
                    color: Colors.white,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                SizedBox(width: width(10)),
              ],
            ),
            SizedBox(height: width(10)),
          ],
        ),
      ),
    );
  }

  @override
  void onSuccessWriteAnswer() {
    _answerController.text = "";
    _detailAnswerController.text = "";
  }

  void _onUpvote(ForumAnswerData data){
    _presenter.executeUpvote(data.idUrlAnswer);
  }

  void _onDownvote(ForumAnswerData data){
    _presenter.executeDownvote(data.idUrlAnswer);
  }

  @override
  void dispose() {
    super.dispose();
    _presenter.stopObserveForum();
  }
}
