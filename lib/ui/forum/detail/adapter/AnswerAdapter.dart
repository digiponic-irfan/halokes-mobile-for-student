import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/extension/Size.dart';
import 'package:halokes_siswa/external_plugin/cached_network_image/cached_network_image.dart';
import 'package:halokes_siswa/network/response/management/ForumAnswerResponse.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';
import 'package:halokes_siswa/utils/ImageUtils.dart';
import 'package:shimmer/shimmer.dart';

class AnswerItem extends StatelessWidget {
  final ForumAnswerData data;
  final bool showMarkAnswerButton;
  final bool isMarkedAnswer;
  final Function(ForumAnswerData) onUpvote;
  final Function(ForumAnswerData) onDownvote;
  final Function(ForumAnswerData) onMarkAsAnswer;

  AnswerItem({@required this.data, @required this.onUpvote, @required this.onDownvote,
    this.onMarkAsAnswer, this.showMarkAnswerButton = false, this.isMarkedAnswer = false}) : assert(data != null);

  @override
  Widget build(BuildContext context) {
    var differentTime = DateTime.now().difference(data.time);
    var displayDifferentTime = "";

    if(differentTime.inDays > 0){
      if(differentTime.inDays < 30){
        displayDifferentTime = "${differentTime.inDays} hari lalu";
      }else {
        displayDifferentTime = data.displayTime;
      }
    }else if(differentTime.inHours > 0){
      displayDifferentTime = "${differentTime.inHours} jam lalu";
    }else if(differentTime.inMinutes > 0){
      displayDifferentTime = "${differentTime.inMinutes} menit lalu";
    }else {
      displayDifferentTime = "Beberapa detik lalu";
    }

    if(isMarkedAnswer){
      return Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(adaptiveWidth(context, 10)),
            color: ColorUtils.primary,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                      child: Row(
                        children: <Widget>[
                          CachedNetworkImage(
                            imageUrl: data.answererFoto,
                            imageBuilder: (_, provider) => Container(
                              width: adaptiveWidth(context, 50),
                              height: adaptiveWidth(context, 50),
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                  image: provider,
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            placeholder: (_, __) => Shimmer.fromColors(
                              child: Container(
                                width: adaptiveWidth(context, 50),
                                height: adaptiveWidth(context, 50),
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Colors.grey[200],
                                ),
                              ),
                              baseColor: Colors.grey[200],
                              highlightColor: Colors.white,
                            ),
                            errorWidget: (_, __, ___) => Container(
                              width: adaptiveWidth(context, 50),
                              height: adaptiveWidth(context, 50),
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                  image: AssetImage(ImageUtils.missing_avatar),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(width: adaptiveWidth(context, 12)),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                StyledText(data.answererName,
                                  size: sp(context, 13),
                                  fontWeight: FontWeight.w600,
                                  color: Colors.white,
                                ),
                                SizedBox(height: adaptiveWidth(context, 3)),
                                StyledText(data.sekolahNama,
                                  size: sp(context, 11),
                                  color: ColorUtils.greyfafa,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Column(
                      children: <Widget>[
                        data.yourVote == 1 ?
                        Container(
                          height: adaptiveWidth(context, 23),
                          width: adaptiveWidth(context, 28),
                        ) :
                        ClipPath(
                          clipper: TrianglePath(),
                          child: Material(
                            color: Colors.white,
                            child: InkWell(
                              onTap: () => onUpvote(data),
                              child: Container(
                                height: adaptiveWidth(context, 23),
                                width: adaptiveWidth(context, 28),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: adaptiveWidth(context, 5)),
                        StyledText("${data.upvoteCount - data.downvoteCount}",
                          size: sp(context, 14),
                          fontWeight: FontWeight.w600,
                          color: Colors.white,
                        ),
                        SizedBox(height: adaptiveWidth(context, 5)),
                        data.yourVote == -1 ?
                        Container(
                          height: adaptiveWidth(context, 23),
                          width: adaptiveWidth(context, 28),
                        ) :
                        ClipPath(
                          clipper: ReverseTrianglePath(),
                          child: Material(
                            color: Colors.white,
                            child: InkWell(
                              onTap: () => onDownvote(data),
                              child: Container(
                                height: adaptiveWidth(context, 23),
                                width: adaptiveWidth(context, 28),
                              ),
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
                SizedBox(height: adaptiveWidth(context, 10)),
                StyledText("Jawaban : ",
                  size: sp(context, 14),
                  fontWeight: FontWeight.w600,
                  color: Colors.white,
                ),
                SizedBox(height: adaptiveWidth(context, 5)),
                StyledText(data.answer,
                  size: sp(context, 12),
                  color: ColorUtils.greyfafa,
                ),
                SizedBox(height: adaptiveWidth(context, 15)),
                StyledText("Penjelasan : ",
                  size: sp(context, 14),
                  fontWeight: FontWeight.w600,
                  color: Colors.white,
                ),
                SizedBox(height: adaptiveWidth(context, 5)),
                StyledText(data.answerDesc,
                  size: sp(context, 12),
                  color: ColorUtils.greyfafa,
                ),
                SizedBox(height: adaptiveWidth(context, 15)),
                StyledText(displayDifferentTime,
                  size: sp(context, 14),
                  fontWeight: FontWeight.w600,
                  color: Colors.white,
                ),
              ],
            ),
          ),
          SizedBox(height: adaptiveWidth(context, 5)),
        ],
      );
    }

    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(adaptiveWidth(context, 10)),
          color: Colors.white,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    child: Row(
                      children: <Widget>[
                        CachedNetworkImage(
                          imageUrl: data.answererFoto,
                          imageBuilder: (_, provider) => Container(
                            width: adaptiveWidth(context, 50),
                            height: adaptiveWidth(context, 50),
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                image: provider,
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          placeholder: (_, __) => Shimmer.fromColors(
                            child: Container(
                              width: adaptiveWidth(context, 50),
                              height: adaptiveWidth(context, 50),
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.grey[200],
                              ),
                            ),
                            baseColor: Colors.grey[200],
                            highlightColor: Colors.white,
                          ),
                          errorWidget: (_, __, ___) => Container(
                            width: adaptiveWidth(context, 50),
                            height: adaptiveWidth(context, 50),
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                image: AssetImage(ImageUtils.missing_avatar),
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(width: adaptiveWidth(context, 12)),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              StyledText(data.answererName,
                                size: sp(context, 13),
                                fontWeight: FontWeight.w600,
                              ),
                              SizedBox(height: adaptiveWidth(context, 3)),
                              StyledText(data.sekolahNama,
                                size: sp(context, 11),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Column(
                    children: <Widget>[
                      data.yourVote == 1 ?
                      Container(
                        height: adaptiveWidth(context, 23),
                        width: adaptiveWidth(context, 28),
                      ) :
                      ClipPath(
                        clipper: TrianglePath(),
                        child: Material(
                          color: ColorUtils.primary,
                          child: InkWell(
                            onTap: () => onUpvote(data),
                            child: Container(
                              height: adaptiveWidth(context, 23),
                              width: adaptiveWidth(context, 28),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: adaptiveWidth(context, 5)),
                      StyledText("${data.upvoteCount - data.downvoteCount}",
                        size: sp(context, 14),
                        fontWeight: FontWeight.w600,
                      ),
                      SizedBox(height: adaptiveWidth(context, 5)),
                      data.yourVote == -1 ?
                      Container(
                        height: adaptiveWidth(context, 23),
                        width: adaptiveWidth(context, 28),
                      ) :
                      ClipPath(
                        clipper: ReverseTrianglePath(),
                        child: Material(
                          color: ColorUtils.primary,
                          child: InkWell(
                            onTap: () => onDownvote(data),
                            child: Container(
                              height: adaptiveWidth(context, 23),
                              width: adaptiveWidth(context, 28),
                            ),
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
              SizedBox(height: adaptiveWidth(context, 10)),
              StyledText("Jawaban : ",
                size: sp(context, 14),
                fontWeight: FontWeight.w600,
              ),
              SizedBox(height: adaptiveWidth(context, 5)),
              StyledText(data.answer,
                size: sp(context, 12),
              ),
              SizedBox(height: adaptiveWidth(context, 15)),
              StyledText("Penjelasan : ",
                size: sp(context, 14),
                fontWeight: FontWeight.w600,
              ),
              SizedBox(height: adaptiveWidth(context, 5)),
              StyledText(data.answerDesc,
                size: sp(context, 12),
              ),
              SizedBox(height: adaptiveWidth(context, 15)),
              StyledText(displayDifferentTime,
                size: sp(context, 14),
                fontWeight: FontWeight.w600,
                color: ColorUtils.grey7070,
              ),
              showMarkAnswerButton ? Row(
                children: <Widget>[
                  Spacer(),
                  Card(
                    color: ColorUtils.primary,
                    clipBehavior: Clip.antiAlias,
                    child: InkWell(
                      onTap: () => onMarkAsAnswer(data),
                      child: Padding(
                        padding: EdgeInsets.all(adaptiveWidth(context, 10)),
                        child: StyledText("Tandai sebagai jawaban yang benar",
                          size: sp(context, 14),
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ],
              ) : SizedBox(),
              showMarkAnswerButton ? SizedBox(height: adaptiveWidth(context, 5)) : SizedBox(),
            ],
          ),
        ),
        SizedBox(height: adaptiveWidth(context, 5)),
      ],
    );
  }
}

class ShimmerAnswerItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(adaptiveWidth(context, 10)),
          color: Colors.white,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Shimmer.fromColors(
                    child: Container(
                      width: adaptiveWidth(context, 50),
                      height: adaptiveWidth(context, 50),
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.grey[200],
                      ),
                    ),
                    baseColor: Colors.grey[200],
                    highlightColor: Colors.white,
                  ),
                  SizedBox(width: adaptiveWidth(context, 12)),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Shimmer.fromColors(
                        child: Container(
                          width: adaptiveWidth(context, 200),
                          height: sp(context, 13),
                          color: Colors.grey[200],
                        ),
                        baseColor: Colors.grey[200],
                        highlightColor: Colors.white,
                      ),
                      SizedBox(height: adaptiveWidth(context, 3)),
                      Shimmer.fromColors(
                        child: Container(
                          width: adaptiveWidth(context, 125),
                          height: sp(context, 11),
                          color: Colors.grey[200],
                        ),
                        baseColor: Colors.grey[200],
                        highlightColor: Colors.white,
                      ),
                    ],
                  ),
                ],
              ),
              SizedBox(height: adaptiveWidth(context, 25)),
              Shimmer.fromColors(
                child: Container(
                  width: adaptiveWidth(context, 100),
                  height: sp(context, 14),
                  color: Colors.grey[200],
                ),
                baseColor: Colors.grey[200],
                highlightColor: Colors.white,
              ),
              SizedBox(height: adaptiveWidth(context, 5)),
              Shimmer.fromColors(
                child: Container(
                  width: double.infinity,
                  height: sp(context, 24),
                  color: Colors.grey[200],
                ),
                baseColor: Colors.grey[200],
                highlightColor: Colors.white,
              ),
              SizedBox(height: adaptiveWidth(context, 15)),
              Shimmer.fromColors(
                child: Container(
                  width: adaptiveWidth(context, 100),
                  height: sp(context, 14),
                  color: Colors.grey[200],
                ),
                baseColor: Colors.grey[200],
                highlightColor: Colors.white,
              ),
              SizedBox(height: adaptiveWidth(context, 5)),
              Shimmer.fromColors(
                child: Container(
                  width: double.infinity,
                  height: sp(context, 24),
                  color: Colors.grey[200],
                ),
                baseColor: Colors.grey[200],
                highlightColor: Colors.white,
              ),
            ],
          ),
        ),
        SizedBox(height: adaptiveWidth(context, 5)),
      ],
    );
  }
}

class TrianglePath extends CustomClipper<Path> {

  @override
  Path getClip(Size size) => getTrianglePath(size.width, size.height);

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => true;

  Path getTrianglePath(double x, double y) {
    return Path()
      ..moveTo(0, y)
      ..lineTo(x / 2, 0)
      ..lineTo(x, y)
      ..lineTo(0, y);
  }
}

class ReverseTrianglePath extends CustomClipper<Path> {

  Path getTrianglePath(double x, double y) {
    return Path()
      ..moveTo(0, 0)
      ..lineTo(x, 0)
      ..lineTo(x / 2, y)
      ..lineTo(0, 0);
  }

  @override
  Path getClip(Size size) => getTrianglePath(size.width, size.height);

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => true;
}
