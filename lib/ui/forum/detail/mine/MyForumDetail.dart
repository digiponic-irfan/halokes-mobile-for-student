import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/extension/BaseStateExt.dart';
import 'package:flutter/material.dart';
import 'package:halokes_siswa/external_plugin/cached_network_image/cached_network_image.dart';
import 'package:halokes_siswa/network/response/forum_chat/MarkAnswerResponse.dart';
import 'package:halokes_siswa/network/response/management/ForumAnswerResponse.dart';
import 'package:halokes_siswa/network/response/management/MyForumResponse.dart';
import 'package:halokes_siswa/ui/forum/detail/DetailForumDelegate.dart';
import 'package:halokes_siswa/ui/forum/detail/DetailForumPresenter.dart';
import 'package:halokes_siswa/ui/forum/detail/adapter/AnswerAdapter.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';
import 'package:halokes_siswa/utils/ImageUtils.dart';
import 'package:mcnmr_common_ext/FutureDelayed.dart';
import 'package:mcnmr_request_wrapper/RequestWrapper.dart';
import 'package:mcnmr_request_wrapper/RequestWrapperWidget.dart';
import 'package:shimmer/shimmer.dart';

class MyForumDetail extends StatefulWidget {
  final MyForumData argument;

  MyForumDetail({@required this.argument});

  @override
  _MyForumDetailState createState() => _MyForumDetailState();
}

class _MyForumDetailState extends BaseState<MyForumDetail> with DetailForumDelegate{
  DetailForumPresenter _presenter;
  RequestWrapper<ForumAnswerResponse> _answerWrapper = RequestWrapper();

  @override
  void initState() {
    super.initState();
    _presenter = DetailForumPresenter(this, this);
  }

  @override
  void afterWidgetBuilt() {
    _presenter.observeForum(widget.argument.idUrlForum, _answerWrapper);
    _presenter.executeGetForumAnswer(_answerWrapper, widget.argument.idUrlForum);
  }

  @override
  void shouldShowLoading(int typeRequest) {
    if(typeRequest != DetailForumPresenter.REQUEST_GET_FORUM_ANSWER){
      super.shouldShowLoading(typeRequest);
    }
  }

  @override
  void shouldHideLoading(int typeRequest) {
    if(typeRequest != DetailForumPresenter.REQUEST_GET_FORUM_ANSWER){
      super.shouldHideLoading(typeRequest);
    }
  }

  @override
  void onNoConnection(int typeRequest) {
    if(typeRequest == DetailForumPresenter.REQUEST_GET_FORUM_ANSWER){
      delay(5000, () => _presenter.executeGetForumAnswer(_answerWrapper, widget.argument.idUrlForum));
    }else {
      super.onNoConnection(typeRequest);
    }
  }

  @override
  void onRequestTimeOut(int typeRequest) {
    if(typeRequest == DetailForumPresenter.REQUEST_GET_FORUM_ANSWER){
      delay(5000, () => _presenter.executeGetForumAnswer(_answerWrapper, widget.argument.idUrlForum));
    }else {
      super.onRequestTimeOut(typeRequest);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorUtils.greyfafa,
      appBar: AppBar(
        title: StyledText("Detail Forum", color: Colors.white),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.white),
          onPressed: finish,
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              width: double.infinity,
              color: Colors.white,
              padding: EdgeInsets.all(width(10)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  StyledText("${widget.argument.grade} - ${widget.argument.mapel}",
                    size: sp(14),
                    fontWeight: FontWeight.w600,
                  ),
                  SizedBox(height: width(6)),
                  StyledText(widget.argument.question,
                    size: sp(12),
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                  ),
                  SizedBox(height: width(12)),
                  widget.argument.image != "" ? CachedNetworkImage(
                    imageUrl: widget.argument.image,
                    imageBuilder: (_, provider) => Container(
                      width: double.infinity,
                      height: width(150),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(width(10)),
                        image: DecorationImage(
                          image: provider,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    placeholder: (_, __) => Shimmer.fromColors(
                      child: Container(
                        width: double.infinity,
                        height: width(150),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(width(10)),
                          color: Colors.grey[200],
                        ),
                      ),
                      baseColor: Colors.grey[200],
                      highlightColor: Colors.white,
                    ),
                    errorWidget: (_, __, ___) => Container(
                      width: double.infinity,
                      height: width(150),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(width(10)),
                        image: DecorationImage(
                          image: AssetImage(ImageUtils.image_not_found),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ) : SizedBox(),
                  SizedBox(height: width(15)),
                  StyledText("7 hari lalu",
                    size: sp(14),
                    fontWeight: FontWeight.w600,
                    color: ColorUtils.grey7070,
                  ),
                ],
              ),
            ),
            SizedBox(height: width(15)),
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: width(10),
              ),
              child: RequestWrapperWidget<ForumAnswerResponse>(
                requestWrapper: _answerWrapper,
                placeholder: (_) => Shimmer.fromColors(
                  child: Container(
                    width: width(150),
                    height: sp(14),
                    color: Colors.grey[200],
                  ),
                  baseColor: Colors.grey[200],
                  highlightColor: Colors.white,
                ),
                builder: (_, data) => StyledText(data.data.length > 0 ? "${data.data.length} Jawaban" : "Belum ada jawaban",
                  size: sp(14),
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
            SizedBox(height: width(5)),
            RequestWrapperWidget<ForumAnswerResponse>(
              requestWrapper: _answerWrapper,
              placeholder: (_) => ListView.builder(
                itemCount: 2,
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemBuilder: (_, index) => ShimmerAnswerItem(),
              ),
              builder: (_, data) => ListView.builder(
                itemCount: data.data.length,
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemBuilder: (_, index) => AnswerItem(data: data.data[index],
                  showMarkAnswerButton: true,
                  isMarkedAnswer: widget.argument.idUrlMarkedAnswer == data.data[index].idUrlAnswer,
                  onDownvote: _onDownvote,
                  onUpvote: _onUpvote,
                  onMarkAsAnswer: _onMarkAsAnswer,
                ),
              ),
            ),
            SizedBox(height: width(5)),
          ],
        ),
      ),
    );
  }

  @override
  void onMarkedAsAnswer(MarkAnswerResponse response) {
    if(widget.argument.idUrlForum == response.data.idUrlForum){
      setState(() => widget.argument.idUrlForum = response.data.idUrlForum);
    }
  }

  void _onUpvote(ForumAnswerData data){
    _presenter.executeUpvote(data.idUrlAnswer);
  }

  void _onDownvote(ForumAnswerData data){
    _presenter.executeDownvote(data.idUrlAnswer);
  }

  void _onMarkAsAnswer(ForumAnswerData data){
    _presenter.executeMarkAnswer(data.idUrlAnswer);
  }
}
