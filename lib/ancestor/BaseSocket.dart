import 'package:socket_io_client/socket_io_client.dart';

class BaseSocket {

  Socket _socket;
  String baseUrl = "";
  String namespace = "";

  Map<String, dynamic Function(dynamic)> _onEvent = Map<String, Function(dynamic)>();

  bool get isConnected => _socket != null ? _socket.connected : false;

  BaseSocket({this.baseUrl, this.namespace});

  Future<void> connect({Map<String, dynamic> header, Map<String, dynamic> query}) async {
    if(_socket != null) return;

    String queryParameter = "";
    query?.forEach((key, value){
      if(queryParameter == ""){
        queryParameter += "$key=$value";
      }else {
        queryParameter += "&$key=$value";
      }
    });

    Map<String, dynamic> config = {
      "transports": ["websocket"],
      "autoConnect": true,
    };

    if(header != null){
      config["extraHeaders"] = header;
    }

    if(queryParameter != ""){
      config["query"] = queryParameter;
    }

    _socket = io("$baseUrl/$namespace", config);
    _onEvent.forEach((key, value) => _socket.on(key, value));
  }

  void disconnect(){
    _socket?.disconnect();
    _socket = null;
  }

  void addEventListener(String eventName, Function(dynamic) action){
    _onEvent[eventName] = action;
  }

  void emit(String actionName, [Map<String, dynamic> data]){
    print("Socket action : $actionName");
    _socket?.emit(actionName, data);
  }

  void emitWithCallback(String actionName, Map<String, dynamic> data, Function callback){
    print("Socket action : $actionName");
    _socket?.emitWithAck(actionName, data, ack: callback);
  }
}