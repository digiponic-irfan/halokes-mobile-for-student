import 'package:halokes_siswa/database/database.dart';
import 'package:halokes_siswa/network/request/GeneralRepository.dart';
import 'package:halokes_siswa/network/request/ManagementRepository.dart';
import 'package:halokes_siswa/network/request/SocketChatRepository.dart';
import 'package:halokes_siswa/network/request/SocketForumRepository.dart';

import 'BaseState.dart';


///
/// You should extends this class when creating another Presenter Class
///
class BasePresenter {
  final BaseState state;

  MyDatabase database;

  GeneralRepository generalRepo;
  ManagementRepository managementRepo;

  SocketChatRepository chatSocketRepo;
  SocketForumRepository forumSocketRepo;

  BasePresenter(this.state){
    database = MyDatabase.instance();

    generalRepo = GeneralRepository(state);
    managementRepo = ManagementRepository(state);

    chatSocketRepo = SocketChatRepository.instance;
    forumSocketRepo = SocketForumRepository.instance;
  }

  void underDevelopment(){
    state.alert(title: "Belum tersedia",
        message: "Fitur masih dalam tahap pengembangan",
        positiveTitle: "Tutup"
    );
  }
}