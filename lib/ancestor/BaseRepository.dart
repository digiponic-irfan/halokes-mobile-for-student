import 'dart:convert';

import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:halokes_siswa/ancestor/BaseResponse.dart';

import 'BaseState.dart';

///
/// Parent of Class which handle communication with APIs/Webservices
///
class BaseRepository{
  static const GET = 0;
  static const POST = 1;
  static const PUT = 2;
  static const DELETE = 3;

  BaseState _baseState;

  Dio _dio = Dio();

  BaseRepository(this._baseState, {String baseUrl = ""}){
    _dio.interceptors.add(LogInterceptor(responseBody: true));
    _dio.options.baseUrl = baseUrl;
    _dio.options.connectTimeout = 60000;
    _dio.options.receiveTimeout = 60000;
    _dio.options.sendTimeout = 60000;
  }

  ///
  /// Check for connectivity
  /// return false if not connected to internet and wifi
  ///
  Future<bool> _connectivityChecker() async {
    var connectionStatus = await (Connectivity().checkConnectivity());
    return connectionStatus != ConnectivityResult.none;
  }

  ///
  /// Execute GET Request Method
  ///
  /// @params url = path
  /// @params params = parameter, ex ?username=xxx&password=xxx
  /// @params typeRequest = TAG of request, it will passed when error happens, obtain it by overriding
  /// shouldShowLoading(), shouldHideLoading(), onRequestTimeOut(), onNoConnection(), onResponseError(),
  /// onUnknownError() in BaseState class
  /// @params cancelToken = Token of cancellable request, read more about it in dio documentation
  /// @params contentType = Header contentType
  ///
  Future<Response<String>> get(String url,
      Map<String, String> params,
      int typeRequest,
      {
        CancelToken cancelToken,
        String contentType = "application/json",
        ResponseException Function(Response<String>) middleware,
      }) {
    return _execute(GET, url, params, typeRequest, cancelToken: cancelToken, contentType: contentType);
  }

  ///
  /// Execute POST Request Method
  ///
  /// @params url = path
  /// @params params = parameter, ex ?username=xxx&password=xxx
  /// @params typeRequest = TAG of request, it will passed when error happens, obtain it by overriding
  /// shouldShowLoading(), shouldHideLoading(), onRequestTimeOut(), onNoConnection(), onResponseError(),
  /// onUnknownError() in BaseState class
  /// @params cancelToken = Token of cancellable request, read more about it in dio documentation
  /// @params contentType = Header contentType
  ///
  Future<Response<String>> post(String url,
      Map<String, dynamic> params,
      int typeRequest,
      {
        CancelToken cancelToken,
        String contentType = "application/json",
        ResponseException Function(Response<String>) middleware,
      }){
    return _execute(POST, url, params, typeRequest, cancelToken: cancelToken, contentType: contentType);
  }

  ///
  /// Execute DELETE Request Method
  ///
  /// @params url = path
  /// @params params = parameter, ex ?username=xxx&password=xxx
  /// @params typeRequest = TAG of request, it will passed when error happens, obtain it by overriding
  /// shouldShowLoading(), shouldHideLoading(), onRequestTimeOut(), onNoConnection(), onResponseError(),
  /// onUnknownError() in BaseState class
  /// @params cancelToken = Token of cancellable request, read more about it in dio documentation
  /// @params contentType = Header contentType
  ///
  Future<Response<String>> delete(String url,
      Map<String, dynamic> params,
      int typeRequest,
      {
        CancelToken cancelToken,
        String contentType = "application/json",
        ResponseException Function(Response<String>) middleware,
      }) {
    return _execute(DELETE, url, params, typeRequest, cancelToken: cancelToken, contentType: contentType);
  }

  ///
  /// Execute PUT Request Method
  ///
  /// @params url = path
  /// @params params = parameter, ex ?username=xxx&password=xxx
  /// @params typeRequest = TAG of request, it will passed when error happens, obtain it by overriding
  /// shouldShowLoading(), shouldHideLoading(), onRequestTimeOut(), onNoConnection(), onResponseError(),
  /// onUnknownError() in BaseState class
  /// @params cancelToken = Token of cancellable request, read more about it in dio documentation
  /// @params contentType = Header contentType
  ///
  Future<Response<String>> put(String url,
      Map<String, dynamic> params,
      int typeRequest,
      {
        CancelToken cancelToken,
        String contentType = "application/json",
        ResponseException Function(Response<String>) middleware,
      }) {
    return _execute(PUT, url, params, typeRequest, cancelToken: cancelToken, contentType: contentType);
  }

  ///
  /// Execute DOWNLOAD request
  ///
  /// @params typeRequest = TAG of request, it will passed when error happens, obtain it by overriding
  /// shouldShowLoading(), shouldHideLoading(), onRequestTimeOut(), onNoConnection(), onResponseError(),
  /// onUnknownError() in BaseState class
  /// @params urlPath = Path of file will be downloaded
  /// @params savePath = Path of file will be stored in storage
  /// @params callback = Callback of progress, contains current size and file actual size
  /// @params completion = Callback when download complete
  /// @params cancelToken = Token for canceling download
  ///
  /// 
  void download(int typeRequest, String urlPath, String savePath, ProgressCallback callback,
    { Function(Response) completion, CancelToken cancelToken}) async {
    _baseState.shouldShowLoading(typeRequest);

    var isConnected = await _connectivityChecker();
    if(!isConnected){
      _baseState.shouldHideLoading(typeRequest);
      _baseState.onNoConnection(typeRequest);
      return null;
    }

    try {
      var result = await _dio.download(
          urlPath,
          savePath,
          onReceiveProgress: callback,
          options: Options(
              receiveTimeout: 24 * 60 * 60 * 1000,
              sendTimeout: 24 * 60 * 60 * 1000
          ),
          deleteOnError: true,
          cancelToken: cancelToken
      );

      _baseState.shouldHideLoading(typeRequest);

      if (completion != null) {
        completion(result);
      }
    } on DioError catch(e){
      if(e.type == DioErrorType.CANCEL) {
        _baseState.shouldHideLoading(typeRequest);
      }else if(e.type == DioErrorType.CONNECT_TIMEOUT ||
          e.type == DioErrorType.RECEIVE_TIMEOUT ||
          e.type == DioErrorType.SEND_TIMEOUT){
        _baseState.shouldHideLoading(typeRequest);
        _baseState.onRequestTimeOut(typeRequest);
      }else {
        if(e.message.contains("SocketException")){
          _baseState.shouldHideLoading(typeRequest);
          _baseState.onNoConnection(typeRequest);
        }else {
          _baseState.shouldHideLoading(typeRequest);
          _baseState.onUnknownError(typeRequest, e.message);
        }
      }
    }
  }

  ///
  /// Execute MULTIPART FORM Request Method
  ///
  /// @params url = path
  /// @params params = parameter, ex ?username=xxx&password=xxx
  /// @params typeRequest = TAG of request, it will passed when error happens, obtain it by overriding
  /// shouldShowLoading(), shouldHideLoading(), onRequestTimeOut(), onNoConnection(), onResponseError(),
  /// onUnknownError() in BaseState class
  /// @params cancelToken = Token of cancellable request, read more about it in dio documentation
  /// @params contentType = Header contentType
  ///
  ///
  Future<Response<String>> formData(String url,
      Map<String, dynamic> params,
      int typeRequest,
      {
        CancelToken cancelToken,
        String contentType = "multipart/form-data"
      }){
    return _execute(POST, url, params, typeRequest,
        cancelToken: cancelToken,
        contentType: contentType,
        isFormData: true);
  }


  ///
  /// Executing request
  ///
  Future<Response<String>> _execute(int method, String url, Map<String, dynamic> params,
      int typeRequest, { CancelToken cancelToken, String contentType = "application/json",
        bool isFormData = false,
        ResponseException Function(Response<String>) middleware,
      }) async {
    _baseState.shouldShowLoading(typeRequest);

    var isConnected = await _connectivityChecker();
    if(!isConnected){
      _baseState.shouldHideLoading(typeRequest);
      _baseState.onNoConnection(typeRequest);
      return null;
    }

    try{
      Response<String> response;

      if(method == GET){
        response = await _dio.get<String>(url, queryParameters: params, options: Options(
            contentType: contentType
        ), cancelToken: cancelToken);
      }else if(method == POST){
        if(isFormData){
          var formData = FormData.fromMap(params);
          response = await _dio.post<String>(url, data: formData, options: Options(
            contentType: contentType,
          ), cancelToken: cancelToken);
        }else {
          response = await _dio.post<String>(url, data: params, options: Options(
            contentType: contentType,
          ), cancelToken: cancelToken);
        }
      }else if(method == PUT){
        response = await _dio.put<String>(url, data: params, options: Options(
            contentType: contentType
        ), cancelToken: cancelToken);
      }else if(method == DELETE){
        response = await _dio.delete<String>(url, data: params, options: Options(
            contentType: contentType
        ), cancelToken: cancelToken);
      }else {
        return null;
      }

      var baseResponse = BaseResponse.fromJson(jsonDecode(response.data));
      if(!baseResponse.status){
        throw ResponseException(baseResponse.message);
      }

      var _middleware = middleware != null ? middleware(response) : null;
      if(_middleware != null){
        throw _middleware;
      }

      _baseState.shouldHideLoading(typeRequest);
      return response;
    } on DioError catch(e){
      if(e.type == DioErrorType.CANCEL) {
        _baseState.shouldHideLoading(typeRequest);
      }else if(e.type == DioErrorType.CONNECT_TIMEOUT ||
          e.type == DioErrorType.RECEIVE_TIMEOUT ||
          e.type == DioErrorType.SEND_TIMEOUT){
        _baseState.shouldHideLoading(typeRequest);
        _baseState.onRequestTimeOut(typeRequest);
      }else {
        if(e.message.contains("SocketException")){
          _baseState.shouldHideLoading(typeRequest);
          _baseState.onNoConnection(typeRequest);
        }else {
          _baseState.shouldHideLoading(typeRequest);
          _baseState.onUnknownError(typeRequest, e.message);
        }
      }
    } on ResponseException catch(e){
      _baseState.shouldHideLoading(typeRequest);
      _baseState.onResponseError(typeRequest, e);
    }

    return null;
  }
}