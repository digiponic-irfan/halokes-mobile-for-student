import 'package:flutter/material.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';

abstract class BaseLifecycleState<T extends StatefulWidget> extends BaseState<T>
    with WidgetsBindingObserver {

  @mustCallSuper
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @mustCallSuper
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    switch(state){
      case AppLifecycleState.resumed:
        onResume();
        break;
      case AppLifecycleState.inactive:
        break;
      case AppLifecycleState.paused:
        onPause();
        break;
      case AppLifecycleState.detached:
        onDestroy();
        break;
    }
  }

  void onPause(){}

  void onResume(){}

  void onDestroy(){}

  @mustCallSuper
  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    onDestroy();
    super.dispose();
  }
}