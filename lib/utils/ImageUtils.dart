class ImageUtils {
  static const image_not_found = "assets/images/image_not_found.png";
  static const elib_latihan_soal = "assets/images/elib_latihan_soal.jpg";
  static const elib_mata_pelajaran = "assets/images/elib_mata_pelajaran.jpg";
  static const elib_video_online = "assets/images/elib_video_online.jpg";
  static const elib_other_book = "assets/images/elib_other_book.jpg";
  static const bg_home = "assets/images/bg_home.svg";
  static const bg_home_curve = "assets/images/bg_home_curve.svg";
  static const ic_menu_absensi = "assets/images/ic_menu_absensi.svg";
  static const ic_menu_ekskul = "assets/images/ic_menu_ekskul.svg";
  static const ic_menu_elibrary = "assets/images/ic_menu_elibrary.svg";
  static const ic_menu_jadwal = "assets/images/ic_menu_jadwal.svg";
  static const ic_menu_kelas = "assets/images/ic_menu_kelas.svg";
  static const ic_menu_konseling = "assets/images/ic_menu_konseling.svg";
  static const ic_menu_nilai = "assets/images/ic_menu_nilai.svg";
  static const ic_menu_tugas = "assets/images/ic_menu_tugas.svg";
  static const ic_attendance_a = "assets/images/ic_attendance_a.png";
  static const ic_attendance_h = "assets/images/ic_attendance_h.png";
  static const ic_attendance_i = "assets/images/ic_attendance_i.png";
  static const ic_attendance_s = "assets/images/ic_attendance_s.png";
  static const ic_student_location = "assets/images/ic_student_location.svg";
  static const ic_student_phone = "assets/images/ic_student_phone.svg";
  static const missing_avatar = "assets/images/missing_avatar.jpg";
  static const ic_logo = "assets/images/ic_logo.png";

  static const ic_pending = "assets/images/ic_pending.svg";
  static const ic_received_by_server = "assets/images/ic_received_by_server.svg";
  static const ic_sent = "assets/images/ic_sent.svg";
  static const ic_read = "assets/images/ic_read.svg";
}