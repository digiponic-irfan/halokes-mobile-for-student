import 'package:flutter/material.dart';

class ColorUtils{
  static const primary = Color(0xff007bc1);
  static const primaryLight = Color(0xff00A3FF);

  static const appbar = Color(0xfffafafa);

  static const success = Color(0xff30c44f);
  static const danger = Color(0xffd75072);
  static const warning = Color(0xfffca424);

  static const greyaaaa = Color(0xffaaaaaa);
  static const grey4e4e = Color(0xff4e4e4e);
  static const greyf1f1 = Color(0xfff1f1f1);
  static const greyfafa = Color(0xfffafafa);
  static const greydede = Color(0xffdedede);
  static const grey7070 = Color(0xff707070);
  static const grey7e7e = Color(0xff7e7e7e);
  static const greye7e7 = Color(0xffe7e7e7);
  static const greya4a4 = Color(0xffa4a4a4);
  static const grey9999 = Color(0xff999999);
  static const grey9f9f = Color(0xff9f9f9f);
  static const grey8585 = Color(0xff858585);

  static const alpha = danger;
  static const sick = success;
  static const permit = warning;

  static const alphaOutside = Color(0x80d75072);
  static const sickOutside = Color(0x8030c44f);
  static const permitOutside = Color(0x80fca424);
}