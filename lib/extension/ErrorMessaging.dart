class ErrorMessaging {
  static const ERROR_TITLE_MESSAGE = "Error";

  static const DEFAULT_MESSAGE = "Terjadi kesalahan";
  static const REQUEST_TIME_OUT_MESSAGE = "Request Time Out, coba beberapa saat lagi";
  static const NO_CONNECTION_MESSAGE = "Tidak ada koneksi internet, cek koneksi Anda";

  static const CLOSE_ACTION_MESSAGE = "Tutup";
  static const TRY_AGAIN_ACTION_MESSAGE = "Coba lagi";
}