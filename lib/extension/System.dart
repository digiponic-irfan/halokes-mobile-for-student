import 'dart:ui';

import 'package:flutter/services.dart';

void brightness(Brightness brightness){
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarBrightness: brightness,
  ));
}

void statusBarColor(Color color){
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: color,
  ));
}