import 'package:intl/intl.dart';

String fromMillisecond(String format, int millisecond){
  var date = DateTime.fromMillisecondsSinceEpoch(millisecond);

  return DateFormat(format).format(date);
}