import 'package:flutter/foundation.dart';
import 'package:halokes_siswa/network/response/general/SemesterResponse.dart';

class SemesterProvider extends ChangeNotifier {
  SemesterData _semesterData;

  void currentlyActiveSemester(SemesterData data){
    if(_semesterData == null){
      _semesterData = data;
      notifyListeners();
    }
  }

  void changeSemester(SemesterData data){
    _semesterData = data;
    notifyListeners();
  }

  SemesterData get selectedSemester => _semesterData;

  void subscribe(Function(SemesterData) subscriber){
    addListener((){
      if(selectedSemester != null){
        subscriber(selectedSemester);
      }
    });
  }
}