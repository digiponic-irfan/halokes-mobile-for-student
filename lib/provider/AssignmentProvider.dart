import 'package:flutter/widgets.dart';
import 'package:halokes_siswa/network/response/general/AssignmentResponse.dart';

class AssignmentProvider extends ChangeNotifier {
  List<String> submittedAssignment = List();

  int getStatus(AssignmentDetail detail){
    if(submittedAssignment.contains(detail.idTugas)){
      return 1;
    }

    return detail.status;
  }

  void submitAssignment(AssignmentDetail detail){
    submittedAssignment.add(detail.idTugas);
    notifyListeners();
  }
}