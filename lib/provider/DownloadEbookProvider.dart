import 'package:flutter/widgets.dart';
import 'package:dio/dio.dart';
import 'package:halokes_siswa/database/database.dart';

class DownloadEbookProvider extends ChangeNotifier{
  Map<String, DownloadEbookTask> _downloadTask = Map();

  void startDownload(String idEbookUrl, String path, CancelToken cancelToken){
    if(!_downloadTask.containsKey(idEbookUrl)){
      _downloadTask[idEbookUrl] = DownloadEbookTask(idEbookUrl: idEbookUrl,
          path: path, cancelToken: cancelToken);
      notifyListeners();
    }
  }

  void updateProgress(String idEbookUrl, int current, int total){
    if(_downloadTask.containsKey(idEbookUrl)){
      _downloadTask[idEbookUrl].updateProgress(current, total);
      notifyListeners();
    }
  }

  void cancel(String idEbookUrl){
    if(_downloadTask.containsKey(idEbookUrl)){
      _downloadTask[idEbookUrl].cancelToken.cancel();
      _downloadTask.remove(idEbookUrl);
      notifyListeners();
    }
  }

  void alreadyDownloaded(String idEbookUrl, DownloadEbookTask task){
    _downloadTask[idEbookUrl] = task;
    notifyListeners();
  }

  bool contains(String idEbookUrl) => _downloadTask.containsKey(idEbookUrl);

  DownloadEbookTask getTask(String idEbookUrl) => _downloadTask[idEbookUrl];

  DownloadEbookStatus getStatus(String idEbookUrl) => _downloadTask[idEbookUrl].status;
}

class DownloadEbookTask {
  String idEbookUrl;
  int current;
  int total;
  String path;
  DownloadEbookStatus status;
  CancelToken cancelToken;

  DownloadEbookTask({@required this.idEbookUrl, @required this.path, @required this.cancelToken}){
    status = DownloadEbookStatus.PREPARING;
  }

  DownloadEbookTask.fromDownloadedEbooksTable(DownloadedEbook ebooks){
    idEbookUrl = ebooks.idEbookUrl;
    path = ebooks.path;
    status = DownloadEbookStatus.DOWNLOADED;
  }

  void updateProgress(int current, int total){
    this.current = current;
    this.total = total;

    if(current == total){
      status = DownloadEbookStatus.DOWNLOADED;
    }else {
      status = DownloadEbookStatus.DOWNLOADING;
    }
  }
}

enum DownloadEbookStatus{
  PREPARING, DOWNLOADING, DOWNLOADED, CANCELLED
}