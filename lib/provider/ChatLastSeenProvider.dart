import 'package:flutter/material.dart';

class ChatLastSeenProvider extends ChangeNotifier{
  Map<String, String> _lastLogoutDate = Map();
  Set<String> _isOnline = Set();


  void someoneOnline(String userIdUrl){
    _isOnline.add(userIdUrl);
    notifyListeners();
  }

  void someoneOffline(String userIdUrl, String lastLogoutDate){
    _isOnline.remove(userIdUrl);
    _lastLogoutDate[userIdUrl] = lastLogoutDate;
    notifyListeners();
  }

  void setLastSeen(String userIdUrl, String lastSeen){
    _lastLogoutDate[userIdUrl] = lastSeen;
    notifyListeners();
  }

  bool isLastSeenExists(String userIdUrl) => _isOnline.contains(userIdUrl) || _lastLogoutDate.containsKey(userIdUrl);

  void clear(){
    _isOnline.clear();
    _lastLogoutDate.clear();
  }

  String getOnlineStatus(String userIdUrl){
    if(_isOnline.contains(userIdUrl)){
      return "Online";
    }

    return _lastLogoutDate[userIdUrl];
  }
}