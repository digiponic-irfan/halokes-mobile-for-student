import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:halokes_siswa/provider/AssignmentProvider.dart';
import 'package:halokes_siswa/provider/ChatLastSeenProvider.dart';
import 'package:halokes_siswa/provider/DownloadEbookProvider.dart';
import 'package:halokes_siswa/provider/SemesterProvider.dart';
import 'package:halokes_siswa/provider/TypingProvider.dart';
import 'package:halokes_siswa/ui/assignment/Assignment.dart';
import 'package:halokes_siswa/ui/attendance/Attendance.dart';
import 'package:halokes_siswa/ui/biodata/Biodata.dart';
import 'package:halokes_siswa/ui/chat/conversation/ChatConversation.dart';
import 'package:halokes_siswa/ui/chat/search/ChatSearch.dart';
import 'package:halokes_siswa/ui/class/Class.dart';
import 'package:halokes_siswa/ui/counseling/Counseling.dart';
import 'package:halokes_siswa/ui/detail_announcement/DetailAnnouncement.dart';
import 'package:halokes_siswa/ui/detail_student/DetailStudent.dart';
import 'package:halokes_siswa/ui/elibrary/Elibrary.dart';
import 'package:halokes_siswa/ui/elibrary/buku/detail/DetailBukuElib.dart';
import 'package:halokes_siswa/ui/elibrary/buku/list/ElibraryBuku.dart';
import 'package:halokes_siswa/ui/elibrary/filter/ElibraryFilter.dart';
import 'package:halokes_siswa/ui/elibrary/video/ElibraryVideo.dart';
import 'package:halokes_siswa/ui/extracurricular/Extracurricular.dart';
import 'package:halokes_siswa/ui/extracurricular/member/ExtracurricularMember.dart';
import 'package:halokes_siswa/ui/forum/create/CreateForum.dart';
import 'package:halokes_siswa/ui/forum/detail/mine/MyForumDetail.dart';
import 'package:halokes_siswa/ui/forum/detail/other/OtherForumDetail.dart';
import 'package:halokes_siswa/ui/forum/filter/FilterForum.dart';
import 'package:halokes_siswa/ui/forum/list/ForumList.dart';
import 'package:halokes_siswa/ui/home/Home.dart';
import 'package:halokes_siswa/ui/image_previewer/ImagePreviewer.dart';
import 'package:halokes_siswa/ui/login/Login.dart';
import 'package:halokes_siswa/ui/payment/Payment.dart';
import 'package:halokes_siswa/ui/schedule/Schedule.dart';
import 'package:halokes_siswa/ui/score/Score.dart';
import 'package:halokes_siswa/ui/splash_screen/SplashScreen.dart';
import 'package:halokes_siswa/ui/update_profile/UpdateProfile.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  static const ROUTE_ASSIGNMENT = "/assignment";
  static const ROUTE_ATTENDANCE = "/attendance";
  static const ROUTE_BIODATA = "/biodata";
  static const ROUTE_CHAT_CONVERSATION = "/chatConversation";
  static const ROUTE_CHAT_SEARCH = "/chatSearch";
  static const ROUTE_CLASS = "/class";
  static const ROUTE_COUNSELING = "/counseling";
  static const ROUTE_DETAIL_ANNOUNCEMENT = "/detailAnnouncement";
  static const ROUTE_DETAIL_ELIB_BOOK = "/detailElibBook";
  static const ROUTE_DETAIL_STUDENT = "/detailStudent";
  static const ROUTE_ELIB = "/elib";
  static const ROUTE_ELIB_BOOK = "/elibBook";
  static const ROUTE_ELIB_FILTER = "/elibFilter";
  static const ROUTE_ELIB_VIDEO = "/elibVideo";
  static const ROUTE_EXTRACURRICULAR = "/extracurricular";
  static const ROUTE_EXTRACURRICULAR_MEMBER = "/extracurricularMember";
  static const ROUTE_FORUM_DETAIL_MINE = "/forumDetailMine";
  static const ROUTE_FORUM_DETAIL_OTHER = "/forumDetailOther";
  static const ROUTE_FORUM_CREATE = "/forumCreate";
  static const ROUTE_FORUM_FILTER = "/forumFilter";
  static const ROUTE_FORUM_LIST = "/forumList";
  static const ROUTE_HOME = "/home";
  static const ROUTE_LOGIN = "/login";
  static const ROUTE_PAYMENT = "/payment";
  static const ROUTE_SCHEDULE = "/schedule";
  static const ROUTE_SCORE = "/score";
  static const ROUTE_SPLASH_SCREEN = "/splashScreen";
  static const ROUTE_UPDATE_PROFILE = "/updateProfile";

  /**
   * Utilities
   */
  static const ROUTE_IMAGE_PREVIEWER = "/imagePreviewer";

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        /**
         * Provider of apps, read more about it in provider https://pub.dev/packages/provider
         */
        ChangeNotifierProvider<AssignmentProvider>(create: (_) => AssignmentProvider()),
        ChangeNotifierProvider<DownloadEbookProvider>(create: (_) => DownloadEbookProvider()),
        ChangeNotifierProvider<SemesterProvider>(create: (_) => SemesterProvider()),
        ChangeNotifierProvider<ChatLastSeenProvider>(create: (_) => ChatLastSeenProvider()),
        ChangeNotifierProvider<TypingProvider>(create: (_) => TypingProvider()),
      ],
      child: MaterialApp(
        title: 'Halokes for Student',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          brightness: Brightness.light,
          primaryColor: ColorUtils.primary,
          scaffoldBackgroundColor: Colors.white,
        ),
        onGenerateRoute: (settings){
          switch(settings.name){
            case ROUTE_ASSIGNMENT :
              return _createRoute(target: Assignment(argument: settings.arguments));
            case ROUTE_ATTENDANCE :
              return _createRoute(target: Attendance(argument: settings.arguments));
            case ROUTE_BIODATA :
              return _createRoute(target: Biodata(argument: settings.arguments));
            case ROUTE_CHAT_CONVERSATION :
              return _createRoute(target: ChatConversation(argument: settings.arguments));
            case ROUTE_CHAT_SEARCH :
              return _createRoute(target: ChatSearch());
            case ROUTE_CLASS :
              return _createRoute(target: Class(argument: settings.arguments));
            case ROUTE_COUNSELING :
              return _createRoute(target: Counseling(argument: settings.arguments));
            case ROUTE_DETAIL_ANNOUNCEMENT :
              return _createRoute(target: DetailAnnouncement(argument: settings.arguments));
            case ROUTE_DETAIL_ELIB_BOOK :
              return _createRoute(target: DetailBukuElib(argument: settings.arguments));
            case ROUTE_DETAIL_STUDENT :
              return _createRoute(target: DetailStudent(argument: settings.arguments));
            case ROUTE_ELIB :
              return _createRoute(target: Elibrary());
            case ROUTE_ELIB_BOOK :
              return _createRoute(target: ElibraryBuku(argument: settings.arguments));
            case ROUTE_ELIB_FILTER :
              return _createStackRoute(target: ElibraryFilter());
            case ROUTE_ELIB_VIDEO :
              return _createRoute(target: ElibraryVideo());
            case ROUTE_EXTRACURRICULAR :
              return _createRoute(target: Extracurricular(argument: settings.arguments));
            case ROUTE_EXTRACURRICULAR_MEMBER :
              return _createRoute(target: ExtracurricularMember(argument: settings.arguments));
            case ROUTE_FORUM_DETAIL_MINE :
              return _createRoute(target: MyForumDetail(argument: settings.arguments));
            case ROUTE_FORUM_DETAIL_OTHER :
              return _createRoute(target: OtherForumDetail(argument: settings.arguments));
            case ROUTE_FORUM_CREATE :
              return _createRoute(target: CreateForum());
            case ROUTE_FORUM_FILTER :
              return _createStackRoute(target: FilterForum(argument: settings.arguments));
            case ROUTE_FORUM_LIST :
              return _createRoute(target: ForumList());
            case ROUTE_HOME :
              return _createRoute(target: Home());
            case ROUTE_LOGIN :
              return _createRoute(target: Login());
            case ROUTE_PAYMENT :
              return _createRoute(target: Payment(argument: settings.arguments));
            case ROUTE_SCHEDULE :
              return _createRoute(target: Schedule(argument: settings.arguments));
            case ROUTE_SCORE :
              return _createRoute(target: Score(argument: settings.arguments));
            case ROUTE_SPLASH_SCREEN :
              return _createRoute(target: SplashScreen());
            case ROUTE_UPDATE_PROFILE :
              return _createRoute(target: UpdateProfile());

            case ROUTE_IMAGE_PREVIEWER :
              return _createRoute(target: ImagePreviewer(argument: settings.arguments));
            default :
              return _createRoute(target: SplashScreen());
          }
        },
      ),
    );
  }

  ///
  /// Create route depends by device platform
  ///
  PageRoute _createRoute({@required Widget target}){
    if(Platform.isIOS){
      return CupertinoPageRoute(builder: (_) => target);
    }else {
      return MaterialPageRoute(builder: (_) => target);
    }
  }

  StackRouter _createStackRoute({@required Widget target}){
    return StackRouter(builder: (_) => target);
  }
}

class StackRouter<T> extends PageRoute<T>{

  StackRouter({
    @required this.builder,
    RouteSettings settings,
    this.maintainState = true,
    bool fullscreenDialog = false,
  }) : super(settings: settings, fullscreenDialog: fullscreenDialog);

  /// Builds the primary contents of the route.
  final WidgetBuilder builder;

  @override
  final bool maintainState;

  @override
  Duration get transitionDuration => const Duration(milliseconds: 300);

  @override
  Color get barrierColor => null;

  @override
  String get barrierLabel => null;

  @override
  bool canTransitionFrom(TransitionRoute<dynamic> previousRoute) {
    return previousRoute is MaterialPageRoute || previousRoute is CupertinoPageRoute;
  }

  @override
  bool canTransitionTo(TransitionRoute<dynamic> nextRoute) {
    // Don't perform outgoing animation if the next route is a fullscreen dialog.
    return (nextRoute is MaterialPageRoute && !nextRoute.fullscreenDialog)
        || (nextRoute is CupertinoPageRoute && !nextRoute.fullscreenDialog);
  }

  @override
  Widget buildPage(
      BuildContext context,
      Animation<double> animation,
      Animation<double> secondaryAnimation,
      ) {
    final Widget result = builder(context);
    assert(() {
      if (result == null) {
        throw FlutterError(
            'The builder for route "${settings.name}" returned null.\n'
                'Route builders must never return null.'
        );
      }
      return true;
    }());
    return Semantics(
      scopesRoute: true,
      explicitChildNodes: true,
      child: result,
    );
  }

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
    final PageTransitionsTheme theme = Theme.of(context).pageTransitionsTheme;
    return theme.buildTransitions<T>(this, context, animation, secondaryAnimation, child);
  }

  @override
  String get debugLabel => '${super.debugLabel}(${settings.name})';

  @override
  bool get opaque => false;
}
