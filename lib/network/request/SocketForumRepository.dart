import 'package:halokes_siswa/ancestor/BaseSocket.dart';

class SocketForumRepository extends BaseSocket{
  static const EVENT_START_OBSERVING = "connect";
  static const EVENT_SERVER_SENDS_NEW_ANSWER = "server sends new answer";
  static const EVENT_SOMEONE_UPVOTE_ANSWER = "someone upvote answer";
  static const EVENT_SOMEONE_DOWNVOTE_ANSWER = "someone downvote answer";
  static const EVENT_SOMEONE_MARK_ANSWER = "someone mark answer";
  static const EVENT_STOP_OBSERVING = "disconnect";

  static const ACTION_WRITE_ANSWER = "i'm write my answer";
  static const ACTION_UPVOTE_ANSWER = "i'm upvote answer";
  static const ACTION_DOWNVOTE_ANSWER = "i'm downvote answer";
  static const ACTION_MARK_ANSWER = "i'm mark answer";

  static final SocketForumRepository instance = SocketForumRepository._internal();

  SocketForumRepository._internal() : super(baseUrl: "http://192.168.43.140:3000", namespace: "forum");
}