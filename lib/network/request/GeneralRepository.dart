
import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:halokes_siswa/ancestor/BaseRepository.dart';
import 'package:halokes_siswa/ancestor/BaseResponse.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/network/response/general/AnnouncementResponse.dart';
import 'package:halokes_siswa/network/response/general/AssignmentResponse.dart';
import 'package:halokes_siswa/network/response/general/AttendanceResponse.dart';
import 'package:halokes_siswa/network/response/general/ChatUserResponse.dart';
import 'package:halokes_siswa/network/response/general/ClassResponse.dart';
import 'package:halokes_siswa/network/response/general/CounselingResponse.dart';
import 'package:halokes_siswa/network/response/general/DetailStudentResponse.dart';
import 'package:halokes_siswa/network/response/general/ExtraResponse.dart';
import 'package:halokes_siswa/network/response/general/FinanceResponse.dart';
import 'package:halokes_siswa/network/response/general/LastSeenResponse.dart';
import 'package:halokes_siswa/network/response/general/ScheduleResponse.dart';
import 'package:halokes_siswa/network/response/general/StudentResponse.dart';
import 'package:halokes_siswa/network/response/management/SchoolResponse.dart';
import 'package:halokes_siswa/network/response/general/ScoreResponse.dart';
import 'package:halokes_siswa/network/response/general/SemesterResponse.dart';
import 'package:halokes_siswa/network/response/general/UserResponse.dart';
import 'package:halokes_siswa/preference/AppPreference.dart';

///
/// All request to APIs/Webservices should written here
/// and the response should written in network/response directory
///
class GeneralRepository extends BaseRepository{

  Future<String> get _schId async {
    var school = await AppPreference.getSchool();
    return school.sekolahUid;
  }

  Future<String> get _userIdUrl async {
    var user = await AppPreference.getUser();
    return user.idUrl;
  }

  GeneralRepository(BaseState baseState) : super(baseState, baseUrl: "http://192.168.1.111:8888/halokes/index.php/api/");

  void executeLogin(int typeRequest, SchoolData school,
      Map<String, dynamic> params,
      Function(UserResponse) completion,
      ResponseException Function(Response<String>) middleware) async {
    var response = await post("auth/login/${school.sekolahUid}", params, typeRequest, middleware: middleware);

    if(response != null){
      completion(UserResponse.fromJson(jsonDecode(response.data)));
    }
  }

  void executeGetSemester(int typeRequest, Function(SemesterResponse) completion) async {
    var response = await get("semester/all/${await _schId}", null, typeRequest);

    if(response != null){
      completion(SemesterResponse.fromJson(jsonDecode(response.data)));
    }
  }

  void executeGetAnnouncement(int typeRequest, Function(AnnouncementResponse) completion) async {
    var response = await get("berita/all/${await _schId}", null, typeRequest);

    if(response != null){
      completion(AnnouncementResponse.fromJson(jsonDecode(response.data)));
    }
  }

  void executeGetAttendance(int typeRequest, String idSemester, Function(AttendanceResponse) completion) async {
    var response = await get("presensi/siswa/${await _userIdUrl}/${await _schId}/$idSemester", null, typeRequest);

    if(response != null){
      completion(AttendanceResponse.fromJson(jsonDecode(response.data)));
    }
  }

  void executeGetScore(int typeRequest, String idSemester, Function(ScoreResponse) completion) async {
    var response = await get("nilai/siswa_dua/${await _userIdUrl}/${await _schId}/$idSemester", null, typeRequest);

    if(response != null){
      completion(ScoreResponse.fromJson(jsonDecode(response.data)));
    }
  }

  void executeGetAssignment(int typeRequest, String idSemester, Function(AssignmentResponse) completion) async {
    var response = await get("tugas/${await _userIdUrl}/${await _schId}/all/$idSemester", null, typeRequest);

    if(response != null){
      completion(AssignmentResponse.fromJson(jsonDecode(response.data)));
    }
  }

  void executeGetExtra(int typeRequest, String idSemester, Function(ExtraResponse) completion) async {
    var response = await get("ekskul/siswa/${await _userIdUrl}/${await _schId}/all/$idSemester", null, typeRequest);

    if(response != null){
      completion(ExtraResponse.fromJson(jsonDecode(response.data)));
    }
  }

  void executeGetCounseling(int typeRequest, Function(CounselingResponse) completion) async {
    var response = await get("konseling/catatan/${await _userIdUrl}/${await _schId}", null, typeRequest);

    if(response != null){
      completion(CounselingResponse.fromJson(jsonDecode(response.data)));
    }
  }

  void executeGetClass(int typeRequest, Function(ClassResponse) completion) async {
    var response = await get("kelas/siswa/${await _userIdUrl}/${await _schId}", null, typeRequest);

    if(response != null){
      completion(ClassResponse.fromJson(jsonDecode(response.data)));
    }
  }

  void executeGetSchedule(int typeRequest, Function(ScheduleResponse) completion) async {
    var response = await get("jadwal/siswa/${await _userIdUrl}/all/${await _schId}", null, typeRequest);

    if(response != null){
      completion(ScheduleResponse.fromJson(jsonDecode(response.data)));
    }
  }

  void executeSubmitAssignment(int typeRequest, Map<String, dynamic> params,
      Function(BaseResponse) completion) async {
    var response = await post("tugas/validasi/${await _schId}", params, typeRequest);

    if(response != null){
      completion(BaseResponse.fromJson(jsonDecode(response.data)));
    }
  }

  void executeGetOtherStudentBio(int typeRequest, String targetId, Function(DetailStudentResponse) completion) async {
    var response = await get("siswa/aktif/$targetId/${await _schId}", null, typeRequest);

    if(response != null){
      completion(DetailStudentResponse.fromJson(jsonDecode(response.data)));
    }
  }

  void executeUpdateProfile(int typeRequest, Map<String, dynamic> params,
      Function(UserResponse) completion) async {
    var response = await formData("siswa/${await _userIdUrl}/edit/emailpass/${await _schId}", params, typeRequest);

    if(response != null){
      completion(UserResponse.fromJson(jsonDecode(response.data)));
    }
  }

  void executeGetAvailableTeacher(int typeRequest, Function(AvailableTeacherResponse) completion) async {
    var response = await post("chat/available/teacher/${await _schId}/${await _userIdUrl}", null, typeRequest);

    if(response != null){
      completion(AvailableTeacherResponse.fromJson(jsonDecode(response.data)));
    }
  }

  void executeGetAvailableParent(int typeRequest, Function(AvailableParentResponse) completion) async {
    var response = await post("chat/available/parent/${await _schId}/${await _userIdUrl}", null, typeRequest);

    if(response != null){
      completion(AvailableParentResponse.fromJson(jsonDecode(response.data)));
    }
  }

  void executeGetAvailableStudent(int typeRequest, Function(AvailableStudentResponse) completion) async {
    var response = await post("chat/available/student/${await _schId}/${await _userIdUrl}", null, typeRequest);

    if(response != null){
      completion(AvailableStudentResponse.fromJson(jsonDecode(response.data)));
    }
  }

  void executeGetLastSeen(int typeRequest, String targetId, Function(LastSeenResponse) completion) async {
    var response = await post("chat/lastseen/user/${await _schId}/$targetId", null, typeRequest);

    if(response != null){
      completion(LastSeenResponse.fromJson(jsonDecode(response.data)));
    }
  }

  void executeGetStudentBio(int typeRequest, String idSemester,
      Function(StudentResponse) completion, {CancelToken cancelToken}) async {
    var response = await get("ringkasan/siswa/${await _userIdUrl}/${await _schId}/$idSemester", null, typeRequest, cancelToken: cancelToken);

    if(response != null){
      completion(StudentResponse.fromJson(jsonDecode(response.data)));
    }
  }

  void executeGetTanggunganKeuangan(int typeRequest, Function(FinanceResponse) completion) async {
    var response = await get("keuangan/tanggungan_siswa/${await _userIdUrl}/${await _schId}", null, typeRequest);

    if(response != null){
      completion(FinanceResponse.fromJson(jsonDecode(response.data)));
    }
  }
}