import 'package:halokes_siswa/ancestor/BaseSocket.dart';

class SocketChatRepository extends BaseSocket{
  String _watchedUserId = "";
  String get watchedUserId => _watchedUserId;

  static const EVENT_CONNECT = "connect";
  static const EVENT_SOMEONE_ONLINE = "someone online";
  static const EVENT_SOMEONE_OFFLINE = "someone offline";
  static const EVENT_SOMEONE_TYPING = "someone typing";
  static const EVENT_SOMEONE_SEND_MESSAGE = "someone send you message";
  static const EVENT_TARGET_RECEIVE_YOUR_MESSAGE = "target receive your message";
  static const EVENT_TARGET_RECEIVE_YOUR_DELAYED_MESSAGE = "target receive your delayed message";
  static const EVENT_TARGET_READ_YOUR_MESSAGE = "target read your message";
  static const EVENT_DISCONNECT = "disconnect";

  static const ACTION_GO_TO_BACKGROUND = "i'm going to background";
  static const ACTION_GO_TO_FOREGROUND = "i'm going to foreground";
  static const ACTION_RECEIVING_MESSAGE = "i'm receiving message";
  static const ACTION_READ_MESSAGE = "i'm reading message";
  static const ACTION_ASK_FOR_DELAYED_MESSAGE = "give me my delayed message";
  static const ACTION_ASK_FOR_UPDATED_MESSAGE_REPORT = "give me my latest report message message";
  static const ACTION_TYPING = "i'm typing";
  static const ACTION_SEND_MESSAGE = "i'm sending message";

  static final SocketChatRepository instance = SocketChatRepository._internal();

  SocketChatRepository._internal() : super(baseUrl: "http://192.168.1.111::3000", namespace: "chat");

  /**
   * Called when you enter ChatConversation
   */
  void watchUser(String userId){
    _watchedUserId = userId;
  }

  /**
   * Called when you exit ChatConversation
   */
  void unwatch(){
    _watchedUserId = "";
  }
}