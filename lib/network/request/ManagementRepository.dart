
import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:halokes_siswa/ancestor/BaseRepository.dart';
import 'package:halokes_siswa/ancestor/BaseResponse.dart';
import 'package:halokes_siswa/ancestor/BaseState.dart';
import 'package:halokes_siswa/network/response/management/AllForumResponse.dart';
import 'package:halokes_siswa/network/response/management/DepartmentResponse.dart';
import 'package:halokes_siswa/network/response/management/EbookResponse.dart';
import 'package:halokes_siswa/network/response/management/ForumAnswerResponse.dart';
import 'package:halokes_siswa/network/response/management/ForumGradeResponse.dart';
import 'package:halokes_siswa/network/response/management/ForumMapelResponse.dart';
import 'package:halokes_siswa/network/response/management/MyForumResponse.dart';
import 'package:halokes_siswa/network/response/management/SchoolResponse.dart';
import 'package:halokes_siswa/network/response/management/SubjectResponse.dart';
import 'package:halokes_siswa/network/response/management/VersionResponse.dart';
import 'package:halokes_siswa/network/response/management/VideoResponse.dart';
import 'package:halokes_siswa/preference/AppPreference.dart';

///
/// All request to APIs/Webservices should written here
/// and the response should written in network/response directory
///
class ManagementRepository extends BaseRepository{

  Future<String> get _schId async {
    var school = await AppPreference.getSchool();
    return school.sekolahUid;
  }

  Future<String> get _userIdUrl async {
    var user = await AppPreference.getUser();
    return user.idUrl;
  }

  ManagementRepository(BaseState baseState) : super(baseState, baseUrl: "http://192.168.1.111:8888/halokes-library/index.php/api/");

  void executeGetVersionInfo(int typeRequest, String currentVersion,
      String environment, String platform,
      Function(VersionResponse) completion) async {
    var response = await get("version/$currentVersion/$environment/$platform", null, typeRequest);

    if(response != null){
      completion(VersionResponse.fromJson(jsonDecode(response.data)));
    }
  }

  void executeGetSchool(int typeRequest, Function(SchoolResponse) completion) async {
    var response = await get("sekolah", null, typeRequest);

    if(response != null){
      completion(SchoolResponse.fromJson(jsonDecode(response.data)));
    }
  }

  void executeGetEbook(int typeRequest, String kategori, Function(EbookResponse) completion) async {
    var response = await get("ebook/all/$kategori", null, typeRequest);

    if(response != null){
      completion(EbookResponse.fromJson(jsonDecode(response.data)));
    }
  }

  void executeFilterEbook(int typeRequest, String kategori, String grade, String subject,
      Function(EbookResponse) completion) async {
    var response = await get("ebook/all/$kategori/$grade/$subject", null, typeRequest);

    if(response != null){
      completion(EbookResponse.fromJson(jsonDecode(response.data)));
    }
  }

  void executeGetVideo(int typeRequest, Function(VideoResponse) completion) async {
    var response = await get("video/all", null, typeRequest);

    if(response != null){
      completion(VideoResponse.fromJson(jsonDecode(response.data)));
    }
  }

  void addEbookViewCounter(int typeRequest, String idUrlEbook, Function(BaseResponse) completion) async {
    var response = await get("ebook/click/$idUrlEbook", null, typeRequest);

    if(response != null){
      completion(BaseResponse.fromJson(jsonDecode(response.data)));
    }
  }

  void addEbookDownloadCounter(int typeRequest, Map<String, dynamic> params,
      Function(BaseResponse) completion) async {
    var response = await get("ebook/download", params, typeRequest);

    if(response != null){
      completion(BaseResponse.fromJson(jsonDecode(response.data)));
    }
  }

  void executeGetSubject(int typeRequest, Function(SubjectResponse) completion) async {
    var response = await get("mapel", null, typeRequest);

    if(response != null){
      completion(SubjectResponse.fromJson(jsonDecode(response.data)));
    }
  }

  void executeGetDepartment(int typeRequest, Function(DepartmentResponse) completion) async {
    var response = await get("jurusan", null, typeRequest);

    if(response != null){
      completion(DepartmentResponse.fromJson(jsonDecode(response.data)));
    }
  }

  void executeGetForumGrade(int typeRequest, Function(ForumGradeResponse) completion) async {
    var response = await get("forum/list/grade", null, typeRequest);

    if(response != null){
      completion(ForumGradeResponse.fromJson(jsonDecode(response.data)));
    }
  }

  void executeGetForumMapel(int typeRequest, Function(ForumMapelResponse) completion) async {
    var response = await get("forum/list/mapel", null, typeRequest);

    if(response != null){
      completion(ForumMapelResponse.fromJson(jsonDecode(response.data)));
    }
  }

  void executeCreateForum(int typeRequest, Map<String, dynamic> params, Function(BaseResponse) completion) async {
    var response = await formData("forum/create/${await _schId}/${await _userIdUrl}", params, typeRequest);

    if(response != null){
      completion(BaseResponse.fromJson(jsonDecode(response.data)));
    }
  }

  void executeGetAllForum(int typeRequest, String lastForumId, String idGrade, String idMapel,
      Function(AllForumResponse) completion, {CancelToken cancelToken}) async {
    var response = await get("forum/${await _userIdUrl}/$lastForumId/$idGrade/$idMapel", null, typeRequest);

    if(response != null){
      completion(AllForumResponse.fromJson(jsonDecode(response.data)));
    }
  }

  void executeGetMyForum(int typeRequest, String lastForumId, String idGrade, String idMapel,
      Function(MyForumResponse) completion, {CancelToken cancelToken}) async {
    var response = await get("forum/mine/${await _userIdUrl}/$lastForumId/$idGrade/$idMapel", null, typeRequest, cancelToken: cancelToken);

    if(response != null){
      completion(MyForumResponse.fromJson(jsonDecode(response.data)));
    }
  }

  void executeGetForumAnswer(int typeRequest, String idUrlForum, Function(ForumAnswerResponse) completion) async {
    var response = await post("forum/$idUrlForum/answer/${await _userIdUrl}", null, typeRequest);

    if(response != null){
      completion(ForumAnswerResponse.fromJson(jsonDecode(response.data)));
    }
  }
}