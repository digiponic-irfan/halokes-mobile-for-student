class ServerReceivedYourMessageResponse {
  String conversationId;
  int receivedByServerAt;

  ServerReceivedYourMessageResponse.fromJson(Map<String, dynamic> json){
    conversationId = json["conversation_id"] ?? "";
    receivedByServerAt = json["received_by_server_at"] ?? 0;
  }
}