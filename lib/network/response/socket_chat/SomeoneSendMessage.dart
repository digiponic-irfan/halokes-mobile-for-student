class SomeoneSendMessageResponse {
  String conversationId;
  String senderUrlId;
  String receiverUrlId;
  String conversation;
  int receivedByServerAt;
  String senderName;
  String senderFoto;

  SomeoneSendMessageResponse.fromJson(Map<String, dynamic> json){
    conversationId = json["conversation_id"] ?? "";
    senderUrlId = json["sender_url_id"] ?? "";
    receiverUrlId = json["receiver_url_id"] ?? "";
    conversation = json["conversation"] ?? "";
    receivedByServerAt = json["received_by_server_at"] ?? 0;
    senderName = json["sender"]["display_name"] ?? "";
    senderFoto = json["sender"]["display_foto"] ?? "";
  }
}