class TargetReceivedYourMessageResponse {
  String conversationId;
  int receivedByTargetAt;

  TargetReceivedYourMessageResponse.fromJson(Map<String, dynamic> json){
    conversationId = json["conversation_id"] ?? "";
    receivedByTargetAt = json["received_by_target_at"] ?? 0;
  }
}