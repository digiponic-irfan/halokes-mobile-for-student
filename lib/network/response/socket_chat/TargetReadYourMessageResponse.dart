class TargetReadYourMessageResponse {
  String receiverId;
  int readAt;

  TargetReadYourMessageResponse.fromJson(Map<String, dynamic> json){
    receiverId = json["receiver_id"] ?? "";
    readAt = json["read_at"] ?? 0;
  }
}