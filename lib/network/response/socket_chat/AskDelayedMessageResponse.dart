class AskDelayedMessageResponse {
  List<AskDelayedMessageData> data = List();

  AskDelayedMessageResponse.fromJson(Map<String, dynamic> json){
    if(json["data"] != null){
      json["data"].forEach((it) => data.add(AskDelayedMessageData.fromJson(it)));
    }
  }
}

class AskDelayedMessageData {
  String idConversation;
  String conversation;
  String senderUrlId;
  String receiverUrlId;
  int receivedByTargetAt;
  AskDelayedMessageSender sender;

  AskDelayedMessageData.fromJson(Map<String, dynamic> json){
    idConversation = json["id_conversation"] ?? "";
    conversation = json["conversation"] ?? "";
    senderUrlId = json["sender_url_id"] ?? "";
    receiverUrlId = json["receiver_url_id"] ?? "";
    receivedByTargetAt = json["received_by_target_at"] ?? 0;

    sender = AskDelayedMessageSender.fromJson(json["sender"]);
  }
}

class AskDelayedMessageSender {
  String name;
  String foto;

  AskDelayedMessageSender.fromJson(Map<String, dynamic> json){
    name = json["display_name"] ?? "";
    foto = json["display_foto"] ?? "";
  }
}