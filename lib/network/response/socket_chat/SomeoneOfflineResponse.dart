import 'package:halokes_siswa/ancestor/BaseResponse.dart';
import 'package:intl/intl.dart';

class SomeoneOfflineResponse extends BaseResponse {
  String message;
  String lastLogout;
  String idUserUrl;

  SomeoneOfflineResponse.fromJson(Map<String, dynamic> json){
    message = json["message"] ?? "";

    if(json["last_logout"] != null){
      var today = DateTime.now();
      var lastSeen = DateFormat("yyyy-MM-dd HH:mm:ss").parse(json["last_logout"]);
      var differDays = today.weekday - lastSeen.weekday;

      if(today.year == lastSeen.year && today.month == lastSeen.month){
        if(differDays == 1){
          lastLogout = "Terakhir dilihat kemarin ini pukul ${DateFormat("HH:mm").format(lastSeen)}";
        }else if(differDays == 0){
          lastLogout = "Terakhir dilihat hari ini pukul ${DateFormat("HH:mm").format(lastSeen)}";
        }else {
          lastLogout = "Terakhir dilihat ${DateFormat("dd MMM").format(lastSeen)} ini pukul ${DateFormat("HH:mm").format(lastSeen)}";
        }
      }else {
        lastLogout = "Terakhir dilihat ${DateFormat("dd MMM").format(lastSeen)} ini pukul ${DateFormat("HH:mm").format(lastSeen)}";
      }
    }else {
      lastLogout = "";
    }

    idUserUrl = json["id_user_url"] ?? "";
  }
}