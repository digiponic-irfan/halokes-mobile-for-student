class SomeoneOnlineResponse {
  String message;
  String idUserUrl;

  SomeoneOnlineResponse.fromJson(Map<String, dynamic> json){
    message = json["message"] ?? "";
    idUserUrl = json["id_user_url"] ?? "";
  }
}