import 'package:halokes_siswa/ancestor/BaseResponse.dart';

class AnnouncementResponse extends BaseResponse {
  List<AnnouncementData> data;

  AnnouncementResponse.fromJson(Map<String, dynamic> json){
    data = List();
    if(json["data"] != null){
      json["data"].forEach((it) => data.add(AnnouncementData.fromJson(it)));
    }
  }
}

class AnnouncementData {
  String idBeritaUrl;
  String judulBerita;
  String isiBerita;
  String gambarBerita ;
  String penulis;
  String tanggal;
  String kategori;

  AnnouncementData.fromJson(Map<String, dynamic> json){
    idBeritaUrl = json["id_berita_url"] ?? "";
    judulBerita = json["judul_berita"] ?? "";
    isiBerita = json["isi_berita"] ?? "";
    gambarBerita = json["gambar_berita"] ?? "";
    penulis = json["penulis"] ?? "";
    tanggal = json["tanggal"] ?? "";
    kategori = json["kategori"] ?? "";
  }
}