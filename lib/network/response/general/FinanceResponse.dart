import 'package:halokes_siswa/ancestor/BaseResponse.dart';

class FinanceResponse extends BaseResponse {
  List<FinanceData> data;

  FinanceResponse.fromJson(Map<String, dynamic> json) : super.fromJson(json){
    data = List();
    if(json["data"] != null){
      json["data"].forEach((it) => data.add(FinanceData.fromJson(it)));
    }
  }
}

class FinanceData {
  String idSiswaKeuanganUrl;
  String keterangan;
  String deskripsi;
  String nominal;
  String jatuhTempo;
  String tanggalPembayaran;
  String statusPembayaran;

  FinanceData.fromJson(Map<String, dynamic> json){
    idSiswaKeuanganUrl = json["id_siswa_keuangan_url"] ?? "";
    keterangan = json["keterangan"] ?? "";
    deskripsi = json["deskripsi"] ?? "";
    nominal = json["nominal"] ?? "";
    jatuhTempo = json["jatuh_tempo"] ?? "";
    tanggalPembayaran = json["tanggal_pembayaran"] ?? "";
    statusPembayaran = json["status_pembayaran"] ?? "";
  }
}