import 'package:halokes_siswa/ancestor/BaseResponse.dart';
import 'package:mcnmr_common_ext/NonNullChecker.dart';

class ScoreResponse extends BaseResponse{
  List<ScoreData> data;

  ScoreResponse.fromJson(Map<String, dynamic> json) : super.fromJson(json){
    data = List();
    if(json["nilai"] != null){
      json["nilai"].forEach((it) => data.add(ScoreData.fromJson(it)));
    }
  }
}

class ScoreData {
  String idMapelJadwalUrl;
  String mapelNama;
  List<String> tugas;
  String uts;
  String uas;
  List<String> ulanganHarian;

  ScoreData.fromJson(Map<String, dynamic> json){
    idMapelJadwalUrl = obtainValue(json["id_mapel_jadwal_url"], "");
    mapelNama = obtainValue(json["mapel_nama"], "");
    tugas = obtainValue(json["TUGAS"].cast<String>(), List());
    uts = obtainValue(json["UTS"], "0");
    uas = obtainValue(json["UAS"], "0");
    ulanganHarian = obtainValue(json["ULANGAN_HARIAN"].cast<String>(), List());
  }
}