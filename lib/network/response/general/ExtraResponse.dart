import 'package:halokes_siswa/ancestor/BaseResponse.dart';
import 'package:mcnmr_common_ext/NonNullChecker.dart';

class ExtraResponse extends BaseResponse{
  List<ExtraData> data;

  ExtraResponse.fromJson(Map<String, dynamic> json) : super.fromJson(json){
    data = List();

    if(json["data"] != null){
      json["data"].forEach((it) => data.add(ExtraData.fromJson(it)));
    }
  }
}

class ExtraData {
  String nama;
  List<ExtraSchedule> jadwal;
  String ekskulUrl;
  String tempat;

  ExtraDetail detail;

  ExtraData.fromJson(Map<String, dynamic> json){
    nama = json["nama"] ?? "";
    ekskulUrl = json["ekskul_url"] ?? "";
    tempat = json["tempat"] ?? "";

    jadwal = List();
    if(json["jadwal"] != null){
      json["jadwal"].forEach((it) => jadwal.add(ExtraSchedule.fromJson(it)));
    }

    if(json["detail"] != null){
      detail = obtainValue(ExtraDetail.fromJson(json["detail"]), null);
    }
  }
}

class ExtraSchedule {
  String idEkskulJadwal;
  String idEkskulJadwalUrl;
  String idEkskul;
  String hari;
  String jamMulai;
  String jamSelesai;
  String status;

  ExtraSchedule.fromJson(Map<String, dynamic> json){
    idEkskulJadwal = json["id_ekskul_jadwal"] ?? "";
    idEkskulJadwalUrl = json["id_ekskul_jadwal_url"] ?? "";
    idEkskul = json["id_ekskul"] ?? "";
    hari = json["hari"] ?? "";
    jamMulai = json["jam_mulai"] ?? "";
    jamSelesai = json["jam_selesai"] ?? "";
    status = json["status"] ?? "";
  }

  @override
  String toString() {
    return "$hari : $jamMulai - $jamSelesai";
  }
}

class ExtraDetail {
  String idEkskul;
  String namaEkskul;
  String deskipsi;
  String pembina;
  String jadwalHari;
  String jadwalJam;
  List<ExtraActivity> kegiatan;
  List<ExtraMember> anggota;

  ExtraDetail.fromJson(Map<String, dynamic> json){
    idEkskul = json["id_ekskul"] ?? "";
    namaEkskul = json["nama_ekskul"] ?? "";
    deskipsi = json["deskripsi"] ?? "";
    pembina = json["pembina"] ?? "";
    jadwalHari = json["jadwal_hari"] ?? "";
    jadwalJam = json["jadwal_jam"] ?? "";

    anggota = List();
    if(json["anggota"]["data_anggota"] != null){
      json["anggota"]["data_anggota"].forEach((it) => anggota.add(ExtraMember.fromJson(it)));
    }

    kegiatan = List();
    if(json["kegiatan"]["data_kegiatan"] != null){
      json["kegiatan"]["data_kegiatan"].forEach((it) => kegiatan.add(ExtraActivity.fromJson(it)));
    }
  }
}

class ExtraActivity {
  String idKegiatan;
  String judul;
  String deskripsi;
  String tempat;
  String tanggal;

  ExtraActivity.fromJson(Map<String, dynamic> json){
    idKegiatan = json["id_kegiatan"] ?? "";
    judul = json["judul"] ?? "";
    deskripsi = json["deskripsi"] ?? "";
    tempat = json["tempat"] ?? "";
    tanggal = json["tanggal"] ?? "";
  }
}

class ExtraMember {
  String idAnggota;
  String nis;
  String namaAnggota;
  String jabatan;
  String status;

  ExtraMember.fromJson(Map<String, dynamic> json){
    idAnggota = json["id_anggota"] ?? "";
    nis = json["nis"] ?? "";
    namaAnggota = json["nama_anggota"] ?? "";
    jabatan = json["jabatan"] ?? "";
    status = json["status"] ?? "";
  }
}