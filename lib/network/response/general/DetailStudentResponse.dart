import 'package:halokes_siswa/ancestor/BaseResponse.dart';
import 'package:mcnmr_common_ext/NonNullChecker.dart';

class DetailStudentResponse extends BaseResponse{
  String idSiswa;
  String idSiswaUrl;
  String idTapel;
  String siswaNama;
  String siswaNisn;
  String siswaNis;
  String siswaTglLahir;
  String siswaTempatLahir;
  String siswaJkel;
  String siswaAlamat;
  String siswaNoHp;
  String siswaEmail;
  String siswaAgama;
  String siswaStatus;
  String siswaFoto;
  String namaAyah;
  String noHpAyah;
  String namaIbu;
  String noHpIbu;
  String namaWali;
  String noHpWali;
  String angkatan;

  DetailStudentResponse.fromJson(Map<String, dynamic> json) : super.fromJson(json){
    idSiswa = obtainValue(json["siswa"]["id_siswa"], "");
    idSiswaUrl = obtainValue(json["siswa"]["id_siswa_url"], "");
    idTapel = obtainValue(json["siswa"]["id_tapel"], "");
    siswaNama = obtainValue(json["siswa"]["siswa_nama"], "");
    siswaNisn = obtainValue(json["siswa"]["siswa_nisn"], "");
    siswaNis = obtainValue(json["siswa"]["siswa_nis"], "");
    siswaTglLahir = obtainValue(json["siswa"]["siswa_tgl_lahir"], "");
    siswaTempatLahir = obtainValue(json["siswa"]["siswa_tempat_lahir"], "");
    siswaJkel = obtainValue(json["siswa"]["siswa_jkel"], "");
    siswaAlamat = obtainValue(json["siswa"]["siswa_alamat"], "");
    siswaNoHp = obtainValue(json["siswa"]["siswa_no_hp"], "");
    siswaEmail = obtainValue(json["siswa"]["siswa_email"], "");
    siswaAgama = obtainValue(json["siswa"]["siswa_agama"], "");
    siswaStatus = obtainValue(json["siswa"]["siswa_status"], "");
    siswaFoto = obtainValue(json["siswa"]['siswa_foto'], "");
    namaAyah = obtainValue(json["siswa"]["nama_ayah"], "");
    noHpAyah = obtainValue(json["siswa"]["no_hp"], "");
    namaIbu = obtainValue(json["siswa"]["nama_ibu"], "");
    noHpIbu = obtainValue(json["siswa"]["no_hp_ibu"], "");
    namaWali = obtainValue(json["siswa"]["nama_wali"], "");
    noHpWali = obtainValue(json["siswa"]["no_hp_wali"], "");
    angkatan = obtainValue(json["siswa"]["angkatan"], "");
  }
}