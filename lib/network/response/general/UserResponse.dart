import 'dart:convert';

import 'package:halokes_siswa/ancestor/BaseResponse.dart';

class UserResponse extends BaseResponse{
  UserData data;

  UserResponse.fromJson(Map<String, dynamic> json) : super.fromJson(json){
    data = UserData.fromJson(json["data"]);
  }
}

class UserData{
  String idUser;
  String idUrl;
  String username;
  String nis;
  String namaSiswa;
  String kelas;
  String role;
  String foto;

  UserData.fromJson(Map<String, dynamic> json){
    idUser = json["id_user"] ?? "";
    idUrl = json["id_url"] ?? "";
    username = json["username"] ?? "";
    nis = json["nis"] ?? "";
    namaSiswa = json["nama_siswa"] ?? "";
    kelas = json["kelas"] ?? "";
    role = json["role"] ?? "";
    foto = json["foto"] ?? "";
  }

  String toJson(){
    return jsonEncode({
      "id_user" : idUser,
      "id_url" : idUrl,
      "username" : username,
      "nis" : nis,
      "nama_siswa" : namaSiswa,
      "kelas" : kelas,
      "role" : role,
      "foto" : foto
    });
  }
}