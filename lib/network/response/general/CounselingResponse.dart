import 'package:halokes_siswa/ancestor/BaseResponse.dart';
import 'package:mcnmr_common_ext/NonNullChecker.dart';

class CounselingResponse extends BaseResponse {
  CounselingData data;

  CounselingResponse.fromJson(Map<String, dynamic> json){
    if(json["data"] != null){
      data = CounselingData.fromJson(json["data"]);
    }
  }
}

class CounselingData {
  String namaSiswa;
  String kelas;
  String statusSiswa;
  Achievement dataPrestasi;
  Violation dataPelanggaran;

  CounselingData.fromJson(Map<String, dynamic> json){
    namaSiswa = obtainValue(json["nama_siswa"], "");
    kelas = obtainValue(json["kelas"], "");
    statusSiswa = obtainValue(json["status_siswa"], "");

    if(json["data_prestasi"] != null){
      dataPrestasi = Achievement.fromJson(json["data_prestasi"]);
    }

    if(json["data_pelanggaran"] != null){
      dataPelanggaran = Violation.fromJson(json["data_pelanggaran"]);
    }
  }
}

class Achievement {
  int poinPrestasi;
  List<AchievementDetail> dataPrestasiDetail;

  Achievement.fromJson(Map<String, dynamic> json){
    poinPrestasi = obtainValue(json["poin_prestasi"], 0);

    dataPrestasiDetail = List();
    if(json["data_prestasi_detail"] != null){
      json["data_prestasi_detail"].forEach((it) => dataPrestasiDetail.add(AchievementDetail.fromJson(it)));
    }
  }
}

class AchievementDetail {
  String namaLomba;
  String tanggal;
  String tingkat;
  String prestasiKeterangan;
  int poin;

  AchievementDetail.fromJson(Map<String, dynamic> json){
    namaLomba = obtainValue(json["nama_lomba"], "");
    tanggal = obtainValue(json["tanggal"], "");
    tingkat = obtainValue(json["tingkat"], "");
    prestasiKeterangan = obtainValue(json["prestasi_keterangan"], "");
    poin = obtainValue(json["poin"], 0);
  }
}

class Violation {
  int poinPelanggaran;
  List<ViolationDetail> dataPelanggaranDetail;

  Violation.fromJson(Map<String, dynamic> json) {
    poinPelanggaran = obtainValue(json["poin_pelanggaran"], "");

    dataPelanggaranDetail = List();
    if(json["data_pelanggaran_detail"] != null){
      json["data_pelanggaran_detail"].forEach((it) => dataPelanggaranDetail.add(ViolationDetail.fromJson(it)));
    }
  }
}

class ViolationDetail {
  String pelanggaran;
  String kategori;
  String tanggal;
  int poin;

  ViolationDetail.fromJson(Map<String, dynamic> json){
    pelanggaran = obtainValue(json["pelanggaran"], "");
    kategori = obtainValue(json["kategori"], "");
    tanggal = obtainValue(json["tanggal"], "");
    poin = obtainValue(json["poin"], 0);
  }
}