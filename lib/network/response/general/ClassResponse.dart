import 'package:halokes_siswa/ancestor/BaseResponse.dart';
import 'package:mcnmr_common_ext/NonNullChecker.dart';

class ClassResponse extends BaseResponse {
  ClassData data;

  ClassResponse.fromJson(Map<String, dynamic> json) : super.fromJson(json){
    if(json["data"] != null){
      data = ClassData.fromJson(json["data"]);
    }
  }
}

class ClassData {
  String idKelas;
  String nis;
  String nama;
  String kelas;
  String waliKelas;
  List<ClassStudentData> dataSiswa;

  ClassData.fromJson(Map<String, dynamic> json) {
    idKelas = obtainValue(json["id_kelas"], "");
    nis = obtainValue(json["nis"], "");
    nama = obtainValue(json["nama"], "");
    kelas = obtainValue(json["kelas"], "");
    waliKelas = obtainValue(json["wali_kelas"], "");

    dataSiswa = List();
    if(json["data_siswa"] != null){
      json["data_siswa"].forEach((it) => dataSiswa.add(ClassStudentData.fromJson(it)));
    }
  }
}

class ClassStudentData {
  String uid;
  String nama;
  String nis;

  ClassStudentData.fromJson(Map<String, dynamic> json){
    uid = obtainValue(json["uid"], "");
    nis = obtainValue(json["nis"], "");
    nama = obtainValue(json["nama"], "");
  }
}