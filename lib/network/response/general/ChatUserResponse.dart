import 'package:halokes_siswa/ancestor/BaseResponse.dart';
import 'package:halokes_siswa/database/database.dart';
import 'package:intl/intl.dart';

class ChatDisplayUser {
  String displayName;
  String displayFotoUrl;
  String displayOnline;

  String idUrl;

  ChatDisplayUser();

  ChatDisplayUser.fromListContact(ListContactResult contact){
    idUrl = contact.idUrl;
    displayName = contact.displayName;
    displayFotoUrl = contact.displayFotoUrl;
    displayOnline = "";
  }
}

class AvailableTeacherResponse extends BaseResponse {
  List<ChatTeacherUser> data;
  AvailableTeacherResponse.fromJson(Map<String, dynamic> json) : super.fromJson(json){
    data = List();
    if(json["data"] != null){
      json["data"].forEach((it) => data.add(ChatTeacherUser.fromJson(it)));
    }
  }
}

class AvailableParentResponse extends BaseResponse {
  List<ChatParentUser> data;
  AvailableParentResponse.fromJson(Map<String, dynamic> json) : super.fromJson(json){
    data = List();
    if(json["data"] != null){
      json["data"].forEach((it) => data.add(ChatParentUser.fromJson(it)));
    }
  }
}

class AvailableStudentResponse extends BaseResponse {
  List<ChatStudentUser> data;
  AvailableStudentResponse.fromJson(Map<String, dynamic> json) : super.fromJson(json){
    data = List();
    if(json["data"] != null){
      json["data"].forEach((it) => data.add(ChatStudentUser.fromJson(it)));
    }
  }
}

class ChatTeacherUser extends ChatDisplayUser {
  String idGuruUrl;
  String lastLogout;
  int statusLogin;
  String guruNama;
  String guruGelarDepan;
  String guruGelarBelakang;
  String guruFoto;

  ChatTeacherUser.fromJson(Map<String, dynamic> json){
    idGuruUrl = json["id_guru_url"] ?? "";

    if(json["last_logout"] != null){
      var today = DateTime.now();
      var lastSeen = DateFormat("yyyy-MM-dd HH:mm:ss").parse(json["last_logout"]);
      var differDays = today.weekday - lastSeen.weekday;

      if(today.year == lastSeen.year && today.month == lastSeen.month){
        if(differDays == 1){
          lastLogout = "Terakhir dilihat kemarin ini pukul ${DateFormat("HH:mm").format(lastSeen)}";
        }else if(differDays == 0){
          lastLogout = "Terakhir dilihat hari ini pukul ${DateFormat("HH:mm").format(lastSeen)}";
        }else {
          lastLogout = "Terakhir dilihat ${DateFormat("dd MMM").format(lastSeen)} ini pukul ${DateFormat("HH:mm").format(lastSeen)}";
        }
      }else {
        lastLogout = "Terakhir dilihat ${DateFormat("dd MMM").format(lastSeen)} ini pukul ${DateFormat("HH:mm").format(lastSeen)}";
      }
    }else {
      lastLogout = "";
    }

    statusLogin = json["status_login"] != null ? int.parse(json["status_login"]) : 0;
    guruNama = json["guru_nama"] ?? "";
    guruGelarDepan = json["guru_gelar_depan"] ?? "";
    guruGelarBelakang = json["guru_gelar_belakang"] ?? "";
    guruFoto = json["guru_foto"] ?? "";
  }

  @override
  String get displayName => "${guruGelarDepan != "" ? "$guruGelarDepan " : ""}$guruNama $guruGelarBelakang";

  @override
  String get displayFotoUrl => guruFoto;

  @override
  String get displayOnline => statusLogin == 0 ? "$lastLogout" : "Online";

  @override
  String get idUrl => idGuruUrl;
}

class ChatParentUser extends ChatDisplayUser {
  String lastLogout;
  int statusLogin;
  String idSiswaOrtuUrl;
  String namaAyah;
  String namaIbu;
  String namaWali;
  String siswaNama;
  String siswaNisn;
  String siswaNis;
  String ortuFoto;

  ChatParentUser.fromJson(Map<String, dynamic> json){
    if(json["last_logout"] != null){
      var today = DateTime.now();
      var lastSeen = DateFormat("yyyy-MM-dd HH:mm:ss").parse(json["last_logout"]);
      var differDays = today.weekday - lastSeen.weekday;

      if(today.year == lastSeen.year && today.month == lastSeen.month){
        if(differDays == 1){
          lastLogout = "Terakhir dilihat kemarin ini pukul ${DateFormat("HH:mm").format(lastSeen)}";
        }else if(differDays == 0){
          lastLogout = "Terakhir dilihat hari ini pukul ${DateFormat("HH:mm").format(lastSeen)}";
        }else {
          lastLogout = "Terakhir dilihat ${DateFormat("dd MMM").format(lastSeen)} ini pukul ${DateFormat("HH:mm").format(lastSeen)}";
        }
      }else {
        lastLogout = "Terakhir dilihat ${DateFormat("dd MMM").format(lastSeen)} ini pukul ${DateFormat("HH:mm").format(lastSeen)}";
      }
    }else {
      lastLogout = "";
    }

    statusLogin = json["status_login"] != null ? int.parse(json["status_login"]) : 0;
    idSiswaOrtuUrl = json["id_siswa_ortu_url"] ?? "";
    namaAyah = json["nama_ayah"] ?? "";
    namaIbu = json["nama_ibu"] ?? "";
    namaWali = json["nama_wali"] ?? "";
    siswaNama = json["siswa_nama"] ?? "";
    siswaNisn = json["siswa_nisn"] ?? "";
    siswaNis = json["siswa_nis"] ?? "";
    ortuFoto = json["ortu_foto"] ?? "";
  }

  @override
  String get displayName => "${namaAyah != "" ? namaAyah : namaIbu != "" ? namaIbu : namaWali} (Wali $siswaNama)";

  @override
  String get displayFotoUrl => ortuFoto;

  @override
  String get displayOnline => statusLogin == 0 ? "$lastLogout" : "Online";

  @override
  String get idUrl => idSiswaOrtuUrl;
}

class ChatStudentUser extends ChatDisplayUser {
  String idSiswaUrl;
  String lastLogout;
  int statusLogin;
  String siswaNama;
  String siswaNisn;
  String siswaNis;
  String siswaFoto;

  ChatStudentUser.fromJson(Map<String, dynamic> json){
    idSiswaUrl = json["id_siswa_url"] ?? "";

    if(json["last_logout"] != null){
      var today = DateTime.now();
      var lastSeen = DateFormat("yyyy-MM-dd HH:mm:ss").parse(json["last_logout"]);
      var differDays = today.weekday - lastSeen.weekday;

      if(today.year == lastSeen.year && today.month == lastSeen.month){
        if(differDays == 1){
          lastLogout = "Terakhir dilihat kemarin ini pukul ${DateFormat("HH:mm").format(lastSeen)}";
        }else if(differDays == 0){
          lastLogout = "Terakhir dilihat hari ini pukul ${DateFormat("HH:mm").format(lastSeen)}";
        }else {
          lastLogout = "Terakhir dilihat ${DateFormat("dd MMM").format(lastSeen)} ini pukul ${DateFormat("HH:mm").format(lastSeen)}";
        }
      }else {
        lastLogout = "Terakhir dilihat ${DateFormat("dd MMM").format(lastSeen)} ini pukul ${DateFormat("HH:mm").format(lastSeen)}";
      }
    }else {
      lastLogout = "";
    }

    statusLogin = json["status_login"] != null ? int.parse(json["status_login"]) : 0;
    siswaNama = json["siswa_nama"];
    siswaNis = json["siswa_nis"];
    siswaFoto = json["siswa_foto"];
  }

  @override
  String get displayName => siswaNama;

  @override
  String get displayFotoUrl => siswaFoto;

  @override
  String get displayOnline => statusLogin == 0 ? "$lastLogout" : "Online";

  @override
  String get idUrl => idSiswaUrl;
}