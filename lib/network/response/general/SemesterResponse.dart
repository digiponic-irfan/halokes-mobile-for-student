import 'package:halokes_siswa/ancestor/BaseResponse.dart';
import 'package:mcnmr_common_ext/NonNullChecker.dart';

class SemesterResponse extends BaseResponse {
  List<TapelData> tapel;
  List<SemesterData> semester;

  SemesterResponse.fromJson(Map<String, dynamic> json) : super.fromJson(json){
    tapel = List();
    if(json["tapel"] != null){
      json["tapel"].forEach((it) => tapel.add(TapelData.fromJson(it)));
    }

    semester = List();
    if(json["smt"] != null){
      json["smt"].forEach((it) => semester.add(SemesterData.fromJson(it)));
    }
  }
}

class TapelData {
  String id;
  String tapelNama;
  String tapelAktif;

  TapelData.fromJson(Map<String, dynamic> json){
    id = obtainValue(json["_id"], "");
    tapelNama = obtainValue(json["tapel_nama"], "");
    tapelAktif = obtainValue(json["tapel_aktif"], "");
  }
}

class SemesterData {
  String id;
  String tapel;
  String tapelAktif;
  String smtNama;
  String semesterMulai;
  String semesterAkhir;
  String smtStatus;

  SemesterData.fromJson(Map<String, dynamic> json){
    id = obtainValue(json["_id"], "");
    tapel = obtainValue(json["tapel"], "");
    tapelAktif = obtainValue(json["tapel_aktif"], "");
    smtNama = obtainValue(json["smt_nama"], "");
    semesterMulai = obtainValue(json["semester_mulai"], "");
    semesterAkhir = obtainValue(json["semester_akhir"], "");
    smtStatus = obtainValue(json["smt_status"], "");
  }

  @override
  String toString() {
    return "$tapel $smtNama";
  }
}