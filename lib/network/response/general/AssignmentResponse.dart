import 'package:halokes_siswa/ancestor/BaseResponse.dart';
import 'package:mcnmr_common_ext/NonNullChecker.dart';

class AssignmentResponse extends BaseResponse {
  List<AssignmentData> data;

  AssignmentResponse.fromJson(Map<String, dynamic> json) : super.fromJson(json){
    data = List();
    if(json["data"] != null){
      json["data"].forEach((it) => data.add(AssignmentData.fromJson(it)));
    }
  }
}

class AssignmentData {
  String idMapel;
  String namaMapel;
  int jumlahTugasMapel;
  List<AssignmentDetail> dataTugas;

  AssignmentData.fromJson(Map<String, dynamic> json){
    idMapel = obtainValue(json["id_mapel"], "");
    namaMapel = obtainValue(json["nama_mapel"], "");
    jumlahTugasMapel = obtainValue(json["jumlah_tugas_mapel"], 0);

    dataTugas = List();
    if(json["data_tugas"] != null){
      json["data_tugas"].forEach((it) => dataTugas.add(AssignmentDetail.fromJson(it)));
    }
  }
}

class AssignmentDetail {
  String idTugas;
  String judulTugas;
  String deskripsi;
  String tglBuat;
  String deadline;
  int sisaWaktu;
  int status;

  AssignmentDetail.fromJson(Map<String, dynamic> json){
    idTugas = obtainValue(json["id_tugas"], "");
    judulTugas = obtainValue(json["judul_tugas"], "");
    deskripsi = obtainValue(json["deskripsi"], "");
    tglBuat = obtainValue(json["tgl_buat"], "");
    deadline = obtainValue(json["deadline"], "");
    sisaWaktu = obtainValue(json["sisa_waktu"], 0);
    status = obtainValue(json["status"], 0);
  }
}