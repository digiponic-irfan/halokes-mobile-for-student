import 'package:flutter/cupertino.dart';
import 'package:halokes_siswa/ancestor/BaseResponse.dart';
import 'package:mcnmr_common_ext/NonNullChecker.dart';
import 'package:intl/intl.dart';

class AttendanceResponse extends BaseResponse{
  AttendanceData data;

  AttendanceResponse.fromJson(Map<String, dynamic> json) : super.fromJson(json){
    data = AttendanceData.fromJson(json["data"]);
  }
}

class AttendanceData {
  String hadir;
  String sakit;
  List<AttendanceDate> tglSakit;
  String izin;
  List<AttendanceDate> tglIzin;
  String alpha;
  List<AttendanceDate> tglAlpha;
  String persenHadir;

  AttendanceData.fromJson(Map<String, dynamic> json){
    hadir = obtainValue(json["hadir"], "");
    sakit = obtainValue(json["sakit"], "");
    izin = obtainValue(json["izin"], "");
    alpha = obtainValue(json["alpha"], "");
    persenHadir = obtainValue(json["persen_hadir"], "0");

    tglAlpha = List();
    if(json["tgl_alpha"] != null){
      json["tgl_alpha"].forEach((it) => tglAlpha.add(AttendanceDate.fromStringDate(it, "dd-MM-yyyy")));
    }

    tglSakit = List();
    if(json["tgl_sakit"] != null){
      json["tgl_sakit"].forEach((it) => tglSakit.add(AttendanceDate.fromStringDate(it, "dd-MM-yyyy")));
    }

    tglIzin = List();
    if(json["tgl_izin"] != null){
      json["tgl_izin"].forEach((it) => tglIzin.add(AttendanceDate.fromStringDate(it, "dd-MM-yyyy")));
    }
  }
}

class AttendanceDate{
  int year;
  int month;
  int day;

  AttendanceDate({@required this.year, @required this.month, @required this.day});

  AttendanceDate.fromStringDate(String date, String format){
    var dtime = DateFormat(format).parse(date);

    year = dtime.year;
    month = dtime.month;
    day = dtime.day;
  }

  AttendanceDate.fromDateTime(DateTime date){
    year = date.year;
    month = date.month;
    day = date.day;
  }

  @override
  String toString() => "$day $month $year";

  @override
  int get hashCode => toString().hashCode;

  @override
  bool operator ==(other) => toString() == other.toString();
}