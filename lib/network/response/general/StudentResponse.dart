import 'package:halokes_siswa/ancestor/BaseResponse.dart';

class StudentResponse extends BaseResponse {
  StudentData data;

  StudentResponse.fromJson(Map<String, dynamic> json) : super.fromJson(json){
    if(json["data"] != null){
      data = StudentData.fromJson(json["data"]);
    }
  }
}

class StudentData {
  String nis;
  String nama;
  int statusSiswa;
  StudentIdentity dataDiri;
  ParentStudentIdentity dataOrtu;
  int totalRr;
  double totalKehadiran;
  int totalPrestasi;
  int totalPelanggaran;
  int totalTugas;
  
  StudentData.fromJson(Map<String, dynamic> json){
    nis = json["nis"] ?? "";
    nama = json["name"] ?? json["nama"] ?? "";
    statusSiswa = json["status_siswa"] ?? 0;
    
    if(json["data_diri"] != null){
      dataDiri = StudentIdentity.fromJson(json["data_diri"]);
    }
    
    if(json["data_ortu"] != null){
      dataOrtu = ParentStudentIdentity.fromJson(json["data_ortu"]);
    }

    totalRr = json["total_rr"] ?? 0;
    totalKehadiran = json["total_kehadiran"] != null ? json["total_kehadiran"].toDouble() : 0.0;
    totalPrestasi = json["total_prestasi"] ?? 0;
    totalPelanggaran = json["total_pelanggaran"] ?? 0;
    totalTugas = json["total_tugas"] ?? 0;
  }
}

class StudentIdentity {
  String namaLengkap;
  String nisn;
  String tempatLahir;
  String tglLahir;
  String jkel;
  String alamat;
  String noHp;
  String email;
  String agama;
  String asalSekolah;
  
  StudentIdentity.fromJson(Map<String, dynamic> json){
    namaLengkap = json["nama_lengkap"] ?? "";
    nisn = json["nisn"] ?? "";
    tempatLahir = json["tempat_lahir"] ?? "";
    tglLahir = json["tgl_lahir"] ?? "";
    jkel = json["jkel"] ?? "";
    alamat = json["alamat"] ?? "";
    noHp = json["no_hp"] ?? "";
    email = json["email"] ?? "";
    agama = json["agama"] ?? "";
    asalSekolah = json["asal_sekolah"] ?? "";
  }
}

class ParentStudentIdentity {
  String namaAyah;
  String nohpAyah;
  String namaIbu;
  String nohpIbu;
  String namaWali;
  String nohpWali;

  ParentStudentIdentity.fromJson(Map<String, dynamic> json){
    namaAyah = json["nama_ayah"] ?? "";
    nohpAyah = json["nohp_ayah"] ?? "";
    namaIbu = json["nama_ibu"] ?? "";
    nohpIbu = json["nohp_ibu"] ?? "";
    namaWali = json["nama_wali"] ?? "";
    nohpWali = json["nohp_wali"] ?? "";
  }
}