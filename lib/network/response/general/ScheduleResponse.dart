import 'package:halokes_siswa/ancestor/BaseResponse.dart';

class ScheduleResponse extends BaseResponse {
  List<ScheduleData> data;

  ScheduleResponse.fromJson(Map<String, dynamic> json) : super.fromJson(json){
    data = List();
    if(json["data"] != null){
      json["data"].forEach((it) => data.add(ScheduleData.fromJson(it)));
    }
  }
}

class ScheduleData {
  String hari;
  List<ScheduleDetail> dataJadwal;

  ScheduleData.fromJson(Map<String, dynamic> json){
    hari = json["hari"] ?? "";

    dataJadwal = List();
    if(json["data_jadwal"] != null){
      json["data_jadwal"].forEach((it) => dataJadwal.add(ScheduleDetail.fromJson(it)));
    }
  }
}

class ScheduleDetail {
  String idJadwal;
  String hari;
  String mapel;
  String guru;
  String jam;

  ScheduleDetail.fromJson(Map<String, dynamic> json){
    idJadwal = json["id_jadwal"] ?? "";
    hari = json["hari"] ?? "";
    mapel = json["mapel"] ?? "";
    guru = json["guru"] ?? "";
    jam = json["jam"] ?? "";
  }
}