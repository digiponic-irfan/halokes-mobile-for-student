import 'package:halokes_siswa/ancestor/BaseResponse.dart';
import 'package:intl/intl.dart';

class ForumAnswerResponse extends BaseResponse{
  List<ForumAnswerData> data = List();

  ForumAnswerResponse.fromJson(Map<String, dynamic> json) : super.fromJson(json){
    json["data"]?.forEach((it) => data.add(ForumAnswerData.fromJson(it)));
  }
}

class ForumAnswerData {
  String idAnswer;
  String idUrlAnswer;
  String idUrlAnswerer;
  String answererName;
  String answererFoto;
  String answererRole;
  String uidSchoolAnswerer;
  String sekolahNama;
  String answer;
  String answerDesc;
  String answerAt;
  int yourVote;
  int upvoteCount;
  int downvoteCount;

  ForumAnswerData.fromJson(Map<String, dynamic> json){
    idAnswer = json["id_answer"] ?? "";
    idUrlAnswer = json["id_url_answer"] ?? "";
    idUrlAnswerer = json["id_url_answerer"] ?? "";
    answererName = json["answerer_name"] ?? "";
    answererFoto = json["answerer_foto"] ?? "";
    answererRole = json["answerer_role"] ?? "";
    uidSchoolAnswerer = json["uid_school_answerer"] ?? "";
    sekolahNama = json["sekolah_nama"] ?? "";
    answer = json["answer"] ?? "";
    answerDesc = json["answer_desc"] ?? "";
    answerAt = json["answer_at"] ?? "";
    yourVote = json["your_vote"] == null ? 0 : int.parse(json["your_vote"]);
    upvoteCount = json["upvote_count"] == null ? 0 : int.parse(json["upvote_count"]);
    downvoteCount = json["downvote_count"] == null ? 0 : int.parse(json["downvote_count"]);
  }

  ForumAnswerData.justId(String id){
    idAnswer = id;
  }

  @override
  int get hashCode => idAnswer.hashCode;

  @override
  bool operator ==(other) => idAnswer == other.idAnswer;

  DateTime get time => DateFormat("yyyy-MM-dd HH:mm:ss").parse(answerAt);

  String get displayTime => DateFormat("dd EEE yyyy").format(DateFormat("yyyy-MM-dd HH:mm:ss").parse(answerAt));

}