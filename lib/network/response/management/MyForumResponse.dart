import 'package:halokes_siswa/ancestor/BaseResponse.dart';
import 'package:intl/intl.dart';

class MyForumResponse extends BaseResponse {
  List<MyForumData> data = List();

  MyForumResponse.fromJson(Map<String, dynamic> json) : super.fromJson(json){
    json["data"]?.forEach((it) => data.add(MyForumData.fromJson(it)));
  }

  void merge(MyForumResponse other){
    data.addAll(other.data);
  }
}

class MyForumData {
  String idForum;
  String idUrlForum;
  String image;
  String grade;
  String mapel;
  String question;
  String idUrlMarkedAnswer;
  String createdAt;

  MyForumData.fromJson(Map<String, dynamic> json){
    idForum = json["id_forum"] ?? "";
    idUrlForum = json["id_url_forum"] ?? "";
    image = json["image"] ?? "";
    grade = json["grade"] ?? "";
    mapel = json["mapel"] ?? "";
    question = json["question"] ?? "";
    idUrlMarkedAnswer = json["id_url_marked_answer"] ?? "";
    createdAt = json["created_at"] ?? "";
  }

  DateTime get time => DateFormat("yyyy-MM-dd HH:mm:ss").parse(createdAt);

  String get displayTime => DateFormat("dd EEE yyyy").format(DateFormat("yyyy-MM-dd HH:mm:ss").parse(createdAt));
}