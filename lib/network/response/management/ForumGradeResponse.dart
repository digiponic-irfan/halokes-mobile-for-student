import 'package:halokes_siswa/ancestor/BaseResponse.dart';

class ForumGradeResponse extends BaseResponse {
  List<ForumGradeData> data = List();

  ForumGradeResponse.fromJson(Map<String, dynamic> json) : super.fromJson(json){
    json["data"]?.forEach((it) => data.add(ForumGradeData.fromJson(it)));
  }
}

class ForumGradeData {
  String idGrade;
  String grade;

  ForumGradeData.fromJson(Map<String, dynamic> json){
    idGrade = json["id_grade"] ?? "";
    grade = json["grade"] ?? "";
  }

  @override
  String toString() => grade;

  @override
  int get hashCode => idGrade.hashCode;

  @override
  bool operator ==(other) => idGrade == other.idGrade;

  ForumGradeData.all(){
    idGrade = "all";
    grade = "Semua";
  }
}