import 'package:halokes_siswa/ancestor/BaseResponse.dart';
import 'package:mcnmr_common_ext/NonNullChecker.dart';

class DepartmentResponse extends BaseResponse {
  List<DepartmentData> data;

  DepartmentResponse.fromJson(Map<String, dynamic> json) : super.fromJson(json){
    data = List();

    if(json["data"] != null){
      json["data"].forEach((it) => data.add(DepartmentData.fromJson(it)));
    }
  }
}

class DepartmentData {
  String idJurusan;
  String idJurusanUrl;
  String jurusanNama;

  DepartmentData.fromJson(Map<String, dynamic> json){
    idJurusan = obtainValue(json["id_jurusan"], "");
    idJurusanUrl = obtainValue(json["id_jurusan_url"], "");
    jurusanNama = obtainValue(json["jurusan_nama"], "");
  }

  DepartmentData.all(){
    idJurusan = "all";
    idJurusanUrl = "all";
    jurusanNama = "Semua";
  }


  @override
  bool operator ==(other) => idJurusan == other.idJurusan;

  @override
  int get hashCode => idJurusan.hashCode;

  @override
  String toString() => jurusanNama;
}