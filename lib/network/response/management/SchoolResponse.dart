import 'dart:convert';

import 'package:halokes_siswa/ancestor/BaseResponse.dart';
import 'package:mcnmr_common_ext/NonNullChecker.dart';

class SchoolResponse extends BaseResponse {
  List<SchoolData> data;

  SchoolResponse.fromJson(Map<String, dynamic> json) : super.fromJson(json){
    data = List();
    if(json["data"] != null){
      json["data"].forEach((it) => data.add(SchoolData.fromJson(it)));
    }
  }
}

class SchoolData {
  String idSekolah;
  String sekolahUid;
  String idSekolahUrl;
  String sekolahNama;
  String sekolahAlamat;
  String sekolahLink;
  String status;

  SchoolData.fromJson(Map<String, dynamic> json){
    idSekolah = obtainValue(json["id_sekolah"], "");
    sekolahUid = obtainValue(json["sekolah_uid"], "");
    idSekolahUrl = obtainValue(json["id_sekolah_url"], "");
    sekolahNama = obtainValue(json["sekolah_nama"], "");
    sekolahAlamat = obtainValue(json["sekolah_alamat"], "");
    sekolahLink = obtainValue(json["sekolah_link"], "");
    status = obtainValue(json["status"], "");
  }

  String toJson() => jsonEncode({
    "id_sekolah" : idSekolah,
    "sekolah_uid" : sekolahUid,
    "id_sekolah_url" : idSekolahUrl,
    "sekolah_nama" : sekolahNama,
    "sekolah_alamat" : sekolahAlamat,
    "sekolah_link" : sekolahLink,
    "status" : status,
  });
}