import 'package:halokes_siswa/ancestor/BaseResponse.dart';
import 'package:mcnmr_common_ext/NonNullChecker.dart';

class EbookResponse extends BaseResponse {
  List<EbookData> data;

  EbookResponse.fromJson(Map<String, dynamic> json) : super.fromJson(json){
    data = List();
    if(json["data"] != null){
      json["data"].forEach((it) => data.add(EbookData.fromJson(it)));
    }
  }
}

class EbookData {
  String idEbook;
  String idEbookUrl;
  String judulEbook;
  String deskripsiEbook;
  String pengarang;
  String penerbit;
  String jenis;
  String views;
  String downloads;
  String thumbnail;
  String file;
  String kelas;
  String mapelNama;

  EbookData.fromJson(Map<String, dynamic> json){
    idEbook = obtainValue(json["id_ebook"], "");
    idEbookUrl = obtainValue(json["id_ebook_url"], "");
    judulEbook = obtainValue(json["judul_ebook"], "");
    deskripsiEbook = obtainValue(json["deskripsi_ebook"], "");
    pengarang = obtainValue(json["pengarang"], "");
    penerbit = obtainValue(json["penerbit"], "");
    jenis = obtainValue(json["jenis"], "");
    views = obtainValue(json["views"], "");
    downloads = obtainValue(json["downloads"], "");
    thumbnail = obtainValue(json["thumbnail"], "");
    file = obtainValue(json["file"], "");
    kelas = obtainValue(json["kelas"], "");
    mapelNama = obtainValue(json["mapel_nama"], "");
  }

  @override
  String toString() {
    return "$judulEbook $deskripsiEbook $pengarang $penerbit";
  }
}