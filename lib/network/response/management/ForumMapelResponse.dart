import 'package:halokes_siswa/ancestor/BaseResponse.dart';

class ForumMapelResponse extends BaseResponse {
  List<ForumMapelData> data = List();

  ForumMapelResponse.fromJson(Map<String, dynamic> json) : super.fromJson(json){
    json["data"]?.forEach((it) => data.add(ForumMapelData.fromJson(it)));
  }
}

class ForumMapelData {
  String idMapel;
  String mapel;

  ForumMapelData.fromJson(Map<String, dynamic> json){
    idMapel = json["id_mapel"] ?? "";
    mapel = json["mapel"] ?? "";
  }

  @override
  String toString() => mapel;

  @override
  int get hashCode => idMapel.hashCode;

  @override
  bool operator ==(other) => idMapel == other.idMapel;

  ForumMapelData.all(){
    idMapel = "all";
    mapel = "Semua";
  }
}