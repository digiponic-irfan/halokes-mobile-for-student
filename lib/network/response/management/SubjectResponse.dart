import 'package:halokes_siswa/ancestor/BaseResponse.dart';
import 'package:mcnmr_common_ext/NonNullChecker.dart';

class SubjectResponse extends BaseResponse {
  List<SubjectData> data;

  SubjectResponse.fromJson(Map<String, dynamic> json) : super.fromJson(json){
    data = List();

    if(json["data"] != null){
      json["data"].forEach((it) => data.add(SubjectData.fromJson(it)));
    }
  }
}

class SubjectData {
  String idMapel;
  String idMapelUrl;
  String mapelNama;

  SubjectData.fromJson(Map<String, dynamic> json){
    idMapel = obtainValue(json["id_mapel"], "");
    idMapelUrl = obtainValue(json["id_mapel_url"], "");
    mapelNama = obtainValue(json["mapel_nama"], "");
  }

  SubjectData.all(){
    idMapel = "all";
    idMapelUrl = "all";
    mapelNama = "Semua";
  }

  @override
  bool operator ==(other) => idMapel == other.idMapel;

  @override
  int get hashCode => idMapel.hashCode;

  @override
  String toString() => mapelNama;
}