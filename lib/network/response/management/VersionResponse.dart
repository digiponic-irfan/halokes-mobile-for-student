import 'package:halokes_siswa/ancestor/BaseResponse.dart';

class VersionResponse extends BaseResponse{
  bool requiredUpdate = false;
  List<VersionData> data;

  VersionResponse.fromJson(Map<String, dynamic> json) : super.fromJson(json){
    requiredUpdate = json["required_update"] ?? false;

    data = List();
    if(json["data"] != null){
      json["data"].forEach((it) => data.add(VersionData.fromJson(it)));
    }
  }
}


class VersionData {
  String versionCode;
  String versionName;
  String whatsNew;

  VersionData.fromJson(Map<String, dynamic> json){
    versionCode = json["version_code"] ?? "";
    versionName = json["version_name"] ?? "";
    whatsNew = json["whats_new"] ?? "";
  }
}