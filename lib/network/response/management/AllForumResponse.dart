import 'package:halokes_siswa/ancestor/BaseResponse.dart';
import 'package:intl/intl.dart';

class AllForumResponse extends BaseResponse {
  List<AllForumData> data = List();

  AllForumResponse.fromJson(Map<String, dynamic> json) : super.fromJson(json){
    json["data"]?.forEach((it) => data.add(AllForumData.fromJson(it)));
  }

  void merge(AllForumResponse other){
    data.addAll(other.data);
  }
}

class AllForumData {
  String idForum;
  String idUrlForum;
  String idQuestioner;
  String uidSchoolQuestioner;
  String image;
  String sekolahNama;
  String grade;
  String mapel;
  String question;
  String createdAt;
  String idUrlMarkedAnswer;
  String questionerName;
  String questionerFoto;
  String questionerRole;

  AllForumData.fromJson(Map<String, dynamic> json){
    idForum = json["id_forum"] ?? "";
    idUrlForum = json["id_url_forum"] ?? "";
    idQuestioner = json["id_questioner"] ?? "";
    uidSchoolQuestioner = json["uid_school_questioner"] ?? "";
    image = json["image"] ?? "";
    sekolahNama = json["sekolah_nama"] ?? "";
    grade = json["grade"] ?? "";
    mapel = json["mapel"] ?? "";
    question = json["question"] ?? "";
    createdAt = json["created_at"] ?? "";
    idUrlMarkedAnswer = json["id_url_marked_answer"] ?? "";
    questionerName = json["questioner_name"] ?? "";
    questionerFoto = json["questioner_foto"] ?? "";
    questionerRole = json["questioner_role"] ?? "";
  }

  DateTime get time => DateFormat("yyyy-MM-dd HH:mm:ss").parse(createdAt);

  String get displayTime => DateFormat("dd EEE yyyy").format(DateFormat("yyyy-MM-dd HH:mm:ss").parse(createdAt));
}