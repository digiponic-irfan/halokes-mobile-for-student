import 'package:halokes_siswa/ancestor/BaseResponse.dart';
import 'package:mcnmr_common_ext/NonNullChecker.dart';

class VideoResponse extends BaseResponse {
  List<VideoData> data;

  VideoResponse.fromJson(Map<String, dynamic> json) : super.fromJson(json){
    data = List();
    if(json["data"] != null){
      json["data"].forEach((it) => data.add(VideoData.fromJson(it)));
    }
  }
}

class VideoData {
  String idVideo;
  String idVideoUrl;
  String judulVideo;
  String deskripsiVideo;
  String linkVideo;
  String status;
  String thumbnailDf;
  String thumbnailHq;
  String thumbnailMq;
  String thumbnailSd;
  String thumbnailMaxres;

  VideoData.fromJson(Map<String, dynamic> json){
    idVideo = obtainValue(json["id_video"], "");
    idVideoUrl = obtainValue(json["id_video_url"], "");
    judulVideo = obtainValue(json["judul_video"], "");
    deskripsiVideo = obtainValue(json["deskripsi_video"], "");
    linkVideo = obtainValue(json["link_video"], "");
    status = obtainValue(json["status"], "");
    thumbnailDf = obtainValue(json["thumbnail_df"], "");
    thumbnailHq = obtainValue(json["thumbnail_hq"], "");
    thumbnailMq = obtainValue(json["thumbnail_mq"], "");
    thumbnailSd = obtainValue(json["thumbnail_sd"], "");
    thumbnailMaxres = obtainValue(json["thumbnail_maxres"], "");
  }
}