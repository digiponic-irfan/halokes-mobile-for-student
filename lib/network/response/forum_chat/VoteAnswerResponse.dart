import 'package:halokes_siswa/ancestor/BaseResponse.dart';

class VoteAnswerResponse extends BaseResponse {
  VoteAnswerData data;

  VoteAnswerResponse.fromJson(Map<String, dynamic> json) : super.fromJson(json){
    if(json["data"] != null){
      data = VoteAnswerData.fromJson(json["data"]);
    }
  }
}

class VoteAnswerData {
  String idAnswer;
  int upvoteCount;
  int downvoteCount;

  VoteAnswerData.fromJson(Map<String, dynamic> json){
    idAnswer = json["id_answer"] ?? "";
    upvoteCount = json["upvote_count"] != null ? int.parse(json["upvote_count"]) : 0;
    downvoteCount = json["downvote_count"] != null ? int.parse(json["downvote_count"]) : 0;
  }
}