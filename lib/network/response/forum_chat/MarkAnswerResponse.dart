import 'package:halokes_siswa/ancestor/BaseResponse.dart';

class MarkAnswerResponse extends BaseResponse {
  MarkAnswerData data;

  MarkAnswerResponse.fromJson(Map<String, dynamic> json) : super.fromJson(json){
    if(json["data"] != null){
      data = MarkAnswerData.fromJson(json["data"]);
    }
  }
}

class MarkAnswerData {
  String idUrlForum;
  String idUrlMarkedAnswer;

  MarkAnswerData.fromJson(Map<String, dynamic> json){
    idUrlForum = json["id_url_forum"] ?? "";
    idUrlMarkedAnswer = json["id_url_marked_answer"] ?? "";
  }
}