import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/extension/Size.dart';

class AndroidBottomSheetDialog extends StatelessWidget {
  final String title;
  final String message;

  AndroidBottomSheetDialog({@required this.title, @required this.message});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: adaptiveWidth(context, 10),
        horizontal: adaptiveWidth(context, 40)
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Align(
            child: Container(
              width: adaptiveWidth(context, 100),
              height: adaptiveWidth(context, 5),
              decoration: BoxDecoration(
                color: Colors.black.withOpacity(0.25),
                borderRadius: BorderRadius.circular(adaptiveWidth(context, 5)),
              ),
            ),
          ),
          SizedBox(height: adaptiveWidth(context, 30)),
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: adaptiveWidth(context, 30)
            ),
            child: StyledText(title,
              size: sp(context, 16),
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(height: adaptiveWidth(context, 20)),
          Container(
            width: double.infinity,
            height: 1,
            color: Colors.black.withOpacity(0.25),
          ),
          SizedBox(height: adaptiveWidth(context, 20)),
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: adaptiveWidth(context, 30)
            ),
            child: StyledText(message,
              size: sp(context, 14),
              fontWeight: FontWeight.w600,
            ),
          ),
          SizedBox(height: adaptiveWidth(context, 80)),
        ],
      ),
    );
  }
}
