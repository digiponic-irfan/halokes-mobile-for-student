import 'package:flutter/material.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/extension/Size.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';

class AndroidMultipleChoiceDialog extends StatelessWidget {
  final String title;
  final List<MultipleChoiceAction> actions;
  final MultipleChoiceAction cancel;

  AndroidMultipleChoiceDialog({@required this.title, @required this.actions, @required this.cancel});

  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          SizedBox(height: adaptiveWidth(context, 10)),
          Padding(
            padding: EdgeInsets.symmetric(
              vertical: adaptiveWidth(context, 10),
              horizontal: adaptiveWidth(context, 20),
            ),
            child: StyledText(title,
              size: sp(context, 16),
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(height: adaptiveWidth(context, 5)),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: actions.map((it) =>
                InkWell(
                  onTap: it.action,
                  child: Container(
                    width: double.infinity,
                    padding: EdgeInsets.symmetric(
                      vertical: adaptiveWidth(context, 10),
                      horizontal: adaptiveWidth(context, 20)
                    ),
                    child: StyledText(it.text),
                  ),
                ),
            ).toList(),
          ),
          SizedBox(height: adaptiveWidth(context, 10)),
          Row(
            children: <Widget>[
              Spacer(),
              FlatButton(
                onPressed: cancel.action,
                child: StyledText(cancel.text,
                  fontWeight: FontWeight.w600,
                  color: ColorUtils.primary,
                ),
              ),
              SizedBox(width: adaptiveWidth(context, 10)),
            ],
          )
        ],
      ),
    );
  }
}

class MultipleChoiceAction{
  final String text;
  final Function() action;

  MultipleChoiceAction({@required this.text, @required this.action});
}