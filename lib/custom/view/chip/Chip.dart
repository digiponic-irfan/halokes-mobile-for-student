import 'package:flutter/material.dart';
import 'package:halokes_siswa/custom/view/text/StyledText.dart';
import 'package:halokes_siswa/extension/Size.dart';
import 'package:halokes_siswa/utils/ColorUtils.dart';
import 'package:shimmer/shimmer.dart';

class ChipItem<T> extends StatelessWidget {
  final T item;
  final Function(T) onSelected;
  final bool Function(T) isSelected;

  ChipItem({@required this.item, @required this.onSelected, @required this.isSelected}) :
        assert(item != null), assert(onSelected != null), assert(isSelected != null);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(adaptiveWidth(context, 20)),
      child: Material(
        type: MaterialType.transparency,
        child: InkWell(
          onTap: () => onSelected(item),
          child: Container(
            padding: EdgeInsets.symmetric(
              horizontal: adaptiveWidth(context, 30),
              vertical: adaptiveWidth(context, 5),
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(adaptiveWidth(context, 20)),
              border: Border.all(color: ColorUtils.primary),
              color: isSelected(item) ? ColorUtils.primary : Colors.transparent,
            ),
            child: StyledText(item.toString(),
              size: sp(context, 12),
              color: isSelected(item) ? Colors.white : Colors.black,
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
      ),
    );
  }
}

class ShimmerChip extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      child: Container(
        width: adaptiveWidth(context, 100),
        padding: EdgeInsets.symmetric(
          horizontal: adaptiveWidth(context, 30),
          vertical: adaptiveWidth(context, 5),
        ),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(adaptiveWidth(context, 20)),
            color: Colors.grey[200]
        ),
        child: StyledText("",
          size: sp(context, 14),
          fontWeight: FontWeight.w600,
        ),
      ),
      baseColor: Colors.grey[200],
      highlightColor: Colors.white,
    );
  }
}