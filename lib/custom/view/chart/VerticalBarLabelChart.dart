// Copyright 2018 the Charts project authors. Please see the AUTHORS file
// for details.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/// Vertical bar chart with bar label renderer example.
// EXCLUDE_FROM_GALLERY_DOCS_START
import 'dart:math';
// EXCLUDE_FROM_GALLERY_DOCS_END
import 'package:halokes_siswa/external_plugin/charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';

class VerticalBarLabelChart extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;

  VerticalBarLabelChart(this.seriesList, {this.animate});

  /// Creates a [BarChart] with sample data and no transition.
  factory VerticalBarLabelChart.withSampleData() {
    return new VerticalBarLabelChart(
      _createSampleData(),
      // Disable animations for image tests.
      animate: false,
    );
  }

  // EXCLUDE_FROM_GALLERY_DOCS_START
  // This section is excluded from being copied to the gallery.
  // It is used for creating random series data to demonstrate animation in
  // the example app only.
  factory VerticalBarLabelChart.withRandomData() {
    return new VerticalBarLabelChart(_createRandomData());
  }

  factory VerticalBarLabelChart.withActualData(List<int> score){
    var scores = <OrdinalSales>[];

    for(int i=0;i<score.length;i++){
      scores.add(OrdinalSales("${i+1}", score[i]));
    }

    return new VerticalBarLabelChart(<charts.Series<OrdinalSales, String>>[
      new charts.Series<OrdinalSales, String>(
          id: 'Score',
          domainFn: (OrdinalSales sales, _) => sales.index,
          measureFn: (OrdinalSales sales, _) => sales.score,
          data: scores,
          labelAccessorFn: (OrdinalSales sales, _) => '${sales.score.toString()}')
    ]);
  }

  factory VerticalBarLabelChart.withActualStringData(List<String> score){
    var intList = List<int>();
    score.forEach((it){
      intList.add(int.parse(it));
    });

    return VerticalBarLabelChart.withActualData(intList);
  }

  /// Create random data.
  static List<charts.Series<OrdinalSales, String>> _createRandomData() {
    final random = new Random();

    final data = [
      new OrdinalSales('1', 30 + random.nextInt(70)),
      new OrdinalSales('2', 30 + random.nextInt(70)),
      new OrdinalSales('3', 30 + random.nextInt(70)),
      new OrdinalSales('4', 30 + random.nextInt(70)),
      new OrdinalSales('5', 30 + random.nextInt(70)),
      new OrdinalSales('6', 30 + random.nextInt(70)),
      new OrdinalSales('7', 30 + random.nextInt(70)),
      new OrdinalSales('8', 30 + random.nextInt(70)),
      new OrdinalSales('9', 30 + random.nextInt(70)),
      new OrdinalSales('10', 30 + random.nextInt(70)),
      new OrdinalSales('11', 30 + random.nextInt(70)),
      new OrdinalSales('12', 30 + random.nextInt(70)),
    ];

    return [
      new charts.Series<OrdinalSales, String>(
          id: 'Sales',
          domainFn: (OrdinalSales sales, _) => sales.index,
          measureFn: (OrdinalSales sales, _) => sales.score,
          data: data,
          // Set a label accessor to control the text of the bar label.
          labelAccessorFn: (OrdinalSales sales, _) =>
          '${sales.score.toString()}'),
      new charts.Series<OrdinalSales, String>(
          id: 'Sales 2',
          domainFn: (OrdinalSales sales, _) => sales.index,
          measureFn: (OrdinalSales sales, _) => sales.score,
          data: data,
          // Set a label accessor to control the text of the bar label.
          labelAccessorFn: (OrdinalSales sales, _) =>
          '${sales.score.toString()}')
    ];
  }
  // EXCLUDE_FROM_GALLERY_DOCS_END

  // [BarLabelDecorator] will automatically position the label
  // inside the bar if the label will fit. If the label will not fit,
  // it will draw outside of the bar.
  // Labels can always display inside or outside using [LabelPosition].
  //
  // Text style for inside / outside can be controlled independently by setting
  // [insideLabelStyleSpec] and [outsideLabelStyleSpec].
  @override
  Widget build(BuildContext context) {
    return new charts.BarChart(
      seriesList,
      animate: animate,
      // Set a bar label decorator.
      // Example configuring different styles for inside/outside:
      //       barRendererDecorator: new charts.BarLabelDecorator(
      //          insideLabelStyleSpec: new charts.TextStyleSpec(...),
      //          outsideLabelStyleSpec: new charts.TextStyleSpec(...)),
      barRendererDecorator: new charts.BarLabelDecorator<String>(),
      domainAxis: new charts.OrdinalAxisSpec(),
    );
  }

  /// Create one series with sample hard coded data.
  static List<charts.Series<OrdinalSales, String>> _createSampleData() {
    final data = [
      new OrdinalSales('2014', 5),
      new OrdinalSales('2015', 25),
      new OrdinalSales('2016', 100),
      new OrdinalSales('2017', 75),
    ];

    return [
      new charts.Series<OrdinalSales, String>(
          id: 'Sales',
          domainFn: (OrdinalSales sales, _) => sales.index,
          measureFn: (OrdinalSales sales, _) => sales.score,
          data: data,
          // Set a label accessor to control the text of the bar label.
          labelAccessorFn: (OrdinalSales sales, _) =>
          '\$${sales.score.toString()}')
    ];
  }
}

/// Sample ordinal data type.
class OrdinalSales {
  final String index;
  final int score;

  OrdinalSales(this.index, this.score);
}
