//  Copyright (c) 2019 Aleksander Woźniak
//  Licensed under Apache License v2.0

part of table_calendar;

class _CellWidget extends StatelessWidget {
  final String text;
  final bool isUnavailable;
  final bool isSelected;
  final bool isToday;
  final bool isWeekend;
  final bool isOutsideMonth;
  final bool isHoliday;
  final bool isAlpha;
  final bool isPermit;
  final bool isSick;
  final CalendarStyle calendarStyle;

  const _CellWidget({
    Key key,
    @required this.text,
    this.isUnavailable = false,
    this.isSelected = false,
    this.isToday = false,
    this.isWeekend = false,
    this.isOutsideMonth = false,
    this.isHoliday = false,
    this.isAlpha = false,
    this.isPermit = false,
    this.isSick = false,
    @required this.calendarStyle,
  })  : assert(text != null),
        assert(calendarStyle != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 250),
      decoration: _buildCellDecoration(),
      margin: const EdgeInsets.all(6.0),
      alignment: Alignment.center,
      child: Text(
        text, style: _buildCellTextStyle(),
      ),
    );
  }

  ///
  // Decoration _buildCellDecoration() {
  //    if (isSelected && calendarStyle.renderSelectedFirst) {
  //      return BoxDecoration(shape: BoxShape.circle, color: calendarStyle.selectedColor);
  //    } else if (isToday) {
  //      return BoxDecoration(shape: BoxShape.circle, color: calendarStyle.todayColor);
  //    } else if (isSelected) {
  //      return BoxDecoration(shape: BoxShape.circle, color: calendarStyle.selectedColor);
  //    } else {
  //      return BoxDecoration(shape: BoxShape.circle);
  //    }
  //  }
  ///

  ///Customizable using this
  Decoration _buildCellDecoration() {
    if(isAlpha){
      if(isOutsideMonth){
        return BoxDecoration(shape: BoxShape.circle, color: calendarStyle.alphaAndOutsideColor);
      }
      return BoxDecoration(shape: BoxShape.circle, color: calendarStyle.alphaColor);
    }else if(isPermit){
      if(isOutsideMonth){
        return BoxDecoration(shape: BoxShape.circle, color: calendarStyle.permitAndOutsideColor);
      }
      return BoxDecoration(shape: BoxShape.circle, color: calendarStyle.permitColor);
    }else if(isSick){
      if(isOutsideMonth){
        return BoxDecoration(shape: BoxShape.circle, color: calendarStyle.sickAndOutsideColor);
      }
      return BoxDecoration(shape: BoxShape.circle, color: calendarStyle.sickColor);
    }

    return BoxDecoration(shape: BoxShape.circle);
  }

  TextStyle _buildCellTextStyle() {
    if (isUnavailable) {
      return calendarStyle.unavailableStyle;
    }else if(isAlpha || isSick || isPermit){
      if(isOutsideMonth){
        return calendarStyle.alphaSickPermitOutsideStyle;
      }
      return calendarStyle.alphaSickPermitStyle;
    } else if (isSelected && calendarStyle.renderSelectedFirst) {
      return calendarStyle.selectedStyle;
    } else if (isToday) {
      return calendarStyle.todayStyle;
    } else if (isSelected) {
      return calendarStyle.selectedStyle;
    } else if (isOutsideMonth && isHoliday) {
      return calendarStyle.outsideHolidayStyle;
    } else if (isHoliday) {
      return calendarStyle.holidayStyle;
    } else if (isOutsideMonth && isWeekend) {
      return calendarStyle.outsideWeekendStyle;
    } else if (isOutsideMonth) {
      return calendarStyle.outsideStyle;
    } else if (isWeekend) {
      return calendarStyle.weekendStyle;
    } else {
      return calendarStyle.weekdayStyle;
    }
  }
}
