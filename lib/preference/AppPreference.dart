import 'dart:convert';

import 'package:halokes_siswa/network/response/management/SchoolResponse.dart';
import 'package:halokes_siswa/network/response/general/UserResponse.dart';
import 'package:shared_preferences/shared_preferences.dart';

///
/// Preference of apps
/// Just like Shared Preference in Android and NSUserDefaults in iOS
///
class AppPreference {
  static SharedPreferences _preferences;

  static const ACCOUNT_PREFERENCE = "ACCOUNT_PREFERENCE";
  static const IS_FIRST_TIME = "IS_FIRST_TIME";
  static const SCHOOL_PREFERENCE = "SCHOOL_PREFERENCE";

  static Future<void> _init() async {
    if(_preferences == null){
      _preferences = await SharedPreferences.getInstance();
    }
  }

  static Future<UserData> getUser() async {
    await _init();

    if(_preferences.containsKey(ACCOUNT_PREFERENCE)){
      return UserData.fromJson(jsonDecode(_preferences.getString(ACCOUNT_PREFERENCE)));
    }else {
      return null;
    }
  }

  static Future<bool> saveUser(UserData user) async {
    await _init();

    return _preferences.setString(ACCOUNT_PREFERENCE, user.toJson());
  }

  static Future<bool> removeUser() async {
    await _init();

    return _preferences.remove(ACCOUNT_PREFERENCE);
  }

  static Future<bool> isFirstTime() async {
    await _init();

    if(_preferences.containsKey(IS_FIRST_TIME)){
      return _preferences.getBool(IS_FIRST_TIME);
    }
    return true;
  }

  static Future<bool> changeFirstTime(bool isFirstTime) async {
    await _init();

    return _preferences.setBool(IS_FIRST_TIME, isFirstTime);
  }

  static Future<SchoolData> getSchool() async {
    await _init();

    if(_preferences.containsKey(SCHOOL_PREFERENCE)){
      return SchoolData.fromJson(jsonDecode(_preferences.getString(SCHOOL_PREFERENCE)));
    }else {
      return null;
    }
  }

  static Future<bool> saveSchool(SchoolData school) async {
    await _init();

    return _preferences.setString(SCHOOL_PREFERENCE, school.toJson());
  }

  static Future<bool> removeSchool() async {
    await _init();

    return _preferences.remove(SCHOOL_PREFERENCE);
  }
}